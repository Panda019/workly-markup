if ("undefined" == typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");
+function (t) {
    "use strict";
    var e = t.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")
}(jQuery), +function (t) {
    "use strict";
    function e() {
        var t = document.createElement("bootstrap"), e = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        };
        for (var i in e)if (void 0 !== t.style[i])return {end: e[i]};
        return !1
    }

    t.fn.emulateTransitionEnd = function (e) {
        var i = !1, s = this;
        t(this).one("bsTransitionEnd", function () {
            i = !0
        });
        var n = function () {
            i || t(s).trigger(t.support.transition.end)
        };
        return setTimeout(n, e), this
    }, t(function () {
        t.support.transition = e(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function (e) {
                return t(e.target).is(this) ? e.handleObj.handler.apply(this, arguments) : void 0
            }
        })
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var i = t(this), n = i.data("bs.alert");
            n || i.data("bs.alert", n = new s(this)), "string" == typeof e && n[e].call(i)
        })
    }

    var i = '[data-dismiss="alert"]', s = function (e) {
        t(e).on("click", i, this.close)
    };
    s.VERSION = "3.3.4", s.TRANSITION_DURATION = 150, s.prototype.close = function (e) {
        function i() {
            a.detach().trigger("closed.bs.alert").remove()
        }

        var n = t(this), o = n.attr("data-target");
        o || (o = n.attr("href"), o = o && o.replace(/.*(?=#[^\s]*$)/, ""));
        var a = t(o);
        e && e.preventDefault(), a.length || (a = n.closest(".alert")), a.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (a.removeClass("in"), t.support.transition && a.hasClass("fade") ? a.one("bsTransitionEnd", i).emulateTransitionEnd(s.TRANSITION_DURATION) : i())
    };
    var n = t.fn.alert;
    t.fn.alert = e, t.fn.alert.Constructor = s, t.fn.alert.noConflict = function () {
        return t.fn.alert = n, this
    }, t(document).on("click.bs.alert.data-api", i, s.prototype.close)
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.button"), o = "object" == typeof e && e;
            n || s.data("bs.button", n = new i(this, o)), "toggle" == e ? n.toggle() : e && n.setState(e)
        })
    }

    var i = function (e, s) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, s), this.isLoading = !1
    };
    i.VERSION = "3.3.4", i.DEFAULTS = {loadingText: "loading..."}, i.prototype.setState = function (e) {
        var i = "disabled", s = this.$element, n = s.is("input") ? "val" : "html", o = s.data();
        e += "Text", null == o.resetText && s.data("resetText", s[n]()), setTimeout(t.proxy(function () {
            s[n](null == o[e] ? this.options[e] : o[e]), "loadingText" == e ? (this.isLoading = !0, s.addClass(i).attr(i, i)) : this.isLoading && (this.isLoading = !1, s.removeClass(i).removeAttr(i))
        }, this), 0)
    }, i.prototype.toggle = function () {
        var t = !0, e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") && (i.prop("checked") && this.$element.hasClass("active") ? t = !1 : e.find(".active").removeClass("active")), t && i.prop("checked", !this.$element.hasClass("active")).trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active"));
        t && this.$element.toggleClass("active")
    };
    var s = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function () {
        return t.fn.button = s, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (i) {
        var s = t(i.target);
        s.hasClass("btn") || (s = s.closest(".btn")), e.call(s, "toggle"), i.preventDefault()
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.carousel"), o = t.extend({}, i.DEFAULTS, s.data(), "object" == typeof e && e), a = "string" == typeof e ? e : o.slide;
            n || s.data("bs.carousel", n = new i(this, o)), "number" == typeof e ? n.to(e) : a ? n[a]() : o.interval && n.pause().cycle()
        })
    }

    var i = function (e, i) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = i, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart"in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, i.prototype.keydown = function (t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, i.prototype.cycle = function (e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, i.prototype.getItemIndex = function (t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, i.prototype.getItemForDirection = function (t, e) {
        var i = this.getItemIndex(e), s = "prev" == t && 0 === i || "next" == t && i == this.$items.length - 1;
        if (s && !this.options.wrap)return e;
        var n = "prev" == t ? -1 : 1, o = (i + n) % this.$items.length;
        return this.$items.eq(o)
    }, i.prototype.to = function (t) {
        var e = this, i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        return t > this.$items.length - 1 || 0 > t ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function () {
            e.to(t)
        }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t))
    }, i.prototype.pause = function (e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, i.prototype.next = function () {
        return this.sliding ? void 0 : this.slide("next")
    }, i.prototype.prev = function () {
        return this.sliding ? void 0 : this.slide("prev")
    }, i.prototype.slide = function (e, s) {
        var n = this.$element.find(".item.active"), o = s || this.getItemForDirection(e, n), a = this.interval, r = "next" == e ? "left" : "right", h = this;
        if (o.hasClass("active"))return this.sliding = !1;
        var l = o[0], c = t.Event("slide.bs.carousel", {relatedTarget: l, direction: r});
        if (this.$element.trigger(c), !c.isDefaultPrevented()) {
            if (this.sliding = !0, a && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var d = t(this.$indicators.children()[this.getItemIndex(o)]);
                d && d.addClass("active")
            }
            var p = t.Event("slid.bs.carousel", {relatedTarget: l, direction: r});
            return t.support.transition && this.$element.hasClass("slide") ? (o.addClass(e), o[0].offsetWidth, n.addClass(r), o.addClass(r), n.one("bsTransitionEnd", function () {
                o.removeClass([e, r].join(" ")).addClass("active"), n.removeClass(["active", r].join(" ")), h.sliding = !1, setTimeout(function () {
                    h.$element.trigger(p)
                }, 0)
            }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (n.removeClass("active"), o.addClass("active"), this.sliding = !1, this.$element.trigger(p)), a && this.cycle(), this
        }
    };
    var s = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function () {
        return t.fn.carousel = s, this
    };
    var n = function (i) {
        var s, n = t(this), o = t(n.attr("data-target") || (s = n.attr("href")) && s.replace(/.*(?=#[^\s]+$)/, ""));
        if (o.hasClass("carousel")) {
            var a = t.extend({}, o.data(), n.data()), r = n.attr("data-slide-to");
            r && (a.interval = !1), e.call(o, a), r && o.data("bs.carousel").to(r), i.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", n).on("click.bs.carousel.data-api", "[data-slide-to]", n), t(window).on("load", function () {
        t('[data-ride="carousel"]').each(function () {
            var i = t(this);
            e.call(i, i.data())
        })
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        var i, s = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
        return t(s)
    }

    function i(e) {
        return this.each(function () {
            var i = t(this), n = i.data("bs.collapse"), o = t.extend({}, s.DEFAULTS, i.data(), "object" == typeof e && e);
            !n && o.toggle && /show|hide/.test(e) && (o.toggle = !1), n || i.data("bs.collapse", n = new s(this, o)), "string" == typeof e && n[e]()
        })
    }

    var s = function (e, i) {
        this.$element = t(e), this.options = t.extend({}, s.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    s.VERSION = "3.3.4", s.TRANSITION_DURATION = 350, s.DEFAULTS = {toggle: !0}, s.prototype.dimension = function () {
        var t = this.$element.hasClass("width");
        return t ? "width" : "height"
    }, s.prototype.show = function () {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, n = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(n && n.length && (e = n.data("bs.collapse"), e && e.transitioning))) {
                var o = t.Event("show.bs.collapse");
                if (this.$element.trigger(o), !o.isDefaultPrevented()) {
                    n && n.length && (i.call(n, "hide"), e || n.data("bs.collapse", null));
                    var a = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[a](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var r = function () {
                        this.$element.removeClass("collapsing").addClass("collapse in")[a](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition)return r.call(this);
                    var h = t.camelCase(["scroll", a].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(r, this)).emulateTransitionEnd(s.TRANSITION_DURATION)[a](this.$element[0][h])
                }
            }
        }
    }, s.prototype.hide = function () {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var n = function () {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return t.support.transition ? void this.$element[i](0).one("bsTransitionEnd", t.proxy(n, this)).emulateTransitionEnd(s.TRANSITION_DURATION) : n.call(this)
            }
        }
    }, s.prototype.toggle = function () {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, s.prototype.getParent = function () {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function (i, s) {
            var n = t(s);
            this.addAriaAndCollapsedClass(e(n), n)
        }, this)).end()
    }, s.prototype.addAriaAndCollapsedClass = function (t, e) {
        var i = t.hasClass("in");
        t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i)
    };
    var n = t.fn.collapse;
    t.fn.collapse = i, t.fn.collapse.Constructor = s, t.fn.collapse.noConflict = function () {
        return t.fn.collapse = n, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (s) {
        var n = t(this);
        n.attr("data-target") || s.preventDefault();
        var o = e(n), a = o.data("bs.collapse"), r = a ? "toggle" : n.data();
        i.call(o, r)
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        e && 3 === e.which || (t(n).remove(), t(o).each(function () {
            var s = t(this), n = i(s), o = {relatedTarget: this};
            n.hasClass("open") && (n.trigger(e = t.Event("hide.bs.dropdown", o)), e.isDefaultPrevented() || (s.attr("aria-expanded", "false"), n.removeClass("open").trigger("hidden.bs.dropdown", o)))
        }))
    }

    function i(e) {
        var i = e.attr("data-target");
        i || (i = e.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var s = i && t(i);
        return s && s.length ? s : e.parent()
    }

    function s(e) {
        return this.each(function () {
            var i = t(this), s = i.data("bs.dropdown");
            s || i.data("bs.dropdown", s = new a(this)), "string" == typeof e && s[e].call(i)
        })
    }

    var n = ".dropdown-backdrop", o = '[data-toggle="dropdown"]', a = function (e) {
        t(e).on("click.bs.dropdown", this.toggle)
    };
    a.VERSION = "3.3.4", a.prototype.toggle = function (s) {
        var n = t(this);
        if (!n.is(".disabled, :disabled")) {
            var o = i(n), a = o.hasClass("open");
            if (e(), !a) {
                "ontouchstart"in document.documentElement && !o.closest(".navbar-nav").length && t('<div class="dropdown-backdrop"/>').insertAfter(t(this)).on("click", e);
                var r = {relatedTarget: this};
                if (o.trigger(s = t.Event("show.bs.dropdown", r)), s.isDefaultPrevented())return;
                n.trigger("focus").attr("aria-expanded", "true"), o.toggleClass("open").trigger("shown.bs.dropdown", r)
            }
            return !1
        }
    }, a.prototype.keydown = function (e) {
        if (/(38|40|27|32)/.test(e.which) && !/input|textarea/i.test(e.target.tagName)) {
            var s = t(this);
            if (e.preventDefault(), e.stopPropagation(), !s.is(".disabled, :disabled")) {
                var n = i(s), a = n.hasClass("open");
                if (!a && 27 != e.which || a && 27 == e.which)return 27 == e.which && n.find(o).trigger("focus"), s.trigger("click");
                var r = " li:not(.disabled):visible a", h = n.find('[role="menu"]' + r + ', [role="listbox"]' + r);
                if (h.length) {
                    var l = h.index(e.target);
                    38 == e.which && l > 0 && l--, 40 == e.which && l < h.length - 1 && l++, ~l || (l = 0), h.eq(l).trigger("focus")
                }
            }
        }
    };
    var r = t.fn.dropdown;
    t.fn.dropdown = s, t.fn.dropdown.Constructor = a, t.fn.dropdown.noConflict = function () {
        return t.fn.dropdown = r, this
    }, t(document).on("click.bs.dropdown.data-api", e).on("click.bs.dropdown.data-api", ".dropdown form", function (t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", o, a.prototype.toggle).on("keydown.bs.dropdown.data-api", o, a.prototype.keydown).on("keydown.bs.dropdown.data-api", '[role="menu"]', a.prototype.keydown).on("keydown.bs.dropdown.data-api", '[role="listbox"]', a.prototype.keydown)
}(jQuery), +function (t) {
    "use strict";
    function e(e, s) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.modal"), a = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e);
            o || n.data("bs.modal", o = new i(this, a)), "string" == typeof e ? o[e](s) : a.show && o.show(s)
        })
    }

    var i = function (e, i) {
        this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function () {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function (t) {
        return this.isShown ? this.hide() : this.show(t)
    }, i.prototype.show = function (e) {
        var s = this, n = t.Event("show.bs.modal", {relatedTarget: e});
        this.$element.trigger(n), this.isShown || n.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
            s.$element.one("mouseup.dismiss.bs.modal", function (e) {
                t(e.target).is(s.$element) && (s.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function () {
            var n = t.support.transition && s.$element.hasClass("fade");
            s.$element.parent().length || s.$element.appendTo(s.$body), s.$element.show().scrollTop(0), s.adjustDialog(), n && s.$element[0].offsetWidth, s.$element.addClass("in").attr("aria-hidden", !1), s.enforceFocus();
            var o = t.Event("shown.bs.modal", {relatedTarget: e});
            n ? s.$dialog.one("bsTransitionEnd", function () {
                s.$element.trigger("focus").trigger(o)
            }).emulateTransitionEnd(i.TRANSITION_DURATION) : s.$element.trigger("focus").trigger(o)
        }))
    }, i.prototype.hide = function (e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal())
    }, i.prototype.enforceFocus = function () {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function (t) {
            this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function () {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function (t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, i.prototype.resize = function () {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, i.prototype.hideModal = function () {
        var t = this;
        this.$element.hide(), this.backdrop(function () {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function () {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function (e) {
        var s = this, n = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var o = t.support.transition && n;
            if (this.$backdrop = t('<div class="modal-backdrop ' + n + '" />').appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function (t) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), o && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e)return;
            o ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var a = function () {
                s.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", a).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : a()
        } else e && e()
    }, i.prototype.handleUpdate = function () {
        this.adjustDialog()
    }, i.prototype.adjustDialog = function () {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, i.prototype.resetAdjustments = function () {
        this.$element.css({paddingLeft: "", paddingRight: ""})
    }, i.prototype.checkScrollbar = function () {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, i.prototype.setScrollbar = function () {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function () {
        this.$body.css("padding-right", this.originalBodyPad)
    }, i.prototype.measureScrollbar = function () {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var s = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function () {
        return t.fn.modal = s, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (i) {
        var s = t(this), n = s.attr("href"), o = t(s.attr("data-target") || n && n.replace(/.*(?=#[^\s]+$)/, "")), a = o.data("bs.modal") ? "toggle" : t.extend({remote: !/#/.test(n) && n}, o.data(), s.data());
        s.is("a") && i.preventDefault(), o.one("show.bs.modal", function (t) {
            t.isDefaultPrevented() || o.one("hidden.bs.modal", function () {
                s.is(":visible") && s.trigger("focus")
            })
        }), e.call(o, a, this)
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.tooltip"), o = "object" == typeof e && e;
            (n || !/destroy|hide/.test(e)) && (n || s.data("bs.tooltip", n = new i(this, o)), "string" == typeof e && n[e]())
        })
    }

    var i = function (t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.init("tooltip", t, e)
    };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 150, i.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip   " role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {selector: "body", padding: 0}
    }, i.prototype.init = function (e, i, s) {
        if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(s), this.$viewport = this.options.viewport && t(this.options.viewport.selector || this.options.viewport), this.$element[0]instanceof document.constructor && !this.options.selector)throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var n = this.options.trigger.split(" "), o = n.length; o--;) {
            var a = n[o];
            if ("click" == a)this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this)); else if ("manual" != a) {
                var r = "hover" == a ? "mouseenter" : "focusin", h = "hover" == a ? "mouseleave" : "focusout";
                this.$element.on(r + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(h + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, i.prototype.getDefaults = function () {
        return i.DEFAULTS
    }, i.prototype.getOptions = function (e) {
        return e = t.extend({}, this.getDefaults(), this.$element.data(), e), e.delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, i.prototype.getDelegateOptions = function () {
        var e = {}, i = this.getDefaults();
        return this._options && t.each(this._options, function (t, s) {
            i[t] != s && (e[t] = s)
        }), e
    }, i.prototype.enter = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i && i.$tip && i.$tip.is(":visible") ? void(i.hoverState = "in") : (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function () {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show())
    }, i.prototype.leave = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function () {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide()
    }, i.prototype.show = function () {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(e);
            var s = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (e.isDefaultPrevented() || !s)return;
            var n = this, o = this.tip(), a = this.getUID(this.type);
            this.setContent(), o.attr("id", a), this.$element.attr("aria-describedby", a), this.options.animation && o.addClass("fade");
            var r = "function" == typeof this.options.placement ? this.options.placement.call(this, o[0], this.$element[0]) : this.options.placement, h = /\s?auto?\s?/i, l = h.test(r);
            l && (r = r.replace(h, "") || "top"), o.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(r).data("bs." + this.type, this), this.options.container ? o.appendTo(this.options.container) : o.insertAfter(this.$element);
            var c = this.getPosition(), d = o[0].offsetWidth, p = o[0].offsetHeight;
            if (l) {
                var u = r, f = this.options.container ? t(this.options.container) : this.$element.parent(), m = this.getPosition(f);
                r = "bottom" == r && c.bottom + p > m.bottom ? "top" : "top" == r && c.top - p < m.top ? "bottom" : "right" == r && c.right + d > m.width ? "left" : "left" == r && c.left - d < m.left ? "right" : r, o.removeClass(u).addClass(r)
            }
            var g = this.getCalculatedOffset(r, c, d, p);
            this.applyPlacement(g, r);
            var v = function () {
                var t = n.hoverState;
                n.$element.trigger("shown.bs." + n.type), n.hoverState = null, "out" == t && n.leave(n)
            };
            t.support.transition && this.$tip.hasClass("fade") ? o.one("bsTransitionEnd", v).emulateTransitionEnd(i.TRANSITION_DURATION) : v()
        }
    }, i.prototype.applyPlacement = function (e, i) {
        var s = this.tip(), n = s[0].offsetWidth, o = s[0].offsetHeight, a = parseInt(s.css("margin-top"), 10), r = parseInt(s.css("margin-left"), 10);
        isNaN(a) && (a = 0), isNaN(r) && (r = 0), e.top = e.top + a, e.left = e.left + r, t.offset.setOffset(s[0], t.extend({
            using: function (t) {
                s.css({top: Math.round(t.top), left: Math.round(t.left)})
            }
        }, e), 0), s.addClass("in");
        var h = s[0].offsetWidth, l = s[0].offsetHeight;
        "top" == i && l != o && (e.top = e.top + o - l);
        var c = this.getViewportAdjustedDelta(i, e, h, l);
        c.left ? e.left += c.left : e.top += c.top;
        var d = /top|bottom/.test(i), p = d ? 2 * c.left - n + h : 2 * c.top - o + l, u = d ? "offsetWidth" : "offsetHeight";
        s.offset(e), this.replaceArrow(p, s[0][u], d)
    }, i.prototype.replaceArrow = function (t, e, i) {
        this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "")
    }, i.prototype.setContent = function () {
        var t = this.tip(), e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, i.prototype.hide = function (e) {
        function s() {
            "in" != n.hoverState && o.detach(), n.$element.removeAttr("aria-describedby").trigger("hidden.bs." + n.type), e && e()
        }

        var n = this, o = t(this.$tip), a = t.Event("hide.bs." + this.type);
        return this.$element.trigger(a), a.isDefaultPrevented() ? void 0 : (o.removeClass("in"), t.support.transition && o.hasClass("fade") ? o.one("bsTransitionEnd", s).emulateTransitionEnd(i.TRANSITION_DURATION) : s(), this.hoverState = null, this)
    }, i.prototype.fixTitle = function () {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, i.prototype.hasContent = function () {
        return this.getTitle()
    }, i.prototype.getPosition = function (e) {
        e = e || this.$element;
        var i = e[0], s = "BODY" == i.tagName, n = i.getBoundingClientRect();
        null == n.width && (n = t.extend({}, n, {width: n.right - n.left, height: n.bottom - n.top}));
        var o = s ? {
            top: 0,
            left: 0
        } : e.offset(), a = {scroll: s ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()}, r = s ? {
            width: t(window).width(),
            height: t(window).height()
        } : null;
        return t.extend({}, n, a, r, o)
    }, i.prototype.getCalculatedOffset = function (t, e, i, s) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - i / 2
        } : "top" == t ? {
            top: e.top - s,
            left: e.left + e.width / 2 - i / 2
        } : "left" == t ? {top: e.top + e.height / 2 - s / 2, left: e.left - i} : {
            top: e.top + e.height / 2 - s / 2,
            left: e.left + e.width
        }
    }, i.prototype.getViewportAdjustedDelta = function (t, e, i, s) {
        var n = {top: 0, left: 0};
        if (!this.$viewport)return n;
        var o = this.options.viewport && this.options.viewport.padding || 0, a = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var r = e.top - o - a.scroll, h = e.top + o - a.scroll + s;
            r < a.top ? n.top = a.top - r : h > a.top + a.height && (n.top = a.top + a.height - h)
        } else {
            var l = e.left - o, c = e.left + o + i;
            l < a.left ? n.left = a.left - l : c > a.width && (n.left = a.left + a.width - c)
        }
        return n
    }, i.prototype.getTitle = function () {
        var t, e = this.$element, i = this.options;
        return t = e.attr("data-original-title") || ("function" == typeof i.title ? i.title.call(e[0]) : i.title)
    }, i.prototype.getUID = function (t) {
        do t += ~~(1e6 * Math.random()); while (document.getElementById(t));
        return t
    }, i.prototype.tip = function () {
        return this.$tip = this.$tip || t(this.options.template)
    }, i.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, i.prototype.enable = function () {
        this.enabled = !0
    }, i.prototype.disable = function () {
        this.enabled = !1
    }, i.prototype.toggleEnabled = function () {
        this.enabled = !this.enabled
    }, i.prototype.toggle = function (e) {
        var i = this;
        e && (i = t(e.currentTarget).data("bs." + this.type), i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i))), i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, i.prototype.destroy = function () {
        var t = this;
        clearTimeout(this.timeout), this.hide(function () {
            t.$element.off("." + t.type).removeData("bs." + t.type)
        })
    };
    var s = t.fn.tooltip;
    t.fn.tooltip = e, t.fn.tooltip.Constructor = i, t.fn.tooltip.noConflict = function () {
        return t.fn.tooltip = s, this
    }
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.popover"), o = "object" == typeof e && e;
            (n || !/destroy|hide/.test(e)) && (n || s.data("bs.popover", n = new i(this, o)), "string" == typeof e && n[e]())
        })
    }

    var i = function (t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip)throw new Error("Popover requires tooltip.js");
    i.VERSION = "3.3.4", i.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), i.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), i.prototype.constructor = i, i.prototype.getDefaults = function () {
        return i.DEFAULTS
    }, i.prototype.setContent = function () {
        var t = this.tip(), e = this.getTitle(), i = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, i.prototype.hasContent = function () {
        return this.getTitle() || this.getContent()
    }, i.prototype.getContent = function () {
        var t = this.$element, e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, i.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var s = t.fn.popover;
    t.fn.popover = e, t.fn.popover.Constructor = i, t.fn.popover.noConflict = function () {
        return t.fn.popover = s, this
    }
}(jQuery), +function (t) {
    "use strict";
    function e(i, s) {
        this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), this.options = t.extend({}, e.DEFAULTS, s), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function i(i) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.scrollspy"), o = "object" == typeof i && i;
            n || s.data("bs.scrollspy", n = new e(this, o)), "string" == typeof i && n[i]()
        })
    }

    e.VERSION = "3.3.4", e.DEFAULTS = {offset: 10}, e.prototype.getScrollHeight = function () {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function () {
        var e = this, i = "offset", s = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (i = "position", s = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
            var e = t(this), n = e.data("target") || e.attr("href"), o = /^#./.test(n) && t(n);
            return o && o.length && o.is(":visible") && [[o[i]().top + s, n]] || null
        }).sort(function (t, e) {
            return t[0] - e[0]
        }).each(function () {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function () {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset, i = this.getScrollHeight(), s = this.options.offset + i - this.$scrollElement.height(), n = this.offsets, o = this.targets, a = this.activeTarget;
        if (this.scrollHeight != i && this.refresh(), e >= s)return a != (t = o[o.length - 1]) && this.activate(t);
        if (a && e < n[0])return this.activeTarget = null, this.clear();
        for (t = n.length; t--;)a != o[t] && e >= n[t] && (void 0 === n[t + 1] || e < n[t + 1]) && this.activate(o[t])
    }, e.prototype.activate = function (e) {
        this.activeTarget = e, this.clear();
        var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]', s = t(i).parents("li").addClass("active");
        s.parent(".dropdown-menu").length && (s = s.closest("li.dropdown").addClass("active")), s.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function () {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var s = t.fn.scrollspy;
    t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function () {
        return t.fn.scrollspy = s, this
    }, t(window).on("load.bs.scrollspy.data-api", function () {
        t('[data-spy="scroll"]').each(function () {
            var e = t(this);
            i.call(e, e.data())
        })
    })
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.tab");
            n || s.data("bs.tab", n = new i(this)), "string" == typeof e && n[e]()
        })
    }

    var i = function (e) {
        this.element = t(e)
    };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 150, i.prototype.show = function () {
        var e = this.element, i = e.closest("ul:not(.dropdown-menu)"), s = e.data("target");
        if (s || (s = e.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var n = i.find(".active:last a"), o = t.Event("hide.bs.tab", {relatedTarget: e[0]}), a = t.Event("show.bs.tab", {relatedTarget: n[0]});
            if (n.trigger(o), e.trigger(a), !a.isDefaultPrevented() && !o.isDefaultPrevented()) {
                var r = t(s);
                this.activate(e.closest("li"), i), this.activate(r, r.parent(), function () {
                    n.trigger({type: "hidden.bs.tab", relatedTarget: e[0]}), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: n[0]
                    })
                })
            }
        }
    }, i.prototype.activate = function (e, s, n) {
        function o() {
            a.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), r ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), n && n()
        }

        var a = s.find("> .active"), r = n && t.support.transition && (a.length && a.hasClass("fade") || !!s.find("> .fade").length);
        a.length && r ? a.one("bsTransitionEnd", o).emulateTransitionEnd(i.TRANSITION_DURATION) : o(), a.removeClass("in")
    };
    var s = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function () {
        return t.fn.tab = s, this
    };
    var n = function (i) {
        i.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', n).on("click.bs.tab.data-api", '[data-toggle="pill"]', n)
}(jQuery), +function (t) {
    "use strict";
    function e(e) {
        return this.each(function () {
            var s = t(this), n = s.data("bs.affix"), o = "object" == typeof e && e;
            n || s.data("bs.affix", n = new i(this, o)), "string" == typeof e && n[e]()
        })
    }

    var i = function (e, s) {
        this.options = t.extend({}, i.DEFAULTS, s), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.3.4", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getState = function (t, e, i, s) {
        var n = this.$target.scrollTop(), o = this.$element.offset(), a = this.$target.height();
        if (null != i && "top" == this.affixed)return i > n ? "top" : !1;
        if ("bottom" == this.affixed)return null != i ? n + this.unpin <= o.top ? !1 : "bottom" : t - s >= n + a ? !1 : "bottom";
        var r = null == this.affixed, h = r ? n : o.top, l = r ? a : e;
        return null != i && i >= n ? "top" : null != s && h + l >= t - s ? "bottom" : !1
    }, i.prototype.getPinnedOffset = function () {
        if (this.pinnedOffset)return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var t = this.$target.scrollTop(), e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, i.prototype.checkPositionWithEventLoop = function () {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function () {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(), s = this.options.offset, n = s.top, o = s.bottom, a = t(document.body).height();
            "object" != typeof s && (o = n = s), "function" == typeof n && (n = s.top(this.$element)), "function" == typeof o && (o = s.bottom(this.$element));
            var r = this.getState(a, e, n, o);
            if (this.affixed != r) {
                null != this.unpin && this.$element.css("top", "");
                var h = "affix" + (r ? "-" + r : ""), l = t.Event(h + ".bs.affix");
                if (this.$element.trigger(l), l.isDefaultPrevented())return;
                this.affixed = r, this.unpin = "bottom" == r ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(h).trigger(h.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == r && this.$element.offset({top: a - e - o})
        }
    };
    var s = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function () {
        return t.fn.affix = s, this
    }, t(window).on("load", function () {
        t('[data-spy="affix"]').each(function () {
            var i = t(this), s = i.data();
            s.offset = s.offset || {}, null != s.offsetBottom && (s.offset.bottom = s.offsetBottom), null != s.offsetTop && (s.offset.top = s.offsetTop), e.call(i, s)
        })
    })
}(jQuery), !function (t) {
    "use strict";
    function e(t, e) {
        return t + ".touchspin_" + e
    }

    function i(i, s) {
        return t.map(i, function (t) {
            return e(t, s)
        })
    }

    var s = 0;
    t.fn.TouchSpin = function (e) {
        if ("destroy" === e)return void this.each(function () {
            var e = t(this), s = e.data();
            t(document).off(i(["mouseup", "touchend", "touchcancel", "mousemove", "touchmove", "scroll", "scrollstart"], s.spinnerid).join(" "))
        });
        var n = {
            min: 0,
            max: 100,
            initval: "",
            step: 1,
            decimals: 0,
            stepinterval: 100,
            forcestepdivisibility: "round",
            stepintervaldelay: 500,
            verticalbuttons: !1,
            verticalupclass: "glyphicon glyphicon-chevron-up",
            verticaldownclass: "glyphicon glyphicon-chevron-down",
            prefix: "",
            postfix: "",
            prefix_extraclass: "",
            postfix_extraclass: "",
            booster: !0,
            boostat: 10,
            maxboostedstep: !1,
            mousewheel: !0,
            buttondown_class: "btn btn-default",
            buttonup_class: "btn btn-default",
            buttondown_txt: "-",
            buttonup_txt: "+"
        }, o = {
            min: "min",
            max: "max",
            initval: "init-val",
            step: "step",
            decimals: "decimals",
            stepinterval: "step-interval",
            verticalbuttons: "vertical-buttons",
            verticalupclass: "vertical-up-class",
            verticaldownclass: "vertical-down-class",
            forcestepdivisibility: "force-step-divisibility",
            stepintervaldelay: "step-interval-delay",
            prefix: "prefix",
            postfix: "postfix",
            prefix_extraclass: "prefix-extra-class",
            postfix_extraclass: "postfix-extra-class",
            booster: "booster",
            boostat: "boostat",
            maxboostedstep: "max-boosted-step",
            mousewheel: "mouse-wheel",
            buttondown_class: "button-down-class",
            buttonup_class: "button-up-class",
            buttondown_txt: "button-down-txt",
            buttonup_txt: "button-up-txt"
        };
        return this.each(function () {
            function a() {
                if (!R.data("alreadyinitialized")) {
                    if (R.data("alreadyinitialized", !0), s += 1, R.data("spinnerid", s), !R.is("input"))return void console.log("Must be an input.");
                    l(), r(), y(), p(), m(), g(), v(), b(), P.input.css("display", "block")
                }
            }

            function r() {
                "" !== T.initval && "" === R.val() && R.val(T.initval)
            }

            function h(t) {
                d(t), y();
                var e = P.input.val();
                "" !== e && (e = Number(P.input.val()), P.input.val(e.toFixed(T.decimals)))
            }

            function l() {
                T = t.extend({}, n, F, c(), e)
            }

            function c() {
                var e = {};
                return t.each(o, function (t, i) {
                    var s = "bts-" + i;
                    R.is("[data-" + s + "]") && (e[t] = R.data(s))
                }), e
            }

            function d(e) {
                T = t.extend({}, T, e)
            }

            function p() {
                var t = R.val(), e = R.parent();
                "" !== t && (t = Number(t).toFixed(T.decimals)), R.data("initvalue", t).val(t), R.addClass("form-control"), e.hasClass("input-group") ? u(e) : f()
            }

            function u(e) {
                e.addClass("bootstrap-touchspin");
                var i, s, n = R.prev(), o = R.next(), a = '<span class="input-group-addon bootstrap-touchspin-prefix">' + T.prefix + "</span>", r = '<span class="input-group-addon bootstrap-touchspin-postfix">' + T.postfix + "</span>";
                n.hasClass("input-group-btn") ? (i = '<button class="' + T.buttondown_class + ' bootstrap-touchspin-down" type="button">' + T.buttondown_txt + "</button>", n.append(i)) : (i = '<span class="input-group-btn"><button class="' + T.buttondown_class + ' bootstrap-touchspin-down" type="button">' + T.buttondown_txt + "</button></span>", t(i).insertBefore(R)), o.hasClass("input-group-btn") ? (s = '<button class="' + T.buttonup_class + ' bootstrap-touchspin-up" type="button">' + T.buttonup_txt + "</button>", o.prepend(s)) : (s = '<span class="input-group-btn"><button class="' + T.buttonup_class + ' bootstrap-touchspin-up" type="button">' + T.buttonup_txt + "</button></span>", t(s).insertAfter(R)), t(a).insertBefore(R), t(r).insertAfter(R), D = e
            }

            function f() {
                var e;
                e = T.verticalbuttons ? '<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix">' + T.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + T.postfix + '</span><span class="input-group-btn-vertical"><button class="' + T.buttondown_class + ' bootstrap-touchspin-up" type="button"><i class="' + T.verticalupclass + '"></i></button><button class="' + T.buttonup_class + ' bootstrap-touchspin-down" type="button"><i class="' + T.verticaldownclass + '"></i></button></span></div>' : '<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="' + T.buttondown_class + ' bootstrap-touchspin-down" type="button">' + T.buttondown_txt + '</button></span><span class="input-group-addon bootstrap-touchspin-prefix">' + T.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + T.postfix + '</span><span class="input-group-btn"><button class="' + T.buttonup_class + ' bootstrap-touchspin-up" type="button">' + T.buttonup_txt + "</button></span></div>", D = t(e).insertBefore(R), t(".bootstrap-touchspin-prefix", D).after(R), R.hasClass("input-sm") ? D.addClass("input-group-sm") : R.hasClass("input-lg") && D.addClass("input-group-lg")
            }

            function m() {
                P = {
                    down: t(".bootstrap-touchspin-down", D),
                    up: t(".bootstrap-touchspin-up", D),
                    input: t("input", D),
                    prefix: t(".bootstrap-touchspin-prefix", D).addClass(T.prefix_extraclass),
                    postfix: t(".bootstrap-touchspin-postfix", D).addClass(T.postfix_extraclass)
                }
            }

            function g() {
                "" === T.prefix && P.prefix.hide(), "" === T.postfix && P.postfix.hide()
            }

            function v() {
                R.on("keydown", function (t) {
                    var e = t.keyCode || t.which;
                    38 === e ? ("up" !== z && (x(), $()), t.preventDefault()) : 40 === e && ("down" !== z && (C(), k()), t.preventDefault())
                }), R.on("keyup", function (t) {
                    var e = t.keyCode || t.which;
                    38 === e ? S() : 40 === e && S()
                }), R.on("blur", function () {
                    y()
                }), P.down.on("keydown", function (t) {
                    var e = t.keyCode || t.which;
                    (32 === e || 13 === e) && ("down" !== z && (C(), k()), t.preventDefault())
                }), P.down.on("keyup", function (t) {
                    var e = t.keyCode || t.which;
                    (32 === e || 13 === e) && S()
                }), P.up.on("keydown", function (t) {
                    var e = t.keyCode || t.which;
                    (32 === e || 13 === e) && ("up" !== z && (x(), $()), t.preventDefault())
                }), P.up.on("keyup", function (t) {
                    var e = t.keyCode || t.which;
                    (32 === e || 13 === e) && S()
                }), P.down.on("mousedown.touchspin", function (t) {
                    P.down.off("touchstart.touchspin"), R.is(":disabled") || (C(), k(), t.preventDefault(), t.stopPropagation())
                }), P.down.on("touchstart.touchspin", function (t) {
                    P.down.off("mousedown.touchspin"), R.is(":disabled") || (C(), k(), t.preventDefault(), t.stopPropagation())
                }), P.up.on("mousedown.touchspin", function (t) {
                    P.up.off("touchstart.touchspin"), R.is(":disabled") || (x(), $(), t.preventDefault(), t.stopPropagation())
                }), P.up.on("touchstart.touchspin", function (t) {
                    P.up.off("mousedown.touchspin"), R.is(":disabled") || (x(), $(), t.preventDefault(), t.stopPropagation())
                }), P.up.on("mouseout touchleave touchend touchcancel", function (t) {
                    z && (t.stopPropagation(), S())
                }), P.down.on("mouseout touchleave touchend touchcancel", function (t) {
                    z && (t.stopPropagation(), S())
                }), P.down.on("mousemove touchmove", function (t) {
                    z && (t.stopPropagation(), t.preventDefault())
                }), P.up.on("mousemove touchmove", function (t) {
                    z && (t.stopPropagation(), t.preventDefault())
                }), t(document).on(i(["mouseup", "touchend", "touchcancel"], s).join(" "), function (t) {
                    z && (t.preventDefault(), S())
                }), t(document).on(i(["mousemove", "touchmove", "scroll", "scrollstart"], s).join(" "), function (t) {
                    z && (t.preventDefault(), S())
                }), R.on("mousewheel DOMMouseScroll", function (t) {
                    if (T.mousewheel && R.is(":focus")) {
                        var e = t.originalEvent.wheelDelta || -t.originalEvent.deltaY || -t.originalEvent.detail;
                        t.stopPropagation(), t.preventDefault(), 0 > e ? C() : x()
                    }
                })
            }

            function b() {
                R.on("touchspin.uponce", function () {
                    S(), x()
                }), R.on("touchspin.downonce", function () {
                    S(), C()
                }), R.on("touchspin.startupspin", function () {
                    $()
                }), R.on("touchspin.startdownspin", function () {
                    k()
                }), R.on("touchspin.stopspin", function () {
                    S()
                }), R.on("touchspin.updatesettings", function (t, e) {
                    h(e)
                })
            }

            function _(t) {
                switch (T.forcestepdivisibility) {
                    case"round":
                        return (Math.round(t / T.step) * T.step).toFixed(T.decimals);
                    case"floor":
                        return (Math.floor(t / T.step) * T.step).toFixed(T.decimals);
                    case"ceil":
                        return (Math.ceil(t / T.step) * T.step).toFixed(T.decimals);
                    default:
                        return t
                }
            }

            function y() {
                var t, e, i;
                t = R.val(), "" !== t && (T.decimals > 0 && "." === t || (e = parseFloat(t), isNaN(e) && (e = 0), i = e, e.toString() !== t && (i = e), e < T.min && (i = T.min), e > T.max && (i = T.max), i = _(i), Number(t).toString() !== i.toString() && (R.val(i), R.trigger("change"))))
            }

            function w() {
                if (T.booster) {
                    var t = Math.pow(2, Math.floor(j / T.boostat)) * T.step;
                    return T.maxboostedstep && t > T.maxboostedstep && (t = T.maxboostedstep, I = Math.round(I / t) * t), Math.max(T.step, t)
                }
                return T.step
            }

            function x() {
                y(), I = parseFloat(P.input.val()), isNaN(I) && (I = 0);
                var t = I, e = w();
                I += e, I > T.max && (I = T.max, R.trigger("touchspin.on.max"), S()), P.input.val(Number(I).toFixed(T.decimals)), t !== I && R.trigger("change")
            }

            function C() {
                y(), I = parseFloat(P.input.val()), isNaN(I) && (I = 0);
                var t = I, e = w();
                I -= e, I < T.min && (I = T.min, R.trigger("touchspin.on.min"), S()), P.input.val(I.toFixed(T.decimals)), t !== I && R.trigger("change")
            }

            function k() {
                S(), j = 0, z = "down", R.trigger("touchspin.on.startspin"), R.trigger("touchspin.on.startdownspin"), E = setTimeout(function () {
                    M = setInterval(function () {
                        j++, C()
                    }, T.stepinterval)
                }, T.stepintervaldelay)
            }

            function $() {
                S(), j = 0, z = "up", R.trigger("touchspin.on.startspin"), R.trigger("touchspin.on.startupspin"), A = setTimeout(function () {
                    O = setInterval(function () {
                        j++, x()
                    }, T.stepinterval)
                }, T.stepintervaldelay)
            }

            function S() {
                switch (clearTimeout(E), clearTimeout(A), clearInterval(M), clearInterval(O), z) {
                    case"up":
                        R.trigger("touchspin.on.stopupspin"), R.trigger("touchspin.on.stopspin");
                        break;
                    case"down":
                        R.trigger("touchspin.on.stopdownspin"), R.trigger("touchspin.on.stopspin")
                }
                j = 0, z = !1
            }

            var T, D, P, I, M, O, E, A, R = t(this), F = R.data(), j = 0, z = !1;
            a()
        })
    }
}(jQuery), function (t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? exports.cropit = e(require("jquery")) : t.cropit = e(t.jQuery)
}(this, function (t) {
    return function (t) {
        function e(s) {
            if (i[s])return i[s].exports;
            var n = i[s] = {exports: {}, id: s, loaded: !1};
            return t[s].call(n.exports, n, n.exports, e), n.loaded = !0, n.exports
        }

        var i = {};
        return e.m = t, e.c = i, e.p = "", e(0)
    }([function (t, e, i) {
        function s(t) {
            return t && t.__esModule ? t : {"default": t}
        }

        var n = i(1), o = s(n), a = i(2), r = s(a), h = i(4), l = i(6), c = function (t, e) {
            return t.each(function () {
                var t = o["default"].data(this, h.PLUGIN_KEY);
                t && e(t)
            })
        }, d = function (t, e, i) {
            var s = t.first().data(h.PLUGIN_KEY);
            return s && o["default"].isFunction(s[e]) ? s[e](i) : null
        }, p = {
            init: function (t) {
                return this.each(function () {
                    if (!o["default"].data(this, h.PLUGIN_KEY)) {
                        var e = new r["default"](o["default"], this, t);
                        o["default"].data(this, h.PLUGIN_KEY, e)
                    }
                })
            }, destroy: function () {
                return this.each(function () {
                    o["default"].removeData(this, h.PLUGIN_KEY)
                })
            }, isZoomable: function () {
                return d(this, "isZoomable")
            }, "export": function (t) {
                return d(this, "getCroppedImageData", t)
            }, imageState: function () {
                return d(this, "getImageState")
            }, imageSize: function () {
                return d(this, "getImageSize")
            }, prop: function (t, e) {
                return (0, l.exists)(e) ? c(this, function (i) {
                    i["set" + (0, l.capitalize)(t)](e)
                }) : d(this, "get" + (0, l.capitalize)(t))
            }, disable: function () {
                return c(this, function (t) {
                    t.disable()
                })
            }, reenable: function () {
                return c(this, function (t) {
                    t.reenable()
                })
            }
        };
        o["default"].fn.cropit = function (t) {
            return p[t] ? p[t].apply(this, Array.prototype.slice.call(arguments, 1)) : ["imageSrc", "offset", "previewSize", "zoom", "initialZoom", "exportZoom", "minZoom", "maxZoom"].indexOf(t) >= 0 ? p.prop.apply(this, arguments) : p.init.apply(this, arguments)
        }
    }, function (e, i) {
        e.exports = t
    }, function (t, e, i) {
        function s(t) {
            return t && t.__esModule ? t : {"default": t}
        }

        function n(t, e) {
            if (!(t instanceof e))throw new TypeError("Cannot call a class as a function")
        }

        Object.defineProperty(e, "__esModule", {value: !0});
        var o = function () {
            function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                    var s = e[i];
                    s.enumerable = s.enumerable || !1, s.configurable = !0, "value"in s && (s.writable = !0), Object.defineProperty(t, s.key, s)
                }
            }

            return function (e, i, s) {
                return i && t(e.prototype, i), s && t(e, s), e
            }
        }(), a = i(1), r = s(a), h = i(3), l = s(h), c = i(4), d = i(5), p = i(6), u = function () {
            function t(e, i, s) {
                n(this, t), this.$el = (0, r["default"])(i);
                var o = (0, d.loadDefaults)(this.$el);
                this.options = r["default"].extend({}, o, s), this.init()
            }

            return o(t, [{
                key: "init", value: function () {
                    var t = this;
                    if (this.image = new Image, this.preImage = new Image, this.image.onload = this.onImageLoaded.bind(this), this.preImage.onload = this.onPreImageLoaded.bind(this), this.image.onerror = this.preImage.onerror = function () {
                            t.onImageError.call(t, c.ERRORS.IMAGE_FAILED_TO_LOAD)
                        }, this.$fileInput = this.options.$fileInput.attr({accept: "image/*"}), this.$preview = this.options.$preview.css({backgroundRepeat: "no-repeat"}), this.$zoomSlider = this.options.$zoomSlider.attr({
                            min: 0,
                            max: 1,
                            step: .01
                        }), this.previewSize = {
                            w: this.options.width || this.$preview.width(),
                            h: this.options.height || this.$preview.height()
                        }, this.options.width && this.$preview.width(this.previewSize.w), this.options.height && this.$preview.height(this.previewSize.h), this.options.imageBackground) {
                        r["default"].isArray(this.options.imageBackgroundBorderWidth) ? this.imageBgBorderWidthArray = this.options.imageBackgroundBorderWidth : (this.imageBgBorderWidthArray = [], [0, 1, 2, 3].forEach(function (e) {
                            t.imageBgBorderWidthArray[e] = t.options.imageBackgroundBorderWidth
                        }));
                        var e = this.options.$previewContainer;
                        this.$imageBg = (0, r["default"])("<img />").addClass(c.CLASS_NAMES.IMAGE_BACKGROUND).attr("alt", "").css("position", "absolute"), this.$imageBgContainer = (0, r["default"])("<div />").addClass(c.CLASS_NAMES.IMAGE_BACKGROUND_CONTAINER).css({
                            position: "absolute",
                            zIndex: 0,
                            left: -this.imageBgBorderWidthArray[3] + window.parseInt(this.$preview.css("border-left-width") || 0),
                            top: -this.imageBgBorderWidthArray[0] + window.parseInt(this.$preview.css("border-top-width") || 0),
                            width: this.previewSize.w + this.imageBgBorderWidthArray[1] + this.imageBgBorderWidthArray[3],
                            height: this.previewSize.h + this.imageBgBorderWidthArray[0] + this.imageBgBorderWidthArray[2]
                        }).append(this.$imageBg), this.imageBgBorderWidthArray[0] > 0 && this.$imageBgContainer.css("overflow", "hidden"), e.css("position", "relative").prepend(this.$imageBgContainer), this.$preview.css("position", "relative"), this.$preview.hover(function () {
                            t.$imageBg.addClass(c.CLASS_NAMES.PREVIEW_HOVERED)
                        }, function () {
                            t.$imageBg.removeClass(c.CLASS_NAMES.PREVIEW_HOVERED)
                        })
                    }
                    this.setInitialZoom(this.options.initialZoom), this.imageLoaded = !1, this.moveContinue = !1, this.zoomer = new l["default"], this.options.allowDragNDrop && r["default"].event.props.push("dataTransfer"), this.bindListeners(), this.options.imageState && this.options.imageState.src && this.loadImage(this.options.imageState.src)
                }
            }, {
                key: "bindListeners", value: function () {
                    this.$fileInput.on("change.cropit", this.onFileChange.bind(this)), this.$preview.on(c.EVENTS.PREVIEW, this.onPreviewEvent.bind(this)), this.$zoomSlider.on(c.EVENTS.ZOOM_INPUT, this.onZoomSliderChange.bind(this)), this.options.allowDragNDrop && (this.$preview.on("dragover.cropit dragleave.cropit", this.onDragOver.bind(this)), this.$preview.on("drop.cropit", this.onDrop.bind(this)))
                }
            }, {
                key: "unbindListeners", value: function () {
                    this.$fileInput.off("change.cropit"), this.$preview.off(c.EVENTS.PREVIEW), this.$preview.off("dragover.cropit dragleave.cropit drop.cropit"), this.$zoomSlider.off(c.EVENTS.ZOOM_INPUT)
                }
            }, {
                key: "onFileChange", value: function (t) {
                    this.options.onFileChange(t), this.$fileInput.get(0).files && this.loadFileReader(this.$fileInput.get(0).files[0])
                }
            }, {
                key: "loadFileReader", value: function (t) {
                    var e = new FileReader;
                    t && t.type.match("image") ? (e.readAsDataURL(t), e.onload = this.onFileReaderLoaded.bind(this), e.onerror = this.onFileReaderError.bind(this)) : t && this.onFileReaderError()
                }
            }, {
                key: "onFileReaderLoaded", value: function (t) {
                    this.loadImage(t.target.result)
                }
            }, {
                key: "onFileReaderError", value: function () {
                    this.options.onFileReaderError()
                }
            }, {
                key: "onDragOver", value: function (t) {
                    t.preventDefault(), t.dataTransfer.dropEffect = "copy", this.$preview.toggleClass(c.CLASS_NAMES.DRAG_HOVERED, "dragover" === t.type)
                }
            }, {
                key: "onDrop", value: function (t) {
                    var e = this;
                    t.preventDefault(), t.stopPropagation();
                    var i = Array.prototype.slice.call(t.dataTransfer.files, 0);
                    i.some(function (t) {
                        return t.type.match("image") ? (e.loadFileReader(t), !0) : !1
                    }), this.$preview.removeClass(c.CLASS_NAMES.DRAG_HOVERED)
                }
            }, {
                key: "loadImage", value: function (t) {
                    t && (this.options.onImageLoading(), this.setImageLoadingClass(), this.preImage.src = t)
                }
            }, {
                key: "setImageSrc", value: function (t) {
                    this.loadImage(t)
                }
            }, {
                key: "onPreImageLoaded", value: function () {
                    return "reject" === this.options.smallImage && (this.preImage.width * this.options.maxZoom < this.previewSize.w * this.options.exportZoom || this.preImage.height * this.options.maxZoom < this.previewSize.h * this.options.exportZoom) ? (this.onImageError(c.ERRORS.SMALL_IMAGE), void(this.image.src && this.setImageLoadedClass())) : (this.options.allowCrossOrigin && (this.image.crossOrigin = 0 === this.preImage.src.indexOf("data:") ? null : "Anonymous"), void(this.image.src = this.imageSrc = this.preImage.src))
                }
            }, {
                key: "onImageLoaded", value: function () {
                    this.imageSize = {
                        w: this.image.width,
                        h: this.image.height
                    }, this.setupZoomer(this.options.imageState && this.options.imageState.zoom || this.initialZoom), this.options.imageState && this.options.imageState.offset ? this.setOffset(this.options.imageState.offset) : this.centerImage(), this.options.imageState = {}, this.$preview.css("background-image", "url(" + this.imageSrc + ")"), this.options.imageBackground && this.$imageBg.attr("src", this.imageSrc), this.setImageLoadedClass(), this.imageLoaded = !0, this.options.onImageLoaded()
                }
            }, {
                key: "onImageError", value: function () {
                    this.options.onImageError.apply(this, arguments), this.removeImageLoadingClass()
                }
            }, {
                key: "setImageLoadingClass", value: function () {
                    this.$preview.removeClass(c.CLASS_NAMES.IMAGE_LOADED).addClass(c.CLASS_NAMES.IMAGE_LOADING)
                }
            }, {
                key: "setImageLoadedClass", value: function () {
                    this.$preview.removeClass(c.CLASS_NAMES.IMAGE_LOADING).addClass(c.CLASS_NAMES.IMAGE_LOADED)
                }
            }, {
                key: "removeImageLoadingClass", value: function () {
                    this.$preview.removeClass(c.CLASS_NAMES.IMAGE_LOADING)
                }
            }, {
                key: "getEventPosition", value: function (t) {
                    return t.originalEvent && t.originalEvent.touches && t.originalEvent.touches[0] && (t = t.originalEvent.touches[0]), t.clientX && t.clientY ? {
                        x: t.clientX,
                        y: t.clientY
                    } : void 0
                }
            }, {
                key: "onPreviewEvent", value: function (t) {
                    return this.imageLoaded ? (this.moveContinue = !1, this.$preview.off(c.EVENTS.PREVIEW_MOVE), "mousedown" === t.type || "touchstart" === t.type ? (this.origin = this.getEventPosition(t), this.moveContinue = !0, this.$preview.on(c.EVENTS.PREVIEW_MOVE, this.onMove.bind(this))) : (0, r["default"])(document.body).focus(), t.stopPropagation(), !1) : void 0
                }
            }, {
                key: "onMove", value: function (t) {
                    var e = this.getEventPosition(t);
                    return this.moveContinue && e && this.setOffset({
                        x: this.offset.x + e.x - this.origin.x,
                        y: this.offset.y + e.y - this.origin.y
                    }), this.origin = e, t.stopPropagation(), !1
                }
            }, {
                key: "setOffset", value: function (t) {
                    t && (0, p.exists)(t.x) && (0, p.exists)(t.y) && (this.offset = this.fixOffset(t), this.$preview.css("background-position", "" + this.offset.x + "px " + this.offset.y + "px"), this.options.imageBackground && this.$imageBg.css({
                        left: this.offset.x + this.imageBgBorderWidthArray[3],
                        top: this.offset.y + this.imageBgBorderWidthArray[0]
                    }), this.options.onOffsetChange(t))
                }
            }, {
                key: "fixOffset", value: function (t) {
                    if (!this.imageLoaded)return t;
                    var e = {x: t.x, y: t.y};
                    return this.options.freeMove || (this.imageSize.w * this.zoom >= this.previewSize.w ? e.x = Math.min(0, Math.max(e.x, this.previewSize.w - this.imageSize.w * this.zoom)) : e.x = Math.max(0, Math.min(e.x, this.previewSize.w - this.imageSize.w * this.zoom)), this.imageSize.h * this.zoom >= this.previewSize.h ? e.y = Math.min(0, Math.max(e.y, this.previewSize.h - this.imageSize.h * this.zoom)) : e.y = Math.max(0, Math.min(e.y, this.previewSize.h - this.imageSize.h * this.zoom))), e.x = (0, p.round)(e.x), e.y = (0, p.round)(e.y), e
                }
            }, {
                key: "centerImage", value: function () {
                    this.imageSize && this.zoom && this.setOffset({
                        x: (this.previewSize.w - this.imageSize.w * this.zoom) / 2,
                        y: (this.previewSize.h - this.imageSize.h * this.zoom) / 2
                    })
                }
            }, {
                key: "onZoomSliderChange", value: function () {
                    if (this.imageLoaded) {
                        this.zoomSliderPos = Number(this.$zoomSlider.val());
                        var t = this.zoomer.getZoom(this.zoomSliderPos);
                        t !== this.zoom && this.setZoom(t)
                    }
                }
            }, {
                key: "enableZoomSlider", value: function () {
                    this.$zoomSlider.removeAttr("disabled"), this.options.onZoomEnabled()
                }
            }, {
                key: "disableZoomSlider", value: function () {
                    this.$zoomSlider.attr("disabled", !0), this.options.onZoomDisabled()
                }
            }, {
                key: "setupZoomer", value: function (t) {
                    this.zoomer.setup({
                        imageSize: this.imageSize,
                        previewSize: this.previewSize,
                        exportZoom: this.options.exportZoom,
                        maxZoom: this.options.maxZoom,
                        minZoom: this.options.minZoom,
                        smallImage: this.options.smallImage
                    }), this.setZoom((0, p.exists)(t) ? t : this.zoom), this.isZoomable() ? this.enableZoomSlider() : this.disableZoomSlider()
                }
            }, {
                key: "setZoom", value: function (t) {
                    t = this.fixZoom(t);
                    var e = (0, p.round)(this.imageSize.w * t), i = (0, p.round)(this.imageSize.h * t);
                    if (this.imageLoaded) {
                        var s = this.zoom, n = this.previewSize.w / 2 - (this.previewSize.w / 2 - this.offset.x) * t / s, o = this.previewSize.h / 2 - (this.previewSize.h / 2 - this.offset.y) * t / s;
                        this.zoom = t, this.setOffset({x: n, y: o})
                    } else this.zoom = t;
                    this.zoomSliderPos = this.zoomer.getSliderPos(this.zoom), this.$zoomSlider.val(this.zoomSliderPos), this.$preview.css("background-size", "" + e + "px " + i + "px"), this.options.imageBackground && this.$imageBg.css({
                        width: e,
                        height: i
                    }), this.options.onZoomChange(t)
                }
            }, {
                key: "fixZoom", value: function (t) {
                    return this.zoomer.fixZoom(t)
                }
            }, {
                key: "isZoomable", value: function () {
                    return this.zoomer.isZoomable()
                }
            }, {
                key: "getCroppedImageData", value: function (t) {
                    if (this.imageSrc) {
                        var e = {type: "image/png", quality: .75, originalSize: !1, fillBg: "#fff"};
                        t = r["default"].extend({}, e, t);
                        var i = t.originalSize ? 1 / this.zoom : this.options.exportZoom, s = {
                            w: this.zoom * i * this.imageSize.w,
                            h: this.zoom * i * this.imageSize.h
                        }, n = (0, r["default"])("<canvas />").attr({
                            width: this.previewSize.w * i,
                            height: this.previewSize.h * i
                        }).get(0), o = n.getContext("2d");
                        return "image/jpeg" === t.type && (o.fillStyle = t.fillBg, o.fillRect(0, 0, n.width, n.height)), o.drawImage(this.image, this.offset.x * i, this.offset.y * i, s.w, s.h), n.toDataURL(t.type, t.quality)
                    }
                }
            }, {
                key: "getImageState", value: function () {
                    return {src: this.imageSrc, offset: this.offset, zoom: this.zoom}
                }
            }, {
                key: "getImageSrc", value: function () {
                    return this.imageSrc
                }
            }, {
                key: "getOffset", value: function () {
                    return this.offset
                }
            }, {
                key: "getZoom", value: function () {
                    return this.zoom
                }
            }, {
                key: "getImageSize", value: function () {
                    return this.imageSize ? {width: this.imageSize.w, height: this.imageSize.h} : null
                }
            }, {
                key: "getInitialZoom", value: function () {
                    return this.options.initialZoom
                }
            }, {
                key: "setInitialZoom", value: function (t) {
                    this.options.initialZoom = t, "min" === t ? this.initialZoom = 0 : "image" === t ? this.initialZoom = 1 : this.initialZoom = 0
                }
            }, {
                key: "getExportZoom", value: function () {
                    return this.options.exportZoom
                }
            }, {
                key: "setExportZoom", value: function (t) {
                    this.options.exportZoom = t, this.setupZoomer()
                }
            }, {
                key: "getMinZoom", value: function () {
                    return this.options.minZoom
                }
            }, {
                key: "setMinZoom", value: function (t) {
                    this.options.minZoom = t, this.setupZoomer()
                }
            }, {
                key: "getMaxZoom", value: function () {
                    return this.options.maxZoom
                }
            }, {
                key: "setMaxZoom", value: function (t) {
                    this.options.maxZoom = t, this.setupZoomer()
                }
            }, {
                key: "getPreviewSize", value: function () {
                    return {width: this.previewSize.w, height: this.previewSize.h}
                }
            }, {
                key: "setPreviewSize", value: function (t) {
                    !t || t.width <= 0 || t.height <= 0 || (this.previewSize = {
                        w: t.width,
                        h: t.height
                    }, this.$preview.css({
                        width: this.previewSize.w,
                        height: this.previewSize.h
                    }), this.options.imageBackground && this.$imageBgContainer.css({
                        width: this.previewSize.w + this.imageBgBorderWidthArray[1] + this.imageBgBorderWidthArray[3],
                        height: this.previewSize.h + this.imageBgBorderWidthArray[0] + this.imageBgBorderWidthArray[2]
                    }), this.imageLoaded && this.setupZoomer())
                }
            }, {
                key: "disable", value: function () {
                    this.unbindListeners(), this.disableZoomSlider(), this.$el.addClass(c.CLASS_NAMES.DISABLED)
                }
            }, {
                key: "reenable", value: function () {
                    this.bindListeners(), this.enableZoomSlider(), this.$el.removeClass(c.CLASS_NAMES.DISABLED)
                }
            }, {
                key: "$", value: function (t) {
                    return this.$el ? this.$el.find(t) : null
                }
            }]), t
        }();
        e["default"] = u, t.exports = e["default"]
    }, function (t, e) {
        function i(t, e) {
            if (!(t instanceof e))throw new TypeError("Cannot call a class as a function")
        }

        Object.defineProperty(e, "__esModule", {value: !0});
        var s = function () {
            function t(t, e) {
                for (var i = 0; i < e.length; i++) {
                    var s = e[i];
                    s.enumerable = s.enumerable || !1, s.configurable = !0, "value"in s && (s.writable = !0), Object.defineProperty(t, s.key, s)
                }
            }

            return function (e, i, s) {
                return i && t(e.prototype, i), s && t(e, s), e
            }
        }(), n = function () {
            function t() {
                i(this, t), this.minZoom = this.maxZoom = 1
            }

            return s(t, [{
                key: "setup", value: function (t) {
                    var e = t.imageSize, i = t.previewSize, s = t.exportZoom, n = t.maxZoom, o = t.minZoom, a = t.smallImage, r = i.w / e.w, h = i.h / e.h;
                    "fit" === o ? this.minZoom = Math.min(r, h) : this.minZoom = Math.max(r, h), "allow" === a && (this.minZoom = Math.min(this.minZoom, 1)), this.maxZoom = Math.max(this.minZoom, n / s)
                }
            }, {
                key: "getZoom", value: function (t) {
                    return this.minZoom && this.maxZoom ? t * (this.maxZoom - this.minZoom) + this.minZoom : null
                }
            }, {
                key: "getSliderPos", value: function (t) {
                    return this.minZoom && this.maxZoom ? this.minZoom === this.maxZoom ? 0 : (t - this.minZoom) / (this.maxZoom - this.minZoom) : null
                }
            }, {
                key: "isZoomable", value: function () {
                    return this.minZoom && this.maxZoom ? this.minZoom !== this.maxZoom : null
                }
            }, {
                key: "fixZoom", value: function (t) {
                    return Math.max(this.minZoom, Math.min(this.maxZoom, t))
                }
            }]), t
        }();
        e["default"] = n, t.exports = e["default"]
    }, function (t, e) {
        Object.defineProperty(e, "__esModule", {value: !0});
        var i = "cropit";
        e.PLUGIN_KEY = i;
        var s = {
            PREVIEW: "cropit-image-preview",
            PREVIEW_CONTAINER: "cropit-image-preview-container",
            FILE_INPUT: "cropit-image-input",
            ZOOM_SLIDER: "cropit-image-zoom-input",
            IMAGE_BACKGROUND: "cropit-image-background",
            IMAGE_BACKGROUND_CONTAINER: "cropit-image-background-container",
            PREVIEW_HOVERED: "cropit-preview-hovered",
            DRAG_HOVERED: "cropit-drag-hovered",
            IMAGE_LOADING: "cropit-image-loading",
            IMAGE_LOADED: "cropit-image-loaded",
            DISABLED: "cropit-disabled"
        };
        e.CLASS_NAMES = s;
        var n = {
            IMAGE_FAILED_TO_LOAD: {code: 0, message: "Image failed to load."},
            SMALL_IMAGE: {code: 1, message: "Image is too small."}
        };
        e.ERRORS = n;
        var o = function (t) {
            return t.map(function (t) {
                return "" + t + ".cropit"
            }).join(" ")
        }, a = {
            PREVIEW: o(["mousedown", "mouseup", "mouseleave", "touchstart", "touchend", "touchcancel", "touchleave"]),
            PREVIEW_MOVE: o(["mousemove", "touchmove"]),
            ZOOM_INPUT: o(["mousemove", "touchmove", "change"])
        };
        e.EVENTS = a
    }, function (t, e, i) {
        Object.defineProperty(e, "__esModule", {value: !0});
        var s = i(4), n = {
            elements: [{
                name: "$preview",
                description: "The HTML element that displays image preview.",
                defaultSelector: "." + s.CLASS_NAMES.PREVIEW
            }, {
                name: "$fileInput",
                description: "File input element.",
                defaultSelector: "input." + s.CLASS_NAMES.FILE_INPUT
            }, {
                name: "$zoomSlider",
                description: "Range input element that controls image zoom.",
                defaultSelector: "input." + s.CLASS_NAMES.ZOOM_SLIDER
            }, {
                name: "$previewContainer",
                description: "Preview container. Only needed when `imageBackground` is true.",
                defaultSelector: "." + s.CLASS_NAMES.PREVIEW_CONTAINER
            }].map(function (t) {
                return t.type = "jQuery element", t["default"] = "$imageCropper.find('" + t.defaultSelector + "')", t
            }),
            values: [{
                name: "width",
                type: "number",
                description: "Width of image preview in pixels. If set, it will override the CSS property.",
                "default": null
            }, {
                name: "height",
                type: "number",
                description: "Height of image preview in pixels. If set, it will override the CSS property.",
                "default": null
            }, {
                name: "imageBackground",
                type: "boolean",
                description: "Whether or not to display the background image beyond the preview area.",
                "default": !1
            }, {
                name: "imageBackgroundBorderWidth",
                type: "array or number",
                description: "Width of background image border in pixels.\n        The four array elements specify the width of background image width on the top, right, bottom, left side respectively.\n        The background image beyond the width will be hidden.\n        If specified as a number, border with uniform width on all sides will be applied.",
                "default": [0, 0, 0, 0]
            }, {
                name: "exportZoom",
                type: "number",
                description: "The ratio between the desired image size to export and the preview size.\n        For example, if the preview size is `300px * 200px`, and `exportZoom = 2`, then\n        the exported image size will be `600px * 400px`.\n        This also affects the maximum zoom level, since the exported image cannot be zoomed to larger than its original size.",
                "default": 1
            }, {
                name: "allowDragNDrop",
                type: "boolean",
                description: "When set to true, you can load an image by dragging it from local file browser onto the preview area.",
                "default": !0
            }, {
                name: "minZoom",
                type: "string",
                description: "This options decides the minimal zoom level of the image.\n        If set to `'fill'`, the image has to fill the preview area, i.e. both width and height must not go smaller than the preview area.\n        If set to `'fit'`, the image can shrink further to fit the preview area, i.e. at least one of its edges must not go smaller than the preview area.",
                "default": "fill"
            }, {
                name: "maxZoom",
                type: "number",
                description: "Determines how big the image can be zoomed. E.g. if set to 1.5, the image can be zoomed to 150% of its original size.",
                "default": 1
            }, {
                name: "initialZoom",
                type: "string",
                description: "Determines the zoom when an image is loaded.\n        When set to `'min'`, image is zoomed to the smallest when loaded.\n        When set to `'image'`, image is zoomed to 100% when loaded.",
                "default": "min"
            }, {
                name: "freeMove",
                type: "boolean",
                description: "When set to true, you can freely move the image instead of being bound to the container borders",
                "default": !1
            }, {
                name: "smallImage",
                type: "string",
                description: "When set to `'reject'`, `onImageError` would be called when cropit loads an image that is smaller than the container.\n        When set to `'allow'`, images smaller than the container can be zoomed down to its original size, overiding `minZoom` option.\n        When set to `'stretch'`, the minimum zoom of small images would follow `minZoom` option.",
                "default": "reject"
            }, {
                name: "allowCrossOrigin",
                type: "boolean",
                description: "Set to true if you need to crop image served from other domains.",
                "default": !1
            }],
            callbacks: [{
                name: "onFileChange",
                description: "Called when user selects a file in the select file input.",
                params: [{name: "event", type: "object", description: "File change event object"}]
            }, {
                name: "onFileReaderError",
                description: "Called when `FileReader` encounters an error while loading the image file."
            }, {name: "onImageLoading", description: "Called when image starts to be loaded."}, {
                name: "onImageLoaded",
                description: "Called when image is loaded."
            }, {
                name: "onImageError",
                description: "Called when image cannot be loaded.",
                params: [{name: "error", type: "object", description: "Error object."}, {
                    name: "error.code",
                    type: "number",
                    description: "Error code. `0` means generic image loading failure. `1` means image is too small."
                }, {name: "error.message", type: "string", description: "A message explaining the error."}]
            }, {
                name: "onZoomEnabled",
                description: "Called when image the zoom slider is enabled."
            }, {
                name: "onZoomDisabled",
                description: "Called when image the zoom slider is disabled."
            }, {
                name: "onZoomChange",
                description: "Called when zoom changes.",
                params: [{name: "zoom", type: "number", description: "New zoom."}]
            }, {
                name: "onOffsetChange",
                description: "Called when image offset changes.",
                params: [{name: "offset", type: "object", description: "New offset, with `x` and `y` values."}]
            }].map(function (t) {
                return t.type = "function", t
            })
        }, o = function (t) {
            var e = {};
            return t && n.elements.forEach(function (i) {
                e[i.name] = t.find(i.defaultSelector)
            }), n.values.forEach(function (t) {
                e[t.name] = t["default"]
            }), n.callbacks.forEach(function (t) {
                e[t.name] = function () {
                }
            }), e
        };
        e.loadDefaults = o, e["default"] = n
    }, function (t, e) {
        Object.defineProperty(e, "__esModule", {value: !0});
        var i = function (t) {
            return "undefined" != typeof t
        };
        e.exists = i;
        var s = function (t) {
            return +(Math.round(100 * t) + "e-2")
        };
        e.round = s;
        var n = function (t) {
            return t.charAt(0).toUpperCase() + t.slice(1)
        };
        e.capitalize = n
    }])
}), function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : jQuery)
}(function (t) {
    var e = 0, i = Array.prototype.slice;
    t.cleanData = function (e) {
        return function (i) {
            var s, n, o;
            for (o = 0; null != (n = i[o]); o++)try {
                s = t._data(n, "events"), s && s.remove && t(n).triggerHandler("remove")
            } catch (a) {
            }
            e(i)
        }
    }(t.cleanData), t.widget = function (e, i, s) {
        var n, o, a, r, h = {}, l = e.split(".")[0];
        return e = e.split(".")[1], n = l + "-" + e, s || (s = i, i = t.Widget), t.expr[":"][n.toLowerCase()] = function (e) {
            return !!t.data(e, n)
        }, t[l] = t[l] || {}, o = t[l][e], a = t[l][e] = function (t, e) {
            return this._createWidget ? void(arguments.length && this._createWidget(t, e)) : new a(t, e)
        }, t.extend(a, o, {
            version: s.version,
            _proto: t.extend({}, s),
            _childConstructors: []
        }), r = new i, r.options = t.widget.extend({}, r.options), t.each(s, function (e, s) {
            return t.isFunction(s) ? void(h[e] = function () {
                var t = function () {
                    return i.prototype[e].apply(this, arguments)
                }, n = function (t) {
                    return i.prototype[e].apply(this, t)
                };
                return function () {
                    var e, i = this._super, o = this._superApply;
                    return this._super = t, this._superApply = n, e = s.apply(this, arguments), this._super = i, this._superApply = o, e
                }
            }()) : void(h[e] = s)
        }), a.prototype = t.widget.extend(r, {widgetEventPrefix: o ? r.widgetEventPrefix || e : e}, h, {
            constructor: a,
            namespace: l,
            widgetName: e,
            widgetFullName: n
        }), o ? (t.each(o._childConstructors, function (e, i) {
            var s = i.prototype;
            t.widget(s.namespace + "." + s.widgetName, a, i._proto)
        }), delete o._childConstructors) : i._childConstructors.push(a), t.widget.bridge(e, a), a
    }, t.widget.extend = function (e) {
        for (var s, n, o = i.call(arguments, 1), a = 0, r = o.length; r > a; a++)for (s in o[a])n = o[a][s], o[a].hasOwnProperty(s) && void 0 !== n && (t.isPlainObject(n) ? e[s] = t.isPlainObject(e[s]) ? t.widget.extend({}, e[s], n) : t.widget.extend({}, n) : e[s] = n);
        return e
    }, t.widget.bridge = function (e, s) {
        var n = s.prototype.widgetFullName || e;
        t.fn[e] = function (o) {
            var a = "string" == typeof o, r = i.call(arguments, 1), h = this;
            return a ? this.each(function () {
                var i, s = t.data(this, n);
                return "instance" === o ? (h = s, !1) : s ? t.isFunction(s[o]) && "_" !== o.charAt(0) ? (i = s[o].apply(s, r), i !== s && void 0 !== i ? (h = i && i.jquery ? h.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + o + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" + o + "'")
            }) : (r.length && (o = t.widget.extend.apply(null, [o].concat(r))), this.each(function () {
                var e = t.data(this, n);
                e ? (e.option(o || {}), e._init && e._init()) : t.data(this, n, new s(o, this))
            })), h
        }
    }, t.Widget = function () {
    }, t.Widget._childConstructors = [], t.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {disabled: !1, create: null},
        _createWidget: function (i, s) {
            s = t(s || this.defaultElement || this)[0], this.element = t(s), this.uuid = e++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = t(), this.hoverable = t(), this.focusable = t(), s !== this && (t.data(s, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function (t) {
                    t.target === s && this.destroy()
                }
            }), this.document = t(s.style ? s.ownerDocument : s.document || s), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this.options = t.widget.extend({}, this.options, this._getCreateOptions(), i), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: t.noop,
        _getCreateEventData: t.noop,
        _create: t.noop,
        _init: t.noop,
        destroy: function () {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: t.noop,
        widget: function () {
            return this.element
        },
        option: function (e, i) {
            var s, n, o, a = e;
            if (0 === arguments.length)return t.widget.extend({}, this.options);
            if ("string" == typeof e)if (a = {}, s = e.split("."), e = s.shift(), s.length) {
                for (n = a[e] = t.widget.extend({}, this.options[e]), o = 0; o < s.length - 1; o++)n[s[o]] = n[s[o]] || {}, n = n[s[o]];
                if (e = s.pop(), 1 === arguments.length)return void 0 === n[e] ? null : n[e];
                n[e] = i
            } else {
                if (1 === arguments.length)return void 0 === this.options[e] ? null : this.options[e];
                a[e] = i
            }
            return this._setOptions(a), this
        },
        _setOptions: function (t) {
            var e;
            for (e in t)this._setOption(e, t[e]);
            return this
        },
        _setOption: function (t, e) {
            return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!e), e && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
        },
        enable: function () {
            return this._setOptions({disabled: !1})
        },
        disable: function () {
            return this._setOptions({disabled: !0})
        },
        _on: function (e, i, s) {
            var n, o = this;
            "boolean" != typeof e && (s = i, i = e, e = !1), s ? (i = n = t(i), this.bindings = this.bindings.add(i)) : (s = i, i = this.element, n = this.widget()), t.each(s, function (s, a) {
                function r() {
                    return e || o.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? o[a] : a).apply(o, arguments) : void 0
                }

                "string" != typeof a && (r.guid = a.guid = a.guid || r.guid || t.guid++);
                var h = s.match(/^([\w:-]*)\s*(.*)$/), l = h[1] + o.eventNamespace, c = h[2];
                c ? n.delegate(c, l, r) : i.bind(l, r)
            })
        },
        _off: function (e, i) {
            i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(i).undelegate(i), this.bindings = t(this.bindings.not(e).get()), this.focusable = t(this.focusable.not(e).get()), this.hoverable = t(this.hoverable.not(e).get())
        },
        _delay: function (t, e) {
            function i() {
                return ("string" == typeof t ? s[t] : t).apply(s, arguments)
            }

            var s = this;
            return setTimeout(i, e || 0)
        },
        _hoverable: function (e) {
            this.hoverable = this.hoverable.add(e), this._on(e, {
                mouseenter: function (e) {
                    t(e.currentTarget).addClass("ui-state-hover")
                }, mouseleave: function (e) {
                    t(e.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function (e) {
            this.focusable = this.focusable.add(e), this._on(e, {
                focusin: function (e) {
                    t(e.currentTarget).addClass("ui-state-focus")
                }, focusout: function (e) {
                    t(e.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function (e, i, s) {
            var n, o, a = this.options[e];
            if (s = s || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], o = i.originalEvent)for (n in o)n in i || (i[n] = o[n]);
            return this.element.trigger(i, s), !(t.isFunction(a) && a.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented())
        }
    }, t.each({show: "fadeIn", hide: "fadeOut"}, function (e, i) {
        t.Widget.prototype["_" + e] = function (s, n, o) {
            "string" == typeof n && (n = {effect: n});
            var a, r = n ? n === !0 || "number" == typeof n ? i : n.effect || i : e;
            n = n || {}, "number" == typeof n && (n = {duration: n}), a = !t.isEmptyObject(n), n.complete = o, n.delay && s.delay(n.delay), a && t.effects && t.effects.effect[r] ? s[e](n) : r !== e && s[r] ? s[r](n.duration, n.easing, o) : s.queue(function (i) {
                t(this)[e](), o && o.call(s[0]), i()
            })
        }
    });
    t.widget
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery", "jquery.ui.widget"], t) : "object" == typeof exports ? t(require("jquery"), require("./vendor/jquery.ui.widget")) : t(window.jQuery)
}(function (t) {
    "use strict";
    function e(e) {
        var i = "dragover" === e;
        return function (s) {
            s.dataTransfer = s.originalEvent && s.originalEvent.dataTransfer;
            var n = s.dataTransfer;
            n && -1 !== t.inArray("Files", n.types) && this._trigger(e, t.Event(e, {delegatedEvent: s})) !== !1 && (s.preventDefault(), i && (n.dropEffect = "copy"))
        }
    }

    t.support.fileInput = !(new RegExp("(Android (1\\.[0156]|2\\.[01]))|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)|(w(eb)?OSBrowser)|(webOS)|(Kindle/(1\\.0|2\\.[05]|3\\.0))").test(window.navigator.userAgent) || t('<input type="file">').prop("disabled")), t.support.xhrFileUpload = !(!window.ProgressEvent || !window.FileReader), t.support.xhrFormDataFileUpload = !!window.FormData, t.support.blobSlice = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice), t.widget("blueimp.fileupload", {
        options: {
            dropZone: t(document),
            pasteZone: void 0,
            fileInput: void 0,
            replaceFileInput: !0,
            paramName: void 0,
            singleFileUploads: !0,
            limitMultiFileUploads: void 0,
            limitMultiFileUploadSize: void 0,
            limitMultiFileUploadSizeOverhead: 512,
            sequentialUploads: !1,
            limitConcurrentUploads: void 0,
            forceIframeTransport: !1,
            redirect: void 0,
            redirectParamName: void 0,
            postMessage: void 0,
            multipart: !0,
            maxChunkSize: void 0,
            uploadedBytes: void 0,
            recalculateProgress: !0,
            progressInterval: 100,
            bitrateInterval: 500,
            autoUpload: !0,
            messages: {uploadedBytes: "Uploaded bytes exceed file size"},
            i18n: function (e, i) {
                return e = this.messages[e] || e.toString(), i && t.each(i, function (t, i) {
                    e = e.replace("{" + t + "}", i)
                }), e
            },
            formData: function (t) {
                return t.serializeArray()
            },
            add: function (e, i) {
                return e.isDefaultPrevented() ? !1 : void((i.autoUpload || i.autoUpload !== !1 && t(this).fileupload("option", "autoUpload")) && i.process().done(function () {
                    i.submit()
                }))
            },
            processData: !1,
            contentType: !1,
            cache: !1,
            timeout: 0
        },
        _specialOptions: ["fileInput", "dropZone", "pasteZone", "multipart", "forceIframeTransport"],
        _blobSlice: t.support.blobSlice && function () {
            var t = this.slice || this.webkitSlice || this.mozSlice;
            return t.apply(this, arguments)
        },
        _BitrateTimer: function () {
            this.timestamp = Date.now ? Date.now() : (new Date).getTime(), this.loaded = 0, this.bitrate = 0, this.getBitrate = function (t, e, i) {
                var s = t - this.timestamp;
                return (!this.bitrate || !i || s > i) && (this.bitrate = (e - this.loaded) * (1e3 / s) * 8, this.loaded = e, this.timestamp = t), this.bitrate
            }
        },
        _isXHRUpload: function (e) {
            return !e.forceIframeTransport && (!e.multipart && t.support.xhrFileUpload || t.support.xhrFormDataFileUpload)
        },
        _getFormData: function (e) {
            var i;
            return "function" === t.type(e.formData) ? e.formData(e.form) : t.isArray(e.formData) ? e.formData : "object" === t.type(e.formData) ? (i = [], t.each(e.formData, function (t, e) {
                i.push({name: t, value: e})
            }), i) : []
        },
        _getTotal: function (e) {
            var i = 0;
            return t.each(e, function (t, e) {
                i += e.size || 1
            }), i
        },
        _initProgressObject: function (e) {
            var i = {loaded: 0, total: 0, bitrate: 0};
            e._progress ? t.extend(e._progress, i) : e._progress = i
        },
        _initResponseObject: function (t) {
            var e;
            if (t._response)for (e in t._response)t._response.hasOwnProperty(e) && delete t._response[e]; else t._response = {}
        },
        _onProgress: function (e, i) {
            if (e.lengthComputable) {
                var s, n = Date.now ? Date.now() : (new Date).getTime();
                if (i._time && i.progressInterval && n - i._time < i.progressInterval && e.loaded !== e.total)return;
                i._time = n, s = Math.floor(e.loaded / e.total * (i.chunkSize || i._progress.total)) + (i.uploadedBytes || 0), this._progress.loaded += s - i._progress.loaded, this._progress.bitrate = this._bitrateTimer.getBitrate(n, this._progress.loaded, i.bitrateInterval), i._progress.loaded = i.loaded = s, i._progress.bitrate = i.bitrate = i._bitrateTimer.getBitrate(n, s, i.bitrateInterval), this._trigger("progress", t.Event("progress", {delegatedEvent: e}), i), this._trigger("progressall", t.Event("progressall", {delegatedEvent: e}), this._progress)
            }
        },
        _initProgressListener: function (e) {
            var i = this, s = e.xhr ? e.xhr() : t.ajaxSettings.xhr();
            s.upload && (t(s.upload).bind("progress", function (t) {
                var s = t.originalEvent;
                t.lengthComputable = s.lengthComputable, t.loaded = s.loaded, t.total = s.total, i._onProgress(t, e)
            }), e.xhr = function () {
                return s
            })
        },
        _isInstanceOf: function (t, e) {
            return Object.prototype.toString.call(e) === "[object " + t + "]"
        },
        _initXHRData: function (e) {
            var i, s = this, n = e.files[0], o = e.multipart || !t.support.xhrFileUpload, a = "array" === t.type(e.paramName) ? e.paramName[0] : e.paramName;
            e.headers = t.extend({}, e.headers), e.contentRange && (e.headers["Content-Range"] = e.contentRange), o && !e.blob && this._isInstanceOf("File", n) || (e.headers["Content-Disposition"] = 'attachment; filename="' + encodeURI(n.name) + '"'), o ? t.support.xhrFormDataFileUpload && (e.postMessage ? (i = this._getFormData(e), e.blob ? i.push({
                name: a,
                value: e.blob
            }) : t.each(e.files, function (s, n) {
                i.push({name: "array" === t.type(e.paramName) && e.paramName[s] || a, value: n})
            })) : (s._isInstanceOf("FormData", e.formData) ? i = e.formData : (i = new FormData, t.each(this._getFormData(e), function (t, e) {
                i.append(e.name, e.value)
            })), e.blob ? i.append(a, e.blob, n.name) : t.each(e.files, function (n, o) {
                (s._isInstanceOf("File", o) || s._isInstanceOf("Blob", o)) && i.append("array" === t.type(e.paramName) && e.paramName[n] || a, o, o.uploadName || o.name)
            })), e.data = i) : (e.contentType = n.type || "application/octet-stream", e.data = e.blob || n), e.blob = null
        },
        _initIframeSettings: function (e) {
            var i = t("<a></a>").prop("href", e.url).prop("host");
            e.dataType = "iframe " + (e.dataType || ""), e.formData = this._getFormData(e), e.redirect && i && i !== location.host && e.formData.push({
                name: e.redirectParamName || "redirect",
                value: e.redirect
            })
        },
        _initDataSettings: function (t) {
            this._isXHRUpload(t) ? (this._chunkedUpload(t, !0) || (t.data || this._initXHRData(t), this._initProgressListener(t)), t.postMessage && (t.dataType = "postmessage " + (t.dataType || ""))) : this._initIframeSettings(t)
        },
        _getParamName: function (e) {
            var i = t(e.fileInput), s = e.paramName;
            return s ? t.isArray(s) || (s = [s]) : (s = [], i.each(function () {
                for (var e = t(this), i = e.prop("name") || "files[]", n = (e.prop("files") || [1]).length; n;)s.push(i), n -= 1
            }), s.length || (s = [i.prop("name") || "files[]"])), s
        },
        _initFormSettings: function (e) {
            e.form && e.form.length || (e.form = t(e.fileInput.prop("form")), e.form.length || (e.form = t(this.options.fileInput.prop("form")))), e.paramName = this._getParamName(e), e.url || (e.url = e.form.prop("action") || location.href), e.type = (e.type || "string" === t.type(e.form.prop("method")) && e.form.prop("method") || "").toUpperCase(), "POST" !== e.type && "PUT" !== e.type && "PATCH" !== e.type && (e.type = "POST"), e.formAcceptCharset || (e.formAcceptCharset = e.form.attr("accept-charset"))
        },
        _getAJAXSettings: function (e) {
            var i = t.extend({}, this.options, e);
            return this._initFormSettings(i), this._initDataSettings(i), i
        },
        _getDeferredState: function (t) {
            return t.state ? t.state() : t.isResolved() ? "resolved" : t.isRejected() ? "rejected" : "pending"
        },
        _enhancePromise: function (t) {
            return t.success = t.done, t.error = t.fail, t.complete = t.always, t
        },
        _getXHRPromise: function (e, i, s) {
            var n = t.Deferred(), o = n.promise();
            return i = i || this.options.context || o, e === !0 ? n.resolveWith(i, s) : e === !1 && n.rejectWith(i, s), o.abort = n.promise, this._enhancePromise(o)
        },
        _addConvenienceMethods: function (e, i) {
            var s = this, n = function (e) {
                return t.Deferred().resolveWith(s, e).promise()
            };
            i.process = function (e, o) {
                return (e || o) && (i._processQueue = this._processQueue = (this._processQueue || n([this])).pipe(function () {
                    return i.errorThrown ? t.Deferred().rejectWith(s, [i]).promise() : n(arguments)
                }).pipe(e, o)), this._processQueue || n([this])
            }, i.submit = function () {
                return "pending" !== this.state() && (i.jqXHR = this.jqXHR = s._trigger("submit", t.Event("submit", {delegatedEvent: e}), this) !== !1 && s._onSend(e, this)), this.jqXHR || s._getXHRPromise()
            }, i.abort = function () {
                return this.jqXHR ? this.jqXHR.abort() : (this.errorThrown = "abort", s._trigger("fail", null, this), s._getXHRPromise(!1))
            }, i.state = function () {
                return this.jqXHR ? s._getDeferredState(this.jqXHR) : this._processQueue ? s._getDeferredState(this._processQueue) : void 0
            }, i.processing = function () {
                return !this.jqXHR && this._processQueue && "pending" === s._getDeferredState(this._processQueue)
            }, i.progress = function () {
                return this._progress
            }, i.response = function () {
                return this._response
            }
        },
        _getUploadedBytes: function (t) {
            var e = t.getResponseHeader("Range"), i = e && e.split("-"), s = i && i.length > 1 && parseInt(i[1], 10);
            return s && s + 1
        },
        _chunkedUpload: function (e, i) {
            e.uploadedBytes = e.uploadedBytes || 0;
            var s, n, o = this, a = e.files[0], r = a.size, h = e.uploadedBytes, l = e.maxChunkSize || r, c = this._blobSlice, d = t.Deferred(), p = d.promise();
            return this._isXHRUpload(e) && c && (h || r > l) && !e.data ? i ? !0 : h >= r ? (a.error = e.i18n("uploadedBytes"), this._getXHRPromise(!1, e.context, [null, "error", a.error])) : (n = function () {
                var i = t.extend({}, e), p = i._progress.loaded;
                i.blob = c.call(a, h, h + l, a.type), i.chunkSize = i.blob.size, i.contentRange = "bytes " + h + "-" + (h + i.chunkSize - 1) + "/" + r, o._initXHRData(i), o._initProgressListener(i), s = (o._trigger("chunksend", null, i) !== !1 && t.ajax(i) || o._getXHRPromise(!1, i.context)).done(function (s, a, l) {
                    h = o._getUploadedBytes(l) || h + i.chunkSize, p + i.chunkSize - i._progress.loaded && o._onProgress(t.Event("progress", {
                        lengthComputable: !0,
                        loaded: h - i.uploadedBytes,
                        total: h - i.uploadedBytes
                    }), i), e.uploadedBytes = i.uploadedBytes = h, i.result = s, i.textStatus = a, i.jqXHR = l, o._trigger("chunkdone", null, i), o._trigger("chunkalways", null, i), r > h ? n() : d.resolveWith(i.context, [s, a, l])
                }).fail(function (t, e, s) {
                    i.jqXHR = t, i.textStatus = e, i.errorThrown = s, o._trigger("chunkfail", null, i), o._trigger("chunkalways", null, i), d.rejectWith(i.context, [t, e, s])
                })
            }, this._enhancePromise(p), p.abort = function () {
                return s.abort()
            }, n(), p) : !1
        },
        _beforeSend: function (t, e) {
            0 === this._active && (this._trigger("start"), this._bitrateTimer = new this._BitrateTimer, this._progress.loaded = this._progress.total = 0, this._progress.bitrate = 0), this._initResponseObject(e), this._initProgressObject(e), e._progress.loaded = e.loaded = e.uploadedBytes || 0, e._progress.total = e.total = this._getTotal(e.files) || 1, e._progress.bitrate = e.bitrate = 0, this._active += 1, this._progress.loaded += e.loaded, this._progress.total += e.total
        },
        _onDone: function (e, i, s, n) {
            var o = n._progress.total, a = n._response;
            n._progress.loaded < o && this._onProgress(t.Event("progress", {
                lengthComputable: !0,
                loaded: o,
                total: o
            }), n), a.result = n.result = e, a.textStatus = n.textStatus = i, a.jqXHR = n.jqXHR = s, this._trigger("done", null, n)
        },
        _onFail: function (t, e, i, s) {
            var n = s._response;
            s.recalculateProgress && (this._progress.loaded -= s._progress.loaded, this._progress.total -= s._progress.total), n.jqXHR = s.jqXHR = t, n.textStatus = s.textStatus = e, n.errorThrown = s.errorThrown = i, this._trigger("fail", null, s)
        },
        _onAlways: function (t, e, i, s) {
            this._trigger("always", null, s)
        },
        _onSend: function (e, i) {
            i.submit || this._addConvenienceMethods(e, i);
            var s, n, o, a, r = this, h = r._getAJAXSettings(i), l = function () {
                return r._sending += 1, h._bitrateTimer = new r._BitrateTimer, s = s || ((n || r._trigger("send", t.Event("send", {delegatedEvent: e}), h) === !1) && r._getXHRPromise(!1, h.context, n) || r._chunkedUpload(h) || t.ajax(h)).done(function (t, e, i) {
                    r._onDone(t, e, i, h)
                }).fail(function (t, e, i) {
                    r._onFail(t, e, i, h)
                }).always(function (t, e, i) {
                    if (r._onAlways(t, e, i, h), r._sending -= 1, r._active -= 1, h.limitConcurrentUploads && h.limitConcurrentUploads > r._sending)for (var s = r._slots.shift(); s;) {
                        if ("pending" === r._getDeferredState(s)) {
                            s.resolve();
                            break
                        }
                        s = r._slots.shift()
                    }
                    0 === r._active && r._trigger("stop")
                })
            };
            return this._beforeSend(e, h), this.options.sequentialUploads || this.options.limitConcurrentUploads && this.options.limitConcurrentUploads <= this._sending ? (this.options.limitConcurrentUploads > 1 ? (o = t.Deferred(), this._slots.push(o), a = o.pipe(l)) : (this._sequence = this._sequence.pipe(l, l), a = this._sequence), a.abort = function () {
                return n = [void 0, "abort", "abort"], s ? s.abort() : (o && o.rejectWith(h.context, n), l())
            }, this._enhancePromise(a)) : l()
        },
        _onAdd: function (e, i) {
            var s, n, o, a, r = this, h = !0, l = t.extend({}, this.options, i), c = i.files, d = c.length, p = l.limitMultiFileUploads, u = l.limitMultiFileUploadSize, f = l.limitMultiFileUploadSizeOverhead, m = 0, g = this._getParamName(l), v = 0;
            if (!d)return !1;
            if (u && void 0 === c[0].size && (u = void 0), (l.singleFileUploads || p || u) && this._isXHRUpload(l))if (l.singleFileUploads || u || !p)if (!l.singleFileUploads && u)for (o = [], s = [], a = 0; d > a; a += 1)m += c[a].size + f, (a + 1 === d || m + c[a + 1].size + f > u || p && a + 1 - v >= p) && (o.push(c.slice(v, a + 1)), n = g.slice(v, a + 1), n.length || (n = g), s.push(n), v = a + 1, m = 0); else s = g; else for (o = [], s = [], a = 0; d > a; a += p)o.push(c.slice(a, a + p)), n = g.slice(a, a + p), n.length || (n = g), s.push(n); else o = [c], s = [g];
            return i.originalFiles = c, t.each(o || c, function (n, a) {
                var l = t.extend({}, i);
                return l.files = o ? a : [a], l.paramName = s[n], r._initResponseObject(l), r._initProgressObject(l), r._addConvenienceMethods(e, l), h = r._trigger("add", t.Event("add", {delegatedEvent: e}), l)
            }), h
        },
        _replaceFileInput: function (e) {
            var i = e.fileInput, s = i.clone(!0), n = i.is(document.activeElement);
            e.fileInputClone = s, t("<form></form>").append(s)[0].reset(), i.after(s).detach(), n && s.focus(), t.cleanData(i.unbind("remove")), this.options.fileInput = this.options.fileInput.map(function (t, e) {
                return e === i[0] ? s[0] : e
            }), i[0] === this.element[0] && (this.element = s)
        },
        _handleFileTreeEntry: function (e, i) {
            var s, n = this, o = t.Deferred(), a = function (t) {
                t && !t.entry && (t.entry = e), o.resolve([t])
            }, r = function (t) {
                n._handleFileTreeEntries(t, i + e.name + "/").done(function (t) {
                    o.resolve(t)
                }).fail(a)
            }, h = function () {
                s.readEntries(function (t) {
                    t.length ? (l = l.concat(t), h()) : r(l)
                }, a)
            }, l = [];
            return i = i || "", e.isFile ? e._file ? (e._file.relativePath = i, o.resolve(e._file)) : e.file(function (t) {
                t.relativePath = i, o.resolve(t)
            }, a) : e.isDirectory ? (s = e.createReader(), h()) : o.resolve([]), o.promise()
        },
        _handleFileTreeEntries: function (e, i) {
            var s = this;
            return t.when.apply(t, t.map(e, function (t) {
                return s._handleFileTreeEntry(t, i)
            })).pipe(function () {
                return Array.prototype.concat.apply([], arguments)
            })
        },
        _getDroppedFiles: function (e) {
            e = e || {};
            var i = e.items;
            return i && i.length && (i[0].webkitGetAsEntry || i[0].getAsEntry) ? this._handleFileTreeEntries(t.map(i, function (t) {
                var e;
                return t.webkitGetAsEntry ? (e = t.webkitGetAsEntry(), e && (e._file = t.getAsFile()), e) : t.getAsEntry()
            })) : t.Deferred().resolve(t.makeArray(e.files)).promise()
        },
        _getSingleFileInputFiles: function (e) {
            e = t(e);
            var i, s, n = e.prop("webkitEntries") || e.prop("entries");
            if (n && n.length)return this._handleFileTreeEntries(n);
            if (i = t.makeArray(e.prop("files")), i.length)void 0 === i[0].name && i[0].fileName && t.each(i, function (t, e) {
                e.name = e.fileName, e.size = e.fileSize
            }); else {
                if (s = e.prop("value"), !s)return t.Deferred().resolve([]).promise();
                i = [{name: s.replace(/^.*\\/, "")}]
            }
            return t.Deferred().resolve(i).promise()
        },
        _getFileInputFiles: function (e) {
            return e instanceof t && 1 !== e.length ? t.when.apply(t, t.map(e, this._getSingleFileInputFiles)).pipe(function () {
                return Array.prototype.concat.apply([], arguments)
            }) : this._getSingleFileInputFiles(e)
        },
        _onChange: function (e) {
            var i = this, s = {fileInput: t(e.target), form: t(e.target.form)};
            this._getFileInputFiles(s.fileInput).always(function (n) {
                s.files = n, i.options.replaceFileInput && i._replaceFileInput(s), i._trigger("change", t.Event("change", {delegatedEvent: e}), s) !== !1 && i._onAdd(e, s)
            })
        },
        _onPaste: function (e) {
            var i = e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.items, s = {files: []};
            i && i.length && (t.each(i, function (t, e) {
                var i = e.getAsFile && e.getAsFile();
                i && s.files.push(i)
            }), this._trigger("paste", t.Event("paste", {delegatedEvent: e}), s) !== !1 && this._onAdd(e, s))
        },
        _onDrop: function (e) {
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
            var i = this, s = e.dataTransfer, n = {};
            s && s.files && s.files.length && (e.preventDefault(), this._getDroppedFiles(s).always(function (s) {
                n.files = s, i._trigger("drop", t.Event("drop", {delegatedEvent: e}), n) !== !1 && i._onAdd(e, n)
            }))
        },
        _onDragOver: e("dragover"),
        _onDragEnter: e("dragenter"),
        _onDragLeave: e("dragleave"),
        _initEventHandlers: function () {
            this._isXHRUpload(this.options) && (this._on(this.options.dropZone, {
                dragover: this._onDragOver,
                drop: this._onDrop,
                dragenter: this._onDragEnter,
                dragleave: this._onDragLeave
            }), this._on(this.options.pasteZone, {paste: this._onPaste})), t.support.fileInput && this._on(this.options.fileInput, {change: this._onChange})
        },
        _destroyEventHandlers: function () {
            this._off(this.options.dropZone, "dragenter dragleave dragover drop"), this._off(this.options.pasteZone, "paste"), this._off(this.options.fileInput, "change")
        },
        _setOption: function (e, i) {
            var s = -1 !== t.inArray(e, this._specialOptions);
            s && this._destroyEventHandlers(), this._super(e, i), s && (this._initSpecialOptions(), this._initEventHandlers())
        },
        _initSpecialOptions: function () {
            var e = this.options;
            void 0 === e.fileInput ? e.fileInput = this.element.is('input[type="file"]') ? this.element : this.element.find('input[type="file"]') : e.fileInput instanceof t || (e.fileInput = t(e.fileInput)), e.dropZone instanceof t || (e.dropZone = t(e.dropZone)), e.pasteZone instanceof t || (e.pasteZone = t(e.pasteZone))
        },
        _getRegExp: function (t) {
            var e = t.split("/"), i = e.pop();
            return e.shift(), new RegExp(e.join("/"), i)
        },
        _isRegExpOption: function (e, i) {
            return "url" !== e && "string" === t.type(i) && /^\/.*\/[igm]{0,3}$/.test(i)
        },
        _initDataAttributes: function () {
            var e = this, i = this.options, s = this.element.data();
            t.each(this.element[0].attributes, function (t, n) {
                var o, a = n.name.toLowerCase();
                /^data-/.test(a) && (a = a.slice(5).replace(/-[a-z]/g, function (t) {
                    return t.charAt(1).toUpperCase()
                }), o = s[a], e._isRegExpOption(a, o) && (o = e._getRegExp(o)), i[a] = o)
            })
        },
        _create: function () {
            this._initDataAttributes(), this._initSpecialOptions(), this._slots = [], this._sequence = this._getXHRPromise(!0), this._sending = this._active = 0, this._initProgressObject(this), this._initEventHandlers()
        },
        active: function () {
            return this._active
        },
        progress: function () {
            return this._progress
        },
        add: function (e) {
            var i = this;
            e && !this.options.disabled && (e.fileInput && !e.files ? this._getFileInputFiles(e.fileInput).always(function (t) {
                e.files = t, i._onAdd(null, e)
            }) : (e.files = t.makeArray(e.files), this._onAdd(null, e)))
        },
        send: function (e) {
            if (e && !this.options.disabled) {
                if (e.fileInput && !e.files) {
                    var i, s, n = this, o = t.Deferred(), a = o.promise();
                    return a.abort = function () {
                        return s = !0, i ? i.abort() : (o.reject(null, "abort", "abort"), a)
                    }, this._getFileInputFiles(e.fileInput).always(function (t) {
                        if (!s) {
                            if (!t.length)return void o.reject();
                            e.files = t, i = n._onSend(null, e), i.then(function (t, e, i) {
                                o.resolve(t, e, i)
                            }, function (t, e, i) {
                                o.reject(t, e, i)
                            })
                        }
                    }), this._enhancePromise(a)
                }
                if (e.files = t.makeArray(e.files), e.files.length)return this._onSend(null, e)
            }
            return this._getXHRPromise(!1, e && e.context)
        }
    })
}), !function (t) {
    "use strict";
    var e = t.HTMLCanvasElement && t.HTMLCanvasElement.prototype, i = t.Blob && function () {
            try {
                return Boolean(new Blob)
            } catch (t) {
                return !1
            }
        }(), s = i && t.Uint8Array && function () {
            try {
                return 100 === new Blob([new Uint8Array(100)]).size
            } catch (t) {
                return !1
            }
        }(), n = t.BlobBuilder || t.WebKitBlobBuilder || t.MozBlobBuilder || t.MSBlobBuilder, o = /^data:((.*?)(;charset=.*?)?)(;base64)?,/, a = (i || n) && t.atob && t.ArrayBuffer && t.Uint8Array && function (t) {
            var e, a, r, h, l, c, d, p, u;
            if (e = t.match(o), !e)throw new Error("invalid data URI");
            for (a = e[2] ? e[1] : "text/plain" + (e[3] || ";charset=US-ASCII"), r = !!e[4], h = t.slice(e[0].length), l = r ? atob(h) : decodeURIComponent(h), c = new ArrayBuffer(l.length), d = new Uint8Array(c), p = 0; p < l.length; p += 1)d[p] = l.charCodeAt(p);
            return i ? new Blob([s ? d : c], {type: a}) : (u = new n, u.append(c), u.getBlob(a))
        };
    t.HTMLCanvasElement && !e.toBlob && (e.mozGetAsFile ? e.toBlob = function (t, i, s) {
        t(s && e.toDataURL && a ? a(this.toDataURL(i, s)) : this.mozGetAsFile("blob", i))
    } : e.toDataURL && a && (e.toBlob = function (t, e, i) {
        t(a(this.toDataURL(e, i)))
    })), "function" == typeof define && define.amd ? define(function () {
        return a
    }) : "object" == typeof module && module.exports ? module.exports = a : t.dataURLtoBlob = a
}(window), !function (t) {
    "use strict";
    var e = function (t, i, s) {
        var n, o, a = document.createElement("img");
        if (a.onerror = i, a.onload = function () {
                !o || s && s.noRevoke || e.revokeObjectURL(o), i && i(e.scale(a, s))
            }, e.isInstanceOf("Blob", t) || e.isInstanceOf("File", t))n = o = e.createObjectURL(t), a._type = t.type; else {
            if ("string" != typeof t)return !1;
            n = t, s && s.crossOrigin && (a.crossOrigin = s.crossOrigin)
        }
        return n ? (a.src = n, a) : e.readFile(t, function (t) {
            var e = t.target;
            e && e.result ? a.src = e.result : i && i(t)
        })
    }, i = window.createObjectURL && window || window.URL && URL.revokeObjectURL && URL || window.webkitURL && webkitURL;
    e.isInstanceOf = function (t, e) {
        return Object.prototype.toString.call(e) === "[object " + t + "]"
    }, e.transformCoordinates = function () {
    }, e.getTransformedOptions = function (t, e) {
        var i, s, n, o, a = e.aspectRatio;
        if (!a)return e;
        i = {};
        for (s in e)e.hasOwnProperty(s) && (i[s] = e[s]);
        return i.crop = !0, n = t.naturalWidth || t.width, o = t.naturalHeight || t.height, n / o > a ? (i.maxWidth = o * a, i.maxHeight = o) : (i.maxWidth = n, i.maxHeight = n / a), i
    }, e.renderImageToCanvas = function (t, e, i, s, n, o, a, r, h, l) {
        return t.getContext("2d").drawImage(e, i, s, n, o, a, r, h, l), t
    }, e.hasCanvasOption = function (t) {
        return t.canvas || t.crop || t.aspectRatio
    }, e.scale = function (t, i) {
        i = i || {};
        var s, n, o, a, r, h, l, c, d, p = document.createElement("canvas"), u = t.getContext || e.hasCanvasOption(i) && p.getContext, f = t.naturalWidth || t.width, m = t.naturalHeight || t.height, g = f, v = m, b = function () {
            var t = Math.max((o || g) / g, (a || v) / v);
            t > 1 && (g *= t, v *= t)
        }, _ = function () {
            var t = Math.min((s || g) / g, (n || v) / v);
            1 > t && (g *= t, v *= t)
        };
        return u && (i = e.getTransformedOptions(t, i), l = i.left || 0, c = i.top || 0, i.sourceWidth ? (r = i.sourceWidth, void 0 !== i.right && void 0 === i.left && (l = f - r - i.right)) : r = f - l - (i.right || 0), i.sourceHeight ? (h = i.sourceHeight, void 0 !== i.bottom && void 0 === i.top && (c = m - h - i.bottom)) : h = m - c - (i.bottom || 0), g = r, v = h), s = i.maxWidth, n = i.maxHeight, o = i.minWidth, a = i.minHeight, u && s && n && i.crop ? (g = s, v = n, d = r / h - s / n, 0 > d ? (h = n * r / s, void 0 === i.top && void 0 === i.bottom && (c = (m - h) / 2)) : d > 0 && (r = s * h / n, void 0 === i.left && void 0 === i.right && (l = (f - r) / 2))) : ((i.contain || i.cover) && (o = s = s || o, a = n = n || a), i.cover ? (_(), b()) : (b(), _())), u ? (p.width = g, p.height = v, e.transformCoordinates(p, i), e.renderImageToCanvas(p, t, l, c, r, h, 0, 0, g, v)) : (t.width = g, t.height = v, t)
    }, e.createObjectURL = function (t) {
        return i ? i.createObjectURL(t) : !1
    }, e.revokeObjectURL = function (t) {
        return i ? i.revokeObjectURL(t) : !1
    }, e.readFile = function (t, e, i) {
        if (window.FileReader) {
            var s = new FileReader;
            if (s.onload = s.onerror = e, i = i || "readAsDataURL", s[i])return s[i](t), s
        }
        return !1
    }, "function" == typeof define && define.amd ? define(function () {
        return e
    }) : "object" == typeof module && module.exports ? module.exports = e : t.loadImage = e
}(window), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["load-image"], t) : t("object" == typeof module && module.exports ? require("./load-image") : window.loadImage)
}(function (t) {
    "use strict";
    if (window.navigator && window.navigator.platform && /iP(hone|od|ad)/.test(window.navigator.platform)) {
        var e = t.renderImageToCanvas;
        t.detectSubsampling = function (t) {
            var e, i;
            return t.width * t.height > 1048576 ? (e = document.createElement("canvas"), e.width = e.height = 1, i = e.getContext("2d"), i.drawImage(t, -t.width + 1, 0), 0 === i.getImageData(0, 0, 1, 1).data[3]) : !1
        }, t.detectVerticalSquash = function (t, e) {
            var i, s, n, o, a, r = t.naturalHeight || t.height, h = document.createElement("canvas"), l = h.getContext("2d");
            for (e && (r /= 2), h.width = 1, h.height = r, l.drawImage(t, 0, 0), i = l.getImageData(0, 0, 1, r).data, s = 0, n = r, o = r; o > s;)a = i[4 * (o - 1) + 3], 0 === a ? n = o : s = o, o = n + s >> 1;
            return o / r || 1
        }, t.renderImageToCanvas = function (i, s, n, o, a, r, h, l, c, d) {
            if ("image/jpeg" === s._type) {
                var p, u, f, m, g = i.getContext("2d"), v = document.createElement("canvas"), b = 1024, _ = v.getContext("2d");
                if (v.width = b, v.height = b, g.save(), p = t.detectSubsampling(s), p && (n /= 2, o /= 2, a /= 2, r /= 2), u = t.detectVerticalSquash(s, p), p || 1 !== u) {
                    for (o *= u, c = Math.ceil(b * c / a), d = Math.ceil(b * d / r / u), l = 0, m = 0; r > m;) {
                        for (h = 0, f = 0; a > f;)_.clearRect(0, 0, b, b), _.drawImage(s, n, o, a, r, -f, -m, a, r), g.drawImage(v, 0, 0, b, b, h, l, c, d), f += b, h += c;
                        m += b, l += d
                    }
                    return g.restore(), i
                }
            }
            return e(i, s, n, o, a, r, h, l, c, d)
        }
    }
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["load-image"], t) : t("object" == typeof module && module.exports ? require("./load-image") : window.loadImage)
}(function (t) {
    "use strict";
    var e = t.hasCanvasOption, i = t.transformCoordinates, s = t.getTransformedOptions;
    t.hasCanvasOption = function (i) {
        return e.call(t, i) || i.orientation
    }, t.transformCoordinates = function (e, s) {
        i.call(t, e, s);
        var n = e.getContext("2d"), o = e.width, a = e.height, r = s.orientation;
        if (r && !(r > 8))switch (r > 4 && (e.width = a, e.height = o), r) {
            case 2:
                n.translate(o, 0), n.scale(-1, 1);
                break;
            case 3:
                n.translate(o, a), n.rotate(Math.PI);
                break;
            case 4:
                n.translate(0, a), n.scale(1, -1);
                break;
            case 5:
                n.rotate(.5 * Math.PI), n.scale(1, -1);
                break;
            case 6:
                n.rotate(.5 * Math.PI), n.translate(0, -a);
                break;
            case 7:
                n.rotate(.5 * Math.PI), n.translate(o, -a), n.scale(-1, 1);
                break;
            case 8:
                n.rotate(-.5 * Math.PI), n.translate(-o, 0)
        }
    }, t.getTransformedOptions = function (e, i) {
        var n, o, a = s.call(t, e, i), r = a.orientation;
        if (!r || r > 8 || 1 === r)return a;
        n = {};
        for (o in a)a.hasOwnProperty(o) && (n[o] = a[o]);
        switch (a.orientation) {
            case 2:
                n.left = a.right, n.right = a.left;
                break;
            case 3:
                n.left = a.right, n.top = a.bottom, n.right = a.left, n.bottom = a.top;
                break;
            case 4:
                n.top = a.bottom, n.bottom = a.top;
                break;
            case 5:
                n.left = a.top, n.top = a.left, n.right = a.bottom, n.bottom = a.right;
                break;
            case 6:
                n.left = a.top, n.top = a.right, n.right = a.bottom, n.bottom = a.left;
                break;
            case 7:
                n.left = a.bottom, n.top = a.right, n.right = a.top, n.bottom = a.left;
                break;
            case 8:
                n.left = a.bottom, n.top = a.left, n.right = a.top, n.bottom = a.right
        }
        return a.orientation > 4 && (n.maxWidth = a.maxHeight, n.maxHeight = a.maxWidth, n.minWidth = a.minHeight, n.minHeight = a.minWidth, n.sourceWidth = a.sourceHeight, n.sourceHeight = a.sourceWidth), n
    }
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["load-image"], t) : t("object" == typeof module && module.exports ? require("./load-image") : window.loadImage)
}(function (t) {
    "use strict";
    var e = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice);
    t.blobSlice = e && function () {
        var t = this.slice || this.webkitSlice || this.mozSlice;
        return t.apply(this, arguments)
    }, t.metaDataParsers = {jpeg: {65505: []}}, t.parseMetaData = function (e, i, s) {
        s = s || {};
        var n = this, o = s.maxMetaDataSize || 262144, a = {}, r = !(window.DataView && e && e.size >= 12 && "image/jpeg" === e.type && t.blobSlice);
        (r || !t.readFile(t.blobSlice.call(e, 0, o), function (e) {
            if (e.target.error)return console.log(e.target.error), void i(a);
            var o, r, h, l, c = e.target.result, d = new DataView(c), p = 2, u = d.byteLength - 4, f = p;
            if (65496 === d.getUint16(0)) {
                for (; u > p && (o = d.getUint16(p), o >= 65504 && 65519 >= o || 65534 === o);) {
                    if (r = d.getUint16(p + 2) + 2, p + r > d.byteLength) {
                        console.log("Invalid meta data: Invalid segment size.");
                        break
                    }
                    if (h = t.metaDataParsers.jpeg[o])for (l = 0; l < h.length; l += 1)h[l].call(n, d, p, r, a, s);
                    p += r, f = p
                }
                !s.disableImageHead && f > 6 && (c.slice ? a.imageHead = c.slice(0, f) : a.imageHead = new Uint8Array(c).subarray(0, f))
            } else console.log("Invalid JPEG file: Missing JPEG marker.");
            i(a)
        }, "readAsArrayBuffer")) && i(a)
    }
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["load-image", "load-image-meta"], t) : "object" == typeof module && module.exports ? t(require("./load-image"), require("./load-image-meta")) : t(window.loadImage)
}(function (t) {
    "use strict";
    t.ExifMap = function () {
        return this
    }, t.ExifMap.prototype.map = {Orientation: 274}, t.ExifMap.prototype.get = function (t) {
        return this[t] || this[this.map[t]]
    }, t.getExifThumbnail = function (t, e, i) {
        var s, n, o;
        if (!i || e + i > t.byteLength)return void console.log("Invalid Exif data: Invalid thumbnail data.");
        for (s = [], n = 0; i > n; n += 1)o = t.getUint8(e + n), s.push((16 > o ? "0" : "") + o.toString(16));
        return "data:image/jpeg,%" + s.join("%")
    }, t.exifTagTypes = {
        1: {
            getValue: function (t, e) {
                return t.getUint8(e)
            }, size: 1
        }, 2: {
            getValue: function (t, e) {
                return String.fromCharCode(t.getUint8(e))
            }, size: 1, ascii: !0
        }, 3: {
            getValue: function (t, e, i) {
                return t.getUint16(e, i)
            }, size: 2
        }, 4: {
            getValue: function (t, e, i) {
                return t.getUint32(e, i)
            }, size: 4
        }, 5: {
            getValue: function (t, e, i) {
                return t.getUint32(e, i) / t.getUint32(e + 4, i)
            }, size: 8
        }, 9: {
            getValue: function (t, e, i) {
                return t.getInt32(e, i)
            }, size: 4
        }, 10: {
            getValue: function (t, e, i) {
                return t.getInt32(e, i) / t.getInt32(e + 4, i)
            }, size: 8
        }
    }, t.exifTagTypes[7] = t.exifTagTypes[1], t.getExifValue = function (e, i, s, n, o, a) {
        var r, h, l, c, d, p, u = t.exifTagTypes[n];
        if (!u)return void console.log("Invalid Exif data: Invalid tag type.");
        if (r = u.size * o, h = r > 4 ? i + e.getUint32(s + 8, a) : s + 8, h + r > e.byteLength)return void console.log("Invalid Exif data: Invalid data offset.");
        if (1 === o)return u.getValue(e, h, a);
        for (l = [], c = 0; o > c; c += 1)l[c] = u.getValue(e, h + c * u.size, a);
        if (u.ascii) {
            for (d = "", c = 0; c < l.length && (p = l[c], "\x00" !== p); c += 1)d += p;
            return d
        }
        return l
    }, t.parseExifTag = function (e, i, s, n, o) {
        var a = e.getUint16(s, n);
        o.exif[a] = t.getExifValue(e, i, s, e.getUint16(s + 2, n), e.getUint32(s + 4, n), n)
    }, t.parseExifTags = function (t, e, i, s, n) {
        var o, a, r;
        if (i + 6 > t.byteLength)return void console.log("Invalid Exif data: Invalid directory offset.");
        if (o = t.getUint16(i, s), a = i + 2 + 12 * o, a + 4 > t.byteLength)return void console.log("Invalid Exif data: Invalid directory size.");
        for (r = 0; o > r; r += 1)this.parseExifTag(t, e, i + 2 + 12 * r, s, n);
        return t.getUint32(a, s)
    }, t.parseExifData = function (e, i, s, n, o) {
        if (!o.disableExif) {
            var a, r, h, l = i + 10;
            if (1165519206 === e.getUint32(i + 4)) {
                if (l + 8 > e.byteLength)return void console.log("Invalid Exif data: Invalid segment size.");
                if (0 !== e.getUint16(i + 8))return void console.log("Invalid Exif data: Missing byte alignment offset.");
                switch (e.getUint16(l)) {
                    case 18761:
                        a = !0;
                        break;
                    case 19789:
                        a = !1;
                        break;
                    default:
                        return void console.log("Invalid Exif data: Invalid byte alignment marker.")
                }
                if (42 !== e.getUint16(l + 2, a))return void console.log("Invalid Exif data: Missing TIFF marker.");
                r = e.getUint32(l + 4, a), n.exif = new t.ExifMap, r = t.parseExifTags(e, l, l + r, a, n), r && !o.disableExifThumbnail && (h = {exif: {}}, r = t.parseExifTags(e, l, l + r, a, h), h.exif[513] && (n.exif.Thumbnail = t.getExifThumbnail(e, l + h.exif[513], h.exif[514]))), n.exif[34665] && !o.disableExifSub && t.parseExifTags(e, l, l + n.exif[34665], a, n), n.exif[34853] && !o.disableExifGps && t.parseExifTags(e, l, l + n.exif[34853], a, n)
            }
        }
    }, t.metaDataParsers.jpeg[65505].push(t.parseExifData)
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["load-image", "load-image-exif"], t) : "object" == typeof module && module.exports ? t(require("./load-image"), require("./load-image-exif")) : t(window.loadImage)
}(function (t) {
    "use strict";
    t.ExifMap.prototype.tags = {
        256: "ImageWidth",
        257: "ImageHeight",
        34665: "ExifIFDPointer",
        34853: "GPSInfoIFDPointer",
        40965: "InteroperabilityIFDPointer",
        258: "BitsPerSample",
        259: "Compression",
        262: "PhotometricInterpretation",
        274: "Orientation",
        277: "SamplesPerPixel",
        284: "PlanarConfiguration",
        530: "YCbCrSubSampling",
        531: "YCbCrPositioning",
        282: "XResolution",
        283: "YResolution",
        296: "ResolutionUnit",
        273: "StripOffsets",
        278: "RowsPerStrip",
        279: "StripByteCounts",
        513: "JPEGInterchangeFormat",
        514: "JPEGInterchangeFormatLength",
        301: "TransferFunction",
        318: "WhitePoint",
        319: "PrimaryChromaticities",
        529: "YCbCrCoefficients",
        532: "ReferenceBlackWhite",
        306: "DateTime",
        270: "ImageDescription",
        271: "Make",
        272: "Model",
        305: "Software",
        315: "Artist",
        33432: "Copyright",
        36864: "ExifVersion",
        40960: "FlashpixVersion",
        40961: "ColorSpace",
        40962: "PixelXDimension",
        40963: "PixelYDimension",
        42240: "Gamma",
        37121: "ComponentsConfiguration",
        37122: "CompressedBitsPerPixel",
        37500: "MakerNote",
        37510: "UserComment",
        40964: "RelatedSoundFile",
        36867: "DateTimeOriginal",
        36868: "DateTimeDigitized",
        37520: "SubSecTime",
        37521: "SubSecTimeOriginal",
        37522: "SubSecTimeDigitized",
        33434: "ExposureTime",
        33437: "FNumber",
        34850: "ExposureProgram",
        34852: "SpectralSensitivity",
        34855: "PhotographicSensitivity",
        34856: "OECF",
        34864: "SensitivityType",
        34865: "StandardOutputSensitivity",
        34866: "RecommendedExposureIndex",
        34867: "ISOSpeed",
        34868: "ISOSpeedLatitudeyyy",
        34869: "ISOSpeedLatitudezzz",
        37377: "ShutterSpeedValue",
        37378: "ApertureValue",
        37379: "BrightnessValue",
        37380: "ExposureBias",
        37381: "MaxApertureValue",
        37382: "SubjectDistance",
        37383: "MeteringMode",
        37384: "LightSource",
        37385: "Flash",
        37396: "SubjectArea",
        37386: "FocalLength",
        41483: "FlashEnergy",
        41484: "SpatialFrequencyResponse",
        41486: "FocalPlaneXResolution",
        41487: "FocalPlaneYResolution",
        41488: "FocalPlaneResolutionUnit",
        41492: "SubjectLocation",
        41493: "ExposureIndex",
        41495: "SensingMethod",
        41728: "FileSource",
        41729: "SceneType",
        41730: "CFAPattern",
        41985: "CustomRendered",
        41986: "ExposureMode",
        41987: "WhiteBalance",
        41988: "DigitalZoomRatio",
        41989: "FocalLengthIn35mmFilm",
        41990: "SceneCaptureType",
        41991: "GainControl",
        41992: "Contrast",
        41993: "Saturation",
        41994: "Sharpness",
        41995: "DeviceSettingDescription",
        41996: "SubjectDistanceRange",
        42016: "ImageUniqueID",
        42032: "CameraOwnerName",
        42033: "BodySerialNumber",
        42034: "LensSpecification",
        42035: "LensMake",
        42036: "LensModel",
        42037: "LensSerialNumber",
        0: "GPSVersionID",
        1: "GPSLatitudeRef",
        2: "GPSLatitude",
        3: "GPSLongitudeRef",
        4: "GPSLongitude",
        5: "GPSAltitudeRef",
        6: "GPSAltitude",
        7: "GPSTimeStamp",
        8: "GPSSatellites",
        9: "GPSStatus",
        10: "GPSMeasureMode",
        11: "GPSDOP",
        12: "GPSSpeedRef",
        13: "GPSSpeed",
        14: "GPSTrackRef",
        15: "GPSTrack",
        16: "GPSImgDirectionRef",
        17: "GPSImgDirection",
        18: "GPSMapDatum",
        19: "GPSDestLatitudeRef",
        20: "GPSDestLatitude",
        21: "GPSDestLongitudeRef",
        22: "GPSDestLongitude",
        23: "GPSDestBearingRef",
        24: "GPSDestBearing",
        25: "GPSDestDistanceRef",
        26: "GPSDestDistance",
        27: "GPSProcessingMethod",
        28: "GPSAreaInformation",
        29: "GPSDateStamp",
        30: "GPSDifferential",
        31: "GPSHPositioningError"
    }, t.ExifMap.prototype.stringValues = {
        ExposureProgram: {
            0: "Undefined",
            1: "Manual",
            2: "Normal program",
            3: "Aperture priority",
            4: "Shutter priority",
            5: "Creative program",
            6: "Action program",
            7: "Portrait mode",
            8: "Landscape mode"
        },
        MeteringMode: {
            0: "Unknown",
            1: "Average",
            2: "CenterWeightedAverage",
            3: "Spot",
            4: "MultiSpot",
            5: "Pattern",
            6: "Partial",
            255: "Other"
        },
        LightSource: {
            0: "Unknown",
            1: "Daylight",
            2: "Fluorescent",
            3: "Tungsten (incandescent light)",
            4: "Flash",
            9: "Fine weather",
            10: "Cloudy weather",
            11: "Shade",
            12: "Daylight fluorescent (D 5700 - 7100K)",
            13: "Day white fluorescent (N 4600 - 5400K)",
            14: "Cool white fluorescent (W 3900 - 4500K)",
            15: "White fluorescent (WW 3200 - 3700K)",
            17: "Standard light A",
            18: "Standard light B",
            19: "Standard light C",
            20: "D55",
            21: "D65",
            22: "D75",
            23: "D50",
            24: "ISO studio tungsten",
            255: "Other"
        },
        Flash: {
            0: "Flash did not fire",
            1: "Flash fired",
            5: "Strobe return light not detected",
            7: "Strobe return light detected",
            9: "Flash fired, compulsory flash mode",
            13: "Flash fired, compulsory flash mode, return light not detected",
            15: "Flash fired, compulsory flash mode, return light detected",
            16: "Flash did not fire, compulsory flash mode",
            24: "Flash did not fire, auto mode",
            25: "Flash fired, auto mode",
            29: "Flash fired, auto mode, return light not detected",
            31: "Flash fired, auto mode, return light detected",
            32: "No flash function",
            65: "Flash fired, red-eye reduction mode",
            69: "Flash fired, red-eye reduction mode, return light not detected",
            71: "Flash fired, red-eye reduction mode, return light detected",
            73: "Flash fired, compulsory flash mode, red-eye reduction mode",
            77: "Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected",
            79: "Flash fired, compulsory flash mode, red-eye reduction mode, return light detected",
            89: "Flash fired, auto mode, red-eye reduction mode",
            93: "Flash fired, auto mode, return light not detected, red-eye reduction mode",
            95: "Flash fired, auto mode, return light detected, red-eye reduction mode"
        },
        SensingMethod: {
            1: "Undefined",
            2: "One-chip color area sensor",
            3: "Two-chip color area sensor",
            4: "Three-chip color area sensor",
            5: "Color sequential area sensor",
            7: "Trilinear sensor",
            8: "Color sequential linear sensor"
        },
        SceneCaptureType: {0: "Standard", 1: "Landscape", 2: "Portrait", 3: "Night scene"},
        SceneType: {1: "Directly photographed"},
        CustomRendered: {0: "Normal process", 1: "Custom process"},
        WhiteBalance: {0: "Auto white balance", 1: "Manual white balance"},
        GainControl: {0: "None", 1: "Low gain up", 2: "High gain up", 3: "Low gain down", 4: "High gain down"},
        Contrast: {0: "Normal", 1: "Soft", 2: "Hard"},
        Saturation: {0: "Normal", 1: "Low saturation", 2: "High saturation"},
        Sharpness: {0: "Normal", 1: "Soft", 2: "Hard"},
        SubjectDistanceRange: {0: "Unknown", 1: "Macro", 2: "Close view", 3: "Distant view"},
        FileSource: {3: "DSC"},
        ComponentsConfiguration: {0: "", 1: "Y", 2: "Cb", 3: "Cr", 4: "R", 5: "G", 6: "B"},
        Orientation: {
            1: "top-left",
            2: "top-right",
            3: "bottom-right",
            4: "bottom-left",
            5: "left-top",
            6: "right-top",
            7: "right-bottom",
            8: "left-bottom"
        }
    }, t.ExifMap.prototype.getText = function (t) {
        var e = this.get(t);
        switch (t) {
            case"LightSource":
            case"Flash":
            case"MeteringMode":
            case"ExposureProgram":
            case"SensingMethod":
            case"SceneCaptureType":
            case"SceneType":
            case"CustomRendered":
            case"WhiteBalance":
            case"GainControl":
            case"Contrast":
            case"Saturation":
            case"Sharpness":
            case"SubjectDistanceRange":
            case"FileSource":
            case"Orientation":
                return this.stringValues[t][e];
            case"ExifVersion":
            case"FlashpixVersion":
                return String.fromCharCode(e[0], e[1], e[2], e[3]);
            case"ComponentsConfiguration":
                return this.stringValues[t][e[0]] + this.stringValues[t][e[1]] + this.stringValues[t][e[2]] + this.stringValues[t][e[3]];
            case"GPSVersionID":
                return e[0] + "." + e[1] + "." + e[2] + "." + e[3]
        }
        return String(e)
    }, function (t) {
        var e, i = t.tags, s = t.map;
        for (e in i)i.hasOwnProperty(e) && (s[i[e]] = e)
    }(t.ExifMap.prototype), t.ExifMap.prototype.getAll = function () {
        var t, e, i = {};
        for (t in this)this.hasOwnProperty(t) && (e = this.tags[t], e && (i[e] = this.getText(e)));
        return i
    }
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : window.jQuery)
}(function (t) {
    "use strict";
    var e = 0;
    t.ajaxTransport("iframe", function (i) {
        if (i.async) {
            var s, n, o, a = i.initialIframeSrc || "javascript:false;";
            return {
                send: function (r, h) {
                    s = t('<form style="display:none;"></form>'), s.attr("accept-charset", i.formAcceptCharset), o = /\?/.test(i.url) ? "&" : "?", "DELETE" === i.type ? (i.url = i.url + o + "_method=DELETE", i.type = "POST") : "PUT" === i.type ? (i.url = i.url + o + "_method=PUT", i.type = "POST") : "PATCH" === i.type && (i.url = i.url + o + "_method=PATCH", i.type = "POST"), e += 1, n = t('<iframe src="' + a + '" name="iframe-transport-' + e + '"></iframe>').bind("load", function () {
                        var e, o = t.isArray(i.paramName) ? i.paramName : [i.paramName];
                        n.unbind("load").bind("load", function () {
                            var e;
                            try {
                                if (e = n.contents(), !e.length || !e[0].firstChild)throw new Error
                            } catch (i) {
                                e = void 0
                            }
                            h(200, "success", {iframe: e}), t('<iframe src="' + a + '"></iframe>').appendTo(s), window.setTimeout(function () {
                                s.remove()
                            }, 0)
                        }), s.prop("target", n.prop("name")).prop("action", i.url).prop("method", i.type), i.formData && t.each(i.formData, function (e, i) {
                            t('<input type="hidden"/>').prop("name", i.name).val(i.value).appendTo(s)
                        }), i.fileInput && i.fileInput.length && "POST" === i.type && (e = i.fileInput.clone(), i.fileInput.after(function (t) {
                            return e[t]
                        }), i.paramName && i.fileInput.each(function (e) {
                            t(this).prop("name", o[e] || i.paramName)
                        }), s.append(i.fileInput).prop("enctype", "multipart/form-data").prop("encoding", "multipart/form-data"), i.fileInput.removeAttr("form")), s.submit(), e && e.length && i.fileInput.each(function (i, s) {
                            var n = t(e[i]);
                            t(s).prop("name", n.prop("name")).attr("form", n.attr("form")), n.replaceWith(s)
                        })
                    }), s.append(n).appendTo(document.body)
                }, abort: function () {
                    n && n.unbind("load").prop("src", a), s && s.remove()
                }
            }
        }
    }), t.ajaxSetup({
        converters: {
            "iframe text": function (e) {
                return e && t(e[0].body).text()
            }, "iframe json": function (e) {
                return e && t.parseJSON(t(e[0].body).text())
            }, "iframe html": function (e) {
                return e && t(e[0].body).html()
            }, "iframe xml": function (e) {
                var i = e && e[0];
                return i && t.isXMLDoc(i) ? i : t.parseXML(i.XMLDocument && i.XMLDocument.xml || t(i.body).html())
            }, "iframe script": function (e) {
                return e && t.globalEval(t(e[0].body).text())
            }
        }
    })
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery", "./jquery.fileupload"], t) : t("object" == typeof exports ? require("jquery") : window.jQuery)
}(function (t) {
    "use strict";
    var e = t.blueimp.fileupload.prototype.options.add;
    t.widget("blueimp.fileupload", t.blueimp.fileupload, {
        options: {
            processQueue: [], add: function (i, s) {
                var n = t(this);
                s.process(function () {
                    return n.fileupload("process", s)
                }), e.call(this, i, s)
            }
        }, processActions: {}, _processFile: function (e, i) {
            var s = this, n = t.Deferred().resolveWith(s, [e]), o = n.promise();
            return this._trigger("process", null, e), t.each(e.processQueue, function (e, n) {
                var a = function (e) {
                    return i.errorThrown ? t.Deferred().rejectWith(s, [i]).promise() : s.processActions[n.action].call(s, e, n)
                };
                o = o.pipe(a, n.always && a)
            }), o.done(function () {
                s._trigger("processdone", null, e), s._trigger("processalways", null, e)
            }).fail(function () {
                s._trigger("processfail", null, e), s._trigger("processalways", null, e)
            }), o
        }, _transformProcessQueue: function (e) {
            var i = [];
            t.each(e.processQueue, function () {
                var s = {}, n = this.action, o = this.prefix === !0 ? n : this.prefix;
                t.each(this, function (i, n) {
                    "string" === t.type(n) && "@" === n.charAt(0) ? s[i] = e[n.slice(1) || (o ? o + i.charAt(0).toUpperCase() + i.slice(1) : i)] : s[i] = n
                }), i.push(s)
            }), e.processQueue = i
        }, processing: function () {
            return this._processing
        }, process: function (e) {
            var i = this, s = t.extend({}, this.options, e);
            return s.processQueue && s.processQueue.length && (this._transformProcessQueue(s), 0 === this._processing && this._trigger("processstart"), t.each(e.files, function (n) {
                var o = n ? t.extend({}, s) : s, a = function () {
                    return e.errorThrown ? t.Deferred().rejectWith(i, [e]).promise() : i._processFile(o, e)
                };
                o.index = n, i._processing += 1, i._processingQueue = i._processingQueue.pipe(a, a).always(function () {
                    i._processing -= 1, 0 === i._processing && i._trigger("processstop")
                })
            })), this._processingQueue
        }, _create: function () {
            this._super(), this._processing = 0, this._processingQueue = t.Deferred().resolveWith(this).promise()
        }
    })
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery", "./jquery.fileupload-process"], t) : t("object" == typeof exports ? require("jquery") : window.jQuery)
}(function (t) {
    "use strict";
    t.blueimp.fileupload.prototype.options.processQueue.push({
        action: "validate",
        always: !0,
        acceptFileTypes: "@",
        maxFileSize: "@",
        minFileSize: "@",
        maxNumberOfFiles: "@",
        disabled: "@disableValidation"
    }), t.widget("blueimp.fileupload", t.blueimp.fileupload, {
        options: {
            getNumberOfFiles: t.noop,
            messages: {
                maxNumberOfFiles: "Maximum number of files exceeded",
                acceptFileTypes: "File type not allowed",
                maxFileSize: "File is too large",
                minFileSize: "File is too small"
            }
        }, processActions: {
            validate: function (e, i) {
                if (i.disabled)return e;
                var s, n = t.Deferred(), o = this.options, a = e.files[e.index];
                return (i.minFileSize || i.maxFileSize) && (s = a.size), "number" === t.type(i.maxNumberOfFiles) && (o.getNumberOfFiles() || 0) + e.files.length > i.maxNumberOfFiles ? a.error = o.i18n("maxNumberOfFiles") : !i.acceptFileTypes || i.acceptFileTypes.test(a.type) || i.acceptFileTypes.test(a.name) ? s > i.maxFileSize ? a.error = o.i18n("maxFileSize") : "number" === t.type(s) && s < i.minFileSize ? a.error = o.i18n("minFileSize") : delete a.error : a.error = o.i18n("acceptFileTypes"), a.error || e.files.error ? (e.files.error = !0, n.rejectWith(this, [e])) : n.resolveWith(this, [e]), n.promise()
            }
        }
    })
}), function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery", "load-image", "load-image-meta", "load-image-exif", "load-image-ios", "canvas-to-blob", "./jquery.fileupload-process"], t) : "object" == typeof exports ? t(require("jquery"), require("load-image")) : t(window.jQuery, window.loadImage)
}(function (t, e) {
    "use strict";
    t.blueimp.fileupload.prototype.options.processQueue.unshift({
        action: "loadImageMetaData",
        disableImageHead: "@",
        disableExif: "@",
        disableExifThumbnail: "@",
        disableExifSub: "@",
        disableExifGps: "@",
        disabled: "@disableImageMetaDataLoad"
    }, {
        action: "loadImage",
        prefix: !0,
        fileTypes: "@",
        maxFileSize: "@",
        noRevoke: "@",
        disabled: "@disableImageLoad"
    }, {
        action: "resizeImage",
        prefix: "image",
        maxWidth: "@",
        maxHeight: "@",
        minWidth: "@",
        minHeight: "@",
        crop: "@",
        orientation: "@",
        forceResize: "@",
        disabled: "@disableImageResize"
    }, {
        action: "saveImage",
        quality: "@imageQuality",
        type: "@imageType",
        disabled: "@disableImageResize"
    }, {action: "saveImageMetaData", disabled: "@disableImageMetaDataSave"}, {
        action: "resizeImage",
        prefix: "preview",
        maxWidth: "@",
        maxHeight: "@",
        minWidth: "@",
        minHeight: "@",
        crop: "@",
        orientation: "@",
        thumbnail: "@",
        canvas: "@",
        disabled: "@disableImagePreview"
    }, {
        action: "setImage",
        name: "@imagePreviewName",
        disabled: "@disableImagePreview"
    }, {
        action: "deleteImageReferences",
        disabled: "@disableImageReferencesDeletion"
    }), t.widget("blueimp.fileupload", t.blueimp.fileupload, {
        options: {
            loadImageFileTypes: /^image\/(gif|jpeg|png|svg\+xml)$/,
            loadImageMaxFileSize: 1e7,
            imageMaxWidth: 1920,
            imageMaxHeight: 1080,
            imageOrientation: !1,
            imageCrop: !1,
            disableImageResize: !0,
            previewMaxWidth: 80,
            previewMaxHeight: 80,
            previewOrientation: !0,
            previewThumbnail: !0,
            previewCrop: !1,
            previewCanvas: !0
        }, processActions: {
            loadImage: function (i, s) {
                if (s.disabled)return i;
                var n = this, o = i.files[i.index], a = t.Deferred();
                return "number" === t.type(s.maxFileSize) && o.size > s.maxFileSize || s.fileTypes && !s.fileTypes.test(o.type) || !e(o, function (t) {
                    t.src && (i.img = t), a.resolveWith(n, [i])
                }, s) ? i : a.promise()
            }, resizeImage: function (i, s) {
                if (s.disabled || !i.canvas && !i.img)return i;
                s = t.extend({canvas: !0}, s);
                var n, o = this, a = t.Deferred(), r = s.canvas && i.canvas || i.img, h = function (t) {
                    t && (t.width !== r.width || t.height !== r.height || s.forceResize) && (i[t.getContext ? "canvas" : "img"] = t), i.preview = t, a.resolveWith(o, [i])
                };
                if (i.exif) {
                    if (s.orientation === !0 && (s.orientation = i.exif.get("Orientation")), s.thumbnail && (n = i.exif.get("Thumbnail")))return e(n, h, s), a.promise();
                    i.orientation ? delete s.orientation : i.orientation = s.orientation
                }
                return r ? (h(e.scale(r, s)), a.promise()) : i
            }, saveImage: function (e, i) {
                if (!e.canvas || i.disabled)return e;
                var s = this, n = e.files[e.index], o = t.Deferred();
                return e.canvas.toBlob ? (e.canvas.toBlob(function (t) {
                    t.name || (n.type === t.type ? t.name = n.name : n.name && (t.name = n.name.replace(/\.\w+$/, "." + t.type.substr(6)))), n.type !== t.type && delete e.imageHead, e.files[e.index] = t, o.resolveWith(s, [e])
                }, i.type || n.type, i.quality), o.promise()) : e
            }, loadImageMetaData: function (i, s) {
                if (s.disabled)return i;
                var n = this, o = t.Deferred();
                return e.parseMetaData(i.files[i.index], function (e) {
                    t.extend(i, e), o.resolveWith(n, [i])
                }, s), o.promise()
            }, saveImageMetaData: function (t, e) {
                if (!(t.imageHead && t.canvas && t.canvas.toBlob) || e.disabled)return t;
                var i = t.files[t.index], s = new Blob([t.imageHead, this._blobSlice.call(i, 20)], {type: i.type});
                return s.name = i.name, t.files[t.index] = s, t
            }, setImage: function (t, e) {
                return t.preview && !e.disabled && (t.files[t.index][e.name || "preview"] = t.preview), t
            }, deleteImageReferences: function (t, e) {
                return e.disabled || (delete t.img, delete t.canvas, delete t.preview, delete t.imageHead), t
            }
        }
    })
}), !function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : jQuery)
}(function (t) {
    var e = function () {
        if (t && t.fn && t.fn.select2 && t.fn.select2.amd)var e = t.fn.select2.amd;
        var e;
        return function () {
            if (!e || !e.requirejs) {
                e ? i = e : e = {};
                var t, i, s;
                !function (e) {
                    function n(t, e) {
                        return y.call(t, e)
                    }

                    function o(t, e) {
                        var i, s, n, o, a, r, h, l, c, d, p, u = e && e.split("/"), f = b.map, m = f && f["*"] || {};
                        if (t && "." === t.charAt(0))if (e) {
                            for (t = t.split("/"), a = t.length - 1, b.nodeIdCompat && x.test(t[a]) && (t[a] = t[a].replace(x, "")), t = u.slice(0, u.length - 1).concat(t), c = 0; c < t.length; c += 1)if (p = t[c], "." === p)t.splice(c, 1), c -= 1; else if (".." === p) {
                                if (1 === c && (".." === t[2] || ".." === t[0]))break;
                                c > 0 && (t.splice(c - 1, 2), c -= 2)
                            }
                            t = t.join("/")
                        } else 0 === t.indexOf("./") && (t = t.substring(2));
                        if ((u || m) && f) {
                            for (i = t.split("/"), c = i.length; c > 0; c -= 1) {
                                if (s = i.slice(0, c).join("/"), u)for (d = u.length; d > 0; d -= 1)if (n = f[u.slice(0, d).join("/")], n && (n = n[s])) {
                                    o = n, r = c;
                                    break
                                }
                                if (o)break;
                                !h && m && m[s] && (h = m[s], l = c)
                            }
                            !o && h && (o = h, r = l), o && (i.splice(0, r, o), t = i.join("/"))
                        }
                        return t
                    }

                    function a(t, i) {
                        return function () {
                            var s = w.call(arguments, 0);
                            return "string" != typeof s[0] && 1 === s.length && s.push(null), u.apply(e, s.concat([t, i]))
                        }
                    }

                    function r(t) {
                        return function (e) {
                            return o(e, t)
                        }
                    }

                    function h(t) {
                        return function (e) {
                            g[t] = e
                        }
                    }

                    function l(t) {
                        if (n(v, t)) {
                            var i = v[t];
                            delete v[t], _[t] = !0, p.apply(e, i)
                        }
                        if (!n(g, t) && !n(_, t))throw new Error("No " + t);
                        return g[t]
                    }

                    function c(t) {
                        var e, i = t ? t.indexOf("!") : -1;
                        return i > -1 && (e = t.substring(0, i), t = t.substring(i + 1, t.length)), [e, t]
                    }

                    function d(t) {
                        return function () {
                            return b && b.config && b.config[t] || {}
                        }
                    }

                    var p, u, f, m, g = {}, v = {}, b = {}, _ = {}, y = Object.prototype.hasOwnProperty, w = [].slice, x = /\.js$/;
                    f = function (t, e) {
                        var i, s = c(t), n = s[0];
                        return t = s[1], n && (n = o(n, e), i = l(n)), n ? t = i && i.normalize ? i.normalize(t, r(e)) : o(t, e) : (t = o(t, e), s = c(t), n = s[0], t = s[1], n && (i = l(n))), {
                            f: n ? n + "!" + t : t,
                            n: t,
                            pr: n,
                            p: i
                        }
                    }, m = {
                        require: function (t) {
                            return a(t)
                        }, exports: function (t) {
                            var e = g[t];
                            return "undefined" != typeof e ? e : g[t] = {}
                        }, module: function (t) {
                            return {id: t, uri: "", exports: g[t], config: d(t)}
                        }
                    }, p = function (t, i, s, o) {
                        var r, c, d, p, u, b, y = [], w = typeof s;
                        if (o = o || t, "undefined" === w || "function" === w) {
                            for (i = !i.length && s.length ? ["require", "exports", "module"] : i, u = 0; u < i.length; u += 1)if (p = f(i[u], o), c = p.f, "require" === c)y[u] = m.require(t); else if ("exports" === c)y[u] = m.exports(t), b = !0; else if ("module" === c)r = y[u] = m.module(t); else if (n(g, c) || n(v, c) || n(_, c))y[u] = l(c); else {
                                if (!p.p)throw new Error(t + " missing " + c);
                                p.p.load(p.n, a(o, !0), h(c), {}), y[u] = g[c]
                            }
                            d = s ? s.apply(g[t], y) : void 0, t && (r && r.exports !== e && r.exports !== g[t] ? g[t] = r.exports : d === e && b || (g[t] = d))
                        } else t && (g[t] = s)
                    }, t = i = u = function (t, i, s, n, o) {
                        if ("string" == typeof t)return m[t] ? m[t](i) : l(f(t, i).f);
                        if (!t.splice) {
                            if (b = t, b.deps && u(b.deps, b.callback), !i)return;
                            i.splice ? (t = i, i = s, s = null) : t = e
                        }
                        return i = i || function () {
                        }, "function" == typeof s && (s = n, n = o), n ? p(e, t, i, s) : setTimeout(function () {
                            p(e, t, i, s)
                        }, 4), u
                    }, u.config = function (t) {
                        return u(t)
                    }, t._defined = g, s = function (t, e, i) {
                        if ("string" != typeof t)throw new Error("See almond README: incorrect module build, no module name");
                        e.splice || (i = e, e = []), n(g, t) || n(v, t) || (v[t] = [t, e, i])
                    }, s.amd = {jQuery: !0}
                }(), e.requirejs = t, e.require = i, e.define = s
            }
        }(), e.define("almond", function () {
        }), e.define("jquery", [], function () {
            var e = t || $;
            return null == e && console && console.error && console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."), e
        }), e.define("select2/utils", ["jquery"], function (t) {
            function e(t) {
                var e = t.prototype, i = [];
                for (var s in e) {
                    var n = e[s];
                    "function" == typeof n && "constructor" !== s && i.push(s)
                }
                return i
            }

            var i = {};
            i.Extend = function (t, e) {
                function i() {
                    this.constructor = t
                }

                var s = {}.hasOwnProperty;
                for (var n in e)s.call(e, n) && (t[n] = e[n]);
                return i.prototype = e.prototype, t.prototype = new i, t.__super__ = e.prototype, t
            }, i.Decorate = function (t, i) {
                function s() {
                    var e = Array.prototype.unshift, s = i.prototype.constructor.length, n = t.prototype.constructor;
                    s > 0 && (e.call(arguments, t.prototype.constructor), n = i.prototype.constructor), n.apply(this, arguments)
                }

                function n() {
                    this.constructor = s
                }

                var o = e(i), a = e(t);
                i.displayName = t.displayName, s.prototype = new n;
                for (var r = 0; r < a.length; r++) {
                    var h = a[r];
                    s.prototype[h] = t.prototype[h]
                }
                for (var l = (function (t) {
                    var e = function () {
                    };
                    t in s.prototype && (e = s.prototype[t]);
                    var n = i.prototype[t];
                    return function () {
                        var t = Array.prototype.unshift;
                        return t.call(arguments, e), n.apply(this, arguments)
                    }
                }), c = 0; c < o.length; c++) {
                    var d = o[c];
                    s.prototype[d] = l(d)
                }
                return s
            };
            var s = function () {
                this.listeners = {}
            };
            return s.prototype.on = function (t, e) {
                this.listeners = this.listeners || {}, t in this.listeners ? this.listeners[t].push(e) : this.listeners[t] = [e]
            }, s.prototype.trigger = function (t) {
                var e = Array.prototype.slice;
                this.listeners = this.listeners || {}, t in this.listeners && this.invoke(this.listeners[t], e.call(arguments, 1)), "*"in this.listeners && this.invoke(this.listeners["*"], arguments)
            }, s.prototype.invoke = function (t, e) {
                for (var i = 0, s = t.length; s > i; i++)t[i].apply(this, e)
            }, i.Observable = s, i.generateChars = function (t) {
                for (var e = "", i = 0; t > i; i++) {
                    var s = Math.floor(36 * Math.random());
                    e += s.toString(36)
                }
                return e
            }, i.bind = function (t, e) {
                return function () {
                    t.apply(e, arguments)
                }
            }, i._convertData = function (t) {
                for (var e in t) {
                    var i = e.split("-"), s = t;
                    if (1 !== i.length) {
                        for (var n = 0; n < i.length; n++) {
                            var o = i[n];
                            o = o.substring(0, 1).toLowerCase() + o.substring(1), o in s || (s[o] = {}), n == i.length - 1 && (s[o] = t[e]), s = s[o]
                        }
                        delete t[e]
                    }
                }
                return t
            }, i.hasScroll = function (e, i) {
                var s = t(i), n = i.style.overflowX, o = i.style.overflowY;
                return n !== o || "hidden" !== o && "visible" !== o ? "scroll" === n || "scroll" === o ? !0 : s.innerHeight() < i.scrollHeight || s.innerWidth() < i.scrollWidth : !1
            }, i.escapeMarkup = function (t) {
                var e = {
                    "\\": "&#92;",
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#39;",
                    "/": "&#47;"
                };
                return "string" != typeof t ? t : String(t).replace(/[&<>"'\/\\]/g, function (t) {
                    return e[t]
                })
            }, i.appendMany = function (e, i) {
                if ("1.7" === t.fn.jquery.substr(0, 3)) {
                    var s = t();
                    t.map(i, function (t) {
                        s = s.add(t)
                    }), i = s
                }
                e.append(i)
            }, i
        }), e.define("select2/results", ["jquery", "./utils"], function (t, e) {
            function i(t, e, s) {
                this.$element = t, this.data = s, this.options = e, i.__super__.constructor.call(this)
            }

            return e.Extend(i, e.Observable), i.prototype.render = function () {
                var e = t('<ul class="select2-results__options" role="tree"></ul>');
                return this.options.get("multiple") && e.attr("aria-multiselectable", "true"), this.$results = e, e
            }, i.prototype.clear = function () {
                this.$results.empty()
            }, i.prototype.displayMessage = function (e) {
                var i = this.options.get("escapeMarkup");
                this.clear(), this.hideLoading();
                var s = t('<li role="treeitem" aria-live="assertive" class="select2-results__option"></li>'), n = this.options.get("translations").get(e.message);
                s.append(i(n(e.args))), s[0].className += " select2-results__message", this.$results.append(s)
            }, i.prototype.hideMessages = function () {
                this.$results.find(".select2-results__message").remove()
            }, i.prototype.append = function (t) {
                this.hideLoading();
                var e = [];
                if (null == t.results || 0 === t.results.length)return void(0 === this.$results.children().length && this.trigger("results:message", {message: "noResults"}));
                t.results = this.sort(t.results);
                for (var i = 0; i < t.results.length; i++) {
                    var s = t.results[i], n = this.option(s);
                    e.push(n)
                }
                this.$results.append(e)
            }, i.prototype.position = function (t, e) {
                var i = e.find(".select2-results");
                i.append(t)
            }, i.prototype.sort = function (t) {
                var e = this.options.get("sorter");
                return e(t)
            }, i.prototype.setClasses = function () {
                var e = this;
                this.data.current(function (i) {
                    var s = t.map(i, function (t) {
                        return t.id.toString()
                    }), n = e.$results.find(".select2-results__option[aria-selected]");
                    n.each(function () {
                        var e = t(this), i = t.data(this, "data"), n = "" + i.id;
                        null != i.element && i.element.selected || null == i.element && t.inArray(n, s) > -1 ? e.attr("aria-selected", "true") : e.attr("aria-selected", "false")
                    });
                    var o = n.filter("[aria-selected=true]");
                    o.length > 0 ? o.first().trigger("mouseenter") : n.first().trigger("mouseenter")
                })
            }, i.prototype.showLoading = function (t) {
                this.hideLoading();
                var e = this.options.get("translations").get("searching"), i = {
                    disabled: !0,
                    loading: !0,
                    text: e(t)
                }, s = this.option(i);
                s.className += " loading-results", this.$results.prepend(s)
            }, i.prototype.hideLoading = function () {
                this.$results.find(".loading-results").remove()
            }, i.prototype.option = function (e) {
                var i = document.createElement("li");
                i.className = "select2-results__option";
                var s = {role: "treeitem", "aria-selected": "false"};
                e.disabled && (delete s["aria-selected"], s["aria-disabled"] = "true"), null == e.id && delete s["aria-selected"], null != e._resultId && (i.id = e._resultId), e.title && (i.title = e.title), e.children && (s.role = "group", s["aria-label"] = e.text, delete s["aria-selected"]);
                for (var n in s) {
                    var o = s[n];
                    i.setAttribute(n, o)
                }
                if (e.children) {
                    var a = t(i), r = document.createElement("strong");
                    r.className = "select2-results__group", t(r), this.template(e, r);
                    for (var h = [], l = 0; l < e.children.length; l++) {
                        var c = e.children[l], d = this.option(c);
                        h.push(d)
                    }
                    var p = t("<ul></ul>", {"class": "select2-results__options select2-results__options--nested"});
                    p.append(h), a.append(r), a.append(p)
                } else this.template(e, i);
                return t.data(i, "data", e), i
            }, i.prototype.bind = function (e, i) {
                var s = this, n = e.id + "-results";
                this.$results.attr("id", n), e.on("results:all", function (t) {
                    s.clear(), s.append(t.data), e.isOpen() && s.setClasses()
                }), e.on("results:append", function (t) {
                    s.append(t.data), e.isOpen() && s.setClasses()
                }), e.on("query", function (t) {
                    s.hideMessages(), s.showLoading(t)
                }), e.on("select", function () {
                    e.isOpen() && s.setClasses()
                }), e.on("unselect", function () {
                    e.isOpen() && s.setClasses()
                }), e.on("open", function () {
                    s.$results.attr("aria-expanded", "true"), s.$results.attr("aria-hidden", "false"), s.setClasses(), s.ensureHighlightVisible()
                }), e.on("close", function () {
                    s.$results.attr("aria-expanded", "false"), s.$results.attr("aria-hidden", "true"),
                        s.$results.removeAttr("aria-activedescendant")
                }), e.on("results:toggle", function () {
                    var t = s.getHighlightedResults();
                    0 !== t.length && t.trigger("mouseup")
                }), e.on("results:select", function () {
                    var t = s.getHighlightedResults();
                    if (0 !== t.length) {
                        var e = t.data("data");
                        "true" == t.attr("aria-selected") ? s.trigger("close", {}) : s.trigger("select", {data: e})
                    }
                }), e.on("results:previous", function () {
                    var t = s.getHighlightedResults(), e = s.$results.find("[aria-selected]"), i = e.index(t);
                    if (0 !== i) {
                        var n = i - 1;
                        0 === t.length && (n = 0);
                        var o = e.eq(n);
                        o.trigger("mouseenter");
                        var a = s.$results.offset().top, r = o.offset().top, h = s.$results.scrollTop() + (r - a);
                        0 === n ? s.$results.scrollTop(0) : 0 > r - a && s.$results.scrollTop(h)
                    }
                }), e.on("results:next", function () {
                    var t = s.getHighlightedResults(), e = s.$results.find("[aria-selected]"), i = e.index(t), n = i + 1;
                    if (!(n >= e.length)) {
                        var o = e.eq(n);
                        o.trigger("mouseenter");
                        var a = s.$results.offset().top + s.$results.outerHeight(!1), r = o.offset().top + o.outerHeight(!1), h = s.$results.scrollTop() + r - a;
                        0 === n ? s.$results.scrollTop(0) : r > a && s.$results.scrollTop(h)
                    }
                }), e.on("results:focus", function (t) {
                    t.element.addClass("select2-results__option--highlighted")
                }), e.on("results:message", function (t) {
                    s.displayMessage(t)
                }), t.fn.mousewheel && this.$results.on("mousewheel", function (t) {
                    var e = s.$results.scrollTop(), i = s.$results.get(0).scrollHeight - s.$results.scrollTop() + t.deltaY, n = t.deltaY > 0 && e - t.deltaY <= 0, o = t.deltaY < 0 && i <= s.$results.height();
                    n ? (s.$results.scrollTop(0), t.preventDefault(), t.stopPropagation()) : o && (s.$results.scrollTop(s.$results.get(0).scrollHeight - s.$results.height()), t.preventDefault(), t.stopPropagation())
                }), this.$results.on("mouseup", ".select2-results__option[aria-selected]", function (e) {
                    var i = t(this), n = i.data("data");
                    return "true" === i.attr("aria-selected") ? void(s.options.get("multiple") ? s.trigger("unselect", {
                        originalEvent: e,
                        data: n
                    }) : s.trigger("close", {})) : void s.trigger("select", {originalEvent: e, data: n})
                }), this.$results.on("mouseenter", ".select2-results__option[aria-selected]", function (e) {
                    var i = t(this).data("data");
                    s.getHighlightedResults().removeClass("select2-results__option--highlighted"), s.trigger("results:focus", {
                        data: i,
                        element: t(this)
                    })
                })
            }, i.prototype.getHighlightedResults = function () {
                var t = this.$results.find(".select2-results__option--highlighted");
                return t
            }, i.prototype.destroy = function () {
                this.$results.remove()
            }, i.prototype.ensureHighlightVisible = function () {
                var t = this.getHighlightedResults();
                if (0 !== t.length) {
                    var e = this.$results.find("[aria-selected]"), i = e.index(t), s = this.$results.offset().top, n = t.offset().top, o = this.$results.scrollTop() + (n - s), a = n - s;
                    o -= 2 * t.outerHeight(!1), 2 >= i ? this.$results.scrollTop(0) : (a > this.$results.outerHeight() || 0 > a) && this.$results.scrollTop(o)
                }
            }, i.prototype.template = function (e, i) {
                var s = this.options.get("templateResult"), n = this.options.get("escapeMarkup"), o = s(e, i);
                null == o ? i.style.display = "none" : "string" == typeof o ? i.innerHTML = n(o) : t(i).append(o)
            }, i
        }), e.define("select2/keys", [], function () {
            var t = {
                BACKSPACE: 8,
                TAB: 9,
                ENTER: 13,
                SHIFT: 16,
                CTRL: 17,
                ALT: 18,
                ESC: 27,
                SPACE: 32,
                PAGE_UP: 33,
                PAGE_DOWN: 34,
                END: 35,
                HOME: 36,
                LEFT: 37,
                UP: 38,
                RIGHT: 39,
                DOWN: 40,
                DELETE: 46
            };
            return t
        }), e.define("select2/selection/base", ["jquery", "../utils", "../keys"], function (t, e, i) {
            function s(t, e) {
                this.$element = t, this.options = e, s.__super__.constructor.call(this)
            }

            return e.Extend(s, e.Observable), s.prototype.render = function () {
                var e = t('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');
                return this._tabindex = 0, null != this.$element.data("old-tabindex") ? this._tabindex = this.$element.data("old-tabindex") : null != this.$element.attr("tabindex") && (this._tabindex = this.$element.attr("tabindex")), e.attr("title", this.$element.attr("title")), e.attr("tabindex", this._tabindex), this.$selection = e, e
            }, s.prototype.bind = function (t, e) {
                var s = this, n = (t.id + "-container", t.id + "-results");
                this.container = t, this.$selection.on("focus", function (t) {
                    s.trigger("focus", t)
                }), this.$selection.on("blur", function (t) {
                    s._handleBlur(t)
                }), this.$selection.on("keydown", function (t) {
                    s.trigger("keypress", t), t.which === i.SPACE && t.preventDefault()
                }), t.on("results:focus", function (t) {
                    s.$selection.attr("aria-activedescendant", t.data._resultId)
                }), t.on("selection:update", function (t) {
                    s.update(t.data)
                }), t.on("open", function () {
                    s.$selection.attr("aria-expanded", "true"), s.$selection.attr("aria-owns", n), s._attachCloseHandler(t)
                }), t.on("close", function () {
                    s.$selection.attr("aria-expanded", "false"), s.$selection.removeAttr("aria-activedescendant"), s.$selection.removeAttr("aria-owns"), s.$selection.focus(), s._detachCloseHandler(t)
                }), t.on("enable", function () {
                    s.$selection.attr("tabindex", s._tabindex)
                }), t.on("disable", function () {
                    s.$selection.attr("tabindex", "-1")
                })
            }, s.prototype._handleBlur = function (e) {
                var i = this;
                window.setTimeout(function () {
                    document.activeElement == i.$selection[0] || t.contains(i.$selection[0], document.activeElement) || i.trigger("blur", e)
                }, 1)
            }, s.prototype._attachCloseHandler = function (e) {
                t(document.body).on("mousedown.select2." + e.id, function (e) {
                    var i = t(e.target), s = i.closest(".select2"), n = t(".select2.select2-container--open");
                    n.each(function () {
                        var e = t(this);
                        if (this != s[0]) {
                            var i = e.data("element");
                            i.select2("close")
                        }
                    })
                })
            }, s.prototype._detachCloseHandler = function (e) {
                t(document.body).off("mousedown.select2." + e.id)
            }, s.prototype.position = function (t, e) {
                var i = e.find(".selection");
                i.append(t)
            }, s.prototype.destroy = function () {
                this._detachCloseHandler(this.container)
            }, s.prototype.update = function (t) {
                throw new Error("The `update` method must be defined in child classes.")
            }, s
        }), e.define("select2/selection/single", ["jquery", "./base", "../utils", "../keys"], function (t, e, i, s) {
            function n() {
                n.__super__.constructor.apply(this, arguments)
            }

            return i.Extend(n, e), n.prototype.render = function () {
                var t = n.__super__.render.call(this);
                return t.addClass("select2-selection--single"), t.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'), t
            }, n.prototype.bind = function (t, e) {
                var i = this;
                n.__super__.bind.apply(this, arguments);
                var s = t.id + "-container";
                this.$selection.find(".select2-selection__rendered").attr("id", s), this.$selection.attr("aria-labelledby", s), this.$selection.on("mousedown", function (t) {
                    1 === t.which && i.trigger("toggle", {originalEvent: t})
                }), this.$selection.on("focus", function (t) {
                }), this.$selection.on("blur", function (t) {
                }), t.on("selection:update", function (t) {
                    i.update(t.data)
                })
            }, n.prototype.clear = function () {
                this.$selection.find(".select2-selection__rendered").empty()
            }, n.prototype.display = function (t, e) {
                var i = this.options.get("templateSelection"), s = this.options.get("escapeMarkup");
                return s(i(t, e))
            }, n.prototype.selectionContainer = function () {
                return t("<span></span>")
            }, n.prototype.update = function (t) {
                if (0 === t.length)return void this.clear();
                var e = t[0], i = this.$selection.find(".select2-selection__rendered"), s = this.display(e, i);
                i.empty().append(s), i.prop("title", e.title || e.text)
            }, n
        }), e.define("select2/selection/multiple", ["jquery", "./base", "../utils"], function (t, e, i) {
            function s(t, e) {
                s.__super__.constructor.apply(this, arguments)
            }

            return i.Extend(s, e), s.prototype.render = function () {
                var t = s.__super__.render.call(this);
                return t.addClass("select2-selection--multiple"), t.html('<ul class="select2-selection__rendered"></ul>'), t
            }, s.prototype.bind = function (e, i) {
                var n = this;
                s.__super__.bind.apply(this, arguments), this.$selection.on("click", function (t) {
                    n.trigger("toggle", {originalEvent: t})
                }), this.$selection.on("click", ".select2-selection__choice__remove", function (e) {
                    if (!n.options.get("disabled")) {
                        var i = t(this), s = i.parent(), o = s.data("data");
                        n.trigger("unselect", {originalEvent: e, data: o})
                    }
                })
            }, s.prototype.clear = function () {
                this.$selection.find(".select2-selection__rendered").empty()
            }, s.prototype.display = function (t, e) {
                var i = this.options.get("templateSelection"), s = this.options.get("escapeMarkup");
                return s(i(t, e))
            }, s.prototype.selectionContainer = function () {
                var e = t('<li class="select2-selection__choice"><span class="select2-selection__choice__remove" role="presentation">&times;</span></li>');
                return e
            }, s.prototype.update = function (t) {
                if (this.clear(), 0 !== t.length) {
                    for (var e = [], s = 0; s < t.length; s++) {
                        var n = t[s], o = this.selectionContainer(), a = this.display(n, o);
                        o.append(a), o.prop("title", n.title || n.text), o.data("data", n), e.push(o)
                    }
                    var r = this.$selection.find(".select2-selection__rendered");
                    i.appendMany(r, e)
                }
            }, s
        }), e.define("select2/selection/placeholder", ["../utils"], function (t) {
            function e(t, e, i) {
                this.placeholder = this.normalizePlaceholder(i.get("placeholder")), t.call(this, e, i)
            }

            return e.prototype.normalizePlaceholder = function (t, e) {
                return "string" == typeof e && (e = {id: "", text: e}), e
            }, e.prototype.createPlaceholder = function (t, e) {
                var i = this.selectionContainer();
                return i.html(this.display(e)), i.addClass("select2-selection__placeholder").removeClass("select2-selection__choice"), i
            }, e.prototype.update = function (t, e) {
                var i = 1 == e.length && e[0].id != this.placeholder.id, s = e.length > 1;
                if (s || i)return t.call(this, e);
                this.clear();
                var n = this.createPlaceholder(this.placeholder);
                this.$selection.find(".select2-selection__rendered").append(n)
            }, e
        }), e.define("select2/selection/allowClear", ["jquery", "../keys"], function (t, e) {
            function i() {
            }

            return i.prototype.bind = function (t, e, i) {
                var s = this;
                t.call(this, e, i), null == this.placeholder && this.options.get("debug") && window.console && console.error && console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."), this.$selection.on("mousedown", ".select2-selection__clear", function (t) {
                    s._handleClear(t)
                }), e.on("keypress", function (t) {
                    s._handleKeyboardClear(t, e)
                })
            }, i.prototype._handleClear = function (t, e) {
                if (!this.options.get("disabled")) {
                    var i = this.$selection.find(".select2-selection__clear");
                    if (0 !== i.length) {
                        e.stopPropagation();
                        for (var s = i.data("data"), n = 0; n < s.length; n++) {
                            var o = {data: s[n]};
                            if (this.trigger("unselect", o), o.prevented)return
                        }
                        this.$element.val(this.placeholder.id).trigger("change"), this.trigger("toggle", {})
                    }
                }
            }, i.prototype._handleKeyboardClear = function (t, i, s) {
                s.isOpen() || (i.which == e.DELETE || i.which == e.BACKSPACE) && this._handleClear(i)
            }, i.prototype.update = function (e, i) {
                if (e.call(this, i), !(this.$selection.find(".select2-selection__placeholder").length > 0 || 0 === i.length)) {
                    var s = t('<span class="select2-selection__clear">&times;</span>');
                    s.data("data", i), this.$selection.find(".select2-selection__rendered").prepend(s)
                }
            }, i
        }), e.define("select2/selection/search", ["jquery", "../utils", "../keys"], function (t, e, i) {
            function s(t, e, i) {
                t.call(this, e, i)
            }

            return s.prototype.render = function (e) {
                var i = t('<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" /></li>');
                this.$searchContainer = i, this.$search = i.find("input");
                var s = e.call(this);
                return this._transferTabIndex(), s
            }, s.prototype.bind = function (t, e, s) {
                var n = this;
                t.call(this, e, s), e.on("open", function () {
                    n.$search.trigger("focus")
                }), e.on("close", function () {
                    n.$search.val(""), n.$search.removeAttr("aria-activedescendant"), n.$search.trigger("focus")
                }), e.on("enable", function () {
                    n.$search.prop("disabled", !1), n._transferTabIndex()
                }), e.on("disable", function () {
                    n.$search.prop("disabled", !0)
                }), e.on("focus", function (t) {
                    n.$search.trigger("focus")
                }), e.on("results:focus", function (t) {
                    n.$search.attr("aria-activedescendant", t.id)
                }), this.$selection.on("focusin", ".select2-search--inline", function (t) {
                    n.trigger("focus", t)
                }), this.$selection.on("focusout", ".select2-search--inline", function (t) {
                    n._handleBlur(t)
                }), this.$selection.on("keydown", ".select2-search--inline", function (t) {
                    t.stopPropagation(), n.trigger("keypress", t), n._keyUpPrevented = t.isDefaultPrevented();
                    var e = t.which;
                    if (e === i.BACKSPACE && "" === n.$search.val()) {
                        var s = n.$searchContainer.prev(".select2-selection__choice");
                        if (s.length > 0) {
                            var o = s.data("data");
                            n.searchRemoveChoice(o), t.preventDefault()
                        }
                    }
                });
                var o = document.documentMode, a = o && 11 >= o;
                this.$selection.on("input.searchcheck", ".select2-search--inline", function (t) {
                    return a ? void n.$selection.off("input.search input.searchcheck") : void n.$selection.off("keyup.search")
                }), this.$selection.on("keyup.search input.search", ".select2-search--inline", function (t) {
                    if (a && "input" === t.type)return void n.$selection.off("input.search input.searchcheck");
                    var e = t.which;
                    e != i.SHIFT && e != i.CTRL && e != i.ALT && e != i.TAB && n.handleSearch(t)
                })
            }, s.prototype._transferTabIndex = function (t) {
                this.$search.attr("tabindex", this.$selection.attr("tabindex")), this.$selection.attr("tabindex", "-1")
            }, s.prototype.createPlaceholder = function (t, e) {
                this.$search.attr("placeholder", e.text)
            }, s.prototype.update = function (t, e) {
                var i = this.$search[0] == document.activeElement;
                this.$search.attr("placeholder", ""), t.call(this, e), this.$selection.find(".select2-selection__rendered").append(this.$searchContainer), this.resizeSearch(), i && this.$search.focus()
            }, s.prototype.handleSearch = function () {
                if (this.resizeSearch(), !this._keyUpPrevented) {
                    var t = this.$search.val();
                    this.trigger("query", {term: t})
                }
                this._keyUpPrevented = !1
            }, s.prototype.searchRemoveChoice = function (t, e) {
                this.trigger("unselect", {data: e}), this.$search.val(e.text), this.handleSearch()
            }, s.prototype.resizeSearch = function () {
                this.$search.css("width", "25px");
                var t = "";
                if ("" !== this.$search.attr("placeholder"))t = this.$selection.find(".select2-selection__rendered").innerWidth(); else {
                    var e = this.$search.val().length + 1;
                    t = .75 * e + "em"
                }
                this.$search.css("width", t)
            }, s
        }), e.define("select2/selection/eventRelay", ["jquery"], function (t) {
            function e() {
            }

            return e.prototype.bind = function (e, i, s) {
                var n = this, o = ["open", "opening", "close", "closing", "select", "selecting", "unselect", "unselecting"], a = ["opening", "closing", "selecting", "unselecting"];
                e.call(this, i, s), i.on("*", function (e, i) {
                    if (-1 !== t.inArray(e, o)) {
                        i = i || {};
                        var s = t.Event("select2:" + e, {params: i});
                        n.$element.trigger(s), -1 !== t.inArray(e, a) && (i.prevented = s.isDefaultPrevented())
                    }
                })
            }, e
        }), e.define("select2/translation", ["jquery", "require"], function (t, e) {
            function i(t) {
                this.dict = t || {}
            }

            return i.prototype.all = function () {
                return this.dict
            }, i.prototype.get = function (t) {
                return this.dict[t]
            }, i.prototype.extend = function (e) {
                this.dict = t.extend({}, e.all(), this.dict)
            }, i._cache = {}, i.loadPath = function (t) {
                if (!(t in i._cache)) {
                    var s = e(t);
                    i._cache[t] = s
                }
                return new i(i._cache[t])
            }, i
        }), e.define("select2/diacritics", [], function () {
            var t = {
                "Ⓐ": "A",
                "Ａ": "A",
                "À": "A",
                "Á": "A",
                "Â": "A",
                "Ầ": "A",
                "Ấ": "A",
                "Ẫ": "A",
                "Ẩ": "A",
                "Ã": "A",
                "Ā": "A",
                "Ă": "A",
                "Ằ": "A",
                "Ắ": "A",
                "Ẵ": "A",
                "Ẳ": "A",
                "Ȧ": "A",
                "Ǡ": "A",
                "Ä": "A",
                "Ǟ": "A",
                "Ả": "A",
                "Å": "A",
                "Ǻ": "A",
                "Ǎ": "A",
                "Ȁ": "A",
                "Ȃ": "A",
                "Ạ": "A",
                "Ậ": "A",
                "Ặ": "A",
                "Ḁ": "A",
                "Ą": "A",
                "Ⱥ": "A",
                "Ɐ": "A",
                "Ꜳ": "AA",
                "Æ": "AE",
                "Ǽ": "AE",
                "Ǣ": "AE",
                "Ꜵ": "AO",
                "Ꜷ": "AU",
                "Ꜹ": "AV",
                "Ꜻ": "AV",
                "Ꜽ": "AY",
                "Ⓑ": "B",
                "Ｂ": "B",
                "Ḃ": "B",
                "Ḅ": "B",
                "Ḇ": "B",
                "Ƀ": "B",
                "Ƃ": "B",
                "Ɓ": "B",
                "Ⓒ": "C",
                "Ｃ": "C",
                "Ć": "C",
                "Ĉ": "C",
                "Ċ": "C",
                "Č": "C",
                "Ç": "C",
                "Ḉ": "C",
                "Ƈ": "C",
                "Ȼ": "C",
                "Ꜿ": "C",
                "Ⓓ": "D",
                "Ｄ": "D",
                "Ḋ": "D",
                "Ď": "D",
                "Ḍ": "D",
                "Ḑ": "D",
                "Ḓ": "D",
                "Ḏ": "D",
                "Đ": "D",
                "Ƌ": "D",
                "Ɗ": "D",
                "Ɖ": "D",
                "Ꝺ": "D",
                "Ǳ": "DZ",
                "Ǆ": "DZ",
                "ǲ": "Dz",
                "ǅ": "Dz",
                "Ⓔ": "E",
                "Ｅ": "E",
                "È": "E",
                "É": "E",
                "Ê": "E",
                "Ề": "E",
                "Ế": "E",
                "Ễ": "E",
                "Ể": "E",
                "Ẽ": "E",
                "Ē": "E",
                "Ḕ": "E",
                "Ḗ": "E",
                "Ĕ": "E",
                "Ė": "E",
                "Ë": "E",
                "Ẻ": "E",
                "Ě": "E",
                "Ȅ": "E",
                "Ȇ": "E",
                "Ẹ": "E",
                "Ệ": "E",
                "Ȩ": "E",
                "Ḝ": "E",
                "Ę": "E",
                "Ḙ": "E",
                "Ḛ": "E",
                "Ɛ": "E",
                "Ǝ": "E",
                "Ⓕ": "F",
                "Ｆ": "F",
                "Ḟ": "F",
                "Ƒ": "F",
                "Ꝼ": "F",
                "Ⓖ": "G",
                "Ｇ": "G",
                "Ǵ": "G",
                "Ĝ": "G",
                "Ḡ": "G",
                "Ğ": "G",
                "Ġ": "G",
                "Ǧ": "G",
                "Ģ": "G",
                "Ǥ": "G",
                "Ɠ": "G",
                "Ꞡ": "G",
                "Ᵹ": "G",
                "Ꝿ": "G",
                "Ⓗ": "H",
                "Ｈ": "H",
                "Ĥ": "H",
                "Ḣ": "H",
                "Ḧ": "H",
                "Ȟ": "H",
                "Ḥ": "H",
                "Ḩ": "H",
                "Ḫ": "H",
                "Ħ": "H",
                "Ⱨ": "H",
                "Ⱶ": "H",
                "Ɥ": "H",
                "Ⓘ": "I",
                "Ｉ": "I",
                "Ì": "I",
                "Í": "I",
                "Î": "I",
                "Ĩ": "I",
                "Ī": "I",
                "Ĭ": "I",
                "İ": "I",
                "Ï": "I",
                "Ḯ": "I",
                "Ỉ": "I",
                "Ǐ": "I",
                "Ȉ": "I",
                "Ȋ": "I",
                "Ị": "I",
                "Į": "I",
                "Ḭ": "I",
                "Ɨ": "I",
                "Ⓙ": "J",
                "Ｊ": "J",
                "Ĵ": "J",
                "Ɉ": "J",
                "Ⓚ": "K",
                "Ｋ": "K",
                "Ḱ": "K",
                "Ǩ": "K",
                "Ḳ": "K",
                "Ķ": "K",
                "Ḵ": "K",
                "Ƙ": "K",
                "Ⱪ": "K",
                "Ꝁ": "K",
                "Ꝃ": "K",
                "Ꝅ": "K",
                "Ꞣ": "K",
                "Ⓛ": "L",
                "Ｌ": "L",
                "Ŀ": "L",
                "Ĺ": "L",
                "Ľ": "L",
                "Ḷ": "L",
                "Ḹ": "L",
                "Ļ": "L",
                "Ḽ": "L",
                "Ḻ": "L",
                "Ł": "L",
                "Ƚ": "L",
                "Ɫ": "L",
                "Ⱡ": "L",
                "Ꝉ": "L",
                "Ꝇ": "L",
                "Ꞁ": "L",
                "Ǉ": "LJ",
                "ǈ": "Lj",
                "Ⓜ": "M",
                "Ｍ": "M",
                "Ḿ": "M",
                "Ṁ": "M",
                "Ṃ": "M",
                "Ɱ": "M",
                "Ɯ": "M",
                "Ⓝ": "N",
                "Ｎ": "N",
                "Ǹ": "N",
                "Ń": "N",
                "Ñ": "N",
                "Ṅ": "N",
                "Ň": "N",
                "Ṇ": "N",
                "Ņ": "N",
                "Ṋ": "N",
                "Ṉ": "N",
                "Ƞ": "N",
                "Ɲ": "N",
                "Ꞑ": "N",
                "Ꞥ": "N",
                "Ǌ": "NJ",
                "ǋ": "Nj",
                "Ⓞ": "O",
                "Ｏ": "O",
                "Ò": "O",
                "Ó": "O",
                "Ô": "O",
                "Ồ": "O",
                "Ố": "O",
                "Ỗ": "O",
                "Ổ": "O",
                "Õ": "O",
                "Ṍ": "O",
                "Ȭ": "O",
                "Ṏ": "O",
                "Ō": "O",
                "Ṑ": "O",
                "Ṓ": "O",
                "Ŏ": "O",
                "Ȯ": "O",
                "Ȱ": "O",
                "Ö": "O",
                "Ȫ": "O",
                "Ỏ": "O",
                "Ő": "O",
                "Ǒ": "O",
                "Ȍ": "O",
                "Ȏ": "O",
                "Ơ": "O",
                "Ờ": "O",
                "Ớ": "O",
                "Ỡ": "O",
                "Ở": "O",
                "Ợ": "O",
                "Ọ": "O",
                "Ộ": "O",
                "Ǫ": "O",
                "Ǭ": "O",
                "Ø": "O",
                "Ǿ": "O",
                "Ɔ": "O",
                "Ɵ": "O",
                "Ꝋ": "O",
                "Ꝍ": "O",
                "Ƣ": "OI",
                "Ꝏ": "OO",
                "Ȣ": "OU",
                "Ⓟ": "P",
                "Ｐ": "P",
                "Ṕ": "P",
                "Ṗ": "P",
                "Ƥ": "P",
                "Ᵽ": "P",
                "Ꝑ": "P",
                "Ꝓ": "P",
                "Ꝕ": "P",
                "Ⓠ": "Q",
                "Ｑ": "Q",
                "Ꝗ": "Q",
                "Ꝙ": "Q",
                "Ɋ": "Q",
                "Ⓡ": "R",
                "Ｒ": "R",
                "Ŕ": "R",
                "Ṙ": "R",
                "Ř": "R",
                "Ȑ": "R",
                "Ȓ": "R",
                "Ṛ": "R",
                "Ṝ": "R",
                "Ŗ": "R",
                "Ṟ": "R",
                "Ɍ": "R",
                "Ɽ": "R",
                "Ꝛ": "R",
                "Ꞧ": "R",
                "Ꞃ": "R",
                "Ⓢ": "S",
                "Ｓ": "S",
                "ẞ": "S",
                "Ś": "S",
                "Ṥ": "S",
                "Ŝ": "S",
                "Ṡ": "S",
                "Š": "S",
                "Ṧ": "S",
                "Ṣ": "S",
                "Ṩ": "S",
                "Ș": "S",
                "Ş": "S",
                "Ȿ": "S",
                "Ꞩ": "S",
                "Ꞅ": "S",
                "Ⓣ": "T",
                "Ｔ": "T",
                "Ṫ": "T",
                "Ť": "T",
                "Ṭ": "T",
                "Ț": "T",
                "Ţ": "T",
                "Ṱ": "T",
                "Ṯ": "T",
                "Ŧ": "T",
                "Ƭ": "T",
                "Ʈ": "T",
                "Ⱦ": "T",
                "Ꞇ": "T",
                "Ꜩ": "TZ",
                "Ⓤ": "U",
                "Ｕ": "U",
                "Ù": "U",
                "Ú": "U",
                "Û": "U",
                "Ũ": "U",
                "Ṹ": "U",
                "Ū": "U",
                "Ṻ": "U",
                "Ŭ": "U",
                "Ü": "U",
                "Ǜ": "U",
                "Ǘ": "U",
                "Ǖ": "U",
                "Ǚ": "U",
                "Ủ": "U",
                "Ů": "U",
                "Ű": "U",
                "Ǔ": "U",
                "Ȕ": "U",
                "Ȗ": "U",
                "Ư": "U",
                "Ừ": "U",
                "Ứ": "U",
                "Ữ": "U",
                "Ử": "U",
                "Ự": "U",
                "Ụ": "U",
                "Ṳ": "U",
                "Ų": "U",
                "Ṷ": "U",
                "Ṵ": "U",
                "Ʉ": "U",
                "Ⓥ": "V",
                "Ｖ": "V",
                "Ṽ": "V",
                "Ṿ": "V",
                "Ʋ": "V",
                "Ꝟ": "V",
                "Ʌ": "V",
                "Ꝡ": "VY",
                "Ⓦ": "W",
                "Ｗ": "W",
                "Ẁ": "W",
                "Ẃ": "W",
                "Ŵ": "W",
                "Ẇ": "W",
                "Ẅ": "W",
                "Ẉ": "W",
                "Ⱳ": "W",
                "Ⓧ": "X",
                "Ｘ": "X",
                "Ẋ": "X",
                "Ẍ": "X",
                "Ⓨ": "Y",
                "Ｙ": "Y",
                "Ỳ": "Y",
                "Ý": "Y",
                "Ŷ": "Y",
                "Ỹ": "Y",
                "Ȳ": "Y",
                "Ẏ": "Y",
                "Ÿ": "Y",
                "Ỷ": "Y",
                "Ỵ": "Y",
                "Ƴ": "Y",
                "Ɏ": "Y",
                "Ỿ": "Y",
                "Ⓩ": "Z",
                "Ｚ": "Z",
                "Ź": "Z",
                "Ẑ": "Z",
                "Ż": "Z",
                "Ž": "Z",
                "Ẓ": "Z",
                "Ẕ": "Z",
                "Ƶ": "Z",
                "Ȥ": "Z",
                "Ɀ": "Z",
                "Ⱬ": "Z",
                "Ꝣ": "Z",
                "ⓐ": "a",
                "ａ": "a",
                "ẚ": "a",
                "à": "a",
                "á": "a",
                "â": "a",
                "ầ": "a",
                "ấ": "a",
                "ẫ": "a",
                "ẩ": "a",
                "ã": "a",
                "ā": "a",
                "ă": "a",
                "ằ": "a",
                "ắ": "a",
                "ẵ": "a",
                "ẳ": "a",
                "ȧ": "a",
                "ǡ": "a",
                "ä": "a",
                "ǟ": "a",
                "ả": "a",
                "å": "a",
                "ǻ": "a",
                "ǎ": "a",
                "ȁ": "a",
                "ȃ": "a",
                "ạ": "a",
                "ậ": "a",
                "ặ": "a",
                "ḁ": "a",
                "ą": "a",
                "ⱥ": "a",
                "ɐ": "a",
                "ꜳ": "aa",
                "æ": "ae",
                "ǽ": "ae",
                "ǣ": "ae",
                "ꜵ": "ao",
                "ꜷ": "au",
                "ꜹ": "av",
                "ꜻ": "av",
                "ꜽ": "ay",
                "ⓑ": "b",
                "ｂ": "b",
                "ḃ": "b",
                "ḅ": "b",
                "ḇ": "b",
                "ƀ": "b",
                "ƃ": "b",
                "ɓ": "b",
                "ⓒ": "c",
                "ｃ": "c",
                "ć": "c",
                "ĉ": "c",
                "ċ": "c",
                "č": "c",
                "ç": "c",
                "ḉ": "c",
                "ƈ": "c",
                "ȼ": "c",
                "ꜿ": "c",
                "ↄ": "c",
                "ⓓ": "d",
                "ｄ": "d",
                "ḋ": "d",
                "ď": "d",
                "ḍ": "d",
                "ḑ": "d",
                "ḓ": "d",
                "ḏ": "d",
                "đ": "d",
                "ƌ": "d",
                "ɖ": "d",
                "ɗ": "d",
                "ꝺ": "d",
                "ǳ": "dz",
                "ǆ": "dz",
                "ⓔ": "e",
                "ｅ": "e",
                "è": "e",
                "é": "e",
                "ê": "e",
                "ề": "e",
                "ế": "e",
                "ễ": "e",
                "ể": "e",
                "ẽ": "e",
                "ē": "e",
                "ḕ": "e",
                "ḗ": "e",
                "ĕ": "e",
                "ė": "e",
                "ë": "e",
                "ẻ": "e",
                "ě": "e",
                "ȅ": "e",
                "ȇ": "e",
                "ẹ": "e",
                "ệ": "e",
                "ȩ": "e",
                "ḝ": "e",
                "ę": "e",
                "ḙ": "e",
                "ḛ": "e",
                "ɇ": "e",
                "ɛ": "e",
                "ǝ": "e",
                "ⓕ": "f",
                "ｆ": "f",
                "ḟ": "f",
                "ƒ": "f",
                "ꝼ": "f",
                "ⓖ": "g",
                "ｇ": "g",
                "ǵ": "g",
                "ĝ": "g",
                "ḡ": "g",
                "ğ": "g",
                "ġ": "g",
                "ǧ": "g",
                "ģ": "g",
                "ǥ": "g",
                "ɠ": "g",
                "ꞡ": "g",
                "ᵹ": "g",
                "ꝿ": "g",
                "ⓗ": "h",
                "ｈ": "h",
                "ĥ": "h",
                "ḣ": "h",
                "ḧ": "h",
                "ȟ": "h",
                "ḥ": "h",
                "ḩ": "h",
                "ḫ": "h",
                "ẖ": "h",
                "ħ": "h",
                "ⱨ": "h",
                "ⱶ": "h",
                "ɥ": "h",
                "ƕ": "hv",
                "ⓘ": "i",
                "ｉ": "i",
                "ì": "i",
                "í": "i",
                "î": "i",
                "ĩ": "i",
                "ī": "i",
                "ĭ": "i",
                "ï": "i",
                "ḯ": "i",
                "ỉ": "i",
                "ǐ": "i",
                "ȉ": "i",
                "ȋ": "i",
                "ị": "i",
                "į": "i",
                "ḭ": "i",
                "ɨ": "i",
                "ı": "i",
                "ⓙ": "j",
                "ｊ": "j",
                "ĵ": "j",
                "ǰ": "j",
                "ɉ": "j",
                "ⓚ": "k",
                "ｋ": "k",
                "ḱ": "k",
                "ǩ": "k",
                "ḳ": "k",
                "ķ": "k",
                "ḵ": "k",
                "ƙ": "k",
                "ⱪ": "k",
                "ꝁ": "k",
                "ꝃ": "k",
                "ꝅ": "k",
                "ꞣ": "k",
                "ⓛ": "l",
                "ｌ": "l",
                "ŀ": "l",
                "ĺ": "l",
                "ľ": "l",
                "ḷ": "l",
                "ḹ": "l",
                "ļ": "l",
                "ḽ": "l",
                "ḻ": "l",
                "ſ": "l",
                "ł": "l",
                "ƚ": "l",
                "ɫ": "l",
                "ⱡ": "l",
                "ꝉ": "l",
                "ꞁ": "l",
                "ꝇ": "l",
                "ǉ": "lj",
                "ⓜ": "m",
                "ｍ": "m",
                "ḿ": "m",
                "ṁ": "m",
                "ṃ": "m",
                "ɱ": "m",
                "ɯ": "m",
                "ⓝ": "n",
                "ｎ": "n",
                "ǹ": "n",
                "ń": "n",
                "ñ": "n",
                "ṅ": "n",
                "ň": "n",
                "ṇ": "n",
                "ņ": "n",
                "ṋ": "n",
                "ṉ": "n",
                "ƞ": "n",
                "ɲ": "n",
                "ŉ": "n",
                "ꞑ": "n",
                "ꞥ": "n",
                "ǌ": "nj",
                "ⓞ": "o",
                "ｏ": "o",
                "ò": "o",
                "ó": "o",
                "ô": "o",
                "ồ": "o",
                "ố": "o",
                "ỗ": "o",
                "ổ": "o",
                "õ": "o",
                "ṍ": "o",
                "ȭ": "o",
                "ṏ": "o",
                "ō": "o",
                "ṑ": "o",
                "ṓ": "o",
                "ŏ": "o",
                "ȯ": "o",
                "ȱ": "o",
                "ö": "o",
                "ȫ": "o",
                "ỏ": "o",
                "ő": "o",
                "ǒ": "o",
                "ȍ": "o",
                "ȏ": "o",
                "ơ": "o",
                "ờ": "o",
                "ớ": "o",
                "ỡ": "o",
                "ở": "o",
                "ợ": "o",
                "ọ": "o",
                "ộ": "o",
                "ǫ": "o",
                "ǭ": "o",
                "ø": "o",
                "ǿ": "o",
                "ɔ": "o",
                "ꝋ": "o",
                "ꝍ": "o",
                "ɵ": "o",
                "ƣ": "oi",
                "ȣ": "ou",
                "ꝏ": "oo",
                "ⓟ": "p",
                "ｐ": "p",
                "ṕ": "p",
                "ṗ": "p",
                "ƥ": "p",
                "ᵽ": "p",
                "ꝑ": "p",
                "ꝓ": "p",
                "ꝕ": "p",
                "ⓠ": "q",
                "ｑ": "q",
                "ɋ": "q",
                "ꝗ": "q",
                "ꝙ": "q",
                "ⓡ": "r",
                "ｒ": "r",
                "ŕ": "r",
                "ṙ": "r",
                "ř": "r",
                "ȑ": "r",
                "ȓ": "r",
                "ṛ": "r",
                "ṝ": "r",
                "ŗ": "r",
                "ṟ": "r",
                "ɍ": "r",
                "ɽ": "r",
                "ꝛ": "r",
                "ꞧ": "r",
                "ꞃ": "r",
                "ⓢ": "s",
                "ｓ": "s",
                "ß": "s",
                "ś": "s",
                "ṥ": "s",
                "ŝ": "s",
                "ṡ": "s",
                "š": "s",
                "ṧ": "s",
                "ṣ": "s",
                "ṩ": "s",
                "ș": "s",
                "ş": "s",
                "ȿ": "s",
                "ꞩ": "s",
                "ꞅ": "s",
                "ẛ": "s",
                "ⓣ": "t",
                "ｔ": "t",
                "ṫ": "t",
                "ẗ": "t",
                "ť": "t",
                "ṭ": "t",
                "ț": "t",
                "ţ": "t",
                "ṱ": "t",
                "ṯ": "t",
                "ŧ": "t",
                "ƭ": "t",
                "ʈ": "t",
                "ⱦ": "t",
                "ꞇ": "t",
                "ꜩ": "tz",
                "ⓤ": "u",
                "ｕ": "u",
                "ù": "u",
                "ú": "u",
                "û": "u",
                "ũ": "u",
                "ṹ": "u",
                "ū": "u",
                "ṻ": "u",
                "ŭ": "u",
                "ü": "u",
                "ǜ": "u",
                "ǘ": "u",
                "ǖ": "u",
                "ǚ": "u",
                "ủ": "u",
                "ů": "u",
                "ű": "u",
                "ǔ": "u",
                "ȕ": "u",
                "ȗ": "u",
                "ư": "u",
                "ừ": "u",
                "ứ": "u",
                "ữ": "u",
                "ử": "u",
                "ự": "u",
                "ụ": "u",
                "ṳ": "u",
                "ų": "u",
                "ṷ": "u",
                "ṵ": "u",
                "ʉ": "u",
                "ⓥ": "v",
                "ｖ": "v",
                "ṽ": "v",
                "ṿ": "v",
                "ʋ": "v",
                "ꝟ": "v",
                "ʌ": "v",
                "ꝡ": "vy",
                "ⓦ": "w",
                "ｗ": "w",
                "ẁ": "w",
                "ẃ": "w",
                "ŵ": "w",
                "ẇ": "w",
                "ẅ": "w",
                "ẘ": "w",
                "ẉ": "w",
                "ⱳ": "w",
                "ⓧ": "x",
                "ｘ": "x",
                "ẋ": "x",
                "ẍ": "x",
                "ⓨ": "y",
                "ｙ": "y",
                "ỳ": "y",
                "ý": "y",
                "ŷ": "y",
                "ỹ": "y",
                "ȳ": "y",
                "ẏ": "y",
                "ÿ": "y",
                "ỷ": "y",
                "ẙ": "y",
                "ỵ": "y",
                "ƴ": "y",
                "ɏ": "y",
                "ỿ": "y",
                "ⓩ": "z",
                "ｚ": "z",
                "ź": "z",
                "ẑ": "z",
                "ż": "z",
                "ž": "z",
                "ẓ": "z",
                "ẕ": "z",
                "ƶ": "z",
                "ȥ": "z",
                "ɀ": "z",
                "ⱬ": "z",
                "ꝣ": "z",
                "Ά": "Α",
                "Έ": "Ε",
                "Ή": "Η",
                "Ί": "Ι",
                "Ϊ": "Ι",
                "Ό": "Ο",
                "Ύ": "Υ",
                "Ϋ": "Υ",
                "Ώ": "Ω",
                "ά": "α",
                "έ": "ε",
                "ή": "η",
                "ί": "ι",
                "ϊ": "ι",
                "ΐ": "ι",
                "ό": "ο",
                "ύ": "υ",
                "ϋ": "υ",
                "ΰ": "υ",
                "ω": "ω",
                "ς": "σ"
            };
            return t
        }), e.define("select2/data/base", ["../utils"], function (t) {
            function e(t, i) {
                e.__super__.constructor.call(this)
            }

            return t.Extend(e, t.Observable), e.prototype.current = function (t) {
                throw new Error("The `current` method must be defined in child classes.")
            }, e.prototype.query = function (t, e) {
                throw new Error("The `query` method must be defined in child classes.")
            }, e.prototype.bind = function (t, e) {
            }, e.prototype.destroy = function () {
            }, e.prototype.generateResultId = function (e, i) {
                var s = e.id + "-result-";
                return s += t.generateChars(4), s += null != i.id ? "-" + i.id.toString() : "-" + t.generateChars(4)
            }, e
        }), e.define("select2/data/select", ["./base", "../utils", "jquery"], function (t, e, i) {
            function s(t, e) {
                this.$element = t, this.options = e, s.__super__.constructor.call(this)
            }

            return e.Extend(s, t), s.prototype.current = function (t) {
                var e = [], s = this;
                this.$element.find(":selected").each(function () {
                    var t = i(this), n = s.item(t);
                    e.push(n)
                }), t(e)
            }, s.prototype.select = function (t) {
                var e = this;
                if (t.selected = !0, i(t.element).is("option"))return t.element.selected = !0, void this.$element.trigger("change");
                if (this.$element.prop("multiple"))this.current(function (s) {
                    var n = [];
                    t = [t], t.push.apply(t, s);
                    for (var o = 0; o < t.length; o++) {
                        var a = t[o].id;
                        -1 === i.inArray(a, n) && n.push(a)
                    }
                    e.$element.val(n), e.$element.trigger("change")
                }); else {
                    var s = t.id;
                    this.$element.val(s), this.$element.trigger("change")
                }
            }, s.prototype.unselect = function (t) {
                var e = this;
                return this.$element.prop("multiple") ? (t.selected = !1, i(t.element).is("option") ? (t.element.selected = !1, void this.$element.trigger("change")) : void this.current(function (s) {
                    for (var n = [], o = 0; o < s.length; o++) {
                        var a = s[o].id;
                        a !== t.id && -1 === i.inArray(a, n) && n.push(a)
                    }
                    e.$element.val(n), e.$element.trigger("change")
                })) : void 0
            }, s.prototype.bind = function (t, e) {
                var i = this;
                this.container = t, t.on("select", function (t) {
                    i.select(t.data)
                }), t.on("unselect", function (t) {
                    i.unselect(t.data)
                })
            }, s.prototype.destroy = function () {
                this.$element.find("*").each(function () {
                    i.removeData(this, "data")
                })
            }, s.prototype.query = function (t, e) {
                var s = [], n = this, o = this.$element.children();
                o.each(function () {
                    var e = i(this);
                    if (e.is("option") || e.is("optgroup")) {
                        var o = n.item(e), a = n.matches(t, o);
                        null !== a && s.push(a)
                    }
                }), e({results: s})
            }, s.prototype.addOptions = function (t) {
                e.appendMany(this.$element, t)
            }, s.prototype.option = function (t) {
                var e;
                t.children ? (e = document.createElement("optgroup"), e.label = t.text) : (e = document.createElement("option"), void 0 !== e.textContent ? e.textContent = t.text : e.innerText = t.text), t.id && (e.value = t.id), t.disabled && (e.disabled = !0), t.selected && (e.selected = !0), t.title && (e.title = t.title);
                var s = i(e), n = this._normalizeItem(t);
                return n.element = e, i.data(e, "data", n), s
            }, s.prototype.item = function (t) {
                var e = {};
                if (e = i.data(t[0], "data"), null != e)return e;
                if (t.is("option"))e = {
                    id: t.val(),
                    text: t.text(),
                    disabled: t.prop("disabled"),
                    selected: t.prop("selected"),
                    title: t.prop("title")
                }; else if (t.is("optgroup")) {
                    e = {text: t.prop("label"), children: [], title: t.prop("title")};
                    for (var s = t.children("option"), n = [], o = 0; o < s.length; o++) {
                        var a = i(s[o]), r = this.item(a);
                        n.push(r)
                    }
                    e.children = n
                }
                return e = this._normalizeItem(e), e.element = t[0], i.data(t[0], "data", e), e
            }, s.prototype._normalizeItem = function (t) {
                i.isPlainObject(t) || (t = {id: t, text: t}), t = i.extend({}, {text: ""}, t);
                var e = {selected: !1, disabled: !1};
                return null != t.id && (t.id = t.id.toString()), null != t.text && (t.text = t.text.toString()), null == t._resultId && t.id && null != this.container && (t._resultId = this.generateResultId(this.container, t)), i.extend({}, e, t)
            }, s.prototype.matches = function (t, e) {
                var i = this.options.get("matcher");
                return i(t, e)
            }, s
        }), e.define("select2/data/array", ["./select", "../utils", "jquery"], function (t, e, i) {
            function s(t, e) {
                var i = e.get("data") || [];
                s.__super__.constructor.call(this, t, e), this.addOptions(this.convertToOptions(i))
            }

            return e.Extend(s, t), s.prototype.select = function (t) {
                var e = this.$element.find("option").filter(function (e, i) {
                    return i.value == t.id.toString()
                });
                0 === e.length && (e = this.option(t), this.addOptions(e)), s.__super__.select.call(this, t)
            }, s.prototype.convertToOptions = function (t) {
                function s(t) {
                    return function () {
                        return i(this).val() == t.id
                    }
                }

                for (var n = this, o = this.$element.find("option"), a = o.map(function () {
                    return n.item(i(this)).id
                }).get(), r = [], h = 0; h < t.length; h++) {
                    var l = this._normalizeItem(t[h]);
                    if (i.inArray(l.id, a) >= 0) {
                        var c = o.filter(s(l)), d = this.item(c), p = i.extend(!0, {}, d, l), u = this.option(p);
                        c.replaceWith(u)
                    } else {
                        var f = this.option(l);
                        if (l.children) {
                            var m = this.convertToOptions(l.children);
                            e.appendMany(f, m)
                        }
                        r.push(f)
                    }
                }
                return r
            }, s
        }), e.define("select2/data/ajax", ["./array", "../utils", "jquery"], function (t, e, i) {
            function s(t, e) {
                this.ajaxOptions = this._applyDefaults(e.get("ajax")), null != this.ajaxOptions.processResults && (this.processResults = this.ajaxOptions.processResults), s.__super__.constructor.call(this, t, e)
            }

            return e.Extend(s, t), s.prototype._applyDefaults = function (t) {
                var e = {
                    data: function (t) {
                        return i.extend({}, t, {q: t.term})
                    }, transport: function (t, e, s) {
                        var n = i.ajax(t);
                        return n.then(e), n.fail(s), n
                    }
                };
                return i.extend({}, e, t, !0)
            }, s.prototype.processResults = function (t) {
                return t
            }, s.prototype.query = function (t, e) {
                function s() {
                    var s = o.transport(o, function (s) {
                        var o = n.processResults(s, t);
                        n.options.get("debug") && window.console && console.error && (o && o.results && i.isArray(o.results) || console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")), e(o)
                    }, function () {
                    });
                    n._request = s
                }

                var n = this;
                null != this._request && (i.isFunction(this._request.abort) && this._request.abort(), this._request = null);
                var o = i.extend({type: "GET"}, this.ajaxOptions);
                "function" == typeof o.url && (o.url = o.url.call(this.$element, t)), "function" == typeof o.data && (o.data = o.data.call(this.$element, t)), this.ajaxOptions.delay && "" !== t.term ? (this._queryTimeout && window.clearTimeout(this._queryTimeout), this._queryTimeout = window.setTimeout(s, this.ajaxOptions.delay)) : s()
            }, s
        }), e.define("select2/data/tags", ["jquery"], function (t) {
            function e(e, i, s) {
                var n = s.get("tags"), o = s.get("createTag");
                if (void 0 !== o && (this.createTag = o), e.call(this, i, s), t.isArray(n))for (var a = 0; a < n.length; a++) {
                    var r = n[a], h = this._normalizeItem(r), l = this.option(h);
                    this.$element.append(l)
                }
            }

            return e.prototype.query = function (t, e, i) {
                function s(t, o) {
                    for (var a = t.results, r = 0; r < a.length; r++) {
                        var h = a[r], l = null != h.children && !s({results: h.children}, !0), c = h.text === e.term;
                        if (c || l)return o ? !1 : (t.data = a, void i(t))
                    }
                    if (o)return !0;
                    var d = n.createTag(e);
                    if (null != d) {
                        var p = n.option(d);
                        p.attr("data-select2-tag", !0), n.addOptions([p]), n.insertTag(a, d)
                    }
                    t.results = a, i(t)
                }

                var n = this;
                return this._removeOldTags(), null == e.term || null != e.page ? void t.call(this, e, i) : void t.call(this, e, s)
            }, e.prototype.createTag = function (e, i) {
                var s = t.trim(i.term);
                return "" === s ? null : {id: s, text: s}
            }, e.prototype.insertTag = function (t, e, i) {
                e.unshift(i)
            }, e.prototype._removeOldTags = function (e) {
                var i = (this._lastTag, this.$element.find("option[data-select2-tag]"));
                i.each(function () {
                    this.selected || t(this).remove()
                })
            }, e
        }), e.define("select2/data/tokenizer", ["jquery"], function (t) {
            function e(t, e, i) {
                var s = i.get("tokenizer");
                void 0 !== s && (this.tokenizer = s), t.call(this, e, i)
            }

            return e.prototype.bind = function (t, e, i) {
                t.call(this, e, i), this.$search = e.dropdown.$search || e.selection.$search || i.find(".select2-search__field")
            }, e.prototype.query = function (t, e, i) {
                function s(t) {
                    n.trigger("select", {data: t})
                }

                var n = this;
                e.term = e.term || "";
                var o = this.tokenizer(e, this.options, s);
                o.term !== e.term && (this.$search.length && (this.$search.val(o.term), this.$search.focus()), e.term = o.term), t.call(this, e, i)
            }, e.prototype.tokenizer = function (e, i, s, n) {
                for (var o = s.get("tokenSeparators") || [], a = i.term, r = 0, h = this.createTag || function (t) {
                        return {id: t.term, text: t.term}
                    }; r < a.length;) {
                    var l = a[r];
                    if (-1 !== t.inArray(l, o)) {
                        var c = a.substr(0, r), d = t.extend({}, i, {term: c}), p = h(d);
                        null != p ? (n(p), a = a.substr(r + 1) || "", r = 0) : r++
                    } else r++
                }
                return {term: a}
            }, e
        }), e.define("select2/data/minimumInputLength", [], function () {
            function t(t, e, i) {
                this.minimumInputLength = i.get("minimumInputLength"), t.call(this, e, i)
            }

            return t.prototype.query = function (t, e, i) {
                return e.term = e.term || "", e.term.length < this.minimumInputLength ? void this.trigger("results:message", {
                    message: "inputTooShort",
                    args: {minimum: this.minimumInputLength, input: e.term, params: e}
                }) : void t.call(this, e, i)
            }, t
        }), e.define("select2/data/maximumInputLength", [], function () {
            function t(t, e, i) {
                this.maximumInputLength = i.get("maximumInputLength"), t.call(this, e, i)
            }

            return t.prototype.query = function (t, e, i) {
                return e.term = e.term || "", this.maximumInputLength > 0 && e.term.length > this.maximumInputLength ? void this.trigger("results:message", {
                    message: "inputTooLong",
                    args: {maximum: this.maximumInputLength, input: e.term, params: e}
                }) : void t.call(this, e, i)
            }, t
        }), e.define("select2/data/maximumSelectionLength", [], function () {
            function t(t, e, i) {
                this.maximumSelectionLength = i.get("maximumSelectionLength"), t.call(this, e, i)
            }

            return t.prototype.query = function (t, e, i) {
                var s = this;
                this.current(function (n) {
                    var o = null != n ? n.length : 0;
                    return s.maximumSelectionLength > 0 && o >= s.maximumSelectionLength ? void s.trigger("results:message", {
                        message: "maximumSelected",
                        args: {maximum: s.maximumSelectionLength}
                    }) : void t.call(s, e, i)
                })
            }, t
        }), e.define("select2/dropdown", ["jquery", "./utils"], function (t, e) {
            function i(t, e) {
                this.$element = t, this.options = e, i.__super__.constructor.call(this)
            }

            return e.Extend(i, e.Observable), i.prototype.render = function () {
                var e = t('<span class="select2-dropdown"><span class="select2-results"></span></span>');
                return e.attr("dir", this.options.get("dir")), this.$dropdown = e, e
            }, i.prototype.bind = function () {
            }, i.prototype.position = function (t, e) {
            }, i.prototype.destroy = function () {
                this.$dropdown.remove()
            }, i
        }), e.define("select2/dropdown/search", ["jquery", "../utils"], function (t, e) {
            function i() {
            }

            return i.prototype.render = function (e) {
                var i = e.call(this), s = t('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" /></span>');
                return this.$searchContainer = s, this.$search = s.find("input"), i.prepend(s), i
            }, i.prototype.bind = function (e, i, s) {
                var n = this;
                e.call(this, i, s), this.$search.on("keydown", function (t) {
                    n.trigger("keypress", t), n._keyUpPrevented = t.isDefaultPrevented()
                }), this.$search.on("input", function (e) {
                    t(this).off("keyup")
                }), this.$search.on("keyup input", function (t) {
                    n.handleSearch(t)
                }), i.on("open", function () {
                    n.$search.attr("tabindex", 0), n.$search.focus(), window.setTimeout(function () {
                        n.$search.focus()
                    }, 0)
                }), i.on("close", function () {
                    n.$search.attr("tabindex", -1), n.$search.val("")
                }), i.on("results:all", function (t) {
                    if (null == t.query.term || "" === t.query.term) {
                        var e = n.showSearch(t);
                        e ? n.$searchContainer.removeClass("select2-search--hide") : n.$searchContainer.addClass("select2-search--hide")
                    }
                })
            }, i.prototype.handleSearch = function (t) {
                if (!this._keyUpPrevented) {
                    var e = this.$search.val();
                    this.trigger("query", {term: e})
                }
                this._keyUpPrevented = !1
            }, i.prototype.showSearch = function (t, e) {
                return !0
            }, i
        }), e.define("select2/dropdown/hidePlaceholder", [], function () {
            function t(t, e, i, s) {
                this.placeholder = this.normalizePlaceholder(i.get("placeholder")), t.call(this, e, i, s)
            }

            return t.prototype.append = function (t, e) {
                e.results = this.removePlaceholder(e.results), t.call(this, e)
            }, t.prototype.normalizePlaceholder = function (t, e) {
                return "string" == typeof e && (e = {id: "", text: e}), e
            }, t.prototype.removePlaceholder = function (t, e) {
                for (var i = e.slice(0), s = e.length - 1; s >= 0; s--) {
                    var n = e[s];
                    this.placeholder.id === n.id && i.splice(s, 1)
                }
                return i
            }, t
        }), e.define("select2/dropdown/infiniteScroll", ["jquery"], function (t) {
            function e(t, e, i, s) {
                this.lastParams = {}, t.call(this, e, i, s), this.$loadingMore = this.createLoadingMore(), this.loading = !1
            }

            return e.prototype.append = function (t, e) {
                this.$loadingMore.remove(), this.loading = !1, t.call(this, e), this.showLoadingMore(e) && this.$results.append(this.$loadingMore)
            }, e.prototype.bind = function (e, i, s) {
                var n = this;
                e.call(this, i, s), i.on("query", function (t) {
                    n.lastParams = t, n.loading = !0
                }), i.on("query:append", function (t) {
                    n.lastParams = t, n.loading = !0
                }), this.$results.on("scroll", function () {
                    var e = t.contains(document.documentElement, n.$loadingMore[0]);
                    if (!n.loading && e) {
                        var i = n.$results.offset().top + n.$results.outerHeight(!1), s = n.$loadingMore.offset().top + n.$loadingMore.outerHeight(!1);
                        i + 50 >= s && n.loadMore()
                    }
                })
            }, e.prototype.loadMore = function () {
                this.loading = !0;
                var e = t.extend({}, {page: 1}, this.lastParams);
                e.page++, this.trigger("query:append", e)
            }, e.prototype.showLoadingMore = function (t, e) {
                return e.pagination && e.pagination.more
            }, e.prototype.createLoadingMore = function () {
                var e = t('<li class="select2-results__option select2-results__option--load-more"role="treeitem" aria-disabled="true"></li>'), i = this.options.get("translations").get("loadingMore");
                return e.html(i(this.lastParams)), e
            }, e
        }), e.define("select2/dropdown/attachBody", ["jquery", "../utils"], function (t, e) {
            function i(e, i, s) {
                this.$dropdownParent = s.get("dropdownParent") || t(document.body), e.call(this, i, s)
            }

            return i.prototype.bind = function (t, e, i) {
                var s = this, n = !1;
                t.call(this, e, i), e.on("open", function () {
                    s._showDropdown(), s._attachPositioningHandler(e), n || (n = !0, e.on("results:all", function () {
                        s._positionDropdown(), s._resizeDropdown()
                    }), e.on("results:append", function () {
                        s._positionDropdown(), s._resizeDropdown()
                    }))
                }), e.on("close", function () {
                    s._hideDropdown(), s._detachPositioningHandler(e)
                }), this.$dropdownContainer.on("mousedown", function (t) {
                    t.stopPropagation()
                })
            }, i.prototype.destroy = function (t) {
                t.call(this), this.$dropdownContainer.remove()
            }, i.prototype.position = function (t, e, i) {
                e.attr("class", i.attr("class")), e.removeClass("select2"), e.addClass("select2-container--open"), e.css({
                    position: "absolute",
                    top: -999999
                }), this.$container = i
            }, i.prototype.render = function (e) {
                var i = t("<span></span>"), s = e.call(this);
                return i.append(s), this.$dropdownContainer = i, i
            }, i.prototype._hideDropdown = function (t) {
                this.$dropdownContainer.detach()
            }, i.prototype._attachPositioningHandler = function (i, s) {
                var n = this, o = "scroll.select2." + s.id, a = "resize.select2." + s.id, r = "orientationchange.select2." + s.id, h = this.$container.parents().filter(e.hasScroll);
                h.each(function () {
                    t(this).data("select2-scroll-position", {x: t(this).scrollLeft(), y: t(this).scrollTop()})
                }), h.on(o, function (e) {
                    var i = t(this).data("select2-scroll-position");
                    t(this).scrollTop(i.y)
                }), t(window).on(o + " " + a + " " + r, function (t) {
                    n._positionDropdown(), n._resizeDropdown()
                })
            }, i.prototype._detachPositioningHandler = function (i, s) {
                var n = "scroll.select2." + s.id, o = "resize.select2." + s.id, a = "orientationchange.select2." + s.id, r = this.$container.parents().filter(e.hasScroll);
                r.off(n), t(window).off(n + " " + o + " " + a)
            }, i.prototype._positionDropdown = function () {
                var e = t(window), i = this.$dropdown.hasClass("select2-dropdown--above"), s = this.$dropdown.hasClass("select2-dropdown--below"), n = null, o = (this.$container.position(), this.$container.offset());
                o.bottom = o.top + this.$container.outerHeight(!1);
                var a = {height: this.$container.outerHeight(!1)};
                a.top = o.top, a.bottom = o.top + a.height;
                var r = {height: this.$dropdown.outerHeight(!1)}, h = {
                    top: e.scrollTop(),
                    bottom: e.scrollTop() + e.height()
                }, l = h.top < o.top - r.height, c = h.bottom > o.bottom + r.height, d = {left: o.left, top: a.bottom};
                if ("static" !== this.$dropdownParent[0].style.position) {
                    var p = this.$dropdownParent.offset();
                    d.top -= p.top, d.left -= p.left
                }
                i || s || (n = "below"), c || !l || i ? !l && c && i && (n = "below") : n = "above", ("above" == n || i && "below" !== n) && (d.top = a.top - r.height), null != n && (this.$dropdown.removeClass("select2-dropdown--below select2-dropdown--above").addClass("select2-dropdown--" + n), this.$container.removeClass("select2-container--below select2-container--above").addClass("select2-container--" + n)), this.$dropdownContainer.css(d)
            }, i.prototype._resizeDropdown = function () {
                var t = {width: this.$container.outerWidth(!1) + "px"};
                this.options.get("dropdownAutoWidth") && (t.minWidth = t.width, t.width = "auto"), this.$dropdown.css(t)
            }, i.prototype._showDropdown = function (t) {
                this.$dropdownContainer.appendTo(this.$dropdownParent), this._positionDropdown(), this._resizeDropdown()
            }, i
        }), e.define("select2/dropdown/minimumResultsForSearch", [], function () {
            function t(e) {
                for (var i = 0, s = 0; s < e.length; s++) {
                    var n = e[s];
                    n.children ? i += t(n.children) : i++
                }
                return i
            }

            function e(t, e, i, s) {
                this.minimumResultsForSearch = i.get("minimumResultsForSearch"), this.minimumResultsForSearch < 0 && (this.minimumResultsForSearch = 1 / 0), t.call(this, e, i, s)
            }

            return e.prototype.showSearch = function (e, i) {
                return t(i.data.results) < this.minimumResultsForSearch ? !1 : e.call(this, i)
            }, e
        }), e.define("select2/dropdown/selectOnClose", [], function () {
            function t() {
            }

            return t.prototype.bind = function (t, e, i) {
                var s = this;
                t.call(this, e, i), e.on("close", function () {
                    s._handleSelectOnClose()
                })
            }, t.prototype._handleSelectOnClose = function () {
                var t = this.getHighlightedResults();
                if (!(t.length < 1)) {
                    var e = t.data("data");
                    null != e.element && e.element.selected || null == e.element && e.selected || this.trigger("select", {data: e})
                }
            }, t
        }), e.define("select2/dropdown/closeOnSelect", [], function () {
            function t() {
            }

            return t.prototype.bind = function (t, e, i) {
                var s = this;
                t.call(this, e, i), e.on("select", function (t) {
                    s._selectTriggered(t)
                }), e.on("unselect", function (t) {
                    s._selectTriggered(t)
                })
            }, t.prototype._selectTriggered = function (t, e) {
                var i = e.originalEvent;
                i && i.ctrlKey || this.trigger("close", {})
            }, t
        }), e.define("select2/i18n/en", [], function () {
            return {
                errorLoading: function () {
                    return "The results could not be loaded."
                }, inputTooLong: function (t) {
                    var e = t.input.length - t.maximum, i = "Please delete " + e + " character";
                    return 1 != e && (i += "s"), i
                }, inputTooShort: function (t) {
                    var e = t.minimum - t.input.length, i = "Please enter " + e + " or more characters";
                    return i
                }, loadingMore: function () {
                    return "Loading more results…"
                }, maximumSelected: function (t) {
                    var e = "You can only select " + t.maximum + " item";
                    return 1 != t.maximum && (e += "s"), e
                }, noResults: function () {
                    return "No results found"
                }, searching: function () {
                    return "Searching…"
                }
            }
        }), e.define("select2/defaults", ["jquery", "require", "./results", "./selection/single", "./selection/multiple", "./selection/placeholder", "./selection/allowClear", "./selection/search", "./selection/eventRelay", "./utils", "./translation", "./diacritics", "./data/select", "./data/array", "./data/ajax", "./data/tags", "./data/tokenizer", "./data/minimumInputLength", "./data/maximumInputLength", "./data/maximumSelectionLength", "./dropdown", "./dropdown/search", "./dropdown/hidePlaceholder", "./dropdown/infiniteScroll", "./dropdown/attachBody", "./dropdown/minimumResultsForSearch", "./dropdown/selectOnClose", "./dropdown/closeOnSelect", "./i18n/en"], function (t, e, i, s, n, o, a, r, h, l, c, d, p, u, f, m, g, v, b, _, y, w, x, C, k, $, S, T, D) {
            function P() {
                this.reset()
            }

            P.prototype.apply = function (d) {
                if (d = t.extend({}, this.defaults, d), null == d.dataAdapter) {
                    if (null != d.ajax ? d.dataAdapter = f : null != d.data ? d.dataAdapter = u : d.dataAdapter = p, d.minimumInputLength > 0 && (d.dataAdapter = l.Decorate(d.dataAdapter, v)), d.maximumInputLength > 0 && (d.dataAdapter = l.Decorate(d.dataAdapter, b)), d.maximumSelectionLength > 0 && (d.dataAdapter = l.Decorate(d.dataAdapter, _)), d.tags && (d.dataAdapter = l.Decorate(d.dataAdapter, m)), (null != d.tokenSeparators || null != d.tokenizer) && (d.dataAdapter = l.Decorate(d.dataAdapter, g)), null != d.query) {
                        var D = e(d.amdBase + "compat/query");
                        d.dataAdapter = l.Decorate(d.dataAdapter, D)
                    }
                    if (null != d.initSelection) {
                        var P = e(d.amdBase + "compat/initSelection");
                        d.dataAdapter = l.Decorate(d.dataAdapter, P)
                    }
                }
                if (null == d.resultsAdapter && (d.resultsAdapter = i, null != d.ajax && (d.resultsAdapter = l.Decorate(d.resultsAdapter, C)), null != d.placeholder && (d.resultsAdapter = l.Decorate(d.resultsAdapter, x)), d.selectOnClose && (d.resultsAdapter = l.Decorate(d.resultsAdapter, S))), null == d.dropdownAdapter) {
                    if (d.multiple)d.dropdownAdapter = y; else {
                        var I = l.Decorate(y, w);
                        d.dropdownAdapter = I
                    }
                    if (0 !== d.minimumResultsForSearch && (d.dropdownAdapter = l.Decorate(d.dropdownAdapter, $)), d.closeOnSelect && (d.dropdownAdapter = l.Decorate(d.dropdownAdapter, T)), null != d.dropdownCssClass || null != d.dropdownCss || null != d.adaptDropdownCssClass) {
                        var M = e(d.amdBase + "compat/dropdownCss");
                        d.dropdownAdapter = l.Decorate(d.dropdownAdapter, M)
                    }
                    d.dropdownAdapter = l.Decorate(d.dropdownAdapter, k)
                }
                if (null == d.selectionAdapter) {
                    if (d.multiple ? d.selectionAdapter = n : d.selectionAdapter = s, null != d.placeholder && (d.selectionAdapter = l.Decorate(d.selectionAdapter, o)), d.allowClear && (d.selectionAdapter = l.Decorate(d.selectionAdapter, a)), d.multiple && (d.selectionAdapter = l.Decorate(d.selectionAdapter, r)), null != d.containerCssClass || null != d.containerCss || null != d.adaptContainerCssClass) {
                        var O = e(d.amdBase + "compat/containerCss");
                        d.selectionAdapter = l.Decorate(d.selectionAdapter, O)
                    }
                    d.selectionAdapter = l.Decorate(d.selectionAdapter, h)
                }
                if ("string" == typeof d.language)if (d.language.indexOf("-") > 0) {
                    var E = d.language.split("-"), A = E[0];
                    d.language = [d.language, A]
                } else d.language = [d.language];
                if (t.isArray(d.language)) {
                    var R = new c;
                    d.language.push("en");
                    for (var F = d.language, j = 0; j < F.length; j++) {
                        var z = F[j], L = {};
                        try {
                            L = c.loadPath(z)
                        } catch (B) {
                            try {
                                z = this.defaults.amdLanguageBase + z, L = c.loadPath(z)
                            } catch (W) {
                                d.debug && window.console && console.warn && console.warn('Select2: The language file for "' + z + '" could not be automatically loaded. A fallback will be used instead.');
                                continue
                            }
                        }
                        R.extend(L)
                    }
                    d.translations = R
                } else {
                    var N = c.loadPath(this.defaults.amdLanguageBase + "en"), H = new c(d.language);
                    H.extend(N), d.translations = H
                }
                return d
            }, P.prototype.reset = function () {
                function e(t) {
                    function e(t) {
                        return d[t] || t
                    }

                    return t.replace(/[^\u0000-\u007E]/g, e)
                }

                function i(s, n) {
                    if ("" === t.trim(s.term))return n;
                    if (n.children && n.children.length > 0) {
                        for (var o = t.extend(!0, {}, n), a = n.children.length - 1; a >= 0; a--) {
                            var r = n.children[a], h = i(s, r);
                            null == h && o.children.splice(a, 1)
                        }
                        return o.children.length > 0 ? o : i(s, o)
                    }
                    var l = e(n.text).toUpperCase(), c = e(s.term).toUpperCase();
                    return l.indexOf(c) > -1 ? n : null
                }

                this.defaults = {
                    amdBase: "./",
                    amdLanguageBase: "./i18n/",
                    closeOnSelect: !0,
                    debug: !1,
                    dropdownAutoWidth: !1,
                    escapeMarkup: l.escapeMarkup,
                    language: D,
                    matcher: i,
                    minimumInputLength: 0,
                    maximumInputLength: 0,
                    maximumSelectionLength: 0,
                    minimumResultsForSearch: 0,
                    selectOnClose: !1,
                    sorter: function (t) {
                        return t
                    },
                    templateResult: function (t) {
                        return t.text
                    },
                    templateSelection: function (t) {
                        return t.text
                    },
                    theme: "default",
                    width: "resolve"
                }
            }, P.prototype.set = function (e, i) {
                var s = t.camelCase(e), n = {};
                n[s] = i;
                var o = l._convertData(n);
                t.extend(this.defaults, o)
            };
            var I = new P;
            return I
        }), e.define("select2/options", ["require", "jquery", "./defaults", "./utils"], function (t, e, i, s) {
            function n(e, n) {
                if (this.options = e, null != n && this.fromElement(n), this.options = i.apply(this.options), n && n.is("input")) {
                    var o = t(this.get("amdBase") + "compat/inputData");
                    this.options.dataAdapter = s.Decorate(this.options.dataAdapter, o)
                }
            }

            return n.prototype.fromElement = function (t) {
                var i = ["select2"];
                null == this.options.multiple && (this.options.multiple = t.prop("multiple")), null == this.options.disabled && (this.options.disabled = t.prop("disabled")), null == this.options.language && (t.prop("lang") ? this.options.language = t.prop("lang").toLowerCase() : t.closest("[lang]").prop("lang") && (this.options.language = t.closest("[lang]").prop("lang"))), null == this.options.dir && (t.prop("dir") ? this.options.dir = t.prop("dir") : t.closest("[dir]").prop("dir") ? this.options.dir = t.closest("[dir]").prop("dir") : this.options.dir = "ltr"), t.prop("disabled", this.options.disabled), t.prop("multiple", this.options.multiple), t.data("select2Tags") && (this.options.debug && window.console && console.warn && console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'), t.data("data", t.data("select2Tags")), t.data("tags", !0)), t.data("ajaxUrl") && (this.options.debug && window.console && console.warn && console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."), t.attr("ajax--url", t.data("ajaxUrl")), t.data("ajax--url", t.data("ajaxUrl")));
                var n = {};
                n = e.fn.jquery && "1." == e.fn.jquery.substr(0, 2) && t[0].dataset ? e.extend(!0, {}, t[0].dataset, t.data()) : t.data();
                var o = e.extend(!0, {}, n);
                o = s._convertData(o);
                for (var a in o)e.inArray(a, i) > -1 || (e.isPlainObject(this.options[a]) ? e.extend(this.options[a], o[a]) : this.options[a] = o[a]);
                return this
            }, n.prototype.get = function (t) {
                return this.options[t]
            }, n.prototype.set = function (t, e) {
                this.options[t] = e
            }, n
        }), e.define("select2/core", ["jquery", "./options", "./utils", "./keys"], function (t, e, i, s) {
            var n = function (t, i) {
                null != t.data("select2") && t.data("select2").destroy(), this.$element = t, this.id = this._generateId(t), i = i || {}, this.options = new e(i, t), n.__super__.constructor.call(this);
                var s = t.attr("tabindex") || 0;
                t.data("old-tabindex", s), t.attr("tabindex", "-1");
                var o = this.options.get("dataAdapter");
                this.dataAdapter = new o(t, this.options);
                var a = this.render();
                this._placeContainer(a);
                var r = this.options.get("selectionAdapter");
                this.selection = new r(t, this.options), this.$selection = this.selection.render(), this.selection.position(this.$selection, a);
                var h = this.options.get("dropdownAdapter");
                this.dropdown = new h(t, this.options), this.$dropdown = this.dropdown.render(), this.dropdown.position(this.$dropdown, a);
                var l = this.options.get("resultsAdapter");
                this.results = new l(t, this.options, this.dataAdapter), this.$results = this.results.render(), this.results.position(this.$results, this.$dropdown);
                var c = this;
                this._bindAdapters(), this._registerDomEvents(), this._registerDataEvents(), this._registerSelectionEvents(), this._registerDropdownEvents(), this._registerResultsEvents(), this._registerEvents(), this.dataAdapter.current(function (t) {
                    c.trigger("selection:update", {data: t})
                }), t.addClass("select2-hidden-accessible"), t.attr("aria-hidden", "true"), this._syncAttributes(), t.data("select2", this)
            };
            return i.Extend(n, i.Observable), n.prototype._generateId = function (t) {
                var e = "";
                return e = null != t.attr("id") ? t.attr("id") : null != t.attr("name") ? t.attr("name") + "-" + i.generateChars(2) : i.generateChars(4), e = "select2-" + e
            }, n.prototype._placeContainer = function (t) {
                t.insertAfter(this.$element);
                var e = this._resolveWidth(this.$element, this.options.get("width"));
                null != e && t.css("width", e)
            }, n.prototype._resolveWidth = function (t, e) {
                var i = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;
                if ("resolve" == e) {
                    var s = this._resolveWidth(t, "style");
                    return null != s ? s : this._resolveWidth(t, "element")
                }
                if ("element" == e) {
                    var n = t.outerWidth(!1);
                    return 0 >= n ? "auto" : n + "px"
                }
                if ("style" == e) {
                    var o = t.attr("style");
                    if ("string" != typeof o)return null;
                    for (var a = o.split(";"), r = 0, h = a.length; h > r; r += 1) {
                        var l = a[r].replace(/\s/g, ""), c = l.match(i);
                        if (null !== c && c.length >= 1)return c[1]
                    }
                    return null
                }
                return e
            }, n.prototype._bindAdapters = function () {
                this.dataAdapter.bind(this, this.$container), this.selection.bind(this, this.$container), this.dropdown.bind(this, this.$container), this.results.bind(this, this.$container)
            }, n.prototype._registerDomEvents = function () {
                var e = this;
                this.$element.on("change.select2", function () {
                    e.dataAdapter.current(function (t) {
                        e.trigger("selection:update", {data: t})
                    })
                }), this._sync = i.bind(this._syncAttributes, this), this.$element[0].attachEvent && this.$element[0].attachEvent("onpropertychange", this._sync);
                var s = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
                null != s ? (this._observer = new s(function (i) {
                    t.each(i, e._sync)
                }), this._observer.observe(this.$element[0], {
                    attributes: !0,
                    subtree: !1
                })) : this.$element[0].addEventListener && this.$element[0].addEventListener("DOMAttrModified", e._sync, !1)
            }, n.prototype._registerDataEvents = function () {
                var t = this;
                this.dataAdapter.on("*", function (e, i) {
                    t.trigger(e, i)
                })
            }, n.prototype._registerSelectionEvents = function () {
                var e = this, i = ["toggle", "focus"];
                this.selection.on("toggle", function () {
                    e.toggleDropdown()
                }), this.selection.on("focus", function (t) {
                    e.focus(t)
                }), this.selection.on("*", function (s, n) {
                    -1 === t.inArray(s, i) && e.trigger(s, n)
                })
            }, n.prototype._registerDropdownEvents = function () {
                var t = this;
                this.dropdown.on("*", function (e, i) {
                    t.trigger(e, i)
                })
            }, n.prototype._registerResultsEvents = function () {
                var t = this;
                this.results.on("*", function (e, i) {
                    t.trigger(e, i)
                })
            }, n.prototype._registerEvents = function () {
                var t = this;
                this.on("open", function () {
                    t.$container.addClass("select2-container--open")
                }), this.on("close", function () {
                    t.$container.removeClass("select2-container--open")
                }), this.on("enable", function () {
                    t.$container.removeClass("select2-container--disabled")
                }), this.on("disable", function () {
                    t.$container.addClass("select2-container--disabled")
                }), this.on("blur", function () {
                    t.$container.removeClass("select2-container--focus")
                }), this.on("query", function (e) {
                    t.isOpen() || t.trigger("open", {}), this.dataAdapter.query(e, function (i) {
                        t.trigger("results:all", {data: i, query: e})
                    })
                }), this.on("query:append", function (e) {
                    this.dataAdapter.query(e, function (i) {
                        t.trigger("results:append", {data: i, query: e})
                    })
                }), this.on("keypress", function (e) {
                    var i = e.which;
                    t.isOpen() ? i === s.ESC || i === s.TAB || i === s.UP && e.altKey ? (t.close(), e.preventDefault()) : i === s.ENTER ? (t.trigger("results:select", {}), e.preventDefault()) : i === s.SPACE && e.ctrlKey ? (t.trigger("results:toggle", {}), e.preventDefault()) : i === s.UP ? (t.trigger("results:previous", {}), e.preventDefault()) : i === s.DOWN && (t.trigger("results:next", {}), e.preventDefault()) : (i === s.ENTER || i === s.SPACE || i === s.DOWN && e.altKey) && (t.open(), e.preventDefault())
                })
            }, n.prototype._syncAttributes = function () {
                this.options.set("disabled", this.$element.prop("disabled")), this.options.get("disabled") ? (this.isOpen() && this.close(), this.trigger("disable", {})) : this.trigger("enable", {})
            }, n.prototype.trigger = function (t, e) {
                var i = n.__super__.trigger, s = {
                    open: "opening",
                    close: "closing",
                    select: "selecting",
                    unselect: "unselecting"
                };
                if (void 0 === e && (e = {}), t in s) {
                    var o = s[t], a = {prevented: !1, name: t, args: e};
                    if (i.call(this, o, a), a.prevented)return void(e.prevented = !0)
                }
                i.call(this, t, e)
            }, n.prototype.toggleDropdown = function () {
                this.options.get("disabled") || (this.isOpen() ? this.close() : this.open())
            }, n.prototype.open = function () {
                this.isOpen() || this.trigger("query", {})
            }, n.prototype.close = function () {
                this.isOpen() && this.trigger("close", {})
            }, n.prototype.isOpen = function () {
                return this.$container.hasClass("select2-container--open")
            }, n.prototype.hasFocus = function () {
                return this.$container.hasClass("select2-container--focus")
            }, n.prototype.focus = function (t) {
                this.hasFocus() || (this.$container.addClass("select2-container--focus"), this.trigger("focus", {}))
            }, n.prototype.enable = function (t) {
                this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'), (null == t || 0 === t.length) && (t = [!0]);
                var e = !t[0];
                this.$element.prop("disabled", e)
            }, n.prototype.data = function () {
                this.options.get("debug") && arguments.length > 0 && window.console && console.warn && console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');
                var t = [];
                return this.dataAdapter.current(function (e) {
                    t = e
                }), t
            }, n.prototype.val = function (e) {
                if (this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'), null == e || 0 === e.length)return this.$element.val();
                var i = e[0];
                t.isArray(i) && (i = t.map(i, function (t) {
                    return t.toString()
                })), this.$element.val(i).trigger("change")
            }, n.prototype.destroy = function () {
                this.$container.remove(), this.$element[0].detachEvent && this.$element[0].detachEvent("onpropertychange", this._sync), null != this._observer ? (this._observer.disconnect(), this._observer = null) : this.$element[0].removeEventListener && this.$element[0].removeEventListener("DOMAttrModified", this._sync, !1), this._sync = null, this.$element.off(".select2"), this.$element.attr("tabindex", this.$element.data("old-tabindex")), this.$element.removeClass("select2-hidden-accessible"), this.$element.attr("aria-hidden", "false"), this.$element.removeData("select2"), this.dataAdapter.destroy(), this.selection.destroy(), this.dropdown.destroy(), this.results.destroy(), this.dataAdapter = null, this.selection = null, this.dropdown = null, this.results = null
            }, n.prototype.render = function () {
                var e = t('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');
                return e.attr("dir", this.options.get("dir")), this.$container = e, this.$container.addClass("select2-container--" + this.options.get("theme")), e.data("element", this.$element), e
            }, n
        }), e.define("jquery-mousewheel", ["jquery"], function (t) {
            return t
        }), e.define("jquery.select2", ["jquery", "jquery-mousewheel", "./select2/core", "./select2/defaults"], function (t, e, i, s) {
            if (null == t.fn.select2) {
                var n = ["open", "close", "destroy"];
                t.fn.select2 = function (e) {
                    if (e = e || {}, "object" == typeof e)return this.each(function () {
                        var s = t.extend(!0, {}, e);
                        new i(t(this), s)
                    }), this;
                    if ("string" == typeof e) {
                        var s;
                        return this.each(function () {
                            var i = t(this).data("select2");
                            null == i && window.console && console.error && console.error("The select2('" + e + "') method was called on an element that is not using Select2.");
                            var n = Array.prototype.slice.call(arguments, 1);
                            s = i[e].apply(i, n)
                        }), t.inArray(e, n) > -1 ? this : s
                    }
                    throw new Error("Invalid arguments for Select2: " + e)
                }
            }
            return null == t.fn.select2.defaults && (t.fn.select2.defaults = s), i
        }), {define: e.define, require: e.require}
    }(), i = e.require("jquery.select2");
    return t.fn.select2.amd = e, i
}), function () {
    if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd)var t = jQuery.fn.select2.amd;
    return t.define("select2/i18n/ru", [], function () {
        function t(t, e, i, s) {
            return 5 > t % 10 && t % 10 > 0 && 5 > t % 100 || t % 100 > 20 ? t % 10 > 1 ? i : e : s
        }

        return {
            errorLoading: function () {
                return "Невозможно загрузить результаты"
            }, inputTooLong: function (e) {
                var i = e.input.length - e.maximum, s = "Пожалуйста, введите на " + i + " символ";
                return s += t(i, "", "a", "ов"), s += " меньше"
            }, inputTooShort: function (e) {
                var i = e.minimum - e.input.length, s = "Пожалуйста, введите еще хотя бы " + i + " символ";
                return s += t(i, "", "a", "ов")
            }, loadingMore: function () {
                return "Загрузка данных…"
            }, maximumSelected: function (e) {
                var i = "Вы можете выбрать не более " + e.maximum + " элемент";
                return i += t(e.maximum, "", "a", "ов")
            }, noResults: function () {
                return "Совпадений не найдено"
            }, searching: function () {
                return "Поиск…"
            }
        }
    }), {define: t.define, require: t.require}
}(), !function (t) {
    function e(t, e) {
        return e.autoStop && (e.circular ? e.autoStop : Math.min(t, e.autoStop))
    }

    function i(t) {
        this.id && (this.id += t)
    }

    t.jCarouselLite = {
        version: "1.9.3",
        curr: 0
    }, t.fn.anim = "undefined" != typeof t.fn.velocity ? t.fn.velocity : t.fn.animate, t.fn.jCarouselLite = function (s) {
        var n = t.extend(!0, {}, t.fn.jCarouselLite.defaults, s), o = Math.ceil, a = Math.abs;
        return this.each(function () {
            function s() {
                return u.slice(m).slice(0, O)
            }

            function r(e, i) {
                if (w)return !1;
                i = i || {};
                var o = m, a = e > m, r = "undefined" != typeof i.speed ? i.speed : n.speed, h = i.offset || 0;
                return n.beforeStart && n.beforeStart.call(T, s(), a, i), n.circular ? (e > m && e > f - O ? (e -= m, m %= I, e = m + e, D.css(x, -m * y.liSize - h)) : m > e && 0 > e && (m += I, e += I, D.css(x, -m * y.liSize - h)), m = e + e % 1) : (0 > e ? e = 0 : e > f - E && (e = f - E), m = e, 0 === m && n.first && n.first.call(this, s(), a), m === f - E && n.last && n.last.call(this, s(), a), n.btnPrev && n.$btnPrev.toggleClass(n.btnDisabledClass, 0 === m), n.btnNext && n.$btnNext.toggleClass(n.btnDisabledClass, m === f - E)), U(m, j), t.jCarouselLite.curr = m, o !== m || i.force ? (w = !0, C[x] = -(m * y.liSize), D.anim(C, r, n.easing, function () {
                    n.afterEnd && n.afterEnd.call(T, s(), a, i), w = !1
                }), m) : (n.afterEnd && n.afterEnd.call(T, s(), a, i), m)
            }

            var h, l, c, d, p, u, f, m, g, v, b, _ = "ontouchend"in document, y = {
                div: {},
                ul: {},
                li: {}
            }, w = !1, x = n.vertical ? "top" : "left", C = {}, k = n.vertical ? "height" : "width", $ = n.vertical ? "outerHeight" : "outerWidth", S = this, T = t(this), D = T.find(n.containerSelector).eq(0), P = D.children(n.itemSelector), I = P.length, M = n.visible, O = o(M), E = Math.floor(M), A = Math.min(n.start, I - 1), R = 1, F = 0, j = {}, z = {}, L = {}, B = n.vertical ? "y" : "x", W = n.vertical ? "x" : "y", N = n.init.call(this, n, P);
            if (N !== !1) {
                var H = function () {
                    h && h.length && (h.remove(), l.remove()), P = D.children(n.liSelector), I = P.length, h = P.slice(I - O).clone(!0).each(i), l = P.slice(0, O).clone(!0).each(i), D.prepend(h).append(l), u = D.children(n.liSelector), f = u.length
                };
                T.data("dirjc", R), T.data(x + "jc", T.css(x)), n.circular ? (H(), A += O, F = O) : (u = D.children(n.liSelector), f = u.length), n.btnGo && n.btnGo.length && (b = t(t.isArray(n.btnGo) && "string" == typeof n.btnGo[0] ? n.btnGo.join() : n.btnGo), b.each(function (e) {
                    t(this).bind("click.jc", function (t) {
                        t.preventDefault();
                        var i = {btnGo: this, btnGoIndex: e};
                        return r(n.circular ? M + e : e, i)
                    })
                }), j.go = 1);
                var U = function (t, e) {
                    t = o(t), u.filter("." + n.activeClass).removeClass(n.activeClass), u.eq(t).addClass(n.activeClass);
                    var i = (t - F) % I, s = i + E;
                    return e.go && (b.removeClass(n.activeClass).removeClass(n.visibleClass), b.eq(i).addClass(n.activeClass), b.slice(i, i + E).addClass(n.visibleClass), s > b.length && b.slice(0, s - b.length).addClass(n.visibleClass)), e.pager && (c.removeClass(n.activeClass), c.eq(o(i / M)).addClass(n.activeClass)), i
                };
                m = A, t.jCarouselLite.curr = m;
                var q = function (t) {
                    var e, i, s;
                    return t ? (y.div[k] = "", y.li = {
                        width: "",
                        height: ""
                    }, y) : (e = u[$](!0), i = e * f, s = e * M, y.div[k] = s + "px", y.ul[k] = i + "px", y.ul[x] = -(m * e) + "px", y.li = {
                        width: u.width(),
                        height: u.height()
                    }, y.liSize = e, y)
                }, V = function (e) {
                    var i, s, o = {
                        div: {visibility: "visible", position: "relative", zIndex: 2, left: "0"},
                        ul: {margin: "0", padding: "0", position: "relative", listStyleType: "none", zIndex: 1},
                        li: {overflow: n.vertical ? "hidden" : "visible", "float": n.vertical ? "none" : "left"}
                    };
                    e && (i = q(!0), T.css(i.div), D.css(i.ul), u.css(i.li)), i = q(), n.autoCSS && t.extend(!0, i, o), n.autoWidth && (s = parseInt(T.css(k), 10), y.liSize = s / n.visible, i.li[k] = y.liSize - (u[$](!0) - parseInt(u.css(k), 10)), i.ul[k] = y.liSize * f + "px", i.ul[x] = -(m * y.liSize) + "px", i.div[k] = s), n.autoCSS && (u.css(i.li), D.css(i.ul), T.css(i.div))
                };
                V();
                var Z = 0, G = e(I, n), Y = "number" == typeof n.auto ? n.auto : n.scroll, Q = function () {
                    S.setAutoAdvance = setTimeout(function () {
                        (!G || G > Z) && (R = T.data("dirjc"), r(m + R * Y, {auto: !0}), Z++, Q())
                    }, n.timeout)
                };
                if (t.each(["btnPrev", "btnNext"], function (e, i) {
                        n[i] && (n["$" + i] = t.isFunction(n[i]) ? n[i].call(T[0]) : t(n[i]), n["$" + i].bind("click.jc", function (t) {
                            t.preventDefault();
                            var i = 0 === e ? m - n.scroll : m + n.scroll;
                            return n.directional && T.data("dirjc", e ? 1 : -1), r(i)
                        }))
                    }), n.circular || (n.btnPrev && 0 === A && n.$btnPrev.addClass(n.btnDisabledClass), n.btnNext && A + E >= f && n.$btnNext.addClass(n.btnDisabledClass)), n.autoPager) {
                    d = o(I / M), c = [];
                    for (var X = 0; d > X; X++)c.push('<li><a href="#">' + (X + 1) + "</a></li>");
                    c.length > 1 && (c = t("<ul>" + c.join("") + "</ul>").appendTo(n.autoPager).find("li"), c.find("a").each(function (e) {
                        t(this).bind("click.jc", function (t) {
                            t.preventDefault();
                            var i = e * M;
                            return n.circular && (i += M), r(i)
                        })
                    }), j.pager = 1)
                }
                U(A, j), n.mouseWheel && T.mousewheel && T.bind("mousewheel.jc", function (t, e) {
                    return r(e > 0 ? m - n.scroll : m + n.scroll)
                }), n.pause && n.auto && !_ && T.bind("mouseenter.jc", function () {
                    T.trigger("pauseCarousel.jc")
                }).bind("mouseleave.jc", function () {
                    T.trigger("resumeCarousel.jc")
                }), n.auto && Q(), t.jCarouselLite.vis = s, T.bind("go.jc", function (t, e, i) {
                    "undefined" == typeof e && (e = "+=1");
                    var s = "string" == typeof e && /(\+=|-=)(\d+)/.exec(e);
                    s ? e = "-=" === s[1] ? m - 1 * s[2] : m + 1 * s[2] : e += A, r(e, i)
                }).bind("startCarousel.jc", function () {
                    clearTimeout(S.setAutoAdvance), S.setAutoAdvance = void 0, T.trigger("go", "+=" + n.scroll), Q(), T.removeData("pausedjc").removeData("stoppedjc")
                }).bind("resumeCarousel.jc", function (t, e) {
                    if (!S.setAutoAdvance) {
                        clearTimeout(S.setAutoAdvance), S.setAutoAdvance = void 0;
                        var i = T.data("stoppedjc");
                        (e || !i) && (Q(), T.removeData("pausedjc"), i && T.removeData("stoppedjc"))
                    }
                }).bind("pauseCarousel.jc", function () {
                    clearTimeout(S.setAutoAdvance), S.setAutoAdvance = void 0, T.data("pausedjc", !0)
                }).bind("stopCarousel.jc", function () {
                    clearTimeout(S.setAutoAdvance), S.setAutoAdvance = void 0, T.data("stoppedjc", !0)
                }).bind("refreshCarousel.jc", function (t, e) {
                    e && n.circular && H(), V(n.autoCSS)
                }).bind("endCarousel.jc", function () {
                    S.setAutoAdvance && (clearTimeout(S.setAutoAdvance), S.setAutoAdvance = void 0), n.btnPrev && n.$btnPrev.addClass(n.btnDisabledClass).unbind(".jc"), n.btnNext && n.$btnNext.addClass(n.btnDisabledClass).unbind(".jc"), n.btnGo && t.each(n.btnGo, function (e, i) {
                        t(i).unbind(".jc")
                    }), n.circular && (u.slice(0, O).remove(), u.slice(-O).remove()), t.each([x + "jc", "pausedjc", "stoppedjc", "dirjc"], function (t, e) {
                        T.removeData(e)
                    }), T.unbind(".jc")
                }), v = {
                    touchstart: function (t) {
                        L.x = 0, L.y = 0, z.x = t.targetTouches[0].pageX, z.y = t.targetTouches[0].pageY, z[x] = parseFloat(D.css(x)), z.time = +new Date
                    }, touchmove: function (t) {
                        var e = t.targetTouches.length;
                        1 === e ? (L.x = t.targetTouches[0].pageX, L.y = t.targetTouches[0].pageY, C[x] = z[x] + (L[B] - z[B]), D.css(C), n.preventTouchWindowScroll && t.preventDefault()) : (L.x = z.x, L.y = z.y)
                    }, touchend: function () {
                        if (L.x) {
                            var t = z[B] - L[B], e = a(t), i = e > n.swipeThresholds[B], s = a(z[W] - L[W]) < n.swipeThresholds[W], o = +new Date - z.time, r = o < n.swipeThresholds.time, h = t > 0 ? "+=" : "-=", l = h + n.scroll, c = {force: !0};
                            r && i && s ? c.speed = n.speed / 2 : !r && e < y.liSize / 2 || !i || r && !s ? l = "+=0" : !r && e > y.liSize / 2 && (l = Math.round(e / y.liSize), l = h + (l > n.visible ? n.visible : l), c.offset = t), T.trigger("go.jc", [l, c]), L = {}
                        }
                    }, handle: function (t) {
                        t = t.originalEvent, v[t.type](t)
                    }
                }, _ && n.swipe && T.bind("touchstart.jc touchmove.jc touchend.jc", v.handle), n.responsive && (g = n.autoCSS, t(window).bind("resize", function () {
                    g && (D.width(2 * D.width()), g = !1), clearTimeout(p), p = setTimeout(function () {
                        T.trigger("refreshCarousel.jc", [!0]), g = n.autoCSS
                    }, 100)
                }))
            }
        }), this
    }, t.fn.jCarouselLite.defaults = {
        containerSelector: "ul",
        itemSelector: "li",
        btnPrev: null,
        btnNext: null,
        btnGo: null,
        autoPager: null,
        btnDisabledClass: "disabled",
        activeClass: "active",
        visibleClass: "vis",
        mouseWheel: !1,
        speed: 200,
        easing: null,
        timeout: 4e3,
        auto: !1,
        directional: !1,
        autoStop: !1,
        pause: !0,
        vertical: !1,
        circular: !0,
        visible: 3,
        start: 0,
        scroll: 1,
        autoCSS: !0,
        responsive: !1,
        autoWidth: !1,
        swipe: !0,
        swipeThresholds: {x: 80, y: 40, time: 150},
        preventTouchWindowScroll: !0,
        init: function () {
        },
        first: null,
        last: null,
        beforeStart: null,
        afterEnd: null
    }
}(jQuery), function () {
    "use strict";
    var t = this, e = t.Chart, i = function (t) {
        this.canvas = t.canvas, this.ctx = t;
        var e = function (t, e) {
            return t["offset" + e] ? t["offset" + e] : document.defaultView.getComputedStyle(t).getPropertyValue(e)
        }, i = this.width = e(t.canvas, "Width"), n = this.height = e(t.canvas, "Height");
        t.canvas.width = i, t.canvas.height = n;
        var i = this.width = t.canvas.width, n = this.height = t.canvas.height;
        return this.aspectRatio = this.width / this.height, s.retinaScale(this), this
    };
    i.defaults = {
        global: {
            animation: !0,
            animationSteps: 60,
            animationEasing: "easeOutQuart",
            showScale: !0,
            scaleOverride: !1,
            scaleSteps: null,
            scaleStepWidth: null,
            scaleStartValue: null,
            scaleLineColor: "rgba(0,0,0,.1)",
            scaleLineWidth: 1,
            scaleShowLabels: !0,
            scaleLabel: "<%=value%>",
            scaleIntegersOnly: !0,
            scaleBeginAtZero: !1,
            scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            scaleFontSize: 12,
            scaleFontStyle: "normal",
            scaleFontColor: "#666",
            responsive: !1,
            maintainAspectRatio: !0,
            showTooltips: !0,
            customTooltips: !1,
            tooltipEvents: ["mousemove", "touchstart", "touchmove", "mouseout"],
            tooltipFillColor: "rgba(0,0,0,0.8)",
            tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            tooltipFontSize: 14,
            tooltipFontStyle: "normal",
            tooltipFontColor: "#fff",
            tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            tooltipTitleFontSize: 14,
            tooltipTitleFontStyle: "bold",
            tooltipTitleFontColor: "#fff",
            tooltipYPadding: 6,
            tooltipXPadding: 6,
            tooltipCaretSize: 8,
            tooltipCornerRadius: 6,
            tooltipXOffset: 10,
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
            multiTooltipTemplate: "<%= value %>",
            multiTooltipKeyBackground: "#fff",
            onAnimationProgress: function () {
            },
            onAnimationComplete: function () {
            }
        }
    }, i.types = {};
    var s = i.helpers = {}, n = s.each = function (t, e, i) {
        var s = Array.prototype.slice.call(arguments, 3);
        if (t)if (t.length === +t.length) {
            var n;
            for (n = 0; n < t.length; n++)e.apply(i, [t[n], n].concat(s))
        } else for (var o in t)e.apply(i, [t[o], o].concat(s))
    }, o = s.clone = function (t) {
        var e = {};
        return n(t, function (i, s) {
            t.hasOwnProperty(s) && (e[s] = i)
        }), e
    }, a = s.extend = function (t) {
        return n(Array.prototype.slice.call(arguments, 1), function (e) {
            n(e, function (i, s) {
                e.hasOwnProperty(s) && (t[s] = i)
            })
        }), t
    }, r = s.merge = function () {
        var t = Array.prototype.slice.call(arguments, 0);
        return t.unshift({}), a.apply(null, t)
    }, h = s.indexOf = function (t, e) {
        if (Array.prototype.indexOf)return t.indexOf(e);
        for (var i = 0; i < t.length; i++)if (t[i] === e)return i;
        return -1
    }, l = (s.where = function (t, e) {
        var i = [];
        return s.each(t, function (t) {
            e(t) && i.push(t)
        }), i
    }, s.findNextWhere = function (t, e, i) {
        i || (i = -1);
        for (var s = i + 1; s < t.length; s++) {
            var n = t[s];
            if (e(n))return n
        }
    }, s.findPreviousWhere = function (t, e, i) {
        i || (i = t.length);
        for (var s = i - 1; s >= 0; s--) {
            var n = t[s];
            if (e(n))return n
        }
    }, s.inherits = function (t) {
        var e = this, i = t && t.hasOwnProperty("constructor") ? t.constructor : function () {
            return e.apply(this, arguments)
        }, s = function () {
            this.constructor = i
        };
        return s.prototype = e.prototype, i.prototype = new s, i.extend = l, t && a(i.prototype, t), i.__super__ = e.prototype, i
    }), c = s.noop = function () {
    }, d = s.uid = function () {
        var t = 0;
        return function () {
            return "chart-" + t++
        }
    }(), p = s.warn = function (t) {
        window.console && "function" == typeof window.console.warn && console.warn(t)
    }, u = s.amd = "function" == typeof define && define.amd, f = s.isNumber = function (t) {
        return !isNaN(parseFloat(t)) && isFinite(t)
    }, m = s.max = function (t) {
        return Math.max.apply(Math, t)
    }, g = s.min = function (t) {
        return Math.min.apply(Math, t)
    }, v = (s.cap = function (t, e, i) {
        if (f(e)) {
            if (t > e)return e
        } else if (f(i) && i > t)return i;
        return t
    }, s.getDecimalPlaces = function (t) {
        return t % 1 !== 0 && f(t) ? t.toString().split(".")[1].length : 0
    }), b = s.radians = function (t) {
        return t * (Math.PI / 180)
    }, _ = (s.getAngleFromPoint = function (t, e) {
        var i = e.x - t.x, s = e.y - t.y, n = Math.sqrt(i * i + s * s), o = 2 * Math.PI + Math.atan2(s, i);
        return 0 > i && 0 > s && (o += 2 * Math.PI), {angle: o, distance: n}
    }, s.aliasPixel = function (t) {
        return t % 2 === 0 ? 0 : .5
    }), y = (s.splineCurve = function (t, e, i, s) {
        var n = Math.sqrt(Math.pow(e.x - t.x, 2) + Math.pow(e.y - t.y, 2)), o = Math.sqrt(Math.pow(i.x - e.x, 2) + Math.pow(i.y - e.y, 2)), a = s * n / (n + o), r = s * o / (n + o);
        return {
            inner: {x: e.x - a * (i.x - t.x), y: e.y - a * (i.y - t.y)},
            outer: {x: e.x + r * (i.x - t.x), y: e.y + r * (i.y - t.y)}
        }
    }, s.calculateOrderOfMagnitude = function (t) {
        return Math.floor(Math.log(t) / Math.LN10)
    }), w = (s.calculateScaleRange = function (t, e, i, s, n) {
        var o = 2, a = Math.floor(e / (1.5 * i)), r = o >= a, h = m(t), l = g(t);
        h === l && (h += .5, l >= .5 && !s ? l -= .5 : h += .5);
        for (var c = Math.abs(h - l), d = y(c), p = Math.ceil(h / (1 * Math.pow(10, d))) * Math.pow(10, d), u = s ? 0 : Math.floor(l / (1 * Math.pow(10, d))) * Math.pow(10, d), f = p - u, v = Math.pow(10, d), b = Math.round(f / v); (b > a || a > 2 * b) && !r;)if (b > a)v *= 2, b = Math.round(f / v), b % 1 !== 0 && (r = !0); else if (n && d >= 0) {
            if (v / 2 % 1 !== 0)break;
            v /= 2, b = Math.round(f / v)
        } else v /= 2, b = Math.round(f / v);
        return r && (b = o, v = f / b), {steps: b, stepValue: v, min: u, max: u + b * v}
    }, s.template = function (t, e) {
        function i(t, e) {
            var i = /\W/.test(t) ? new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};with(obj){p.push('" + t.replace(/[\r\t\n]/g, " ").split("<%").join("	").replace(/((^|%>)[^\t]*)'/g, "$1\r").replace(/\t=(.*?)%>/g, "',$1,'").split("	").join("');").split("%>").join("p.push('").split("\r").join("\\'") + "');}return p.join('');") : s[t] = s[t];
            return e ? i(e) : i
        }

        if (t instanceof Function)return t(e);
        var s = {};
        return i(t, e)
    }), x = (s.generateLabels = function (t, e, i, s) {
        var o = new Array(e);
        return labelTemplateString && n(o, function (e, n) {
            o[n] = w(t, {value: i + s * (n + 1)})
        }), o
    }, s.easingEffects = {
        linear: function (t) {
            return t
        }, easeInQuad: function (t) {
            return t * t
        }, easeOutQuad: function (t) {
            return -1 * t * (t - 2)
        }, easeInOutQuad: function (t) {
            return (t /= .5) < 1 ? .5 * t * t : -.5 * (--t * (t - 2) - 1)
        }, easeInCubic: function (t) {
            return t * t * t
        }, easeOutCubic: function (t) {
            return 1 * ((t = t / 1 - 1) * t * t + 1)
        }, easeInOutCubic: function (t) {
            return (t /= .5) < 1 ? .5 * t * t * t : .5 * ((t -= 2) * t * t + 2)
        }, easeInQuart: function (t) {
            return t * t * t * t
        }, easeOutQuart: function (t) {
            return -1 * ((t = t / 1 - 1) * t * t * t - 1)
        }, easeInOutQuart: function (t) {
            return (t /= .5) < 1 ? .5 * t * t * t * t : -.5 * ((t -= 2) * t * t * t - 2)
        }, easeInQuint: function (t) {
            return 1 * (t /= 1) * t * t * t * t
        }, easeOutQuint: function (t) {
            return 1 * ((t = t / 1 - 1) * t * t * t * t + 1)
        }, easeInOutQuint: function (t) {
            return (t /= .5) < 1 ? .5 * t * t * t * t * t : .5 * ((t -= 2) * t * t * t * t + 2)
        }, easeInSine: function (t) {
            return -1 * Math.cos(t / 1 * (Math.PI / 2)) + 1
        }, easeOutSine: function (t) {
            return 1 * Math.sin(t / 1 * (Math.PI / 2))
        }, easeInOutSine: function (t) {
            return -.5 * (Math.cos(Math.PI * t / 1) - 1)
        }, easeInExpo: function (t) {
            return 0 === t ? 1 : 1 * Math.pow(2, 10 * (t / 1 - 1))
        }, easeOutExpo: function (t) {
            return 1 === t ? 1 : 1 * (-Math.pow(2, -10 * t / 1) + 1)
        }, easeInOutExpo: function (t) {
            return 0 === t ? 0 : 1 === t ? 1 : (t /= .5) < 1 ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (-Math.pow(2, -10 * --t) + 2)
        }, easeInCirc: function (t) {
            return t >= 1 ? t : -1 * (Math.sqrt(1 - (t /= 1) * t) - 1)
        }, easeOutCirc: function (t) {
            return 1 * Math.sqrt(1 - (t = t / 1 - 1) * t)
        }, easeInOutCirc: function (t) {
            return (t /= .5) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1)
        }, easeInElastic: function (t) {
            var e = 1.70158, i = 0, s = 1;
            return 0 === t ? 0 : 1 == (t /= 1) ? 1 : (i || (i = .3), s < Math.abs(1) ? (s = 1, e = i / 4) : e = i / (2 * Math.PI) * Math.asin(1 / s), -(s * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (1 * t - e) * Math.PI / i)))
        }, easeOutElastic: function (t) {
            var e = 1.70158, i = 0, s = 1;
            return 0 === t ? 0 : 1 == (t /= 1) ? 1 : (i || (i = .3), s < Math.abs(1) ? (s = 1, e = i / 4) : e = i / (2 * Math.PI) * Math.asin(1 / s), s * Math.pow(2, -10 * t) * Math.sin(2 * (1 * t - e) * Math.PI / i) + 1)
        }, easeInOutElastic: function (t) {
            var e = 1.70158, i = 0, s = 1;
            return 0 === t ? 0 : 2 == (t /= .5) ? 1 : (i || (i = .3 * 1.5), s < Math.abs(1) ? (s = 1, e = i / 4) : e = i / (2 * Math.PI) * Math.asin(1 / s), 1 > t ? -.5 * s * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (1 * t - e) * Math.PI / i) : s * Math.pow(2, -10 * (t -= 1)) * Math.sin(2 * (1 * t - e) * Math.PI / i) * .5 + 1)
        }, easeInBack: function (t) {
            var e = 1.70158;
            return 1 * (t /= 1) * t * ((e + 1) * t - e)
        }, easeOutBack: function (t) {
            var e = 1.70158;
            return 1 * ((t = t / 1 - 1) * t * ((e + 1) * t + e) + 1)
        }, easeInOutBack: function (t) {
            var e = 1.70158;
            return (t /= .5) < 1 ? .5 * t * t * (((e *= 1.525) + 1) * t - e) : .5 * ((t -= 2) * t * (((e *= 1.525) + 1) * t + e) + 2)
        }, easeInBounce: function (t) {
            return 1 - x.easeOutBounce(1 - t)
        }, easeOutBounce: function (t) {
            return (t /= 1) < 1 / 2.75 ? 7.5625 * t * t : 2 / 2.75 > t ? 1 * (7.5625 * (t -= 1.5 / 2.75) * t + .75) : 2.5 / 2.75 > t ? 1 * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 * (7.5625 * (t -= 2.625 / 2.75) * t + .984375)
        }, easeInOutBounce: function (t) {
            return .5 > t ? .5 * x.easeInBounce(2 * t) : .5 * x.easeOutBounce(2 * t - 1) + .5
        }
    }), C = s.requestAnimFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (t) {
                return window.setTimeout(t, 1e3 / 60)
            }
    }(), k = s.cancelAnimFrame = function () {
        return window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || window.msCancelAnimationFrame || function (t) {
                return window.clearTimeout(t, 1e3 / 60)
            }
    }(), $ = (s.animationLoop = function (t, e, i, s, n, o) {
        var a = 0, r = x[i] || x.linear, h = function () {
            a++;
            var i = a / e, l = r(i);
            t.call(o, l, i, a), s.call(o, l, i), e > a ? o.animationFrame = C(h) : n.apply(o)
        };
        C(h)
    }, s.getRelativePosition = function (t) {
        var e, i, s = t.originalEvent || t, n = t.currentTarget || t.srcElement, o = n.getBoundingClientRect();
        return s.touches ? (e = s.touches[0].clientX - o.left, i = s.touches[0].clientY - o.top) : (e = s.clientX - o.left, i = s.clientY - o.top), {
            x: e,
            y: i
        }
    }, s.addEvent = function (t, e, i) {
        t.addEventListener ? t.addEventListener(e, i) : t.attachEvent ? t.attachEvent("on" + e, i) : t["on" + e] = i
    }), S = s.removeEvent = function (t, e, i) {
        t.removeEventListener ? t.removeEventListener(e, i, !1) : t.detachEvent ? t.detachEvent("on" + e, i) : t["on" + e] = c
    }, T = (s.bindEvents = function (t, e, i) {
        t.events || (t.events = {}), n(e, function (e) {
            t.events[e] = function () {
                i.apply(t, arguments)
            }, $(t.chart.canvas, e, t.events[e])
        })
    }, s.unbindEvents = function (t, e) {
        n(e, function (e, i) {
            S(t.chart.canvas, i, e)
        })
    }), D = s.getMaximumWidth = function (t) {
        var e = t.parentNode;
        return e.clientWidth
    }, P = s.getMaximumHeight = function (t) {
        var e = t.parentNode;
        return e.clientHeight
    }, I = (s.getMaximumSize = s.getMaximumWidth, s.retinaScale = function (t) {
        var e = t.ctx, i = t.canvas.width, s = t.canvas.height;
        window.devicePixelRatio && (e.canvas.style.width = i + "px", e.canvas.style.height = s + "px", e.canvas.height = s * window.devicePixelRatio, e.canvas.width = i * window.devicePixelRatio, e.scale(window.devicePixelRatio, window.devicePixelRatio))
    }), M = s.clear = function (t) {
        t.ctx.clearRect(0, 0, t.width, t.height)
    }, O = s.fontString = function (t, e, i) {
        return e + " " + t + "px " + i
    }, E = s.longestText = function (t, e, i) {
        t.font = e;
        var s = 0;
        return n(i, function (e) {
            var i = t.measureText(e).width;
            s = i > s ? i : s
        }), s
    }, A = s.drawRoundedRectangle = function (t, e, i, s, n, o) {
        t.beginPath(), t.moveTo(e + o, i), t.lineTo(e + s - o, i), t.quadraticCurveTo(e + s, i, e + s, i + o), t.lineTo(e + s, i + n - o), t.quadraticCurveTo(e + s, i + n, e + s - o, i + n), t.lineTo(e + o, i + n), t.quadraticCurveTo(e, i + n, e, i + n - o), t.lineTo(e, i + o), t.quadraticCurveTo(e, i, e + o, i), t.closePath()
    };
    i.instances = {}, i.Type = function (t, e, s) {
        this.options = e, this.chart = s, this.id = d(), i.instances[this.id] = this, e.responsive && this.resize(), this.initialize.call(this, t)
    }, a(i.Type.prototype, {
        initialize: function () {
            return this
        }, clear: function () {
            return M(this.chart), this
        }, stop: function () {
            return k(this.animationFrame), this
        }, resize: function (t) {
            this.stop();
            var e = this.chart.canvas, i = D(this.chart.canvas), s = this.options.maintainAspectRatio ? i / this.chart.aspectRatio : P(this.chart.canvas);
            return e.width = this.chart.width = i, e.height = this.chart.height = s, I(this.chart), "function" == typeof t && t.apply(this, Array.prototype.slice.call(arguments, 1)), this
        }, reflow: c, render: function (t) {
            return t && this.reflow(), this.options.animation && !t ? s.animationLoop(this.draw, this.options.animationSteps, this.options.animationEasing, this.options.onAnimationProgress, this.options.onAnimationComplete, this) : (this.draw(), this.options.onAnimationComplete.call(this)), this
        }, generateLegend: function () {
            return w(this.options.legendTemplate, this)
        }, destroy: function () {
            this.clear(), T(this, this.events);
            var t = this.chart.canvas;
            t.width = this.chart.width, t.height = this.chart.height, t.style.removeProperty ? (t.style.removeProperty("width"), t.style.removeProperty("height")) : (t.style.removeAttribute("width"), t.style.removeAttribute("height")), delete i.instances[this.id]
        }, showTooltip: function (t, e) {
            "undefined" == typeof this.activeElements && (this.activeElements = []);
            var o = function (t) {
                var e = !1;
                return t.length !== this.activeElements.length ? e = !0 : (n(t, function (t, i) {
                    t !== this.activeElements[i] && (e = !0)
                }, this), e)
            }.call(this, t);
            if (o || e) {
                if (this.activeElements = t, this.draw(), this.options.customTooltips && this.options.customTooltips(!1), t.length > 0)if (this.datasets && this.datasets.length > 1) {
                    for (var a, r, l = this.datasets.length - 1; l >= 0 && (a = this.datasets[l].points || this.datasets[l].bars || this.datasets[l].segments, r = h(a, t[0]), -1 === r); l--);
                    var c = [], d = [], p = function () {
                        var t, e, i, n, o, a = [], h = [], l = [];
                        return s.each(this.datasets, function (e) {
                            t = e.points || e.bars || e.segments, t[r] && t[r].hasValue() && a.push(t[r])
                        }), s.each(a, function (t) {
                            h.push(t.x), l.push(t.y), c.push(s.template(this.options.multiTooltipTemplate, t)), d.push({
                                fill: t._saved.fillColor || t.fillColor,
                                stroke: t._saved.strokeColor || t.strokeColor
                            })
                        }, this), o = g(l), i = m(l), n = g(h), e = m(h), {
                            x: n > this.chart.width / 2 ? n : e,
                            y: (o + i) / 2
                        }
                    }.call(this, r);
                    new i.MultiTooltip({
                        x: p.x,
                        y: p.y,
                        xPadding: this.options.tooltipXPadding,
                        yPadding: this.options.tooltipYPadding,
                        xOffset: this.options.tooltipXOffset,
                        fillColor: this.options.tooltipFillColor,
                        textColor: this.options.tooltipFontColor,
                        fontFamily: this.options.tooltipFontFamily,
                        fontStyle: this.options.tooltipFontStyle,
                        fontSize: this.options.tooltipFontSize,
                        titleTextColor: this.options.tooltipTitleFontColor,
                        titleFontFamily: this.options.tooltipTitleFontFamily,
                        titleFontStyle: this.options.tooltipTitleFontStyle,
                        titleFontSize: this.options.tooltipTitleFontSize,
                        cornerRadius: this.options.tooltipCornerRadius,
                        labels: c,
                        legendColors: d,
                        legendColorBackground: this.options.multiTooltipKeyBackground,
                        title: t[0].label,
                        chart: this.chart,
                        ctx: this.chart.ctx,
                        custom: this.options.customTooltips
                    }).draw()
                } else n(t, function (t) {
                    var e = t.tooltipPosition();
                    new i.Tooltip({
                        x: Math.round(e.x),
                        y: Math.round(e.y),
                        xPadding: this.options.tooltipXPadding,
                        yPadding: this.options.tooltipYPadding,
                        fillColor: this.options.tooltipFillColor,
                        textColor: this.options.tooltipFontColor,
                        fontFamily: this.options.tooltipFontFamily,
                        fontStyle: this.options.tooltipFontStyle,
                        fontSize: this.options.tooltipFontSize,
                        caretHeight: this.options.tooltipCaretSize,
                        cornerRadius: this.options.tooltipCornerRadius,
                        text: w(this.options.tooltipTemplate, t),
                        chart: this.chart,
                        custom: this.options.customTooltips
                    }).draw()
                }, this);
                return this
            }
        }, toBase64Image: function () {
            return this.chart.canvas.toDataURL.apply(this.chart.canvas, arguments)
        }
    }), i.Type.extend = function (t) {
        var e = this, s = function () {
            return e.apply(this, arguments)
        };
        if (s.prototype = o(e.prototype), a(s.prototype, t), s.extend = i.Type.extend, t.name || e.prototype.name) {
            var n = t.name || e.prototype.name, h = i.defaults[e.prototype.name] ? o(i.defaults[e.prototype.name]) : {};
            i.defaults[n] = a(h, t.defaults), i.types[n] = s, i.prototype[n] = function (t, e) {
                var o = r(i.defaults.global, i.defaults[n], e || {});
                return new s(t, o, this)
            }
        } else p("Name not provided for this chart, so it hasn't been registered");
        return e
    }, i.Element = function (t) {
        a(this, t), this.initialize.apply(this, arguments), this.save()
    }, a(i.Element.prototype, {
        initialize: function () {
        }, restore: function (t) {
            return t ? n(t, function (t) {
                this[t] = this._saved[t]
            }, this) : a(this, this._saved), this
        }, save: function () {
            return this._saved = o(this), delete this._saved._saved, this
        }, update: function (t) {
            return n(t, function (t, e) {
                this._saved[e] = this[e], this[e] = t
            }, this), this
        }, transition: function (t, e) {
            return n(t, function (t, i) {
                this[i] = (t - this._saved[i]) * e + this._saved[i]
            }, this), this
        }, tooltipPosition: function () {
            return {x: this.x, y: this.y}
        }, hasValue: function () {
            return f(this.value)
        }
    }), i.Element.extend = l, i.Point = i.Element.extend({
        display: !0, inRange: function (t, e) {
            var i = this.hitDetectionRadius + this.radius;
            return Math.pow(t - this.x, 2) + Math.pow(e - this.y, 2) < Math.pow(i, 2)
        }, draw: function () {
            if (this.display) {
                var t = this.ctx;
                t.beginPath(), t.arc(this.x, this.y, this.radius, 0, 2 * Math.PI), t.closePath(), t.strokeStyle = this.strokeColor, t.lineWidth = this.strokeWidth, t.fillStyle = this.fillColor, t.fill(), t.stroke()
            }
        }
    }), i.Arc = i.Element.extend({
        inRange: function (t, e) {
            var i = s.getAngleFromPoint(this, {
                x: t,
                y: e
            }), n = i.angle >= this.startAngle && i.angle <= this.endAngle, o = i.distance >= this.innerRadius && i.distance <= this.outerRadius;
            return n && o
        }, tooltipPosition: function () {
            var t = this.startAngle + (this.endAngle - this.startAngle) / 2, e = (this.outerRadius - this.innerRadius) / 2 + this.innerRadius;
            return {x: this.x + Math.cos(t) * e, y: this.y + Math.sin(t) * e}
        }, draw: function (t) {
            var e = this.ctx;
            e.beginPath(), e.arc(this.x, this.y, this.outerRadius, this.startAngle, this.endAngle), e.arc(this.x, this.y, this.innerRadius, this.endAngle, this.startAngle, !0), e.closePath(), e.strokeStyle = this.strokeColor, e.lineWidth = this.strokeWidth, e.fillStyle = this.fillColor, e.fill(), e.lineJoin = "bevel", this.showStroke && e.stroke()
        }
    }), i.Rectangle = i.Element.extend({
        draw: function () {
            var t = this.ctx, e = this.width / 2, i = this.x - e, s = this.x + e, n = this.base - (this.base - this.y), o = this.strokeWidth / 2;
            this.showStroke && (i += o, s -= o, n += o), t.beginPath(), t.fillStyle = this.fillColor, t.strokeStyle = this.strokeColor, t.lineWidth = this.strokeWidth, t.moveTo(i, this.base), t.lineTo(i, n), t.lineTo(s, n), t.lineTo(s, this.base), t.fill(), this.showStroke && t.stroke()
        }, height: function () {
            return this.base - this.y
        }, inRange: function (t, e) {
            return t >= this.x - this.width / 2 && t <= this.x + this.width / 2 && e >= this.y && e <= this.base
        }
    }), i.Tooltip = i.Element.extend({
        draw: function () {
            var t = this.chart.ctx;
            t.font = O(this.fontSize, this.fontStyle, this.fontFamily), this.xAlign = "center", this.yAlign = "above";
            var e = this.caretPadding = 2, i = t.measureText(this.text).width + 2 * this.xPadding, s = this.fontSize + 2 * this.yPadding, n = s + this.caretHeight + e;
            this.x + i / 2 > this.chart.width ? this.xAlign = "left" : this.x - i / 2 < 0 && (this.xAlign = "right"), this.y - n < 0 && (this.yAlign = "below");
            var o = this.x - i / 2, a = this.y - n;
            if (t.fillStyle = this.fillColor, this.custom)this.custom(this); else {
                switch (this.yAlign) {
                    case"above":
                        t.beginPath(), t.moveTo(this.x, this.y - e), t.lineTo(this.x + this.caretHeight, this.y - (e + this.caretHeight)), t.lineTo(this.x - this.caretHeight, this.y - (e + this.caretHeight)), t.closePath(), t.fill();
                        break;
                    case"below":
                        a = this.y + e + this.caretHeight, t.beginPath(), t.moveTo(this.x, this.y + e), t.lineTo(this.x + this.caretHeight, this.y + e + this.caretHeight), t.lineTo(this.x - this.caretHeight, this.y + e + this.caretHeight), t.closePath(), t.fill()
                }
                switch (this.xAlign) {
                    case"left":
                        o = this.x - i + (this.cornerRadius + this.caretHeight);
                        break;
                    case"right":
                        o = this.x - (this.cornerRadius + this.caretHeight)
                }
                A(t, o, a, i, s, this.cornerRadius), t.fill(), t.fillStyle = this.textColor, t.textAlign = "center", t.textBaseline = "middle", t.fillText(this.text, o + i / 2, a + s / 2)
            }
        }
    }), i.MultiTooltip = i.Element.extend({
        initialize: function () {
            this.font = O(this.fontSize, this.fontStyle, this.fontFamily), this.titleFont = O(this.titleFontSize, this.titleFontStyle, this.titleFontFamily), this.height = this.labels.length * this.fontSize + (this.labels.length - 1) * (this.fontSize / 2) + 2 * this.yPadding + 1.5 * this.titleFontSize, this.ctx.font = this.titleFont;
            var t = this.ctx.measureText(this.title).width, e = E(this.ctx, this.font, this.labels) + this.fontSize + 3, i = m([e, t]);
            this.width = i + 2 * this.xPadding;
            var s = this.height / 2;
            this.y - s < 0 ? this.y = s : this.y + s > this.chart.height && (this.y = this.chart.height - s), this.x > this.chart.width / 2 ? this.x -= this.xOffset + this.width : this.x += this.xOffset
        }, getLineHeight: function (t) {
            var e = this.y - this.height / 2 + this.yPadding, i = t - 1;
            return 0 === t ? e + this.titleFontSize / 2 : e + (1.5 * this.fontSize * i + this.fontSize / 2) + 1.5 * this.titleFontSize
        }, draw: function () {
            if (this.custom)this.custom(this); else {
                A(this.ctx, this.x, this.y - this.height / 2, this.width, this.height, this.cornerRadius);
                var t = this.ctx;
                t.fillStyle = this.fillColor, t.fill(), t.closePath(), t.textAlign = "left", t.textBaseline = "middle", t.fillStyle = this.titleTextColor, t.font = this.titleFont, t.fillText(this.title, this.x + this.xPadding, this.getLineHeight(0)), t.font = this.font, s.each(this.labels, function (e, i) {
                    t.fillStyle = this.textColor, t.fillText(e, this.x + this.xPadding + this.fontSize + 3, this.getLineHeight(i + 1)), t.fillStyle = this.legendColorBackground, t.fillRect(this.x + this.xPadding, this.getLineHeight(i + 1) - this.fontSize / 2, this.fontSize, this.fontSize), t.fillStyle = this.legendColors[i].fill, t.fillRect(this.x + this.xPadding, this.getLineHeight(i + 1) - this.fontSize / 2, this.fontSize, this.fontSize)
                }, this)
            }
        }
    }), i.Scale = i.Element.extend({
        initialize: function () {
            this.fit()
        }, buildYLabels: function () {
            this.yLabels = [];
            for (var t = v(this.stepValue), e = 0; e <= this.steps; e++)this.yLabels.push(w(this.templateString, {value: (this.min + e * this.stepValue).toFixed(t)}));
            this.yLabelWidth = this.display && this.showLabels ? E(this.ctx, this.font, this.yLabels) : 0
        }, addXLabel: function (t) {
            this.xLabels.push(t), this.valuesCount++, this.fit()
        }, removeXLabel: function () {
            this.xLabels.shift(), this.valuesCount--, this.fit()
        }, fit: function () {
            this.startPoint = this.display ? this.fontSize : 0, this.endPoint = this.display ? this.height - 1.5 * this.fontSize - 5 : this.height, this.startPoint += this.padding, this.endPoint -= this.padding;
            var t, e = this.endPoint - this.startPoint;
            for (this.calculateYRange(e), this.buildYLabels(), this.calculateXLabelRotation(); e > this.endPoint - this.startPoint;)e = this.endPoint - this.startPoint, t = this.yLabelWidth, this.calculateYRange(e), this.buildYLabels(), t < this.yLabelWidth && this.calculateXLabelRotation()
        }, calculateXLabelRotation: function () {
            this.ctx.font = this.font;
            var t, e, i = this.ctx.measureText(this.xLabels[0]).width, s = this.ctx.measureText(this.xLabels[this.xLabels.length - 1]).width;
            if (this.xScalePaddingRight = s / 2 + 3, this.xScalePaddingLeft = i / 2 > this.yLabelWidth + 10 ? i / 2 : this.yLabelWidth + 10, this.xLabelRotation = 0, this.display) {
                var n, o = E(this.ctx, this.font, this.xLabels);
                this.xLabelWidth = o;
                for (var a = Math.floor(this.calculateX(1) - this.calculateX(0)) - 6; this.xLabelWidth > a && 0 === this.xLabelRotation || this.xLabelWidth > a && this.xLabelRotation <= 90 && this.xLabelRotation > 0;)n = Math.cos(b(this.xLabelRotation)), t = n * i, e = n * s, t + this.fontSize / 2 > this.yLabelWidth + 8 && (this.xScalePaddingLeft = t + this.fontSize / 2), this.xScalePaddingRight = this.fontSize / 2, this.xLabelRotation++, this.xLabelWidth = n * o;
                this.xLabelRotation > 0 && (this.endPoint -= Math.sin(b(this.xLabelRotation)) * o + 3)
            } else this.xLabelWidth = 0, this.xScalePaddingRight = this.padding, this.xScalePaddingLeft = this.padding
        }, calculateYRange: c, drawingArea: function () {
            return this.startPoint - this.endPoint
        }, calculateY: function (t) {
            var e = this.drawingArea() / (this.min - this.max);
            return this.endPoint - e * (t - this.min)
        }, calculateX: function (t) {
            var e = (this.xLabelRotation > 0, this.width - (this.xScalePaddingLeft + this.xScalePaddingRight)), i = e / Math.max(this.valuesCount - (this.offsetGridLines ? 0 : 1), 1), s = i * t + this.xScalePaddingLeft;
            return this.offsetGridLines && (s += i / 2), Math.round(s)
        }, update: function (t) {
            s.extend(this, t), this.fit()
        }, draw: function () {
            var t = this.ctx, e = (this.endPoint - this.startPoint) / this.steps, i = Math.round(this.xScalePaddingLeft);
            this.display && (t.fillStyle = this.textColor, t.font = this.font, n(this.yLabels, function (n, o) {
                var a = this.endPoint - e * o, r = Math.round(a), h = this.showHorizontalLines;
                t.textAlign = "right", t.textBaseline = "middle", this.showLabels && t.fillText(n, i - 10, a), 0 !== o || h || (h = !0), h && t.beginPath(), o > 0 ? (t.lineWidth = this.gridLineWidth, t.strokeStyle = this.gridLineColor) : (t.lineWidth = this.lineWidth, t.strokeStyle = this.lineColor), r += s.aliasPixel(t.lineWidth), h && (t.moveTo(i, r), t.lineTo(this.width, r), t.stroke(), t.closePath()), t.lineWidth = this.lineWidth, t.strokeStyle = this.lineColor, t.beginPath(), t.moveTo(i - 5, r), t.lineTo(i, r), t.stroke(), t.closePath()
            }, this), n(this.xLabels, function (e, i) {
                var s = this.calculateX(i) + _(this.lineWidth), n = this.calculateX(i - (this.offsetGridLines ? .5 : 0)) + _(this.lineWidth), o = this.xLabelRotation > 0, a = this.showVerticalLines;
                0 !== i || a || (a = !0), a && t.beginPath(), i > 0 ? (t.lineWidth = this.gridLineWidth, t.strokeStyle = this.gridLineColor) : (t.lineWidth = this.lineWidth, t.strokeStyle = this.lineColor), a && (t.moveTo(n, this.endPoint), t.lineTo(n, this.startPoint - 3), t.stroke(), t.closePath()), t.lineWidth = this.lineWidth, t.strokeStyle = this.lineColor, t.beginPath(), t.moveTo(n, this.endPoint), t.lineTo(n, this.endPoint + 5), t.stroke(), t.closePath(), t.save(), t.translate(s, o ? this.endPoint + 12 : this.endPoint + 8), t.rotate(-1 * b(this.xLabelRotation)), t.font = this.font, t.textAlign = o ? "right" : "center", t.textBaseline = o ? "middle" : "top", t.fillText(e, 0, 0), t.restore()
            }, this))
        }
    }), i.RadialScale = i.Element.extend({
        initialize: function () {
            this.size = g([this.height, this.width]), this.drawingArea = this.display ? this.size / 2 - (this.fontSize / 2 + this.backdropPaddingY) : this.size / 2
        }, calculateCenterOffset: function (t) {
            var e = this.drawingArea / (this.max - this.min);
            return (t - this.min) * e
        }, update: function () {
            this.lineArc ? this.drawingArea = this.display ? this.size / 2 - (this.fontSize / 2 + this.backdropPaddingY) : this.size / 2 : this.setScaleSize(), this.buildYLabels()
        }, buildYLabels: function () {
            this.yLabels = [];
            for (var t = v(this.stepValue), e = 0; e <= this.steps; e++)this.yLabels.push(w(this.templateString, {value: (this.min + e * this.stepValue).toFixed(t)}))
        }, getCircumference: function () {
            return 2 * Math.PI / this.valuesCount
        }, setScaleSize: function () {
            var t, e, i, s, n, o, a, r, h, l, c, d, p = g([this.height / 2 - this.pointLabelFontSize - 5, this.width / 2]), u = this.width, m = 0;
            for (this.ctx.font = O(this.pointLabelFontSize, this.pointLabelFontStyle, this.pointLabelFontFamily), e = 0; e < this.valuesCount; e++)t = this.getPointPosition(e, p), i = this.ctx.measureText(w(this.templateString, {value: this.labels[e]})).width + 5, 0 === e || e === this.valuesCount / 2 ? (s = i / 2, t.x + s > u && (u = t.x + s, n = e), t.x - s < m && (m = t.x - s, a = e)) : e < this.valuesCount / 2 ? t.x + i > u && (u = t.x + i, n = e) : e > this.valuesCount / 2 && t.x - i < m && (m = t.x - i, a = e);
            h = m, l = Math.ceil(u - this.width), o = this.getIndexAngle(n), r = this.getIndexAngle(a), c = l / Math.sin(o + Math.PI / 2), d = h / Math.sin(r + Math.PI / 2), c = f(c) ? c : 0, d = f(d) ? d : 0, this.drawingArea = p - (d + c) / 2, this.setCenterPoint(d, c)
        }, setCenterPoint: function (t, e) {
            var i = this.width - e - this.drawingArea, s = t + this.drawingArea;
            this.xCenter = (s + i) / 2, this.yCenter = this.height / 2
        }, getIndexAngle: function (t) {
            var e = 2 * Math.PI / this.valuesCount;
            return t * e - Math.PI / 2
        }, getPointPosition: function (t, e) {
            var i = this.getIndexAngle(t);
            return {x: Math.cos(i) * e + this.xCenter, y: Math.sin(i) * e + this.yCenter}
        }, draw: function () {
            if (this.display) {
                var t = this.ctx;
                if (n(this.yLabels, function (e, i) {
                        if (i > 0) {
                            var s, n = i * (this.drawingArea / this.steps), o = this.yCenter - n;
                            if (this.lineWidth > 0)if (t.strokeStyle = this.lineColor, t.lineWidth = this.lineWidth, this.lineArc)t.beginPath(), t.arc(this.xCenter, this.yCenter, n, 0, 2 * Math.PI), t.closePath(), t.stroke(); else {
                                t.beginPath();
                                for (var a = 0; a < this.valuesCount; a++)s = this.getPointPosition(a, this.calculateCenterOffset(this.min + i * this.stepValue)), 0 === a ? t.moveTo(s.x, s.y) : t.lineTo(s.x, s.y);
                                t.closePath(), t.stroke()
                            }
                            if (this.showLabels) {
                                if (t.font = O(this.fontSize, this.fontStyle, this.fontFamily), this.showLabelBackdrop) {
                                    var r = t.measureText(e).width;
                                    t.fillStyle = this.backdropColor, t.fillRect(this.xCenter - r / 2 - this.backdropPaddingX, o - this.fontSize / 2 - this.backdropPaddingY, r + 2 * this.backdropPaddingX, this.fontSize + 2 * this.backdropPaddingY)
                                }
                                t.textAlign = "center", t.textBaseline = "middle", t.fillStyle = this.fontColor, t.fillText(e, this.xCenter, o)
                            }
                        }
                    }, this), !this.lineArc) {
                    t.lineWidth = this.angleLineWidth, t.strokeStyle = this.angleLineColor;
                    for (var e = this.valuesCount - 1; e >= 0; e--) {
                        if (this.angleLineWidth > 0) {
                            var i = this.getPointPosition(e, this.calculateCenterOffset(this.max));
                            t.beginPath(), t.moveTo(this.xCenter, this.yCenter), t.lineTo(i.x, i.y), t.stroke(), t.closePath()
                        }
                        var s = this.getPointPosition(e, this.calculateCenterOffset(this.max) + 5);
                        t.font = O(this.pointLabelFontSize, this.pointLabelFontStyle, this.pointLabelFontFamily), t.fillStyle = this.pointLabelFontColor;
                        var o = this.labels.length, a = this.labels.length / 2, r = a / 2, h = r > e || e > o - r, l = e === r || e === o - r;
                        t.textAlign = 0 === e ? "center" : e === a ? "center" : a > e ? "left" : "right", t.textBaseline = l ? "middle" : h ? "bottom" : "top", t.fillText(this.labels[e], s.x, s.y)
                    }
                }
            }
        }
    }), s.addEvent(window, "resize", function () {
        var t;
        return function () {
            clearTimeout(t), t = setTimeout(function () {
                n(i.instances, function (t) {
                    t.options.responsive && t.resize(t.render, !0)
                })
            }, 50)
        }
    }()), u ? define(function () {
        return i
    }) : "object" == typeof module && module.exports && (module.exports = i), t.Chart = i, i.noConflict = function () {
        return t.Chart = e, i
    }
}.call(this), function () {
    "use strict";
    var t = this, e = t.Chart, i = e.helpers, s = {
        scaleBeginAtZero: !0,
        scaleShowGridLines: !0,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: !0,
        scaleShowVerticalLines: !0,
        barShowStroke: !0,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
    };
    e.Type.extend({
        name: "Bar", defaults: s, initialize: function (t) {
            var s = this.options;
            this.ScaleClass = e.Scale.extend({
                offsetGridLines: !0, calculateBarX: function (t, e, i) {
                    var n = this.calculateBaseWidth(), o = this.calculateX(i) - n / 2, a = this.calculateBarWidth(t);
                    return o + a * e + e * s.barDatasetSpacing + a / 2
                }, calculateBaseWidth: function () {
                    return this.calculateX(1) - this.calculateX(0) - 2 * s.barValueSpacing
                }, calculateBarWidth: function (t) {
                    var e = this.calculateBaseWidth() - (t - 1) * s.barDatasetSpacing;
                    return e / t
                }
            }), this.datasets = [], this.options.showTooltips && i.bindEvents(this, this.options.tooltipEvents, function (t) {
                var e = "mouseout" !== t.type ? this.getBarsAtEvent(t) : [];
                this.eachBars(function (t) {
                    t.restore(["fillColor", "strokeColor"])
                }), i.each(e, function (t) {
                    t.fillColor = t.highlightFill, t.strokeColor = t.highlightStroke
                }), this.showTooltip(e)
            }), this.BarClass = e.Rectangle.extend({
                strokeWidth: this.options.barStrokeWidth,
                showStroke: this.options.barShowStroke,
                ctx: this.chart.ctx
            }), i.each(t.datasets, function (e) {
                var s = {label: e.label || null, fillColor: e.fillColor, strokeColor: e.strokeColor, bars: []};
                this.datasets.push(s), i.each(e.data, function (i, n) {
                    s.bars.push(new this.BarClass({
                        value: i,
                        label: t.labels[n],
                        datasetLabel: e.label,
                        strokeColor: e.strokeColor,
                        fillColor: e.fillColor,
                        highlightFill: e.highlightFill || e.fillColor,
                        highlightStroke: e.highlightStroke || e.strokeColor
                    }))
                }, this)
            }, this), this.buildScale(t.labels), this.BarClass.prototype.base = this.scale.endPoint, this.eachBars(function (t, e, s) {
                i.extend(t, {
                    width: this.scale.calculateBarWidth(this.datasets.length),
                    x: this.scale.calculateBarX(this.datasets.length, s, e),
                    y: this.scale.endPoint
                }), t.save()
            }, this), this.render()
        }, update: function () {
            this.scale.update(), i.each(this.activeElements, function (t) {
                t.restore(["fillColor", "strokeColor"])
            }), this.eachBars(function (t) {
                t.save()
            }), this.render()
        }, eachBars: function (t) {
            i.each(this.datasets, function (e, s) {
                i.each(e.bars, t, this, s)
            }, this)
        }, getBarsAtEvent: function (t) {
            for (var e, s = [], n = i.getRelativePosition(t), o = function (t) {
                s.push(t.bars[e])
            }, a = 0; a < this.datasets.length; a++)for (e = 0; e < this.datasets[a].bars.length; e++)if (this.datasets[a].bars[e].inRange(n.x, n.y))return i.each(this.datasets, o), s;
            return s
        }, buildScale: function (t) {
            var e = this, s = function () {
                var t = [];
                return e.eachBars(function (e) {
                    t.push(e.value)
                }), t
            }, n = {
                templateString: this.options.scaleLabel,
                height: this.chart.height,
                width: this.chart.width,
                ctx: this.chart.ctx,
                textColor: this.options.scaleFontColor,
                fontSize: this.options.scaleFontSize,
                fontStyle: this.options.scaleFontStyle,
                fontFamily: this.options.scaleFontFamily,
                valuesCount: t.length,
                beginAtZero: this.options.scaleBeginAtZero,
                integersOnly: this.options.scaleIntegersOnly,
                calculateYRange: function (t) {
                    var e = i.calculateScaleRange(s(), t, this.fontSize, this.beginAtZero, this.integersOnly);
                    i.extend(this, e)
                },
                xLabels: t,
                font: i.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
                lineWidth: this.options.scaleLineWidth,
                lineColor: this.options.scaleLineColor,
                showHorizontalLines: this.options.scaleShowHorizontalLines,
                showVerticalLines: this.options.scaleShowVerticalLines,
                gridLineWidth: this.options.scaleShowGridLines ? this.options.scaleGridLineWidth : 0,
                gridLineColor: this.options.scaleShowGridLines ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
                padding: this.options.showScale ? 0 : this.options.barShowStroke ? this.options.barStrokeWidth : 0,
                showLabels: this.options.scaleShowLabels,
                display: this.options.showScale
            };
            this.options.scaleOverride && i.extend(n, {
                calculateYRange: i.noop,
                steps: this.options.scaleSteps,
                stepValue: this.options.scaleStepWidth,
                min: this.options.scaleStartValue,
                max: this.options.scaleStartValue + this.options.scaleSteps * this.options.scaleStepWidth
            }), this.scale = new this.ScaleClass(n)
        }, addData: function (t, e) {
            i.each(t, function (t, i) {
                this.datasets[i].bars.push(new this.BarClass({
                    value: t,
                    label: e,
                    x: this.scale.calculateBarX(this.datasets.length, i, this.scale.valuesCount + 1),
                    y: this.scale.endPoint,
                    width: this.scale.calculateBarWidth(this.datasets.length),
                    base: this.scale.endPoint,
                    strokeColor: this.datasets[i].strokeColor,
                    fillColor: this.datasets[i].fillColor
                }))
            }, this), this.scale.addXLabel(e), this.update()
        }, removeData: function () {
            this.scale.removeXLabel(), i.each(this.datasets, function (t) {
                t.bars.shift()
            }, this), this.update()
        }, reflow: function () {
            i.extend(this.BarClass.prototype, {y: this.scale.endPoint, base: this.scale.endPoint});
            var t = i.extend({height: this.chart.height, width: this.chart.width});
            this.scale.update(t)
        }, draw: function (t) {
            var e = t || 1;
            this.clear(), this.chart.ctx, this.scale.draw(e), i.each(this.datasets, function (t, s) {
                i.each(t.bars, function (t, i) {
                    t.hasValue() && (t.base = this.scale.endPoint, t.transition({
                        x: this.scale.calculateBarX(this.datasets.length, s, i),
                        y: this.scale.calculateY(t.value),
                        width: this.scale.calculateBarWidth(this.datasets.length)
                    }, e).draw())
                }, this)
            }, this)
        }
    })
}.call(this), function () {
    "use strict";
    var t = this, e = t.Chart, i = e.helpers, s = {
        segmentShowStroke: !0,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 50,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: !0,
        animateScale: !1,
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    };
    e.Type.extend({
        name: "Doughnut", defaults: s, initialize: function (t) {
            this.segments = [], this.outerRadius = (i.min([this.chart.width, this.chart.height]) - this.options.segmentStrokeWidth / 2) / 2, this.SegmentArc = e.Arc.extend({
                ctx: this.chart.ctx,
                x: this.chart.width / 2,
                y: this.chart.height / 2
            }), this.options.showTooltips && i.bindEvents(this, this.options.tooltipEvents, function (t) {
                var e = "mouseout" !== t.type ? this.getSegmentsAtEvent(t) : [];
                i.each(this.segments, function (t) {
                    t.restore(["fillColor"])
                }), i.each(e, function (t) {
                    t.fillColor = t.highlightColor
                }), this.showTooltip(e)
            }), this.calculateTotal(t), i.each(t, function (t, e) {
                this.addData(t, e, !0)
            }, this), this.render()
        }, getSegmentsAtEvent: function (t) {
            var e = [], s = i.getRelativePosition(t);
            return i.each(this.segments, function (t) {
                t.inRange(s.x, s.y) && e.push(t)
            }, this), e
        }, addData: function (t, e, i) {
            var s = e || this.segments.length;
            this.segments.splice(s, 0, new this.SegmentArc({
                value: t.value,
                outerRadius: this.options.animateScale ? 0 : this.outerRadius,
                innerRadius: this.options.animateScale ? 0 : this.outerRadius / 100 * this.options.percentageInnerCutout,
                fillColor: t.color,
                highlightColor: t.highlight || t.color,
                showStroke: this.options.segmentShowStroke,
                strokeWidth: this.options.segmentStrokeWidth,
                strokeColor: this.options.segmentStrokeColor,
                startAngle: 1.5 * Math.PI,
                circumference: this.options.animateRotate ? 0 : this.calculateCircumference(t.value),
                label: t.label
            })), i || (this.reflow(), this.update())
        }, calculateCircumference: function (t) {
            return 2 * Math.PI * (Math.abs(t) / this.total)
        }, calculateTotal: function (t) {
            this.total = 0, i.each(t, function (t) {
                this.total += Math.abs(t.value)
            }, this)
        }, update: function () {
            this.calculateTotal(this.segments), i.each(this.activeElements, function (t) {
                t.restore(["fillColor"])
            }), i.each(this.segments, function (t) {
                t.save()
            }), this.render()
        }, removeData: function (t) {
            var e = i.isNumber(t) ? t : this.segments.length - 1;
            this.segments.splice(e, 1), this.reflow(), this.update()
        }, reflow: function () {
            i.extend(this.SegmentArc.prototype, {
                x: this.chart.width / 2,
                y: this.chart.height / 2
            }), this.outerRadius = (i.min([this.chart.width, this.chart.height]) - this.options.segmentStrokeWidth / 2) / 2, i.each(this.segments, function (t) {
                t.update({
                    outerRadius: this.outerRadius,
                    innerRadius: this.outerRadius / 100 * this.options.percentageInnerCutout
                })
            }, this)
        }, draw: function (t) {
            var e = t ? t : 1;
            this.clear(), i.each(this.segments, function (t, i) {
                t.transition({
                    circumference: this.calculateCircumference(t.value),
                    outerRadius: this.outerRadius,
                    innerRadius: this.outerRadius / 100 * this.options.percentageInnerCutout
                }, e), t.endAngle = t.startAngle + t.circumference, t.draw(), 0 === i && (t.startAngle = 1.5 * Math.PI), i < this.segments.length - 1 && (this.segments[i + 1].startAngle = t.endAngle)
            }, this)
        }
    }), e.types.Doughnut.extend({name: "Pie", defaults: i.merge(s, {percentageInnerCutout: 0})})
}.call(this), function () {
    "use strict";
    var t = this, e = t.Chart, i = e.helpers, s = {
        scaleShowGridLines: !0,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: !0,
        scaleShowVerticalLines: !0,
        bezierCurve: !0,
        bezierCurveTension: .4,
        pointDot: !0,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: !0,
        datasetStrokeWidth: 2,
        datasetFill: !0,
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
    };
    e.Type.extend({
        name: "Line", defaults: s, initialize: function (t) {
            this.PointClass = e.Point.extend({
                strokeWidth: this.options.pointDotStrokeWidth,
                radius: this.options.pointDotRadius,
                display: this.options.pointDot,
                hitDetectionRadius: this.options.pointHitDetectionRadius,
                ctx: this.chart.ctx,
                inRange: function (t) {
                    return Math.pow(t - this.x, 2) < Math.pow(this.radius + this.hitDetectionRadius, 2)
                }
            }), this.datasets = [], this.options.showTooltips && i.bindEvents(this, this.options.tooltipEvents, function (t) {
                var e = "mouseout" !== t.type ? this.getPointsAtEvent(t) : [];
                this.eachPoints(function (t) {
                    t.restore(["fillColor", "strokeColor"])
                }), i.each(e, function (t) {
                    t.fillColor = t.highlightFill, t.strokeColor = t.highlightStroke
                }), this.showTooltip(e)
            }), i.each(t.datasets, function (e) {
                var s = {
                    label: e.label || null,
                    fillColor: e.fillColor,
                    strokeColor: e.strokeColor,
                    pointColor: e.pointColor,
                    pointStrokeColor: e.pointStrokeColor,
                    points: []
                };
                this.datasets.push(s), i.each(e.data, function (i, n) {
                    s.points.push(new this.PointClass({
                        value: i,
                        label: t.labels[n],
                        datasetLabel: e.label,
                        strokeColor: e.pointStrokeColor,
                        fillColor: e.pointColor,
                        highlightFill: e.pointHighlightFill || e.pointColor,
                        highlightStroke: e.pointHighlightStroke || e.pointStrokeColor
                    }))
                }, this), this.buildScale(t.labels), this.eachPoints(function (t, e) {
                    i.extend(t, {x: this.scale.calculateX(e), y: this.scale.endPoint}), t.save()
                }, this)
            }, this), this.render()
        }, update: function () {
            this.scale.update(), i.each(this.activeElements, function (t) {
                t.restore(["fillColor", "strokeColor"])
            }), this.eachPoints(function (t) {
                t.save()
            }), this.render()
        }, eachPoints: function (t) {
            i.each(this.datasets, function (e) {
                i.each(e.points, t, this)
            }, this)
        }, getPointsAtEvent: function (t) {
            var e = [], s = i.getRelativePosition(t);
            return i.each(this.datasets, function (t) {
                i.each(t.points, function (t) {
                    t.inRange(s.x, s.y) && e.push(t)
                })
            }, this), e
        }, buildScale: function (t) {
            var s = this, n = function () {
                var t = [];
                return s.eachPoints(function (e) {
                    t.push(e.value)
                }), t
            }, o = {
                templateString: this.options.scaleLabel,
                height: this.chart.height,
                width: this.chart.width,
                ctx: this.chart.ctx,
                textColor: this.options.scaleFontColor,
                fontSize: this.options.scaleFontSize,
                fontStyle: this.options.scaleFontStyle,
                fontFamily: this.options.scaleFontFamily,
                valuesCount: t.length,
                beginAtZero: this.options.scaleBeginAtZero,
                integersOnly: this.options.scaleIntegersOnly,
                calculateYRange: function (t) {
                    var e = i.calculateScaleRange(n(), t, this.fontSize, this.beginAtZero, this.integersOnly);
                    i.extend(this, e)
                },
                xLabels: t,
                font: i.fontString(this.options.scaleFontSize, this.options.scaleFontStyle, this.options.scaleFontFamily),
                lineWidth: this.options.scaleLineWidth,
                lineColor: this.options.scaleLineColor,
                showHorizontalLines: this.options.scaleShowHorizontalLines,
                showVerticalLines: this.options.scaleShowVerticalLines,
                gridLineWidth: this.options.scaleShowGridLines ? this.options.scaleGridLineWidth : 0,
                gridLineColor: this.options.scaleShowGridLines ? this.options.scaleGridLineColor : "rgba(0,0,0,0)",
                padding: this.options.showScale ? 0 : this.options.pointDotRadius + this.options.pointDotStrokeWidth,
                showLabels: this.options.scaleShowLabels,
                display: this.options.showScale
            };
            this.options.scaleOverride && i.extend(o, {
                calculateYRange: i.noop,
                steps: this.options.scaleSteps,
                stepValue: this.options.scaleStepWidth,
                min: this.options.scaleStartValue,
                max: this.options.scaleStartValue + this.options.scaleSteps * this.options.scaleStepWidth
            }), this.scale = new e.Scale(o)
        }, addData: function (t, e) {
            i.each(t, function (t, i) {
                this.datasets[i].points.push(new this.PointClass({
                    value: t,
                    label: e,
                    x: this.scale.calculateX(this.scale.valuesCount + 1),
                    y: this.scale.endPoint,
                    strokeColor: this.datasets[i].pointStrokeColor,
                    fillColor: this.datasets[i].pointColor
                }))
            }, this), this.scale.addXLabel(e), this.update()
        }, removeData: function () {
            this.scale.removeXLabel(), i.each(this.datasets, function (t) {
                t.points.shift()
            }, this), this.update()
        }, reflow: function () {
            var t = i.extend({height: this.chart.height, width: this.chart.width});
            this.scale.update(t)
        }, draw: function (t) {
            var e = t || 1;
            this.clear();
            var s = this.chart.ctx, n = function (t) {
                return null !== t.value
            }, o = function (t, e, s) {
                return i.findNextWhere(e, n, s) || t
            }, a = function (t, e, s) {
                return i.findPreviousWhere(e, n, s) || t
            };
            this.scale.draw(e), i.each(this.datasets, function (t) {
                var r = i.where(t.points, n);
                i.each(t.points, function (t, i) {
                    t.hasValue() && t.transition({y: this.scale.calculateY(t.value), x: this.scale.calculateX(i)}, e)
                }, this), this.options.bezierCurve && i.each(r, function (t, e) {
                    var s = e > 0 && e < r.length - 1 ? this.options.bezierCurveTension : 0;
                    t.controlPoints = i.splineCurve(a(t, r, e), t, o(t, r, e), s), t.controlPoints.outer.y > this.scale.endPoint ? t.controlPoints.outer.y = this.scale.endPoint : t.controlPoints.outer.y < this.scale.startPoint && (t.controlPoints.outer.y = this.scale.startPoint), t.controlPoints.inner.y > this.scale.endPoint ? t.controlPoints.inner.y = this.scale.endPoint : t.controlPoints.inner.y < this.scale.startPoint && (t.controlPoints.inner.y = this.scale.startPoint)
                }, this), s.lineWidth = this.options.datasetStrokeWidth, s.strokeStyle = t.strokeColor, s.beginPath(), i.each(r, function (t, e) {
                    if (0 === e)s.moveTo(t.x, t.y); else if (this.options.bezierCurve) {
                        var i = a(t, r, e);
                        s.bezierCurveTo(i.controlPoints.outer.x, i.controlPoints.outer.y, t.controlPoints.inner.x, t.controlPoints.inner.y, t.x, t.y)
                    } else s.lineTo(t.x, t.y)
                }, this), s.stroke(), this.options.datasetFill && r.length > 0 && (s.lineTo(r[r.length - 1].x, this.scale.endPoint), s.lineTo(r[0].x, this.scale.endPoint), s.fillStyle = t.fillColor, s.closePath(), s.fill()), i.each(r, function (t) {
                    t.draw()
                })
            }, this)
        }
    })
}.call(this), function () {
    "use strict";
    var t = this, e = t.Chart, i = e.helpers, s = {
        scaleShowLabelBackdrop: !0,
        scaleBackdropColor: "rgba(255,255,255,0.75)",
        scaleBeginAtZero: !0,
        scaleBackdropPaddingY: 2,
        scaleBackdropPaddingX: 2,
        scaleShowLine: !0,
        segmentShowStroke: !0,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: !0,
        animateScale: !1,
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    };
    e.Type.extend({
        name: "PolarArea", defaults: s, initialize: function (t) {
            this.segments = [], this.SegmentArc = e.Arc.extend({
                showStroke: this.options.segmentShowStroke,
                strokeWidth: this.options.segmentStrokeWidth,
                strokeColor: this.options.segmentStrokeColor,
                ctx: this.chart.ctx,
                innerRadius: 0,
                x: this.chart.width / 2,
                y: this.chart.height / 2
            }), this.scale = new e.RadialScale({
                display: this.options.showScale,
                fontStyle: this.options.scaleFontStyle,
                fontSize: this.options.scaleFontSize,
                fontFamily: this.options.scaleFontFamily,
                fontColor: this.options.scaleFontColor,
                showLabels: this.options.scaleShowLabels,
                showLabelBackdrop: this.options.scaleShowLabelBackdrop,
                backdropColor: this.options.scaleBackdropColor,
                backdropPaddingY: this.options.scaleBackdropPaddingY,
                backdropPaddingX: this.options.scaleBackdropPaddingX,
                lineWidth: this.options.scaleShowLine ? this.options.scaleLineWidth : 0,
                lineColor: this.options.scaleLineColor,
                lineArc: !0,
                width: this.chart.width,
                height: this.chart.height,
                xCenter: this.chart.width / 2,
                yCenter: this.chart.height / 2,
                ctx: this.chart.ctx,
                templateString: this.options.scaleLabel,
                valuesCount: t.length
            }), this.updateScaleRange(t), this.scale.update(), i.each(t, function (t, e) {
                this.addData(t, e, !0)
            }, this), this.options.showTooltips && i.bindEvents(this, this.options.tooltipEvents, function (t) {
                var e = "mouseout" !== t.type ? this.getSegmentsAtEvent(t) : [];
                i.each(this.segments, function (t) {
                    t.restore(["fillColor"])
                }), i.each(e, function (t) {
                    t.fillColor = t.highlightColor
                }), this.showTooltip(e)
            }), this.render()
        }, getSegmentsAtEvent: function (t) {
            var e = [], s = i.getRelativePosition(t);
            return i.each(this.segments, function (t) {
                t.inRange(s.x, s.y) && e.push(t)
            }, this), e
        }, addData: function (t, e, i) {
            var s = e || this.segments.length;
            this.segments.splice(s, 0, new this.SegmentArc({
                fillColor: t.color,
                highlightColor: t.highlight || t.color,
                label: t.label,
                value: t.value,
                outerRadius: this.options.animateScale ? 0 : this.scale.calculateCenterOffset(t.value),
                circumference: this.options.animateRotate ? 0 : this.scale.getCircumference(),
                startAngle: 1.5 * Math.PI
            })), i || (this.reflow(), this.update())
        }, removeData: function (t) {
            var e = i.isNumber(t) ? t : this.segments.length - 1;
            this.segments.splice(e, 1), this.reflow(), this.update()
        }, calculateTotal: function (t) {
            this.total = 0, i.each(t, function (t) {
                this.total += t.value
            }, this), this.scale.valuesCount = this.segments.length
        }, updateScaleRange: function (t) {
            var e = [];
            i.each(t, function (t) {
                e.push(t.value)
            });
            var s = this.options.scaleOverride ? {
                steps: this.options.scaleSteps,
                stepValue: this.options.scaleStepWidth,
                min: this.options.scaleStartValue,
                max: this.options.scaleStartValue + this.options.scaleSteps * this.options.scaleStepWidth
            } : i.calculateScaleRange(e, i.min([this.chart.width, this.chart.height]) / 2, this.options.scaleFontSize, this.options.scaleBeginAtZero, this.options.scaleIntegersOnly);
            i.extend(this.scale, s, {
                size: i.min([this.chart.width, this.chart.height]),
                xCenter: this.chart.width / 2,
                yCenter: this.chart.height / 2
            })
        }, update: function () {
            this.calculateTotal(this.segments), i.each(this.segments, function (t) {
                t.save()
            }), this.reflow(), this.render()
        }, reflow: function () {
            i.extend(this.SegmentArc.prototype, {
                x: this.chart.width / 2,
                y: this.chart.height / 2
            }), this.updateScaleRange(this.segments), this.scale.update(), i.extend(this.scale, {
                xCenter: this.chart.width / 2,
                yCenter: this.chart.height / 2
            }), i.each(this.segments, function (t) {
                t.update({outerRadius: this.scale.calculateCenterOffset(t.value)})
            }, this)
        }, draw: function (t) {
            var e = t || 1;
            this.clear(), i.each(this.segments, function (t, i) {
                t.transition({
                    circumference: this.scale.getCircumference(),
                    outerRadius: this.scale.calculateCenterOffset(t.value)
                }, e), t.endAngle = t.startAngle + t.circumference, 0 === i && (t.startAngle = 1.5 * Math.PI), i < this.segments.length - 1 && (this.segments[i + 1].startAngle = t.endAngle), t.draw()
            }, this), this.scale.draw()
        }
    })
}.call(this), function () {
    "use strict";
    var t = this, e = t.Chart, i = e.helpers;
    e.Type.extend({
        name: "Radar",
        defaults: {
            scaleShowLine: !0,
            angleShowLineOut: !0,
            scaleShowLabels: !1,
            scaleBeginAtZero: !0,
            angleLineColor: "rgba(0,0,0,.1)",
            angleLineWidth: 1,
            pointLabelFontFamily: "'Arial'",
            pointLabelFontStyle: "normal",
            pointLabelFontSize: 10,
            pointLabelFontColor: "#666",
            pointDot: !0,
            pointDotRadius: 3,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: !0,
            datasetStrokeWidth: 2,
            datasetFill: !0,
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
        },
        initialize: function (t) {
            this.PointClass = e.Point.extend({
                strokeWidth: this.options.pointDotStrokeWidth,
                radius: this.options.pointDotRadius,
                display: this.options.pointDot,
                hitDetectionRadius: this.options.pointHitDetectionRadius,
                ctx: this.chart.ctx
            }), this.datasets = [], this.buildScale(t), this.options.showTooltips && i.bindEvents(this, this.options.tooltipEvents, function (t) {
                var e = "mouseout" !== t.type ? this.getPointsAtEvent(t) : [];
                this.eachPoints(function (t) {
                    t.restore(["fillColor", "strokeColor"])
                }), i.each(e, function (t) {
                    t.fillColor = t.highlightFill, t.strokeColor = t.highlightStroke
                }), this.showTooltip(e)
            }), i.each(t.datasets, function (e) {
                var s = {
                    label: e.label || null,
                    fillColor: e.fillColor,
                    strokeColor: e.strokeColor,
                    pointColor: e.pointColor,
                    pointStrokeColor: e.pointStrokeColor,
                    points: []
                };
                this.datasets.push(s), i.each(e.data, function (i, n) {
                    var o;
                    this.scale.animation || (o = this.scale.getPointPosition(n, this.scale.calculateCenterOffset(i))), s.points.push(new this.PointClass({
                        value: i,
                        label: t.labels[n],
                        datasetLabel: e.label,
                        x: this.options.animation ? this.scale.xCenter : o.x,
                        y: this.options.animation ? this.scale.yCenter : o.y,
                        strokeColor: e.pointStrokeColor,
                        fillColor: e.pointColor,
                        highlightFill: e.pointHighlightFill || e.pointColor,
                        highlightStroke: e.pointHighlightStroke || e.pointStrokeColor
                    }))
                }, this)
            }, this), this.render()
        },
        eachPoints: function (t) {
            i.each(this.datasets, function (e) {
                i.each(e.points, t, this)
            }, this)
        },
        getPointsAtEvent: function (t) {
            var e = i.getRelativePosition(t), s = i.getAngleFromPoint({
                x: this.scale.xCenter,
                y: this.scale.yCenter
            }, e), n = 2 * Math.PI / this.scale.valuesCount, o = Math.round((s.angle - 1.5 * Math.PI) / n), a = [];
            return (o >= this.scale.valuesCount || 0 > o) && (o = 0), s.distance <= this.scale.drawingArea && i.each(this.datasets, function (t) {
                a.push(t.points[o])
            }), a
        },
        buildScale: function (t) {
            this.scale = new e.RadialScale({
                display: this.options.showScale,
                fontStyle: this.options.scaleFontStyle,
                fontSize: this.options.scaleFontSize,
                fontFamily: this.options.scaleFontFamily,
                fontColor: this.options.scaleFontColor,
                showLabels: this.options.scaleShowLabels,
                showLabelBackdrop: this.options.scaleShowLabelBackdrop,
                backdropColor: this.options.scaleBackdropColor,
                backdropPaddingY: this.options.scaleBackdropPaddingY,
                backdropPaddingX: this.options.scaleBackdropPaddingX,
                lineWidth: this.options.scaleShowLine ? this.options.scaleLineWidth : 0,
                lineColor: this.options.scaleLineColor,
                angleLineColor: this.options.angleLineColor,
                angleLineWidth: this.options.angleShowLineOut ? this.options.angleLineWidth : 0,
                pointLabelFontColor: this.options.pointLabelFontColor,
                pointLabelFontSize: this.options.pointLabelFontSize,
                pointLabelFontFamily: this.options.pointLabelFontFamily,
                pointLabelFontStyle: this.options.pointLabelFontStyle,
                height: this.chart.height,
                width: this.chart.width,
                xCenter: this.chart.width / 2,
                yCenter: this.chart.height / 2,
                ctx: this.chart.ctx,
                templateString: this.options.scaleLabel,
                labels: t.labels,
                valuesCount: t.datasets[0].data.length
            }), this.scale.setScaleSize(), this.updateScaleRange(t.datasets), this.scale.buildYLabels()
        },
        updateScaleRange: function (t) {
            var e = function () {
                var e = [];
                return i.each(t, function (t) {
                    t.data ? e = e.concat(t.data) : i.each(t.points, function (t) {
                        e.push(t.value)
                    })
                }), e
            }(), s = this.options.scaleOverride ? {
                steps: this.options.scaleSteps,
                stepValue: this.options.scaleStepWidth,
                min: this.options.scaleStartValue,
                max: this.options.scaleStartValue + this.options.scaleSteps * this.options.scaleStepWidth
            } : i.calculateScaleRange(e, i.min([this.chart.width, this.chart.height]) / 2, this.options.scaleFontSize, this.options.scaleBeginAtZero, this.options.scaleIntegersOnly);
            i.extend(this.scale, s)
        },
        addData: function (t, e) {
            this.scale.valuesCount++, i.each(t, function (t, i) {
                var s = this.scale.getPointPosition(this.scale.valuesCount, this.scale.calculateCenterOffset(t));
                this.datasets[i].points.push(new this.PointClass({
                    value: t,
                    label: e,
                    x: s.x,
                    y: s.y,
                    strokeColor: this.datasets[i].pointStrokeColor,
                    fillColor: this.datasets[i].pointColor
                }))
            }, this), this.scale.labels.push(e), this.reflow(), this.update()
        },
        removeData: function () {
            this.scale.valuesCount--, this.scale.labels.shift(), i.each(this.datasets, function (t) {
                t.points.shift()
            }, this), this.reflow(), this.update()
        },
        update: function () {
            this.eachPoints(function (t) {
                t.save()
            }), this.reflow(), this.render()
        },
        reflow: function () {
            i.extend(this.scale, {
                width: this.chart.width,
                height: this.chart.height,
                size: i.min([this.chart.width, this.chart.height]),
                xCenter: this.chart.width / 2,
                yCenter: this.chart.height / 2
            }), this.updateScaleRange(this.datasets), this.scale.setScaleSize(), this.scale.buildYLabels()
        },
        draw: function (t) {
            var e = t || 1, s = this.chart.ctx;
            this.clear(), this.scale.draw(), i.each(this.datasets, function (t) {
                i.each(t.points, function (t, i) {
                    t.hasValue() && t.transition(this.scale.getPointPosition(i, this.scale.calculateCenterOffset(t.value)), e)
                }, this), s.lineWidth = this.options.datasetStrokeWidth, s.strokeStyle = t.strokeColor, s.beginPath(), i.each(t.points, function (t, e) {
                    0 === e ? s.moveTo(t.x, t.y) : s.lineTo(t.x, t.y)
                }, this), s.closePath(), s.stroke(), s.fillStyle = t.fillColor, s.fill(), i.each(t.points, function (t) {
                    t.hasValue() && t.draw()
                })
            }, this)
        }
    })
}.call(this), function (t, e, i, s, n) {
    var o = 0, a = function () {
        var e = s.userAgent, i = /msie\s\d+/i;
        return 0 < e.search(i) && (e = i.exec(e).toString(), e = e.split(" ")[1], 9 > e) ? (t("html").addClass("lt-ie9"), !0) : !1
    }();
    Function.prototype.bind || (Function.prototype.bind = function (t) {
        var e = this, i = [].slice;
        if ("function" != typeof e)throw new TypeError;
        var s = i.call(arguments, 1), n = function () {
            if (this instanceof n) {
                var o = function () {
                };
                o.prototype = e.prototype;
                var o = new o, a = e.apply(o, s.concat(i.call(arguments)));
                return Object(a) === a ? a : o
            }
            return e.apply(t, s.concat(i.call(arguments)))
        };
        return n
    }), Array.prototype.indexOf || (Array.prototype.indexOf = function (t, e) {
        var i;
        if (null == this)throw new TypeError('"this" is null or not defined');
        var s = Object(this), n = s.length >>> 0;
        if (0 === n)return -1;
        if (i = +e || 0, 1 / 0 === Math.abs(i) && (i = 0), i >= n)return -1;
        for (i = Math.max(i >= 0 ? i : n - Math.abs(i), 0); n > i;) {
            if (i in s && s[i] === t)return i;
            i++
        }
        return -1
    });
    var r = function (s, n, o) {
        this.VERSION = "2.1.2", this.input = s, this.plugin_count = o, this.old_to = this.old_from = this.update_tm = this.calc_count = this.current_plugin = 0, this.raf_id = this.old_min_interval = null, this.is_update = this.is_key = this.no_diapason = this.force_redraw = this.dragging = !1, this.is_start = !0, this.is_click = this.is_resize = this.is_active = this.is_finish = !1, this.$cache = {
            win: t(i),
            body: t(e.body),
            input: t(s),
            cont: null,
            rs: null,
            min: null,
            max: null,
            from: null,
            to: null,
            single: null,
            bar: null,
            line: null,
            s_single: null,
            s_from: null,
            s_to: null,
            shad_single: null,
            shad_from: null,
            shad_to: null,
            edge: null,
            grid: null,
            grid_labels: []
        }, this.coords = {
            x_gap: 0,
            x_pointer: 0,
            w_rs: 0,
            w_rs_old: 0,
            w_handle: 0,
            p_gap: 0,
            p_gap_left: 0,
            p_gap_right: 0,
            p_step: 0,
            p_pointer: 0,
            p_handle: 0,
            p_single_fake: 0,
            p_single_real: 0,
            p_from_fake: 0,
            p_from_real: 0,
            p_to_fake: 0,
            p_to_real: 0,
            p_bar_x: 0,
            p_bar_w: 0,
            grid_gap: 0,
            big_num: 0,
            big: [],
            big_w: [],
            big_p: [],
            big_x: []
        }, this.labels = {
            w_min: 0,
            w_max: 0,
            w_from: 0,
            w_to: 0,
            w_single: 0,
            p_min: 0,
            p_max: 0,
            p_from_fake: 0,
            p_from_left: 0,
            p_to_fake: 0,
            p_to_left: 0,
            p_single_fake: 0,
            p_single_left: 0
        };
        var a = this.$cache.input;
        s = a.prop("value");
        var r;
        o = {
            type: "single",
            min: 10,
            max: 100,
            from: null,
            to: null,
            step: 1,
            min_interval: 0,
            max_interval: 0,
            drag_interval: !1,
            values: [],
            p_values: [],
            from_fixed: !1,
            from_min: null,
            from_max: null,
            from_shadow: !1,
            to_fixed: !1,
            to_min: null,
            to_max: null,
            to_shadow: !1,
            prettify_enabled: !0,
            prettify_separator: " ",
            prettify: null,
            force_edges: !1,
            keyboard: !1,
            keyboard_step: 5,
            grid: !1,
            grid_margin: !0,
            grid_num: 4,
            grid_snap: !1,
            hide_min_max: !1,
            hide_from_to: !1,
            prefix: "",
            postfix: "",
            max_postfix: "",
            decorate_both: !0,
            values_separator: " — ",
            input_values_separator: ";",
            disable: !1,
            onStart: null,
            onChange: null,
            onFinish: null,
            onUpdate: null
        }, a = {
            type: a.data("type"),
            min: a.data("min"),
            max: a.data("max"),
            from: a.data("from"),
            to: a.data("to"),
            step: a.data("step"),
            min_interval: a.data("minInterval"),
            max_interval: a.data("maxInterval"),
            drag_interval: a.data("dragInterval"),
            values: a.data("values"),
            from_fixed: a.data("fromFixed"),
            from_min: a.data("fromMin"),
            from_max: a.data("fromMax"),
            from_shadow: a.data("fromShadow"),
            to_fixed: a.data("toFixed"),
            to_min: a.data("toMin"),
            to_max: a.data("toMax"),
            to_shadow: a.data("toShadow"),
            prettify_enabled: a.data("prettifyEnabled"),
            prettify_separator: a.data("prettifySeparator"),
            force_edges: a.data("forceEdges"),
            keyboard: a.data("keyboard"),
            keyboard_step: a.data("keyboardStep"),
            grid: a.data("grid"),
            grid_margin: a.data("gridMargin"),
            grid_num: a.data("gridNum"),
            grid_snap: a.data("gridSnap"),
            hide_min_max: a.data("hideMinMax"),
            hide_from_to: a.data("hideFromTo"),
            prefix: a.data("prefix"),
            postfix: a.data("postfix"),
            max_postfix: a.data("maxPostfix"),
            decorate_both: a.data("decorateBoth"),
            values_separator: a.data("valuesSeparator"),
            input_values_separator: a.data("inputValuesSeparator"),
            disable: a.data("disable")
        }, a.values = a.values && a.values.split(",");
        for (r in a)a.hasOwnProperty(r) && (a[r] || 0 === a[r] || delete a[r]);
        s && (s = s.split(a.input_values_separator || n.input_values_separator || ";"), s[0] && s[0] == +s[0] && (s[0] = +s[0]), s[1] && s[1] == +s[1] && (s[1] = +s[1]), n && n.values && n.values.length ? (o.from = s[0] && n.values.indexOf(s[0]), o.to = s[1] && n.values.indexOf(s[1])) : (o.from = s[0] && +s[0], o.to = s[1] && +s[1])), t.extend(o, n), t.extend(o, a), this.options = o, this.validate(), this.result = {
            input: this.$cache.input,
            slider: null,
            min: this.options.min,
            max: this.options.max,
            from: this.options.from,
            from_percent: 0,
            from_value: null,
            to: this.options.to,
            to_percent: 0,
            to_value: null
        }, this.init()
    };
    r.prototype = {
        init: function (t) {
            this.no_diapason = !1, this.coords.p_step = this.convertToPercent(this.options.step, !0), this.target = "base", this.toggleInput(), this.append(), this.setMinMax(), t ? (this.force_redraw = !0, this.calc(!0), this.callOnUpdate()) : (this.force_redraw = !0, this.calc(!0), this.callOnStart()), this.updateScene()
        }, append: function () {
            this.$cache.input.before('<span class="irs js-irs-' + this.plugin_count + '"></span>'), this.$cache.input.prop("readonly", !0), this.$cache.cont = this.$cache.input.prev(), this.result.slider = this.$cache.cont, this.$cache.cont.html('<span class="irs"><span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span><span class="irs-min">0</span><span class="irs-max">1</span><span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span></span><span class="irs-grid"></span><span class="irs-bar"></span>'), this.$cache.rs = this.$cache.cont.find(".irs"), this.$cache.min = this.$cache.cont.find(".irs-min"), this.$cache.max = this.$cache.cont.find(".irs-max"), this.$cache.from = this.$cache.cont.find(".irs-from"), this.$cache.to = this.$cache.cont.find(".irs-to"), this.$cache.single = this.$cache.cont.find(".irs-single"), this.$cache.bar = this.$cache.cont.find(".irs-bar"), this.$cache.line = this.$cache.cont.find(".irs-line"), this.$cache.grid = this.$cache.cont.find(".irs-grid"), "single" === this.options.type ? (this.$cache.cont.append('<span class="irs-bar-edge"></span><span class="irs-shadow shadow-single"></span><span class="irs-slider single"></span>'), this.$cache.edge = this.$cache.cont.find(".irs-bar-edge"), this.$cache.s_single = this.$cache.cont.find(".single"), this.$cache.from[0].style.visibility = "hidden", this.$cache.to[0].style.visibility = "hidden", this.$cache.shad_single = this.$cache.cont.find(".shadow-single")) : (this.$cache.cont.append('<span class="irs-shadow shadow-from"></span><span class="irs-shadow shadow-to"></span><span class="irs-slider from"></span><span class="irs-slider to"></span>'), this.$cache.s_from = this.$cache.cont.find(".from"), this.$cache.s_to = this.$cache.cont.find(".to"), this.$cache.shad_from = this.$cache.cont.find(".shadow-from"), this.$cache.shad_to = this.$cache.cont.find(".shadow-to"), this.setTopHandler()), this.options.hide_from_to && (this.$cache.from[0].style.display = "none", this.$cache.to[0].style.display = "none", this.$cache.single[0].style.display = "none"), this.appendGrid(), this.options.disable ? (this.appendDisableMask(), this.$cache.input[0].disabled = !0) : (this.$cache.cont.removeClass("irs-disabled"), this.$cache.input[0].disabled = !1, this.bindEvents()), this.options.drag_interval && (this.$cache.bar[0].style.cursor = "ew-resize")
        }, setTopHandler: function () {
            var t = this.options.max, e = this.options.to;
            this.options.from > this.options.min && e === t ? this.$cache.s_from.addClass("type_last") : t > e && this.$cache.s_to.addClass("type_last")
        }, changeLevel: function (t) {
            switch (t) {
                case"single":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single_fake);
                    break;
                case"from":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake), this.$cache.s_from.addClass("state_hover"), this.$cache.s_from.addClass("type_last"), this.$cache.s_to.removeClass("type_last");
                    break;
                case"to":
                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to_fake), this.$cache.s_to.addClass("state_hover"), this.$cache.s_to.addClass("type_last"), this.$cache.s_from.removeClass("type_last");
                    break;
                case"both":
                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake), this.coords.p_gap_right = this.toFixed(this.coords.p_to_fake - this.coords.p_pointer), this.$cache.s_to.removeClass("type_last"), this.$cache.s_from.removeClass("type_last")
            }
        }, appendDisableMask: function () {
            this.$cache.cont.append('<span class="irs-disable-mask"></span>'), this.$cache.cont.addClass("irs-disabled")
        }, remove: function () {
            this.$cache.cont.remove(), this.$cache.cont = null, this.$cache.line.off("keydown.irs_" + this.plugin_count), this.$cache.body.off("touchmove.irs_" + this.plugin_count), this.$cache.body.off("mousemove.irs_" + this.plugin_count), this.$cache.win.off("touchend.irs_" + this.plugin_count), this.$cache.win.off("mouseup.irs_" + this.plugin_count), a && (this.$cache.body.off("mouseup.irs_" + this.plugin_count), this.$cache.body.off("mouseleave.irs_" + this.plugin_count)), this.$cache.grid_labels = [], this.coords.big = [], this.coords.big_w = [], this.coords.big_p = [], this.coords.big_x = [], cancelAnimationFrame(this.raf_id)
        }, bindEvents: function () {
            this.no_diapason || (this.$cache.body.on("touchmove.irs_" + this.plugin_count, this.pointerMove.bind(this)), this.$cache.body.on("mousemove.irs_" + this.plugin_count, this.pointerMove.bind(this)), this.$cache.win.on("touchend.irs_" + this.plugin_count, this.pointerUp.bind(this)), this.$cache.win.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this)), this.$cache.line.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.line.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.options.drag_interval && "double" === this.options.type ? (this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "both")), this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"))) : (this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"))), "single" === this.options.type ? (this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single")), this.$cache.s_single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single")), this.$cache.shad_single.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single")), this.$cache.s_single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single")), this.$cache.edge.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.shad_single.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"))) : (this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, null)), this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, null)), this.$cache.from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from")), this.$cache.s_from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from")), this.$cache.to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to")), this.$cache.s_to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to")), this.$cache.shad_from.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.shad_to.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from")), this.$cache.s_from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from")), this.$cache.to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to")), this.$cache.s_to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to")), this.$cache.shad_from.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click")), this.$cache.shad_to.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"))), this.options.keyboard && this.$cache.line.on("keydown.irs_" + this.plugin_count, this.key.bind(this, "keyboard")), a && (this.$cache.body.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this)), this.$cache.body.on("mouseleave.irs_" + this.plugin_count, this.pointerUp.bind(this))))
        }, pointerMove: function (t) {
            this.dragging && (this.coords.x_pointer = (t.pageX || t.originalEvent.touches && t.originalEvent.touches[0].pageX) - this.coords.x_gap, this.calc())
        }, pointerUp: function (e) {
            this.current_plugin === this.plugin_count && this.is_active && (this.is_active = !1, this.$cache.cont.find(".state_hover").removeClass("state_hover"), this.force_redraw = !0, a && t("*").prop("unselectable", !1), this.updateScene(), this.restoreOriginalMinInterval(), (t.contains(this.$cache.cont[0], e.target) || this.dragging) && (this.is_finish = !0, this.callOnFinish()), this.dragging = !1)
        }, pointerDown: function (e, i) {
            i.preventDefault();
            var s = i.pageX || i.originalEvent.touches && i.originalEvent.touches[0].pageX;
            2 !== i.button && ("both" === e && this.setTempMinInterval(), e || (e = this.target), this.current_plugin = this.plugin_count, this.target = e, this.dragging = this.is_active = !0, this.coords.x_gap = this.$cache.rs.offset().left, this.coords.x_pointer = s - this.coords.x_gap, this.calcPointerPercent(), this.changeLevel(e), a && t("*").prop("unselectable", !0), this.$cache.line.trigger("focus"), this.updateScene())
        }, pointerClick: function (t, e) {
            e.preventDefault();
            var i = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
            2 !== e.button && (this.current_plugin = this.plugin_count, this.target = t, this.is_click = !0, this.coords.x_gap = this.$cache.rs.offset().left, this.coords.x_pointer = +(i - this.coords.x_gap).toFixed(), this.force_redraw = !0, this.calc(), this.$cache.line.trigger("focus"))
        }, key: function (t, e) {
            if (!(this.current_plugin !== this.plugin_count || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey)) {
                switch (e.which) {
                    case 83:
                    case 65:
                    case 40:
                    case 37:
                        e.preventDefault(), this.moveByKey(!1);
                        break;
                    case 87:
                    case 68:
                    case 38:
                    case 39:
                        e.preventDefault(), this.moveByKey(!0)
                }
                return !0
            }
        }, moveByKey: function (t) {
            var e = this.coords.p_pointer, e = t ? e + this.options.keyboard_step : e - this.options.keyboard_step;
            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * e), this.is_key = !0, this.calc()
        }, setMinMax: function () {
            this.options && (this.options.hide_min_max ? (this.$cache.min[0].style.display = "none", this.$cache.max[0].style.display = "none") : (this.options.values.length ? (this.$cache.min.html(this.decorate(this.options.p_values[this.options.min])),
                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]))) : (this.$cache.min.html(this.decorate(this._prettify(this.options.min), this.options.min)), this.$cache.max.html(this.decorate(this._prettify(this.options.max), this.options.max))), this.labels.w_min = this.$cache.min.outerWidth(!1), this.labels.w_max = this.$cache.max.outerWidth(!1)))
        }, setTempMinInterval: function () {
            var t = this.result.to - this.result.from;
            null === this.old_min_interval && (this.old_min_interval = this.options.min_interval), this.options.min_interval = t
        }, restoreOriginalMinInterval: function () {
            null !== this.old_min_interval && (this.options.min_interval = this.old_min_interval, this.old_min_interval = null)
        }, calc: function (t) {
            if (this.options && (this.calc_count++, (10 === this.calc_count || t) && (this.calc_count = 0, this.coords.w_rs = this.$cache.rs.outerWidth(!1), this.calcHandlePercent()), this.coords.w_rs)) {
                switch (this.calcPointerPercent(), t = this.getHandleX(), "click" === this.target && (this.coords.p_gap = this.coords.p_handle / 2, t = this.getHandleX(), this.target = this.options.drag_interval ? "both_one" : this.chooseHandle(t)), this.target) {
                    case"base":
                        var e = (this.options.max - this.options.min) / 100;
                        t = (this.result.from - this.options.min) / e, e = (this.result.to - this.options.min) / e, this.coords.p_single_real = this.toFixed(t), this.coords.p_from_real = this.toFixed(t), this.coords.p_to_real = this.toFixed(e), this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max), this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max), this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max), this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real), this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real), this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real), this.target = null;
                        break;
                    case"single":
                        if (this.options.from_fixed)break;
                        this.coords.p_single_real = this.convertToRealPercent(t), this.coords.p_single_real = this.calcWithStep(this.coords.p_single_real), this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max), this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
                        break;
                    case"from":
                        if (this.options.from_fixed)break;
                        this.coords.p_from_real = this.convertToRealPercent(t), this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real), this.coords.p_from_real > this.coords.p_to_real && (this.coords.p_from_real = this.coords.p_to_real), this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max), this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from"), this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, "from"), this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
                        break;
                    case"to":
                        if (this.options.to_fixed)break;
                        this.coords.p_to_real = this.convertToRealPercent(t), this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real), this.coords.p_to_real < this.coords.p_from_real && (this.coords.p_to_real = this.coords.p_from_real), this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max), this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to"), this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, "to"), this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                        break;
                    case"both":
                        if (this.options.from_fixed || this.options.to_fixed)break;
                        t = this.toFixed(t + .1 * this.coords.p_handle), this.coords.p_from_real = this.convertToRealPercent(t) - this.coords.p_gap_left, this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real), this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max), this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from"), this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real), this.coords.p_to_real = this.convertToRealPercent(t) + this.coords.p_gap_right, this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real), this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max), this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to"), this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
                        break;
                    case"both_one":
                        if (this.options.from_fixed || this.options.to_fixed)break;
                        var i = this.convertToRealPercent(t);
                        t = this.result.to_percent - this.result.from_percent;
                        var s = t / 2, e = i - s, i = i + s;
                        0 > e && (e = 0, i = e + t), i > 100 && (i = 100, e = i - t), this.coords.p_from_real = this.calcWithStep(e), this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max), this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real), this.coords.p_to_real = this.calcWithStep(i), this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max), this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real)
                }
                "single" === this.options.type ? (this.coords.p_bar_x = this.coords.p_handle / 2, this.coords.p_bar_w = this.coords.p_single_fake, this.result.from_percent = this.coords.p_single_real, this.result.from = this.convertToValue(this.coords.p_single_real), this.options.values.length && (this.result.from_value = this.options.values[this.result.from])) : (this.coords.p_bar_x = this.toFixed(this.coords.p_from_fake + this.coords.p_handle / 2), this.coords.p_bar_w = this.toFixed(this.coords.p_to_fake - this.coords.p_from_fake), this.result.from_percent = this.coords.p_from_real, this.result.from = this.convertToValue(this.coords.p_from_real), this.result.to_percent = this.coords.p_to_real, this.result.to = this.convertToValue(this.coords.p_to_real), this.options.values.length && (this.result.from_value = this.options.values[this.result.from], this.result.to_value = this.options.values[this.result.to])), this.calcMinMax(), this.calcLabels()
            }
        }, calcPointerPercent: function () {
            this.coords.w_rs ? (0 > this.coords.x_pointer || isNaN(this.coords.x_pointer) ? this.coords.x_pointer = 0 : this.coords.x_pointer > this.coords.w_rs && (this.coords.x_pointer = this.coords.w_rs), this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100)) : this.coords.p_pointer = 0
        }, convertToRealPercent: function (t) {
            return t / (100 - this.coords.p_handle) * 100
        }, convertToFakePercent: function (t) {
            return t / 100 * (100 - this.coords.p_handle)
        }, getHandleX: function () {
            var t = 100 - this.coords.p_handle, e = this.toFixed(this.coords.p_pointer - this.coords.p_gap);
            return 0 > e ? e = 0 : e > t && (e = t), e
        }, calcHandlePercent: function () {
            this.coords.w_handle = "single" === this.options.type ? this.$cache.s_single.outerWidth(!1) : this.$cache.s_from.outerWidth(!1), this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100)
        }, chooseHandle: function (t) {
            return "single" === this.options.type ? "single" : t >= this.coords.p_from_real + (this.coords.p_to_real - this.coords.p_from_real) / 2 ? this.options.to_fixed ? "from" : "to" : this.options.from_fixed ? "to" : "from"
        }, calcMinMax: function () {
            this.coords.w_rs && (this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100, this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100)
        }, calcLabels: function () {
            this.coords.w_rs && !this.options.hide_from_to && ("single" === this.options.type ? (this.labels.w_single = this.$cache.single.outerWidth(!1), this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100, this.labels.p_single_left = this.coords.p_single_fake + this.coords.p_handle / 2 - this.labels.p_single_fake / 2) : (this.labels.w_from = this.$cache.from.outerWidth(!1), this.labels.p_from_fake = this.labels.w_from / this.coords.w_rs * 100, this.labels.p_from_left = this.coords.p_from_fake + this.coords.p_handle / 2 - this.labels.p_from_fake / 2, this.labels.p_from_left = this.toFixed(this.labels.p_from_left), this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from_fake), this.labels.w_to = this.$cache.to.outerWidth(!1), this.labels.p_to_fake = this.labels.w_to / this.coords.w_rs * 100, this.labels.p_to_left = this.coords.p_to_fake + this.coords.p_handle / 2 - this.labels.p_to_fake / 2, this.labels.p_to_left = this.toFixed(this.labels.p_to_left), this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to_fake), this.labels.w_single = this.$cache.single.outerWidth(!1), this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100, this.labels.p_single_left = (this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to_fake) / 2 - this.labels.p_single_fake / 2, this.labels.p_single_left = this.toFixed(this.labels.p_single_left)), this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake))
        }, updateScene: function () {
            this.raf_id && (cancelAnimationFrame(this.raf_id), this.raf_id = null), clearTimeout(this.update_tm), this.update_tm = null, this.options && (this.drawHandles(), this.is_active ? this.raf_id = requestAnimationFrame(this.updateScene.bind(this)) : this.update_tm = setTimeout(this.updateScene.bind(this), 300))
        }, drawHandles: function () {
            this.coords.w_rs = this.$cache.rs.outerWidth(!1), this.coords.w_rs && (this.coords.w_rs !== this.coords.w_rs_old && (this.target = "base", this.is_resize = !0), (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) && (this.setMinMax(), this.calc(!0), this.drawLabels(), this.options.grid && (this.calcGridMargin(), this.calcGridLabels()), this.force_redraw = !0, this.coords.w_rs_old = this.coords.w_rs, this.drawShadow()), this.coords.w_rs && (this.dragging || this.force_redraw || this.is_key) && ((this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) && (this.drawLabels(), this.$cache.bar[0].style.left = this.coords.p_bar_x + "%", this.$cache.bar[0].style.width = this.coords.p_bar_w + "%", "single" === this.options.type ? (this.$cache.s_single[0].style.left = this.coords.p_single_fake + "%", this.$cache.single[0].style.left = this.labels.p_single_left + "%", this.options.values.length ? this.$cache.input.prop("value", this.result.from_value) : this.$cache.input.prop("value", this.result.from), this.$cache.input.data("from", this.result.from)) : (this.$cache.s_from[0].style.left = this.coords.p_from_fake + "%", this.$cache.s_to[0].style.left = this.coords.p_to_fake + "%", (this.old_from !== this.result.from || this.force_redraw) && (this.$cache.from[0].style.left = this.labels.p_from_left + "%"), (this.old_to !== this.result.to || this.force_redraw) && (this.$cache.to[0].style.left = this.labels.p_to_left + "%"), this.$cache.single[0].style.left = this.labels.p_single_left + "%", this.options.values.length ? this.$cache.input.prop("value", this.result.from_value + this.options.input_values_separator + this.result.to_value) : this.$cache.input.prop("value", this.result.from + this.options.input_values_separator + this.result.to), this.$cache.input.data("from", this.result.from), this.$cache.input.data("to", this.result.to)), this.old_from === this.result.from && this.old_to === this.result.to || this.is_start || this.$cache.input.trigger("change"), this.old_from = this.result.from, this.old_to = this.result.to, this.is_resize || this.is_update || this.is_start || this.is_finish || this.callOnChange(), (this.is_key || this.is_click) && (this.is_click = this.is_key = !1, this.callOnFinish()), this.is_finish = this.is_resize = this.is_update = !1), this.force_redraw = this.is_click = this.is_key = this.is_start = !1))
        }, drawLabels: function () {
            if (this.options) {
                var t, e = this.options.values.length, i = this.options.p_values;
                if (!this.options.hide_from_to)if ("single" === this.options.type)e = e ? this.decorate(i[this.result.from]) : this.decorate(this._prettify(this.result.from), this.result.from), this.$cache.single.html(e), this.calcLabels(), this.$cache.min[0].style.visibility = this.labels.p_single_left < this.labels.p_min + 1 ? "hidden" : "visible", this.$cache.max[0].style.visibility = this.labels.p_single_left + this.labels.p_single_fake > 100 - this.labels.p_max - 1 ? "hidden" : "visible"; else {
                    e ? (this.options.decorate_both ? (e = this.decorate(i[this.result.from]), e += this.options.values_separator, e += this.decorate(i[this.result.to])) : e = this.decorate(i[this.result.from] + this.options.values_separator + i[this.result.to]), t = this.decorate(i[this.result.from]), i = this.decorate(i[this.result.to])) : (this.options.decorate_both ? (e = this.decorate(this._prettify(this.result.from), this.result.from), e += this.options.values_separator, e += this.decorate(this._prettify(this.result.to), this.result.to)) : e = this.decorate(this._prettify(this.result.from) + this.options.values_separator + this._prettify(this.result.to), this.result.to), t = this.decorate(this._prettify(this.result.from), this.result.from), i = this.decorate(this._prettify(this.result.to), this.result.to)), this.$cache.single.html(e), this.$cache.from.html(t), this.$cache.to.html(i), this.calcLabels(), i = Math.min(this.labels.p_single_left, this.labels.p_from_left), e = this.labels.p_single_left + this.labels.p_single_fake, t = this.labels.p_to_left + this.labels.p_to_fake;
                    var s = Math.max(e, t);
                    this.labels.p_from_left + this.labels.p_from_fake >= this.labels.p_to_left ? (this.$cache.from[0].style.visibility = "hidden", this.$cache.to[0].style.visibility = "hidden", this.$cache.single[0].style.visibility = "visible", this.result.from === this.result.to ? ("from" === this.target ? this.$cache.from[0].style.visibility = "visible" : "to" === this.target && (this.$cache.to[0].style.visibility = "visible"), this.$cache.single[0].style.visibility = "hidden", s = t) : (this.$cache.from[0].style.visibility = "hidden", this.$cache.to[0].style.visibility = "hidden", this.$cache.single[0].style.visibility = "visible", s = Math.max(e, t))) : (this.$cache.from[0].style.visibility = "visible", this.$cache.to[0].style.visibility = "visible", this.$cache.single[0].style.visibility = "hidden"), this.$cache.min[0].style.visibility = i < this.labels.p_min + 1 ? "hidden" : "visible", this.$cache.max[0].style.visibility = s > 100 - this.labels.p_max - 1 ? "hidden" : "visible"
                }
            }
        }, drawShadow: function () {
            var t = this.options, e = this.$cache, i = "number" == typeof t.from_min && !isNaN(t.from_min), s = "number" == typeof t.from_max && !isNaN(t.from_max), n = "number" == typeof t.to_min && !isNaN(t.to_min), o = "number" == typeof t.to_max && !isNaN(t.to_max);
            "single" === t.type ? t.from_shadow && (i || s) ? (i = this.convertToPercent(i ? t.from_min : t.min), s = this.convertToPercent(s ? t.from_max : t.max) - i, i = this.toFixed(i - this.coords.p_handle / 100 * i), s = this.toFixed(s - this.coords.p_handle / 100 * s), i += this.coords.p_handle / 2, e.shad_single[0].style.display = "block", e.shad_single[0].style.left = i + "%", e.shad_single[0].style.width = s + "%") : e.shad_single[0].style.display = "none" : (t.from_shadow && (i || s) ? (i = this.convertToPercent(i ? t.from_min : t.min), s = this.convertToPercent(s ? t.from_max : t.max) - i, i = this.toFixed(i - this.coords.p_handle / 100 * i), s = this.toFixed(s - this.coords.p_handle / 100 * s), i += this.coords.p_handle / 2, e.shad_from[0].style.display = "block", e.shad_from[0].style.left = i + "%", e.shad_from[0].style.width = s + "%") : e.shad_from[0].style.display = "none", t.to_shadow && (n || o) ? (n = this.convertToPercent(n ? t.to_min : t.min), t = this.convertToPercent(o ? t.to_max : t.max) - n, n = this.toFixed(n - this.coords.p_handle / 100 * n), t = this.toFixed(t - this.coords.p_handle / 100 * t), n += this.coords.p_handle / 2, e.shad_to[0].style.display = "block", e.shad_to[0].style.left = n + "%", e.shad_to[0].style.width = t + "%") : e.shad_to[0].style.display = "none")
        }, callOnStart: function () {
            this.options.onStart && "function" == typeof this.options.onStart && this.options.onStart(this.result)
        }, callOnChange: function () {
            this.options.onChange && "function" == typeof this.options.onChange && this.options.onChange(this.result)
        }, callOnFinish: function () {
            this.options.onFinish && "function" == typeof this.options.onFinish && this.options.onFinish(this.result)
        }, callOnUpdate: function () {
            this.options.onUpdate && "function" == typeof this.options.onUpdate && this.options.onUpdate(this.result)
        }, toggleInput: function () {
            this.$cache.input.toggleClass("irs-hidden-input")
        }, convertToPercent: function (t, e) {
            var i = this.options.max - this.options.min;
            return i ? this.toFixed((e ? t : t - this.options.min) / (i / 100)) : (this.no_diapason = !0, 0)
        }, convertToValue: function (t) {
            var e, i, s = this.options.min, n = this.options.max, o = s.toString().split(".")[1], a = n.toString().split(".")[1], r = 0, h = 0;
            return 0 === t ? this.options.min : 100 === t ? this.options.max : (o && (r = e = o.length), a && (r = i = a.length), e && i && (r = e >= i ? e : i), 0 > s && (h = Math.abs(s), s = +(s + h).toFixed(r), n = +(n + h).toFixed(r)), t = (n - s) / 100 * t + s, (s = this.options.step.toString().split(".")[1]) ? t = +t.toFixed(s.length) : (t /= this.options.step, t *= this.options.step, t = +t.toFixed(0)), h && (t -= h), h = s ? +t.toFixed(s.length) : this.toFixed(t), h < this.options.min ? h = this.options.min : h > this.options.max && (h = this.options.max), h)
        }, calcWithStep: function (t) {
            var e = Math.round(t / this.coords.p_step) * this.coords.p_step;
            return e > 100 && (e = 100), 100 === t && (e = 100), this.toFixed(e)
        }, checkMinInterval: function (t, e, i) {
            var s = this.options;
            return s.min_interval ? (t = this.convertToValue(t), e = this.convertToValue(e), "from" === i ? e - t < s.min_interval && (t = e - s.min_interval) : t - e < s.min_interval && (t = e + s.min_interval), this.convertToPercent(t)) : t
        }, checkMaxInterval: function (t, e, i) {
            var s = this.options;
            return s.max_interval ? (t = this.convertToValue(t), e = this.convertToValue(e), "from" === i ? e - t > s.max_interval && (t = e - s.max_interval) : t - e > s.max_interval && (t = e + s.max_interval), this.convertToPercent(t)) : t
        }, checkDiapason: function (t, e, i) {
            t = this.convertToValue(t);
            var s = this.options;
            return "number" != typeof e && (e = s.min), "number" != typeof i && (i = s.max), e > t && (t = e), t > i && (t = i), this.convertToPercent(t)
        }, toFixed: function (t) {
            return t = t.toFixed(9), +t
        }, _prettify: function (t) {
            return this.options.prettify_enabled ? this.options.prettify && "function" == typeof this.options.prettify ? this.options.prettify(t) : this.prettify(t) : t
        }, prettify: function (t) {
            return t.toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + this.options.prettify_separator)
        }, checkEdges: function (t, e) {
            return this.options.force_edges ? (0 > t ? t = 0 : t > 100 - e && (t = 100 - e), this.toFixed(t)) : this.toFixed(t)
        }, validate: function () {
            var t, e, i = this.options, s = this.result, n = i.values, o = n.length;
            if ("string" == typeof i.min && (i.min = +i.min), "string" == typeof i.max && (i.max = +i.max), "string" == typeof i.from && (i.from = +i.from), "string" == typeof i.to && (i.to = +i.to), "string" == typeof i.step && (i.step = +i.step), "string" == typeof i.from_min && (i.from_min = +i.from_min), "string" == typeof i.from_max && (i.from_max = +i.from_max), "string" == typeof i.to_min && (i.to_min = +i.to_min), "string" == typeof i.to_max && (i.to_max = +i.to_max), "string" == typeof i.keyboard_step && (i.keyboard_step = +i.keyboard_step), "string" == typeof i.grid_num && (i.grid_num = +i.grid_num), i.max < i.min && (i.max = i.min), o)for (i.p_values = [], i.min = 0, i.max = o - 1, i.step = 1, i.grid_num = i.max, i.grid_snap = !0, e = 0; o > e; e++)t = +n[e], isNaN(t) ? t = n[e] : (n[e] = t, t = this._prettify(t)), i.p_values.push(t);
            ("number" != typeof i.from || isNaN(i.from)) && (i.from = i.min), ("number" != typeof i.to || isNaN(i.from)) && (i.to = i.max), "single" === i.type ? (i.from < i.min && (i.from = i.min), i.from > i.max && (i.from = i.max)) : ((i.from < i.min || i.from > i.max) && (i.from = i.min), (i.to > i.max || i.to < i.min) && (i.to = i.max), i.from > i.to && (i.from = i.to)), ("number" != typeof i.step || isNaN(i.step) || !i.step || 0 > i.step) && (i.step = 1), ("number" != typeof i.keyboard_step || isNaN(i.keyboard_step) || !i.keyboard_step || 0 > i.keyboard_step) && (i.keyboard_step = 5), "number" == typeof i.from_min && i.from < i.from_min && (i.from = i.from_min), "number" == typeof i.from_max && i.from > i.from_max && (i.from = i.from_max), "number" == typeof i.to_min && i.to < i.to_min && (i.to = i.to_min), "number" == typeof i.to_max && i.from > i.to_max && (i.to = i.to_max), s && (s.min !== i.min && (s.min = i.min), s.max !== i.max && (s.max = i.max), (s.from < s.min || s.from > s.max) && (s.from = i.from), (s.to < s.min || s.to > s.max) && (s.to = i.to)), ("number" != typeof i.min_interval || isNaN(i.min_interval) || !i.min_interval || 0 > i.min_interval) && (i.min_interval = 0), ("number" != typeof i.max_interval || isNaN(i.max_interval) || !i.max_interval || 0 > i.max_interval) && (i.max_interval = 0), i.min_interval && i.min_interval > i.max - i.min && (i.min_interval = i.max - i.min), i.max_interval && i.max_interval > i.max - i.min && (i.max_interval = i.max - i.min)
        }, decorate: function (t, e) {
            var i = "", s = this.options;
            return s.prefix && (i += s.prefix), i += t, s.max_postfix && (s.values.length && t === s.p_values[s.max] ? (i += s.max_postfix, s.postfix && (i += " ")) : e === s.max && (i += s.max_postfix, s.postfix && (i += " "))), s.postfix && (i += s.postfix), i
        }, updateFrom: function () {
            this.result.from = this.options.from, this.result.from_percent = this.convertToPercent(this.result.from), this.options.values && (this.result.from_value = this.options.values[this.result.from])
        }, updateTo: function () {
            this.result.to = this.options.to, this.result.to_percent = this.convertToPercent(this.result.to), this.options.values && (this.result.to_value = this.options.values[this.result.to])
        }, updateResult: function () {
            this.result.min = this.options.min, this.result.max = this.options.max, this.updateFrom(), this.updateTo()
        }, appendGrid: function () {
            if (this.options.grid) {
                var t, e, i = this.options;
                t = i.max - i.min;
                var s, n, o = i.grid_num, a = 0, r = 0, h = 4, l = 0, c = "";
                for (this.calcGridMargin(), i.grid_snap ? (o = t / i.step, a = this.toFixed(i.step / (t / 100))) : a = this.toFixed(100 / o), o > 4 && (h = 3), o > 7 && (h = 2), o > 14 && (h = 1), o > 28 && (h = 0), t = 0; o + 1 > t; t++) {
                    for (s = h, r = this.toFixed(a * t), r > 100 && (r = 100, s -= 2, 0 > s && (s = 0)), this.coords.big[t] = r, n = (r - a * (t - 1)) / (s + 1), e = 1; s >= e && 0 !== r; e++)l = this.toFixed(r - n * e), c += '<span class="irs-grid-pol small" style="left: ' + l + '%"></span>';
                    c += '<span class="irs-grid-pol" style="left: ' + r + '%"></span>', l = this.convertToValue(r), l = i.values.length ? i.p_values[l] : this._prettify(l), c += '<span class="irs-grid-text js-grid-text-' + t + '" style="left: ' + r + '%">' + l + "</span>"
                }
                this.coords.big_num = Math.ceil(o + 1), this.$cache.cont.addClass("irs-with-grid"), this.$cache.grid.html(c), this.cacheGridLabels()
            }
        }, cacheGridLabels: function () {
            var t, e, i = this.coords.big_num;
            for (e = 0; i > e; e++)t = this.$cache.grid.find(".js-grid-text-" + e), this.$cache.grid_labels.push(t);
            this.calcGridLabels()
        }, calcGridLabels: function () {
            var t, e;
            e = [];
            var i = [], s = this.coords.big_num;
            for (t = 0; s > t; t++)this.coords.big_w[t] = this.$cache.grid_labels[t].outerWidth(!1), this.coords.big_p[t] = this.toFixed(this.coords.big_w[t] / this.coords.w_rs * 100), this.coords.big_x[t] = this.toFixed(this.coords.big_p[t] / 2), e[t] = this.toFixed(this.coords.big[t] - this.coords.big_x[t]), i[t] = this.toFixed(e[t] + this.coords.big_p[t]);
            for (this.options.force_edges && (e[0] < -this.coords.grid_gap && (e[0] = -this.coords.grid_gap, i[0] = this.toFixed(e[0] + this.coords.big_p[0]), this.coords.big_x[0] = this.coords.grid_gap), i[s - 1] > 100 + this.coords.grid_gap && (i[s - 1] = 100 + this.coords.grid_gap, e[s - 1] = this.toFixed(i[s - 1] - this.coords.big_p[s - 1]), this.coords.big_x[s - 1] = this.toFixed(this.coords.big_p[s - 1] - this.coords.grid_gap))), this.calcGridCollision(2, e, i), this.calcGridCollision(4, e, i), t = 0; s > t; t++)e = this.$cache.grid_labels[t][0], e.style.marginLeft = -this.coords.big_x[t] + "%"
        }, calcGridCollision: function (t, e, i) {
            var s, n, o, a = this.coords.big_num;
            for (s = 0; a > s && (n = s + t / 2, !(n >= a)); s += t)o = this.$cache.grid_labels[n][0], o.style.visibility = i[s] <= e[n] ? "visible" : "hidden"
        }, calcGridMargin: function () {
            this.options.grid_margin && (this.coords.w_rs = this.$cache.rs.outerWidth(!1), this.coords.w_rs && (this.coords.w_handle = "single" === this.options.type ? this.$cache.s_single.outerWidth(!1) : this.$cache.s_from.outerWidth(!1), this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100), this.coords.grid_gap = this.toFixed(this.coords.p_handle / 2 - .1), this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + "%", this.$cache.grid[0].style.left = this.coords.grid_gap + "%"))
        }, update: function (e) {
            this.input && (this.is_update = !0, this.options.from = this.result.from, this.options.to = this.result.to, this.options = t.extend(this.options, e), this.validate(), this.updateResult(e), this.toggleInput(), this.remove(), this.init(!0))
        }, reset: function () {
            this.input && (this.updateResult(), this.update())
        }, destroy: function () {
            this.input && (this.toggleInput(), this.$cache.input.prop("readonly", !1), t.data(this.input, "ionRangeSlider", null), this.remove(), this.options = this.input = null)
        }
    }, t.fn.ionRangeSlider = function (e) {
        return this.each(function () {
            t.data(this, "ionRangeSlider") || t.data(this, "ionRangeSlider", new r(this, e, o++))
        })
    }, function () {
        for (var t = 0, e = ["ms", "moz", "webkit", "o"], s = 0; s < e.length && !i.requestAnimationFrame; ++s)i.requestAnimationFrame = i[e[s] + "RequestAnimationFrame"], i.cancelAnimationFrame = i[e[s] + "CancelAnimationFrame"] || i[e[s] + "CancelRequestAnimationFrame"];
        i.requestAnimationFrame || (i.requestAnimationFrame = function (e, s) {
            var n = (new Date).getTime(), o = Math.max(0, 16 - (n - t)), a = i.setTimeout(function () {
                e(n + o)
            }, o);
            return t = n + o, a
        }), i.cancelAnimationFrame || (i.cancelAnimationFrame = function (t) {
            clearTimeout(t)
        })
    }()
}(jQuery, document, window, navigator), function (t) {
    t.fn.tableHeadFixer = function (e) {
        function i() {
            var e = t(p.table);
            if (p.head) {
                if (p.left > 0) {
                    var i = e.find("thead tr");
                    i.each(function (e, i) {
                        l(i, function (e) {
                            t(e).css("z-index", p["z-index"] + 10)
                        })
                    })
                }
                if (p.right > 0) {
                    var i = e.find("thead tr");
                    i.each(function (e, i) {
                        c(i, function (e) {
                            t(e).css("z-index", p["z-index"] + 1)
                        })
                    })
                }
            }
            if (p.foot) {
                if (p.left > 0) {
                    var i = e.find("tfoot tr");
                    i.each(function (e, i) {
                        l(i, function (e) {
                            t(e).css("z-index", p["z-index"])
                        })
                    })
                }
                if (p.right > 0) {
                    var i = e.find("tfoot tr");
                    i.each(function (e, i) {
                        c(i, function (e) {
                            t(e).css("z-index", p["z-index"])
                        })
                    })
                }
            }
        }

        function s() {
            var e = t(p.parent), i = t(p.table);
            e.append(i), e.css({"overflow-x": "auto", "overflow-y": "auto"}), e.scroll(function () {
                var t = e[0].scrollWidth, i = e[0].clientWidth, s = e[0].scrollHeight, n = e[0].clientHeight, o = e.scrollTop(), a = e.scrollLeft();
                p.head && this.find("thead tr > *").css("top", o), p.foot && this.find("tfoot tr > *").css("bottom", s - n - o), p.left > 0 && p.leftColumns.css("left", a), p.right > 0 && p.rightColumns.css("right", t - i - a)
            }.bind(i))
        }

        function n() {
            var e = t(p.table).find("thead"), i = (e.find("tr"), e.find("tr > *"));
            h(i), i.css({position: "relative"})
        }

        function o() {
            var e = t(p.table).find("tfoot"), i = (e.find("tr"), e.find("tr > *"));
            h(i), i.css({position: "relative"})
        }

        function a() {
            var e = t(p.table);
            p.leftColumns = t();
            var i = e.find("tr");
            i.each(function (t, e) {
                l(e, function (t) {
                    p.leftColumns = p.leftColumns.add(t)
                })
            });
            var s = p.leftColumns;
            s.each(function (e, i) {
                var i = t(i);
                h(i), i.css({position: "relative"})
            })
        }

        function r() {
            var e = t(p.table);
            p.right;
            p.rightColumns = t();
            var i = e.find("tr");
            i.each(function (t, e) {
                c(e, function (t) {
                    p.rightColumns = p.rightColumns.add(t)
                })
            });
            var s = p.rightColumns;
            s.each(function (e, i) {
                var i = t(i);
                h(i), i.css({position: "relative"})
            })
        }

        function h(e) {
            e.each(function (e, i) {
                var i = t(i);
                t(i).parent()
            })
        }

        function l(e, i) {
            for (var s = p.left, n = 1, o = 1; s >= o; o += n) {
                var a = n > 1 ? o - 1 : o, r = t(e).find("> *:nth-child(" + a + ")"), h = r.prop("colspan");
                (r.length && r.cellPos().left < s) && i(r), n = h
            }
        }

        function c(e, i) {
            for (var s = p.right, n = 1, o = 1; s >= o; o += n) {
                var a = n > 1 ? o - 1 : o, r = t(e).find("> *:nth-last-child(" + a + ")"), h = r.prop("colspan");
                i(r), n = h
            }
        }

        var d = {head: !0, foot: !1, left: 0, right: 0, "z-index": 0}, p = t.extend({}, d, e);
        return this.each(function () {
            p.table = this, p.parent = t(p.table).parent(), s(), 1 == p.head && n(), 1 == p.foot && o(), p.left > 0 && a(), p.right > 0 && r(), i(), t(p.parent).trigger("scroll"), t(window).resize(function () {
                t(p.parent).trigger("scroll")
            })
        })
    }
}(jQuery), function (t) {
    function e(e) {
        var i = [];
        e.children("tr").each(function (e, s) {
            t(s).children("td, th").each(function (s, n) {
                var o, a, r = t(n), h = 0 | r.attr("colspan"), l = 0 | r.attr("rowspan");
                for (h = h ? h : 1, l = l ? l : 1; i[e] && i[e][s]; ++s);
                for (o = s; s + h > o; ++o)for (a = e; e + l > a; ++a)i[a] || (i[a] = []), i[a][o] = !0;
                var c = {top: e, left: s};
                r.data("cellPos", c)
            })
        })
    }

    t.fn.cellPos = function (t) {
        var i = this.first(), s = i.data("cellPos");
        if (!s || t) {
            var n = i.closest("table, thead, tbody, tfoot");
            e(n)
        }
        return s = i.data("cellPos")
    }
}(jQuery);
var Datepicker;
!function (t, e, i) {
    var s, n, o, a = "datepicker", r = ".datepicker-here", h = !1, l = '<div class="datepicker"><i class="datepicker--pointer"></i><nav class="datepicker--nav"></nav><div class="datepicker--content"></div></div>', c = {
        classes: "",
        inline: !1,
        language: "ru",
        startDate: new Date,
        firstDay: "",
        weekends: [6, 0],
        dateFormat: "",
        altField: "",
        altFieldDateFormat: "@",
        toggleSelected: !0,
        keyboardNav: !0,
        position: "bottom left",
        offset: 12,
        view: "days",
        minView: "days",
        showOtherMonths: !0,
        selectOtherMonths: !0,
        moveToOtherMonthsOnSelect: !0,
        showOtherYears: !0,
        selectOtherYears: !0,
        moveToOtherYearsOnSelect: !0,
        minDate: "",
        maxDate: "",
        disableNavWhenOutOfRange: !0,
        multipleDates: !1,
        multipleDatesSeparator: ",",
        range: !1,
        todayButton: !1,
        clearButton: !1,
        showEvent: "focus",
        autoClose: !1,
        monthsField: "monthsShort",
        prevHtml: '<svg><path d="M 17,12 l -5,5 l 5,5"></path></svg>',
        nextHtml: '<svg><path d="M 14,12 l 5,5 l -5,5"></path></svg>',
        navTitles: {days: "MM, <i>yyyy</i>", months: "yyyy", years: "yyyy1 - yyyy2"},
        onSelect: "",
        onChangeMonth: "",
        onChangeYear: "",
        onChangeDecade: "",
        onChangeView: "",
        onRenderCell: ""
    }, d = {
        ctrlRight: [17, 39],
        ctrlUp: [17, 38],
        ctrlLeft: [17, 37],
        ctrlDown: [17, 40],
        shiftRight: [16, 39],
        shiftUp: [16, 38],
        shiftLeft: [16, 37],
        shiftDown: [16, 40],
        altUp: [18, 38],
        altRight: [18, 39],
        altLeft: [18, 37],
        altDown: [18, 40],
        ctrlShiftUp: [16, 17, 38]
    };
    Datepicker = function (t, n) {
        this.el = t, this.$el = e(t), this.opts = e.extend(!0, {}, c, n, this.$el.data()), s == i && (s = e("body")), this.opts.startDate || (this.opts.startDate = new Date), "INPUT" == this.el.nodeName && (this.elIsInput = !0), this.opts.altField && (this.$altField = "string" == typeof this.opts.altField ? e(this.opts.altField) : this.opts.altField), this.inited = !1, this.visible = !1, this.silent = !1, this.currentDate = this.opts.startDate, this.currentView = this.opts.view, this._createShortCuts(), this.selectedDates = [], this.views = {}, this.keys = [], this.minRange = "", this.maxRange = "", this.init()
    }, o = Datepicker, o.prototype = {
        viewIndexes: ["days", "months", "years"], init: function () {
            h || this.opts.inline || !this.elIsInput || this._buildDatepickersContainer(), this._buildBaseHtml(), this._defineLocale(this.opts.language), this._syncWithMinMaxDates(), this.elIsInput && (this.opts.inline || (this._setPositionClasses(this.opts.position), this._bindEvents()), this.opts.keyboardNav && this._bindKeyboardEvents(), this.$datepicker.on("mousedown", this._onMouseDownDatepicker.bind(this)), this.$datepicker.on("mouseup", this._onMouseUpDatepicker.bind(this))), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this.views[this.currentView] = new Datepicker.Body(this, this.currentView, this.opts), this.views[this.currentView].show(), this.nav = new Datepicker.Navigation(this, this.opts), this.view = this.currentView, this.$datepicker.on("mouseenter", ".datepicker--cell", this._onMouseEnterCell.bind(this)), this.$datepicker.on("mouseleave", ".datepicker--cell", this._onMouseLeaveCell.bind(this)), this.inited = !0
        }, _createShortCuts: function () {
            this.minDate = this.opts.minDate ? this.opts.minDate : new Date(-86399999136e5), this.maxDate = this.opts.maxDate ? this.opts.maxDate : new Date(86399999136e5)
        }, _bindEvents: function () {
            this.$el.on(this.opts.showEvent + ".adp", this._onShowEvent.bind(this)), this.$el.on("blur.adp", this._onBlur.bind(this)), this.$el.on("input.adp", this._onInput.bind(this)), e(t).on("resize.adp", this._onResize.bind(this))
        }, _bindKeyboardEvents: function () {
            this.$el.on("keydown.adp", this._onKeyDown.bind(this)), this.$el.on("keyup.adp", this._onKeyUp.bind(this)), this.$el.on("hotKey.adp", this._onHotKey.bind(this))
        }, isWeekend: function (t) {
            return -1 !== this.opts.weekends.indexOf(t)
        }, _defineLocale: function (t) {
            "string" == typeof t ? (this.loc = Datepicker.language[t], this.loc || (console.warn("Can't find language \"" + t + '" in Datepicker.language, will use "ru" instead'), this.loc = e.extend(!0, {}, Datepicker.language.ru)), this.loc = e.extend(!0, {}, Datepicker.language.ru, Datepicker.language[t])) : this.loc = e.extend(!0, {}, Datepicker.language.ru, t), this.opts.dateFormat && (this.loc.dateFormat = this.opts.dateFormat), "" !== this.opts.firstDay && (this.loc.firstDay = this.opts.firstDay)
        }, _buildDatepickersContainer: function () {
            h = !0, s.append('<div class="datepickers-container" id="datepickers-container"></div>'), n = e("#datepickers-container")
        }, _buildBaseHtml: function () {
            var t, i = e('<div class="datepicker-inline">');
            t = "INPUT" == this.el.nodeName ? this.opts.inline ? i.insertAfter(this.$el) : n : i.appendTo(this.$el), this.$datepicker = e(l).appendTo(t), this.$content = e(".datepicker--content", this.$datepicker), this.$nav = e(".datepicker--nav", this.$datepicker)
        }, _triggerOnChange: function () {
            if (!this.selectedDates.length)return this.opts.onSelect("", "", this);
            var t, e = this.selectedDates, i = o.getParsedDate(e[0]), s = this, n = new Date(i.year, i.month, i.date);
            t = e.map(function (t) {
                return s.formatDate(s.loc.dateFormat, t)
            }).join(this.opts.multipleDatesSeparator), (this.opts.multipleDates || this.opts.range) && (n = e.map(function (t) {
                var e = o.getParsedDate(t);
                return new Date(e.year, e.month, e.date);
            })), this.opts.onSelect(t, n, this)
        }, next: function () {
            var t = this.parsedDate, e = this.opts;
            switch (this.view) {
                case"days":
                    this.date = new Date(t.year, t.month + 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case"months":
                    this.date = new Date(t.year + 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
                    break;
                case"years":
                    this.date = new Date(t.year + 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade)
            }
        }, prev: function () {
            var t = this.parsedDate, e = this.opts;
            switch (this.view) {
                case"days":
                    this.date = new Date(t.year, t.month - 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case"months":
                    this.date = new Date(t.year - 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
                    break;
                case"years":
                    this.date = new Date(t.year - 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade)
            }
        }, formatDate: function (t, e) {
            e = e || this.date;
            var i = t, s = this._getWordBoundaryRegExp, n = this.loc, a = o.getDecade(e), r = o.getParsedDate(e);
            switch (!0) {
                case/@/.test(i):
                    i = i.replace(/@/, e.getTime());
                case/dd/.test(i):
                    i = i.replace(s("dd"), r.fullDate);
                case/d/.test(i):
                    i = i.replace(s("d"), r.date);
                case/DD/.test(i):
                    i = i.replace(s("DD"), n.days[r.day]);
                case/D/.test(i):
                    i = i.replace(s("D"), n.daysShort[r.day]);
                case/mm/.test(i):
                    i = i.replace(s("mm"), r.fullMonth);
                case/m/.test(i):
                    i = i.replace(s("m"), r.month + 1);
                case/MM/.test(i):
                    i = i.replace(s("MM"), this.loc.months[r.month]);
                case/M/.test(i):
                    i = i.replace(s("M"), n.monthsShort[r.month]);
                case/yyyy/.test(i):
                    i = i.replace(s("yyyy"), r.year);
                case/yyyy1/.test(i):
                    i = i.replace(s("yyyy1"), a[0]);
                case/yyyy2/.test(i):
                    i = i.replace(s("yyyy2"), a[1]);
                case/yy/.test(i):
                    i = i.replace(s("yy"), r.year.toString().slice(-2))
            }
            return i
        }, _getWordBoundaryRegExp: function (t) {
            return new RegExp("\\b(?=[a-zA-Z0-9äöüßÄÖÜ<])" + t + "(?![>a-zA-Z0-9äöüßÄÖÜ])")
        }, selectDate: function (t) {
            var e = this, i = e.opts, s = e.parsedDate, n = e.selectedDates, o = n.length, a = "";
            if (t instanceof Date) {
                if ("days" == e.view && t.getMonth() != s.month && i.moveToOtherMonthsOnSelect && (a = new Date(t.getFullYear(), t.getMonth(), 1)), "years" == e.view && t.getFullYear() != s.year && i.moveToOtherYearsOnSelect && (a = new Date(t.getFullYear(), 0, 1)), a && (e.silent = !0, e.date = a, e.silent = !1, e.nav._render()), i.multipleDates && !i.range) {
                    if (o === i.multipleDates)return;
                    e._isSelected(t) || e.selectedDates.push(t)
                } else i.range ? 2 == o ? (e.selectedDates = [t], e.minRange = t, e.maxRange = "") : 1 == o ? (e.selectedDates.push(t), e.maxRange ? e.minRange = t : e.maxRange = t, e.selectedDates = [e.minRange, e.maxRange]) : (e.selectedDates = [t], e.minRange = t) : e.selectedDates = [t];
                e._setInputValue(), i.onSelect && e._triggerOnChange(), i.autoClose && (i.multipleDates || i.range ? i.range && 2 == e.selectedDates.length && e.hide() : e.hide()), e.views[this.currentView]._render()
            }
        }, removeDate: function (t) {
            var e = this.selectedDates, i = this;
            return t instanceof Date ? e.some(function (s, n) {
                return o.isSame(s, t) ? (e.splice(n, 1), i.selectedDates.length || (i.minRange = "", i.maxRange = ""), i.views[i.currentView]._render(), i._setInputValue(), i.opts.onSelect && i._triggerOnChange(), !0) : void 0
            }) : void 0
        }, today: function () {
            this.silent = !0, this.view = this.opts.minView, this.silent = !1, this.date = new Date
        }, clear: function () {
            this.selectedDates = [], this.minRange = "", this.maxRange = "", this.views[this.currentView]._render(), this._setInputValue(), this.opts.onSelect && this._triggerOnChange()
        }, update: function (t, i) {
            var s = arguments.length;
            return 2 == s ? this.opts[t] = i : 1 == s && "object" == typeof t && (this.opts = e.extend(!0, this.opts, t)), this._createShortCuts(), this._syncWithMinMaxDates(), this._defineLocale(this.opts.language), this.nav._addButtonsIfNeed(), this.nav._render(), this.views[this.currentView]._render(), this.elIsInput && !this.opts.inline && (this._setPositionClasses(this.opts.position), this.visible && this.setPosition(this.opts.position)), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this
        }, _syncWithMinMaxDates: function () {
            var t = this.date.getTime();
            this.silent = !0, this.minTime > t && (this.date = this.minDate), this.maxTime < t && (this.date = this.maxDate), this.silent = !1
        }, _isSelected: function (t, e) {
            return this.selectedDates.some(function (i) {
                return o.isSame(i, t, e)
            })
        }, _setInputValue: function () {
            var t, e = this, i = e.opts, s = e.loc.dateFormat, n = i.altFieldDateFormat, o = e.selectedDates.map(function (t) {
                return e.formatDate(s, t)
            });
            i.altField && e.$altField.length && (t = this.selectedDates.map(function (t) {
                return e.formatDate(n, t)
            }), t = t.join(this.opts.multipleDatesSeparator), this.$altField.val(t)), o = o.join(this.opts.multipleDatesSeparator), this.$el.val(o)
        }, _isInRange: function (t, e) {
            var i = t.getTime(), s = o.getParsedDate(t), n = o.getParsedDate(this.minDate), a = o.getParsedDate(this.maxDate), r = new Date(s.year, s.month, n.date).getTime(), h = new Date(s.year, s.month, a.date).getTime(), l = {
                day: i >= this.minTime && i <= this.maxTime,
                month: r >= this.minTime && h <= this.maxTime,
                year: s.year >= n.year && s.year <= a.year
            };
            return e ? l[e] : l.day
        }, _getDimensions: function (t) {
            var e = t.offset();
            return {width: t.outerWidth(), height: t.outerHeight(), left: e.left, top: e.top}
        }, _getDateFromCell: function (t) {
            var e = this.parsedDate, s = t.data("year") || e.year, n = t.data("month") == i ? e.month : t.data("month"), o = t.data("date") || 1;
            return new Date(s, n, o)
        }, _setPositionClasses: function (t) {
            t = t.split(" ");
            var e = t[0], i = t[1], s = "datepicker -" + e + "-" + i + "- -from-" + e + "-";
            this.visible && (s += " active"), this.$datepicker.removeAttr("class").addClass(s)
        }, setPosition: function (t) {
            t = t || this.opts.position;
            var e, i, s = this._getDimensions(this.$el), n = this._getDimensions(this.$datepicker), o = t.split(" "), a = this.opts.offset, r = o[0], h = o[1];
            switch (r) {
                case"top":
                    e = s.top - n.height - a;
                    break;
                case"right":
                    i = s.left + s.width + a;
                    break;
                case"bottom":
                    e = s.top + s.height + a;
                    break;
                case"left":
                    i = s.left - n.width - a
            }
            switch (h) {
                case"top":
                    e = s.top;
                    break;
                case"right":
                    i = s.left + s.width - n.width;
                    break;
                case"bottom":
                    e = s.top + s.height - n.height;
                    break;
                case"left":
                    i = s.left;
                    break;
                case"center":
                    /left|right/.test(r) ? e = s.top + s.height / 2 - n.height / 2 : i = s.left + s.width / 2 - n.width / 2
            }
            this.$datepicker.css({left: i, top: e})
        }, show: function () {
            this.setPosition(this.opts.position), this.$datepicker.addClass("active"), this.visible = !0
        }, hide: function () {
            this.$datepicker.removeClass("active").css({left: "-100000px"}), this.focused = "", this.keys = [], this.inFocus = !1, this.visible = !1, this.$el.blur()
        }, down: function (t) {
            this._changeView(t, "down")
        }, up: function (t) {
            this._changeView(t, "up")
        }, _changeView: function (t, e) {
            t = t || this.focused || this.date;
            var i = "up" == e ? this.viewIndex + 1 : this.viewIndex - 1;
            i > 2 && (i = 2), 0 > i && (i = 0), this.silent = !0, this.date = new Date(t.getFullYear(), t.getMonth(), 1), this.silent = !1, this.view = this.viewIndexes[i]
        }, _handleHotKey: function (t) {
            var e, i, s, n = o.getParsedDate(this._getFocusedDate()), a = this.opts, r = !1, h = !1, l = !1, c = n.year, d = n.month, p = n.date;
            switch (t) {
                case"ctrlRight":
                case"ctrlUp":
                    d += 1, r = !0;
                    break;
                case"ctrlLeft":
                case"ctrlDown":
                    d -= 1, r = !0;
                    break;
                case"shiftRight":
                case"shiftUp":
                    h = !0, c += 1;
                    break;
                case"shiftLeft":
                case"shiftDown":
                    h = !0, c -= 1;
                    break;
                case"altRight":
                case"altUp":
                    l = !0, c += 10;
                    break;
                case"altLeft":
                case"altDown":
                    l = !0, c -= 10;
                    break;
                case"ctrlShiftUp":
                    this.up()
            }
            s = o.getDaysCount(new Date(c, d)), i = new Date(c, d, p), p > s && (p = s), i.getTime() < this.minTime ? i = this.minDate : i.getTime() > this.maxTime && (i = this.maxDate), this.focused = i, e = o.getParsedDate(i), r && a.onChangeMonth && a.onChangeMonth(e.month, e.year), h && a.onChangeYear && a.onChangeYear(e.year), l && a.onChangeDecade && a.onChangeDecade(this.curDecade)
        }, _registerKey: function (t) {
            var e = this.keys.some(function (e) {
                return e == t
            });
            e || this.keys.push(t)
        }, _unRegisterKey: function (t) {
            var e = this.keys.indexOf(t);
            this.keys.splice(e, 1)
        }, _isHotKeyPressed: function () {
            var t, e = !1, i = this, s = this.keys.sort();
            for (var n in d)t = d[n], s.length == t.length && t.every(function (t, e) {
                return t == s[e]
            }) && (i._trigger("hotKey", n), e = !0);
            return e
        }, _trigger: function (t, e) {
            this.$el.trigger(t, e)
        }, _focusNextCell: function (t, e) {
            e = e || this.cellType;
            var i = o.getParsedDate(this._getFocusedDate()), s = i.year, n = i.month, a = i.date;
            if (!this._isHotKeyPressed()) {
                switch (t) {
                    case 37:
                        "day" == e ? a -= 1 : "", "month" == e ? n -= 1 : "", "year" == e ? s -= 1 : "";
                        break;
                    case 38:
                        "day" == e ? a -= 7 : "", "month" == e ? n -= 3 : "", "year" == e ? s -= 4 : "";
                        break;
                    case 39:
                        "day" == e ? a += 1 : "", "month" == e ? n += 1 : "", "year" == e ? s += 1 : "";
                        break;
                    case 40:
                        "day" == e ? a += 7 : "", "month" == e ? n += 3 : "", "year" == e ? s += 4 : ""
                }
                var r = new Date(s, n, a);
                r.getTime() < this.minTime ? r = this.minDate : r.getTime() > this.maxTime && (r = this.maxDate), this.focused = r
            }
        }, _getFocusedDate: function () {
            var t = this.focused || this.selectedDates[this.selectedDates.length - 1], e = this.parsedDate;
            if (!t)switch (this.view) {
                case"days":
                    t = new Date(e.year, e.month, (new Date).getDate());
                    break;
                case"months":
                    t = new Date(e.year, e.month, 1);
                    break;
                case"years":
                    t = new Date(e.year, 0, 1)
            }
            return t
        }, _getCell: function (t, e) {
            e = e || this.cellType;
            var i, s = o.getParsedDate(t), n = '.datepicker--cell[data-year="' + s.year + '"]';
            switch (e) {
                case"month":
                    n = '[data-month="' + s.month + '"]';
                    break;
                case"day":
                    n += '[data-month="' + s.month + '"][data-date="' + s.date + '"]'
            }
            return i = this.views[this.currentView].$el.find(n), i.length ? i : ""
        }, destroy: function () {
            var t = this;
            t.$el.off(".adp").data("datepicker", ""), t.selectedDates = [], t.focused = "", t.views = {}, t.keys = [], t.minRange = "", t.maxRange = "", t.opts.inline || !t.elIsInput ? t.$datepicker.closest(".datepicker-inline").remove() : t.$datepicker.remove()
        }, _onShowEvent: function () {
            this.visible || this.show()
        }, _onBlur: function () {
            !this.inFocus && this.visible && this.hide()
        }, _onMouseDownDatepicker: function (t) {
            this.inFocus = !0
        }, _onMouseUpDatepicker: function (t) {
            this.inFocus = !1, this.$el.focus()
        }, _onInput: function () {
            var t = this.$el.val();
            t || this.clear()
        }, _onResize: function () {
            this.visible && this.setPosition()
        }, _onKeyDown: function (t) {
            var e = t.which;
            if (this._registerKey(e), e >= 37 && 40 >= e && (t.preventDefault(), this._focusNextCell(e)), 13 == e && this.focused) {
                if (this._getCell(this.focused).hasClass("-disabled-"))return;
                if (this.view != this.opts.minView)this.down(); else {
                    var i = this._isSelected(this.focused, this.cellType);
                    i ? i && this.opts.toggleSelected && this.removeDate(this.focused) : this.selectDate(this.focused)
                }
            }
            27 == e && this.hide()
        }, _onKeyUp: function (t) {
            var e = t.which;
            this._unRegisterKey(e)
        }, _onHotKey: function (t, e) {
            this._handleHotKey(e)
        }, _onMouseEnterCell: function (t) {
            var i = e(t.target).closest(".datepicker--cell"), s = this._getDateFromCell(i);
            this.silent = !0, this.focused && (this.focused = ""), i.addClass("-focus-"), this.focused = s, this.silent = !1, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", o.less(this.minRange, this.focused) && (this.maxRange = this.minRange, this.minRange = ""), this.views[this.currentView]._update())
        }, _onMouseLeaveCell: function (t) {
            var i = e(t.target).closest(".datepicker--cell");
            i.removeClass("-focus-"), this.silent = !0, this.focused = "", this.silent = !1
        }, set focused(t) {
            if (!t && this.focused) {
                var e = this._getCell(this.focused);
                e.length && e.removeClass("-focus-")
            }
            this._focused = t, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", o.less(this.minRange, this._focused) && (this.maxRange = this.minRange, this.minRange = "")), this.silent || (this.date = t)
        }, get focused() {
            return this._focused
        }, get parsedDate() {
            return o.getParsedDate(this.date)
        }, set date(t) {
            return t instanceof Date ? (this.currentDate = t, this.inited && !this.silent && (this.views[this.view]._render(), this.nav._render(), this.visible && this.elIsInput && this.setPosition()), t) : void 0
        }, get date() {
            return this.currentDate
        }, set view(t) {
            return this.viewIndex = this.viewIndexes.indexOf(t), this.viewIndex < 0 ? void 0 : (this.prevView = this.currentView, this.currentView = t, this.inited && (this.views[t] ? this.views[t]._render() : this.views[t] = new Datepicker.Body(this, t, this.opts), this.views[this.prevView].hide(), this.views[t].show(), this.nav._render(), this.opts.onChangeView && this.opts.onChangeView(t), this.elIsInput && this.visible && this.setPosition()), t)
        }, get view() {
            return this.currentView
        }, get cellType() {
            return this.view.substring(0, this.view.length - 1)
        }, get minTime() {
            var t = o.getParsedDate(this.minDate);
            return new Date(t.year, t.month, t.date).getTime()
        }, get maxTime() {
            var t = o.getParsedDate(this.maxDate);
            return new Date(t.year, t.month, t.date).getTime()
        }, get curDecade() {
            return o.getDecade(this.date)
        }
    }, o.getDaysCount = function (t) {
        return new Date(t.getFullYear(), t.getMonth() + 1, 0).getDate()
    }, o.getParsedDate = function (t) {
        return {
            year: t.getFullYear(),
            month: t.getMonth(),
            fullMonth: t.getMonth() + 1 < 10 ? "0" + (t.getMonth() + 1) : t.getMonth() + 1,
            date: t.getDate(),
            fullDate: t.getDate() < 10 ? "0" + t.getDate() : t.getDate(),
            day: t.getDay()
        }
    }, o.getDecade = function (t) {
        var e = 10 * Math.floor(t.getFullYear() / 10);
        return [e, e + 9]
    }, o.template = function (t, e) {
        return t.replace(/#\{([\w]+)\}/g, function (t, i) {
            return e[i] || 0 === e[i] ? e[i] : void 0
        })
    }, o.isSame = function (t, e, i) {
        if (!t || !e)return !1;
        var s = o.getParsedDate(t), n = o.getParsedDate(e), a = i ? i : "day", r = {
            day: s.date == n.date && s.month == n.month && s.year == n.year,
            month: s.month == n.month && s.year == n.year,
            year: s.year == n.year
        };
        return r[a]
    }, o.less = function (t, e, i) {
        return t && e ? e.getTime() < t.getTime() : !1
    }, o.bigger = function (t, e, i) {
        return t && e ? e.getTime() > t.getTime() : !1
    }, Datepicker.language = {
        ru: {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            daysShort: ["Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            clear: "Очистить",
            dateFormat: "dd.mm.yyyy",
            firstDay: 1
        }
    }, e.fn[a] = function (t) {
        return this.each(function () {
            if (e.data(this, a)) {
                var i = e.data(this, a);
                i.opts = e.extend(!0, i.opts, t), i.update()
            } else e.data(this, a, new Datepicker(this, t))
        })
    }, e(function () {
        e(r).datepicker()
    })
}(window, jQuery), function () {
    var t = {
        days: '<div class="datepicker--days datepicker--body"><div class="datepicker--days-names"></div><div class="datepicker--cells datepicker--cells-days"></div></div>',
        months: '<div class="datepicker--months datepicker--body"><div class="datepicker--cells datepicker--cells-months"></div></div>',
        years: '<div class="datepicker--years datepicker--body"><div class="datepicker--cells datepicker--cells-years"></div></div>'
    }, e = Datepicker;
    e.Body = function (t, e, i) {
        this.d = t, this.type = e, this.opts = i, this.init()
    }, e.Body.prototype = {
        init: function () {
            this._buildBaseHtml(), this._render(), this._bindEvents()
        }, _bindEvents: function () {
            this.$el.on("click", ".datepicker--cell", $.proxy(this._onClickCell, this))
        }, _buildBaseHtml: function () {
            this.$el = $(t[this.type]).appendTo(this.d.$content), this.$names = $(".datepicker--days-names", this.$el), this.$cells = $(".datepicker--cells", this.$el)
        }, _getDayNamesHtml: function (t, e, i, s) {
            return e = void 0 != e ? e : t, i = i ? i : "", s = void 0 != s ? s : 0, s > 7 ? i : 7 == e ? this._getDayNamesHtml(t, 0, i, ++s) : (i += '<div class="datepicker--day-name' + (this.d.isWeekend(e) ? " -weekend-" : "") + '">' + this.d.loc.daysMin[e] + "</div>", this._getDayNamesHtml(t, ++e, i, ++s))
        }, _getCellContents: function (t, i) {
            var s = "datepicker--cell datepicker--cell-" + i, n = new Date, o = this.d, a = o.opts, r = e.getParsedDate(t), h = {}, l = r.date;
            switch (a.onRenderCell && (h = a.onRenderCell(t, i) || {}, l = h.html ? h.html : l, s += h.classes ? " " + h.classes : ""), i) {
                case"day":
                    o.isWeekend(r.day) && (s += " -weekend-"), r.month != this.d.parsedDate.month && (s += " -other-month-", a.selectOtherMonths || (s += " -disabled-"), a.showOtherMonths || (l = ""));
                    break;
                case"month":
                    l = o.loc[o.opts.monthsField][r.month];
                    break;
                case"year":
                    var c = o.curDecade;
                    l = r.year, (r.year < c[0] || r.year > c[1]) && (s += " -other-decade-", a.selectOtherYears || (s += " -disabled-"), a.showOtherYears || (l = ""))
            }
            return a.onRenderCell && (h = a.onRenderCell(t, i) || {}, l = h.html ? h.html : l, s += h.classes ? " " + h.classes : ""), a.range && (e.isSame(o.minRange, t, i) && (s += " -range-from-"), e.isSame(o.maxRange, t, i) && (s += " -range-to-"), 1 == o.selectedDates.length && o.focused ? ((e.bigger(o.minRange, t) && e.less(o.focused, t) || e.less(o.maxRange, t) && e.bigger(o.focused, t)) && (s += " -in-range-"), e.less(o.maxRange, t) && e.isSame(o.focused, t) && (s += " -range-from-"), e.bigger(o.minRange, t) && e.isSame(o.focused, t) && (s += " -range-to-")) : 2 == o.selectedDates.length && e.bigger(o.minRange, t) && e.less(o.maxRange, t) && (s += " -in-range-")), e.isSame(n, t, i) && (s += " -current-"), o.focused && e.isSame(t, o.focused, i) && (s += " -focus-"), o._isSelected(t, i) && (s += " -selected-"), (!o._isInRange(t, i) || h.disabled) && (s += " -disabled-"), {
                html: l,
                classes: s
            }
        }, _getDaysHtml: function (t) {
            var i = e.getDaysCount(t), s = new Date(t.getFullYear(), t.getMonth(), 1).getDay(), n = new Date(t.getFullYear(), t.getMonth(), i).getDay(), o = s - this.d.loc.firstDay, a = 6 - n + this.d.loc.firstDay;
            o = 0 > o ? o + 7 : o, a = a > 6 ? a - 7 : a;
            for (var r, h, l = -o + 1, c = "", d = l, p = i + a; p >= d; d++)h = t.getFullYear(), r = t.getMonth(), c += this._getDayHtml(new Date(h, r, d));
            return c
        }, _getDayHtml: function (t) {
            var e = this._getCellContents(t, "day");
            return '<div class="' + e.classes + '" data-date="' + t.getDate() + '" data-month="' + t.getMonth() + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>"
        }, _getMonthsHtml: function (t) {
            for (var i = "", s = e.getParsedDate(t), n = 0; 12 > n;)i += this._getMonthHtml(new Date(s.year, n)), n++;
            return i
        }, _getMonthHtml: function (t) {
            var e = this._getCellContents(t, "month");
            return '<div class="' + e.classes + '" data-month="' + t.getMonth() + '">' + e.html + "</div>"
        }, _getYearsHtml: function (t) {
            var i = (e.getParsedDate(t), e.getDecade(t)), s = i[0] - 1, n = "", o = s;
            for (o; o <= i[1] + 1; o++)n += this._getYearHtml(new Date(o, 0));
            return n
        }, _getYearHtml: function (t) {
            var e = this._getCellContents(t, "year");
            return '<div class="' + e.classes + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>"
        }, _renderTypes: {
            days: function () {
                var t = this._getDayNamesHtml(this.d.loc.firstDay), e = this._getDaysHtml(this.d.currentDate);
                this.$cells.html(e), this.$names.html(t)
            }, months: function () {
                var t = this._getMonthsHtml(this.d.currentDate);
                this.$cells.html(t)
            }, years: function () {
                var t = this._getYearsHtml(this.d.currentDate);
                this.$cells.html(t)
            }
        }, _render: function () {
            this._renderTypes[this.type].bind(this)()
        }, _update: function () {
            var t, e, i, s = $(".datepicker--cell", this.$cells), n = this;
            s.each(function (s, o) {
                e = $(this), i = n.d._getDateFromCell($(this)), t = n._getCellContents(i, n.d.cellType), e.attr("class", t.classes)
            })
        }, show: function () {
            this.$el.addClass("active"), this.acitve = !0
        }, hide: function () {
            this.$el.removeClass("active"), this.active = !1
        }, _handleClick: function (t) {
            var e = t.data("date") || 1, i = t.data("month") || 0, s = t.data("year") || this.d.parsedDate.year;
            if (this.d.view != this.opts.minView)return void this.d.down(new Date(s, i, e));
            var n = new Date(s, i, e), o = this.d._isSelected(n, this.d.cellType);
            o ? o && this.opts.toggleSelected && this.d.removeDate(n) : this.d.selectDate(n)
        }, _onClickCell: function (t) {
            var e = $(t.target).closest(".datepicker--cell");
            e.hasClass("-disabled-") || this._handleClick.bind(this)(e)
        }
    }
}(), function () {
    var t = '<div class="datepicker--nav-action" data-action="prev">#{prevHtml}</div><div class="datepicker--nav-title">#{title}</div><div class="datepicker--nav-action" data-action="next">#{nextHtml}</div>', e = '<div class="datepicker--buttons"></div>', i = '<span class="datepicker--button" data-action="#{action}">#{label}</span>';
    Datepicker.Navigation = function (t, e) {
        this.d = t, this.opts = e, this.$buttonsContainer = "", this.init()
    }, Datepicker.Navigation.prototype = {
        init: function () {
            this._buildBaseHtml(), this._bindEvents()
        }, _bindEvents: function () {
            this.d.$nav.on("click", ".datepicker--nav-action", $.proxy(this._onClickNavButton, this)), this.d.$nav.on("click", ".datepicker--nav-title", $.proxy(this._onClickNavTitle, this)), this.d.$datepicker.on("click", ".datepicker--button", $.proxy(this._onClickNavButton, this))
        }, _buildBaseHtml: function () {
            this._render(), this._addButtonsIfNeed()
        }, _addButtonsIfNeed: function () {
            this.opts.todayButton && this._addButton("today"), this.opts.clearButton && this._addButton("clear")
        }, _render: function () {
            var e = this._getTitle(this.d.currentDate), i = Datepicker.template(t, $.extend({title: e}, this.opts));
            this.d.$nav.html(i), "years" == this.d.view && $(".datepicker--nav-title", this.d.$nav).addClass("-disabled-"), this.setNavStatus()
        }, _getTitle: function (t) {
            return this.d.formatDate(this.opts.navTitles[this.d.view], t)
        }, _addButton: function (t) {
            this.$buttonsContainer.length || this._addButtonsContainer();
            var e = {action: t, label: this.d.loc[t]}, s = Datepicker.template(i, e);
            $("[data-action=" + t + "]", this.$buttonsContainer).length || this.$buttonsContainer.append(s)
        }, _addButtonsContainer: function () {
            this.d.$datepicker.append(e), this.$buttonsContainer = $(".datepicker--buttons", this.d.$datepicker)
        }, setNavStatus: function () {
            if ((this.opts.minDate || this.opts.maxDate) && this.opts.disableNavWhenOutOfRange) {
                var t = this.d.parsedDate, e = t.month, i = t.year, s = t.date;
                switch (this.d.view) {
                    case"days":
                        this.d._isInRange(new Date(i, e - 1, s), "month") || this._disableNav("prev"), this.d._isInRange(new Date(i, e + 1, s), "month") || this._disableNav("next");
                        break;
                    case"months":
                        this.d._isInRange(new Date(i - 1, e, s), "year") || this._disableNav("prev"), this.d._isInRange(new Date(i + 1, e, s), "year") || this._disableNav("next");
                        break;
                    case"years":
                        this.d._isInRange(new Date(i - 10, e, s), "year") || this._disableNav("prev"), this.d._isInRange(new Date(i + 10, e, s), "year") || this._disableNav("next")
                }
            }
        }, _disableNav: function (t) {
            $('[data-action="' + t + '"]', this.d.$nav).addClass("-disabled-")
        }, _activateNav: function (t) {
            $('[data-action="' + t + '"]', this.d.$nav).removeClass("-disabled-")
        }, _onClickNavButton: function (t) {
            var e = $(t.target).closest("[data-action]"), i = e.data("action");
            this.d[i]()
        }, _onClickNavTitle: function (t) {
            return $(t.target).hasClass("-disabled-") ? void 0 : "days" == this.d.view ? this.d.view = "months" : void(this.d.view = "years")
        }
    }
}(), !function (t) {
    "object" == typeof exports && exports && "object" == typeof module && module && module.exports === exports ? t(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function (t) {
    function e(t) {
        var e = t[0];
        return e.offsetWidth > 0 && e.offsetHeight > 0
    }

    function i(e) {
        if (e.minTime && (e.minTime = y(e.minTime)), e.maxTime && (e.maxTime = y(e.maxTime)), e.durationTime && "function" != typeof e.durationTime && (e.durationTime = y(e.durationTime)), "now" == e.scrollDefault)e.scrollDefault = function () {
            return e.roundingFunction(y(new Date), e)
        }; else if (e.scrollDefault && "function" != typeof e.scrollDefault) {
            var i = e.scrollDefault;
            e.scrollDefault = function () {
                return e.roundingFunction(y(i), e)
            }
        } else e.minTime && (e.scrollDefault = function () {
            return e.roundingFunction(e.minTime, e)
        });
        if ("string" === t.type(e.timeFormat) && e.timeFormat.match(/[gh]/) && (e._twelveHourTime = !0), e.showOnFocus === !1 && -1 != e.showOn.indexOf("focus") && e.showOn.splice(e.showOn.indexOf("focus"), 1), e.disableTimeRanges.length > 0) {
            for (var s in e.disableTimeRanges)e.disableTimeRanges[s] = [y(e.disableTimeRanges[s][0]), y(e.disableTimeRanges[s][1])];
            e.disableTimeRanges = e.disableTimeRanges.sort(function (t, e) {
                return t[0] - e[0]
            });
            for (var s = e.disableTimeRanges.length - 1; s > 0; s--)e.disableTimeRanges[s][0] <= e.disableTimeRanges[s - 1][1] && (e.disableTimeRanges[s - 1] = [Math.min(e.disableTimeRanges[s][0], e.disableTimeRanges[s - 1][0]), Math.max(e.disableTimeRanges[s][1], e.disableTimeRanges[s - 1][1])], e.disableTimeRanges.splice(s, 1))
        }
        return e
    }

    function s(e) {
        var i = e.data("timepicker-settings"), s = e.data("timepicker-list");
        if (s && s.length && (s.remove(), e.data("timepicker-list", !1)), i.useSelect) {
            s = t("<select />", {"class": "ui-timepicker-select"});
            var a = s
        } else {
            s = t("<ul />", {"class": "ui-timepicker-list"});
            var a = t("<div />", {"class": "ui-timepicker-wrapper", tabindex: -1});
            a.css({display: "none", position: "absolute"}).append(s)
        }
        if (i.noneOption)if (i.noneOption === !0 && (i.noneOption = i.useSelect ? "Time..." : "None"), t.isArray(i.noneOption)) {
            for (var r in i.noneOption)if (parseInt(r, 10) == r) {
                var l = n(i.noneOption[r], i.useSelect);
                s.append(l)
            }
        } else {
            var l = n(i.noneOption, i.useSelect);
            s.append(l)
        }
        i.className && a.addClass(i.className), null === i.minTime && null === i.durationTime || !i.showDuration || ("function" == typeof i.step ? "function" : i.step, a.addClass("ui-timepicker-with-duration"), a.addClass("ui-timepicker-step-" + i.step));
        var d = i.minTime;
        "function" == typeof i.durationTime ? d = y(i.durationTime()) : null !== i.durationTime && (d = i.durationTime);
        var p = null !== i.minTime ? i.minTime : 0, f = null !== i.maxTime ? i.maxTime : p + x - 1;
        p > f && (f += x), f === x - 1 && "string" === t.type(i.timeFormat) && i.show2400 && (f = x);
        var m = i.disableTimeRanges, g = 0, w = m.length, C = i.step;
        "function" != typeof C && (C = function () {
            return i.step
        });
        for (var r = p, $ = 0; f >= r; $++, r += 60 * C($)) {
            var S = r, T = _(S, i);
            if (i.useSelect) {
                var D = t("<option />", {value: T});
                D.text(T)
            } else {
                var D = t("<li />");
                D.addClass(43200 > S % 86400 ? "ui-timepicker-am" : "ui-timepicker-pm"), D.data("time", 86400 >= S ? S : S % 86400), D.text(T)
            }
            if ((null !== i.minTime || null !== i.durationTime) && i.showDuration) {
                var P = b(r - d, i.step);
                if (i.useSelect)D.text(D.text() + " (" + P + ")"); else {
                    var I = t("<span />", {"class": "ui-timepicker-duration"});
                    I.text(" (" + P + ")"), D.append(I)
                }
            }
            w > g && (S >= m[g][1] && (g += 1), m[g] && S >= m[g][0] && S < m[g][1] && (i.useSelect ? D.prop("disabled", !0) : D.addClass("ui-timepicker-disabled"))), s.append(D)
        }
        if (a.data("timepicker-input", e), e.data("timepicker-list", a), i.useSelect)e.val() && s.val(o(y(e.val()), i)), s.on("focus", function () {
            t(this).data("timepicker-input").trigger("showTimepicker")
        }), s.on("blur", function () {
            t(this).data("timepicker-input").trigger("hideTimepicker")
        }), s.on("change", function () {
            u(e, t(this).val(), "select")
        }), u(e, s.val(), "initial"), e.hide().after(s); else {
            var M = i.appendTo;
            "string" == typeof M ? M = t(M) : "function" == typeof M && (M = M(e)), M.append(a), c(e, s), s.on("mousedown touchstart", "li", function (i) {
                e.off("focus.timepicker"), e.on("focus.timepicker-ie-hack", function () {
                    e.off("focus.timepicker-ie-hack"), e.on("focus.timepicker", k.show)
                }), h(e) || e[0].focus(), s.find("li").removeClass("ui-timepicker-selected"), t(this).addClass("ui-timepicker-selected"), v(e) && (e.trigger("hideTimepicker"), s.on("mouseup.timepicker touchend.timepicker", "li", function (t) {
                    s.off("mouseup.timepicker touchend.timepicker"), a.hide()
                }))
            })
        }
    }

    function n(e, i) {
        var s, n, o;
        return "object" == typeof e ? (s = e.label, n = e.className, o = e.value) : "string" == typeof e ? s = e : t.error("Invalid noneOption value"), i ? t("<option />", {
            value: o,
            "class": n,
            text: s
        }) : t("<li />", {"class": n, text: s}).data("time", String(o))
    }

    function o(t, e) {
        return t = e.roundingFunction(t, e), null !== t ? _(t, e) : void 0
    }

    function a() {
        return new Date(1970, 0, 1, 0, 0, 0)
    }

    function r(e) {
        var i = t(e.target), s = i.closest(".ui-timepicker-input");
        0 === s.length && 0 === i.closest(".ui-timepicker-wrapper").length && (k.hide(), t(document).unbind(".ui-timepicker"), t(window).unbind(".ui-timepicker"))
    }

    function h(t) {
        var e = t.data("timepicker-settings");
        return (window.navigator.msMaxTouchPoints || "ontouchstart"in document) && e.disableTouchKeyboard
    }

    function l(e, i, s) {
        if (!s && 0 !== s)return !1;
        var n = e.data("timepicker-settings"), o = !1, s = n.roundingFunction(s, n);
        return i.find("li").each(function (e, i) {
            var n = t(i);
            return "number" == typeof n.data("time") && n.data("time") == s ? (o = n, !1) : void 0
        }), o
    }

    function c(t, e) {
        e.find("li").removeClass("ui-timepicker-selected");
        var i = y(p(t), t.data("timepicker-settings"));
        if (null !== i) {
            var s = l(t, e, i);
            if (s) {
                var n = s.offset().top - e.offset().top;
                (n + s.outerHeight() > e.outerHeight() || 0 > n) && e.scrollTop(e.scrollTop() + s.position().top - s.outerHeight()), s.addClass("ui-timepicker-selected")
            }
        }
    }

    function d(e, i) {
        if ("" !== this.value && "timepicker" != i) {
            var s = t(this);
            if (!s.is(":focus") || e && "change" == e.type) {
                var n = s.data("timepicker-settings"), o = y(this.value, n);
                if (null === o)return void s.trigger("timeFormatError");
                var a = !1;
                null !== n.minTime && o < n.minTime ? a = !0 : null !== n.maxTime && o > n.maxTime && (a = !0), t.each(n.disableTimeRanges, function () {
                    return o >= this[0] && o < this[1] ? (a = !0, !1) : void 0
                }), n.forceRoundTime && (o = n.roundingFunction(o, n));
                var r = _(o, n);
                a ? u(s, r, "error") && s.trigger("timeRangeError") : u(s, r)
            }
        }
    }

    function p(t) {
        return t.is("input") ? t.val() : t.data("ui-timepicker-value")
    }

    function u(t, e, i) {
        if (t.is("input")) {
            t.val(e);
            var s = t.data("timepicker-settings");
            s.useSelect && "select" != i && "initial" != i && t.data("timepicker-list").val(o(y(e), s))
        }
        return t.data("ui-timepicker-value") != e ? (t.data("ui-timepicker-value", e), "select" == i ? t.trigger("selectTime").trigger("changeTime").trigger("change", "timepicker") : "error" != i && t.trigger("changeTime"), !0) : (t.trigger("selectTime"), !1)
    }

    function f(t) {
        switch (t.keyCode) {
            case 13:
            case 9:
                return;
            default:
                t.preventDefault()
        }
    }

    function m(i) {
        var s = t(this), n = s.data("timepicker-list");
        if (!n || !e(n)) {
            if (40 != i.keyCode)return !0;
            k.show.call(s.get(0)), n = s.data("timepicker-list"), h(s) || s.focus()
        }
        switch (i.keyCode) {
            case 13:
                return v(s) && k.hide.apply(this), i.preventDefault(), !1;
            case 38:
                var o = n.find(".ui-timepicker-selected");
                return o.length ? o.is(":first-child") || (o.removeClass("ui-timepicker-selected"), o.prev().addClass("ui-timepicker-selected"), o.prev().position().top < o.outerHeight() && n.scrollTop(n.scrollTop() - o.outerHeight())) : (n.find("li").each(function (e, i) {
                    return t(i).position().top > 0 ? (o = t(i), !1) : void 0
                }), o.addClass("ui-timepicker-selected")), !1;
            case 40:
                return o = n.find(".ui-timepicker-selected"), 0 === o.length ? (n.find("li").each(function (e, i) {
                    return t(i).position().top > 0 ? (o = t(i), !1) : void 0
                }), o.addClass("ui-timepicker-selected")) : o.is(":last-child") || (o.removeClass("ui-timepicker-selected"), o.next().addClass("ui-timepicker-selected"), o.next().position().top + 2 * o.outerHeight() > n.outerHeight() && n.scrollTop(n.scrollTop() + o.outerHeight())), !1;
            case 27:
                n.find("li").removeClass("ui-timepicker-selected"), k.hide();
                break;
            case 9:
                k.hide();
                break;
            default:
                return !0
        }
    }

    function g(i) {
        var s = t(this), n = s.data("timepicker-list"), o = s.data("timepicker-settings");
        if (!n || !e(n) || o.disableTextInput)return !0;
        switch (i.keyCode) {
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 65:
            case 77:
            case 80:
            case 186:
            case 8:
            case 46:
                o.typeaheadHighlight ? c(s, n) : n.hide()
        }
    }

    function v(t) {
        var e = t.data("timepicker-settings"), i = t.data("timepicker-list"), s = null, n = i.find(".ui-timepicker-selected");
        return n.hasClass("ui-timepicker-disabled") ? !1 : (n.length && (s = n.data("time")), null !== s && ("string" != typeof s && (s = _(s, e)), u(t, s, "select")), !0)
    }

    function b(t, e) {
        t = Math.abs(t);
        var i, s, n = Math.round(t / 60), o = [];
        return 60 > n ? o = [n, C.mins] : (i = Math.floor(n / 60), s = n % 60, 30 == e && 30 == s && (i += C.decimal + 5), o.push(i), o.push(1 == i ? C.hr : C.hrs), 30 != e && s && (o.push(s), o.push(C.mins))), o.join(" ")
    }

    function _(e, i) {
        if (null === e)return null;
        var s = new Date(w.valueOf() + 1e3 * e);
        if (isNaN(s.getTime()))return null;
        if ("function" === t.type(i.timeFormat))return i.timeFormat(s);
        for (var n, o, a = "", r = 0; r < i.timeFormat.length; r++)switch (o = i.timeFormat.charAt(r)) {
            case"a":
                a += s.getHours() > 11 ? C.pm : C.am;
                break;
            case"A":
                a += s.getHours() > 11 ? C.PM : C.AM;
                break;
            case"g":
                n = s.getHours() % 12, a += 0 === n ? "12" : n;
                break;
            case"G":
                n = s.getHours(), e === x && (n = 24), a += n;
                break;
            case"h":
                n = s.getHours() % 12, 0 !== n && 10 > n && (n = "0" + n), a += 0 === n ? "12" : n;
                break;
            case"H":
                n = s.getHours(), e === x && (n = i.show2400 ? 24 : 0), a += n > 9 ? n : "0" + n;
                break;
            case"i":
                var h = s.getMinutes();
                a += h > 9 ? h : "0" + h;
                break;
            case"s":
                e = s.getSeconds(), a += e > 9 ? e : "0" + e;
                break;
            case"\\":
                r++, a += i.timeFormat.charAt(r);
                break;
            default:
                a += o
        }
        return a
    }

    function y(t, e) {
        if ("" === t)return null;
        if (!t || t + 0 == t)return t;
        if ("object" == typeof t)return 3600 * t.getHours() + 60 * t.getMinutes() + t.getSeconds();
        t = t.toLowerCase().replace(/[\s\.]/g, ""), ("a" == t.slice(-1) || "p" == t.slice(-1)) && (t += "m");
        var i = "(" + C.am.replace(".", "") + "|" + C.pm.replace(".", "") + "|" + C.AM.replace(".", "") + "|" + C.PM.replace(".", "") + ")?", s = new RegExp("^" + i + "([0-9]?[0-9])\\W?([0-5][0-9])?\\W?([0-5][0-9])?" + i + "$"), n = t.match(s);
        if (!n)return null;
        var o = parseInt(1 * n[2], 10), a = o > 24 ? o % 24 : o, r = n[1] || n[5], h = a;
        if (12 >= a && r) {
            var l = r == C.pm || r == C.PM;
            h = 12 == a ? l ? 12 : 0 : a + (l ? 12 : 0)
        }
        var c = 1 * n[3] || 0, d = 1 * n[4] || 0, p = 3600 * h + 60 * c + d;
        if (12 > a && !r && e && e._twelveHourTime && e.scrollDefault) {
            var u = p - e.scrollDefault();
            0 > u && u >= x / -2 && (p = (p + x / 2) % x)
        }
        return p
    }

    var w = a(), x = 86400, C = {
        am: "am",
        pm: "pm",
        AM: "AM",
        PM: "PM",
        decimal: ".",
        mins: "mins",
        hr: "hr",
        hrs: "hrs"
    }, k = {
        init: function (e) {
            return this.each(function () {
                var n = t(this), o = [];
                for (var a in t.fn.timepicker.defaults)n.data(a) && (o[a] = n.data(a));
                var r = t.extend({}, t.fn.timepicker.defaults, o, e);
                if (r.lang && (C = t.extend(C, r.lang)), r = i(r), n.data("timepicker-settings", r), n.addClass("ui-timepicker-input"), r.useSelect)s(n); else {
                    if (n.prop("autocomplete", "off"), r.showOn)for (var h in r.showOn)n.on(r.showOn[h] + ".timepicker", k.show);
                    n.on("change.timepicker", d), n.on("keydown.timepicker", m), n.on("keyup.timepicker", g), r.disableTextInput && n.on("keydown.timepicker", f), d.call(n.get(0))
                }
            })
        }, show: function (i) {
            var n = t(this), o = n.data("timepicker-settings");
            if (i && i.preventDefault(), o.useSelect)return void n.data("timepicker-list").focus();
            h(n) && n.blur();
            var a = n.data("timepicker-list");
            if (!n.prop("readonly") && (a && 0 !== a.length && "function" != typeof o.durationTime || (s(n),
                    a = n.data("timepicker-list")), !e(a))) {
                n.data("ui-timepicker-value", n.val()), c(n, a), k.hide(), a.show();
                var d = {};
                o.orientation.match(/r/) ? d.left = n.offset().left + n.outerWidth() - a.outerWidth() + parseInt(a.css("marginLeft").replace("px", ""), 10) : d.left = n.offset().left + parseInt(a.css("marginLeft").replace("px", ""), 10);
                var u;
                u = o.orientation.match(/t/) ? "t" : o.orientation.match(/b/) ? "b" : n.offset().top + n.outerHeight(!0) + a.outerHeight() > t(window).height() + t(window).scrollTop() ? "t" : "b", "t" == u ? (a.addClass("ui-timepicker-positioned-top"), d.top = n.offset().top - a.outerHeight() + parseInt(a.css("marginTop").replace("px", ""), 10)) : (a.removeClass("ui-timepicker-positioned-top"), d.top = n.offset().top + n.outerHeight() + parseInt(a.css("marginTop").replace("px", ""), 10)), a.offset(d);
                var f = a.find(".ui-timepicker-selected");
                if (!f.length) {
                    var m = y(p(n));
                    null !== m ? f = l(n, a, m) : o.scrollDefault && (f = l(n, a, o.scrollDefault()))
                }
                if (f && f.length) {
                    var g = a.scrollTop() + f.position().top - f.outerHeight();
                    a.scrollTop(g)
                } else a.scrollTop(0);
                return o.stopScrollPropagation && t(document).on("wheel.ui-timepicker", ".ui-timepicker-wrapper", function (e) {
                    e.preventDefault();
                    var i = t(this).scrollTop();
                    t(this).scrollTop(i + e.originalEvent.deltaY)
                }), t(document).on("touchstart.ui-timepicker mousedown.ui-timepicker", r), t(window).on("resize.ui-timepicker", r), o.closeOnWindowScroll && t(document).on("scroll.ui-timepicker", r), n.trigger("showTimepicker"), this
            }
        }, hide: function (i) {
            var s = t(this), n = s.data("timepicker-settings");
            return n && n.useSelect && s.blur(), t(".ui-timepicker-wrapper").each(function () {
                var i = t(this);
                if (e(i)) {
                    var s = i.data("timepicker-input"), n = s.data("timepicker-settings");
                    n && n.selectOnBlur && v(s), i.hide(), s.trigger("hideTimepicker")
                }
            }), this
        }, option: function (e, n) {
            return "string" == typeof e && "undefined" == typeof n ? t(this).data("timepicker-settings")[e] : this.each(function () {
                var o = t(this), a = o.data("timepicker-settings"), r = o.data("timepicker-list");
                "object" == typeof e ? a = t.extend(a, e) : "string" == typeof e && (a[e] = n), a = i(a), o.data("timepicker-settings", a), r && (r.remove(), o.data("timepicker-list", !1)), a.useSelect && s(o)
            })
        }, getSecondsFromMidnight: function () {
            return y(p(this))
        }, getTime: function (t) {
            var e = this, i = p(e);
            if (!i)return null;
            var s = y(i);
            if (null === s)return null;
            t || (t = w);
            var n = new Date(t);
            return n.setHours(s / 3600), n.setMinutes(s % 3600 / 60), n.setSeconds(s % 60), n.setMilliseconds(0), n
        }, setTime: function (t) {
            var e = this, i = e.data("timepicker-settings");
            if (i.forceRoundTime)var s = o(y(t), i); else var s = _(y(t), i);
            return t && null === s && i.noneOption && (s = t), u(e, s), e.data("timepicker-list") && c(e, e.data("timepicker-list")), this
        }, remove: function () {
            var t = this;
            if (t.hasClass("ui-timepicker-input")) {
                var e = t.data("timepicker-settings");
                return t.removeAttr("autocomplete", "off"), t.removeClass("ui-timepicker-input"), t.removeData("timepicker-settings"), t.off(".timepicker"), t.data("timepicker-list") && t.data("timepicker-list").remove(), e.useSelect && t.show(), t.removeData("timepicker-list"), this
            }
        }
    };
    t.fn.timepicker = function (e) {
        return this.length ? k[e] ? this.hasClass("ui-timepicker-input") ? k[e].apply(this, Array.prototype.slice.call(arguments, 1)) : this : "object" != typeof e && e ? void t.error("Method " + e + " does not exist on jQuery.timepicker") : k.init.apply(this, arguments) : this
    }, t.fn.timepicker.defaults = {
        appendTo: "body",
        className: null,
        closeOnWindowScroll: !1,
        disableTextInput: !1,
        disableTimeRanges: [],
        disableTouchKeyboard: !1,
        durationTime: null,
        forceRoundTime: !1,
        maxTime: null,
        minTime: null,
        noneOption: !1,
        orientation: "l",
        roundingFunction: function (t, e) {
            if (null === t)return null;
            if ("number" != typeof e.step)return t;
            var i = t % (60 * e.step);
            return i >= 30 * e.step ? t += 60 * e.step - i : t -= i, t
        },
        scrollDefault: null,
        selectOnBlur: !1,
        show2400: !1,
        showDuration: !1,
        showOn: ["click", "focus"],
        showOnFocus: !0,
        step: 30,
        stopScrollPropagation: !1,
        timeFormat: "g:ia",
        typeaheadHighlight: !0,
        useSelect: !1
    }
}), !function (t, e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : t.Tipped = e(jQuery)
}(this, function ($) {
    function degrees(t) {
        return 180 * t / Math.PI
    }

    function radian(t) {
        return t * Math.PI / 180
    }

    function sec(t) {
        return 1 / Math.cos(t)
    }

    function sfcc(t) {
        return String.fromCharCode.apply(String, t.replace(" ", "").split(","))
    }

    function deepExtend(t, e) {
        for (var i in e)e[i] && e[i].constructor && e[i].constructor === Object ? (t[i] = $.extend({}, t[i]) || {}, deepExtend(t[i], e[i])) : t[i] = e[i];
        return t
    }

    function Spin() {
        return this.initialize.apply(this, _slice.call(arguments))
    }

    function Visible() {
        return this.initialize.apply(this, _slice.call(arguments))
    }

    function Skin() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Stem() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Tooltip() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Collection(t) {
        this.element = t
    }

    var Tipped = {};
    $.extend(Tipped, {version: "4.4.2"}), Tipped.Skins = {
        base: {
            afterUpdate: !1,
            ajax: {},
            cache: !0,
            container: !1,
            containment: {selector: "viewport", padding: 5},
            close: !1,
            detach: !0,
            fadeIn: 200,
            fadeOut: 200,
            showDelay: 75,
            hideDelay: 25,
            hideAfter: !1,
            hideOn: {element: "mouseleave"},
            hideOthers: !1,
            position: "top",
            inline: !1,
            offset: {x: 0, y: 0},
            onHide: !1,
            onShow: !1,
            padding: !0,
            radius: !0,
            shadow: !0,
            showOn: {element: "mousemove"},
            size: "medium",
            spinner: !0,
            stem: !0,
            target: "element",
            voila: !0
        },
        reset: {
            ajax: !1,
            hideOn: {element: "mouseleave", tooltip: "mouseleave"},
            showOn: {element: "mouseenter", tooltip: "mouseenter"}
        }
    }, $.each("dark".split(" "), function (t, e) {
        Tipped.Skins[e] = {}
    });
    var Browser = function (t) {
        function e(e) {
            var i = new RegExp(e + "([\\d.]+)").exec(t);
            return i ? parseFloat(i[1]) : !0
        }

        return {
            IE: !(!window.attachEvent || -1 !== t.indexOf("Opera")) && e("MSIE "),
            Opera: t.indexOf("Opera") > -1 && (!!window.opera && opera.version && parseFloat(opera.version()) || 7.55),
            WebKit: t.indexOf("AppleWebKit/") > -1 && e("AppleWebKit/"),
            Gecko: t.indexOf("Gecko") > -1 && -1 === t.indexOf("KHTML") && e("rv:"),
            MobileSafari: !!t.match(/Apple.*Mobile.*Safari/),
            Chrome: t.indexOf("Chrome") > -1 && e("Chrome/"),
            ChromeMobile: t.indexOf("CrMo") > -1 && e("CrMo/"),
            Android: t.indexOf("Android") > -1 && e("Android "),
            IEMobile: t.indexOf("IEMobile") > -1 && e("IEMobile/")
        }
    }(navigator.userAgent), Support = function () {
        function t(t) {
            return i(t, "prefix")
        }

        function e(t, e) {
            for (var i in t)if (void 0 !== s.style[t[i]])return "prefix" == e ? t[i] : !0;
            return !1
        }

        function i(t, i) {
            var s = t.charAt(0).toUpperCase() + t.substr(1), o = (t + " " + n.join(s + " ") + s).split(" ");
            return e(o, i)
        }

        var s = document.createElement("div"), n = "Webkit Moz O ms Khtml".split(" ");
        return {
            css: {animation: i("animation"), transform: i("transform"), prefixed: t},
            shadow: i("boxShadow") && i("pointerEvents"),
            touch: function () {
                try {
                    return !!("ontouchstart"in window || window.DocumentTouch && document instanceof DocumentTouch)
                } catch (t) {
                    return !1
                }
            }()
        }
    }(), _slice = Array.prototype.slice, _ = {
        wrap: function (t, e) {
            var i = t;
            return function () {
                var t = [$.proxy(i, this)].concat(_slice.call(arguments));
                return e.apply(this, t)
            }
        }, isElement: function (t) {
            return t && 1 == t.nodeType
        }, isText: function (t) {
            return t && 3 == t.nodeType
        }, isDocumentFragment: function (t) {
            return t && 11 == t.nodeType
        }, delay: function (t, e) {
            var i = _slice.call(arguments, 2);
            return setTimeout(function () {
                return t.apply(t, i)
            }, e)
        }, defer: function (t) {
            return _.delay.apply(this, [t, 1].concat(_slice.call(arguments, 1)))
        }, pointer: function (t) {
            return {x: t.pageX, y: t.pageY}
        }, element: {
            isAttached: function () {
                function t(t) {
                    for (var e = t; e && e.parentNode;)e = e.parentNode;
                    return e
                }

                return function (e) {
                    var i = t(e);
                    return !(!i || !i.body)
                }
            }()
        }
    }, getUID = function () {
        var t = 0, e = "_tipped-uid-";
        return function (i) {
            for (i = i || e, t++; document.getElementById(i + t);)t++;
            return i + t
        }
    }(), Position = {
        positions: ["topleft", "topmiddle", "topright", "righttop", "rightmiddle", "rightbottom", "bottomright", "bottommiddle", "bottomleft", "leftbottom", "leftmiddle", "lefttop"],
        regex: {
            toOrientation: /^(top|left|bottom|right)(top|left|bottom|right|middle|center)$/,
            horizontal: /^(top|bottom)/,
            isCenter: /(middle|center)/,
            side: /^(top|bottom|left|right)/
        },
        toDimension: function () {
            var t = {top: "height", left: "width", bottom: "height", right: "width"};
            return function (e) {
                return t[e]
            }
        }(),
        isCenter: function (t) {
            return !!t.toLowerCase().match(this.regex.isCenter)
        },
        isCorner: function (t) {
            return !this.isCenter(t)
        },
        getOrientation: function (t) {
            return t.toLowerCase().match(this.regex.horizontal) ? "horizontal" : "vertical"
        },
        getSide: function (t) {
            var e = null, i = t.toLowerCase().match(this.regex.side);
            return i && i[1] && (e = i[1]), e
        },
        split: function (t) {
            return t.toLowerCase().match(this.regex.toOrientation)
        },
        _flip: {top: "bottom", bottom: "top", left: "right", right: "left"},
        flip: function (t, e) {
            var i = this.split(t);
            return e ? this.inverseCornerPlane(this.flip(this.inverseCornerPlane(t))) : this._flip[i[1]] + i[2]
        },
        inverseCornerPlane: function (t) {
            if (Position.isCorner(t)) {
                var e = this.split(t);
                return e[2] + e[1]
            }
            return t
        },
        adjustOffsetBasedOnPosition: function (t, e, i) {
            var s = $.extend({}, t), n = {horizontal: "x", vertical: "y"}, o = {x: "y", y: "x"}, a = {
                top: {right: "x"},
                bottom: {left: "x"},
                left: {bottom: "y"},
                right: {top: "y"}
            }, r = Position.getOrientation(e);
            if (r == Position.getOrientation(i)) {
                if (Position.getSide(e) != Position.getSide(i)) {
                    var h = o[n[r]];
                    s[h] *= -1
                }
            } else {
                var l = s.x;
                s.x = s.y, s.y = l;
                var c = a[Position.getSide(e)][Position.getSide(i)];
                c && (s[c] *= -1), s[n[Position.getOrientation(i)]] = 0
            }
            return s
        },
        getBoxFromPoints: function (t, e, i, s) {
            var n = Math.min(t, i), o = Math.max(t, i), a = Math.min(e, s), r = Math.max(e, s);
            return {left: n, top: a, width: Math.max(o - n, 0), height: Math.max(r - a, 0)}
        },
        isPointWithinBox: function (t, e, i, s, n, o) {
            var a = this.getBoxFromPoints(i, s, n, o);
            return t >= a.left && t <= a.left + a.width && e >= a.top && e <= a.top + a.height
        },
        isPointWithinBoxLayout: function (t, e, i) {
            return this.isPointWithinBox(t, e, i.position.left, i.position.top, i.position.left + i.dimensions.width, i.position.top + i.dimensions.height)
        },
        getDistance: function (t, e, i, s) {
            return Math.sqrt(Math.pow(Math.abs(i - t), 2) + Math.pow(Math.abs(s - e), 2))
        },
        intersectsLine: function () {
            var t = function (t, e, i, s, n, o) {
                var a = (o - e) * (i - t) - (s - e) * (n - t);
                return a > 0 ? !0 : 0 > a ? !1 : !0
            };
            return function (e, i, s, n, o, a, r, h, l) {
                if (!l)return t(e, i, o, a, r, h) != t(s, n, o, a, r, h) && t(e, i, s, n, o, a) != t(e, i, s, n, r, h);
                var c, d, p, u;
                c = s - e, d = n - i, p = r - o, u = h - a;
                var f, m;
                if (f = (-d * (e - o) + c * (i - a)) / (-p * d + c * u), m = (p * (i - a) - u * (e - o)) / (-p * d + c * u), f >= 0 && 1 >= f && m >= 0 && 1 >= m) {
                    var g = e + m * c, v = i + m * d;
                    return {x: g, y: v}
                }
                return !1
            }
        }()
    }, Bounds = {
        viewport: function () {
            var t;
            return t = Browser.MobileSafari || Browser.Android && Browser.Gecko ? {
                width: window.innerWidth,
                height: window.innerHeight
            } : {height: $(window).height(), width: $(window).width()}
        }
    }, Mouse = {
        _buffer: {pageX: 0, pageY: 0},
        _dimensions: {width: 30, height: 30},
        _shift: {x: 2, y: 10},
        getPosition: function (t) {
            var e = this.getActualPosition(t);
            return {
                left: e.left - Math.round(.5 * this._dimensions.width) + this._shift.x,
                top: e.top - Math.round(.5 * this._dimensions.height) + this._shift.y
            }
        },
        getActualPosition: function (t) {
            var e = t && "number" == $.type(t.pageX) ? t : this._buffer;
            return {left: e.pageX, top: e.pageY}
        },
        getDimensions: function () {
            return this._dimensions
        }
    }, Color = function () {
        function t(t) {
            return ("0" + parseInt(t).toString(16)).slice(-2)
        }

        function e(e) {
            return e = e.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/), "#" + t(e[1]) + t(e[2]) + t(e[3])
        }

        var i = {
            _default: "#000000",
            aqua: "#00ffff",
            black: "#000000",
            blue: "#0000ff",
            fuchsia: "#ff00ff",
            gray: "#808080",
            green: "#008000",
            lime: "#00ff00",
            maroon: "#800000",
            navy: "#000080",
            olive: "#808000",
            purple: "#800080",
            red: "#ff0000",
            silver: "#c0c0c0",
            teal: "#008080",
            white: "#ffffff",
            yellow: "#ffff00"
        };
        return {
            toRGB: function (t) {
                if (/^rgba?\(/.test(t))return e(t);
                i[t] && (t = i[t]);
                var s = t.replace("#", "");
                return /^(?:[0-9a-fA-F]{3}){1,2}$/.test(s) || i._default, 3 == s.length && (s = s.charAt(0) + s.charAt(0) + s.charAt(1) + s.charAt(1) + s.charAt(2) + s.charAt(2)), "#" + s
            }
        }
    }();
    Spin.supported = Support.css.transform && Support.css.animation, $.extend(Spin.prototype, {
        initialize: function () {
            this.options = $.extend({}, arguments[0] || {}), this.build(), this.start()
        }, build: function () {
            var t = 2 * (this.options.length + this.options.radius), e = {height: t, width: t};
            this.element = $("<div>").addClass("tpd-spin").css(e), this.element.append(this._rotate = $("<div>").addClass("tpd-spin-rotate")), this.element.css({
                "margin-left": -.5 * e.width,
                "margin-top": -.5 * e.height
            });
            for (var i = this.options.lines, s = 0; i > s; s++) {
                var n, o;
                this._rotate.append(n = $("<div>").addClass("tpd-spin-frame").append(o = $("<div>").addClass("tpd-spin-line"))), o.css({
                    "background-color": this.options.color,
                    width: this.options.width,
                    height: this.options.length,
                    "margin-left": -.5 * this.options.width,
                    "border-radius": Math.round(.5 * this.options.width)
                }), n.css({opacity: (1 / i * (s + 1)).toFixed(2)});
                var a = {};
                a[Support.css.prefixed("transform")] = "rotate(" + 360 / i * (s + 1) + "deg)", n.css(a)
            }
        }, start: function () {
            var t = {};
            t[Support.css.prefixed("animation")] = "tpd-spin 1s infinite steps(" + this.options.lines + ")", this._rotate.css(t)
        }, stop: function () {
            var t = {};
            t[Support.css.prefixed("animation")] = "none", this._rotate.css(t), this.element.detach()
        }
    }), $.extend(Visible.prototype, {
        initialize: function (t) {
            return t = "array" == $.type(t) ? t : [t], this.elements = t, this._restore = [], $.each(t, $.proxy(function (t, e) {
                var i = $(e).is(":visible");
                i || $(e).show(), this._restore.push({element: e, visible: i})
            }, this)), this
        }, restore: function () {
            $.each(this._restore, function (t, e) {
                e.visible || $(e.element).show()
            }), this._restore = null
        }
    });
    var AjaxCache = function () {
        var t = [];
        return {
            get: function (e) {
                for (var i = null, s = 0; s < t.length; s++)t[s] && t[s].url == e.url && (t[s].type || "GET").toUpperCase() == (e.type || "GET").toUpperCase() && $.param(t[s].data || {}) == $.param(e.data || {}) && (i = t[s]);
                return i
            }, set: function (e, i, s) {
                var n = this.get(e);
                n || (n = $.extend({callbacks: {}}, e), t.push(n)), n.callbacks[i] = s
            }, remove: function (e) {
                for (var i = 0; i < t.length; i++)t[i] && t[i].url == e && delete t[i]
            }, clear: function () {
                t = []
            }
        }
    }(), Voila = function (t) {
        function e(i, s, n) {
            if (!(this instanceof e))return new e(i, s, n);
            var o = t.type(arguments[1]), a = "object" === o ? arguments[1] : {}, r = "function" === o ? arguments[1] : "function" === t.type(arguments[2]) ? arguments[2] : !1;
            return this.options = t.extend({method: "onload"}, a), this.deferred = new jQuery.Deferred, r && this.always(r), this._processed = 0, this.images = [], this._add(i), this
        }

        t.extend(e.prototype, {
            _add: function (e) {
                var s = "string" == t.type(e) ? t(e) : e instanceof jQuery || e.length > 0 ? e : [e];
                t.each(s, t.proxy(function (e, s) {
                    var n = t(), o = t(s);
                    n = o.is("img") ? n.add(o) : n.add(o.find("img")), n.each(t.proxy(function (e, s) {
                        this.images.push(new i(s, t.proxy(function (t) {
                            this._progress(t)
                        }, this), t.proxy(function (t) {
                            this._progress(t)
                        }, this), this.options))
                    }, this))
                }, this)), this.images.length < 1 && setTimeout(t.proxy(function () {
                    this._resolve()
                }, this))
            }, abort: function () {
                this._progress = this._notify = this._reject = this._resolve = function () {
                }, t.each(this.images, function (t, e) {
                    e.abort()
                }), this.images = []
            }, _progress: function (t) {
                this._processed++, t.isLoaded || (this._broken = !0), this._notify(t), this._processed == this.images.length && this[this._broken ? "_reject" : "_resolve"]()
            }, _notify: function (t) {
                this.deferred.notify(this, t)
            }, _reject: function () {
                this.deferred.reject(this)
            }, _resolve: function () {
                this.deferred.resolve(this)
            }, always: function (t) {
                return this.deferred.always(t), this
            }, done: function (t) {
                return this.deferred.done(t), this
            }, fail: function (t) {
                return this.deferred.fail(t), this
            }, progress: function (t) {
                return this.deferred.progress(t), this
            }
        });
        var i = function (t) {
            var e = function () {
                return this.initialize.apply(this, Array.prototype.slice.call(arguments))
            };
            t.extend(e.prototype, {
                initialize: function () {
                    this.options = t.extend({
                        test: function () {
                        }, success: function () {
                        }, timeout: function () {
                        }, callAt: !1, intervals: [[0, 0], [1e3, 10], [2e3, 50], [4e3, 100], [2e4, 500]]
                    }, arguments[0] || {}), this._test = this.options.test, this._success = this.options.success, this._timeout = this.options.timeout, this._ipos = 0, this._time = 0, this._delay = this.options.intervals[this._ipos][1], this._callTimeouts = [], this.poll(), this._createCallsAt()
                }, poll: function () {
                    this._polling = setTimeout(t.proxy(function () {
                        if (this._test())return void this.success();
                        if (this._time += this._delay, this._time >= this.options.intervals[this._ipos][0]) {
                            if (!this.options.intervals[this._ipos + 1])return void("function" == t.type(this._timeout) && this._timeout());
                            this._ipos++, this._delay = this.options.intervals[this._ipos][1]
                        }
                        this.poll()
                    }, this), this._delay)
                }, success: function () {
                    this.abort(), this._success()
                }, _createCallsAt: function () {
                    this.options.callAt && t.each(this.options.callAt, t.proxy(function (e, i) {
                        var s = i[0], n = i[1], o = setTimeout(t.proxy(function () {
                            n()
                        }, this), s);
                        this._callTimeouts.push(o)
                    }, this))
                }, _stopCallTimeouts: function () {
                    t.each(this._callTimeouts, function (t, e) {
                        clearTimeout(e)
                    }), this._callTimeouts = []
                }, abort: function () {
                    this._stopCallTimeouts(), this._polling && (clearTimeout(this._polling), this._polling = null)
                }
            });
            var i = function () {
                return this.initialize.apply(this, Array.prototype.slice.call(arguments))
            };
            return t.extend(i.prototype, {
                supports: {
                    naturalWidth: function () {
                        return "naturalWidth"in new Image
                    }()
                }, initialize: function (e, i, s) {
                    return this.img = t(e)[0], this.successCallback = i, this.errorCallback = s, this.isLoaded = !1, this.options = t.extend({
                        method: "onload",
                        pollFallbackAfter: 1e3
                    }, arguments[3] || {}), "onload" != this.options.method && this.supports.naturalWidth ? void this.poll() : void this.load()
                }, poll: function () {
                    this._poll = new e({
                        test: t.proxy(function () {
                            return this.img.naturalWidth > 0
                        }, this), success: t.proxy(function () {
                            this.success()
                        }, this), timeout: t.proxy(function () {
                            this.error()
                        }, this), callAt: [[this.options.pollFallbackAfter, t.proxy(function () {
                            this.load()
                        }, this)]]
                    })
                }, load: function () {
                    this._loading = setTimeout(t.proxy(function () {
                        var e = new Image;
                        this._onloadImage = e, e.onload = t.proxy(function () {
                            e.onload = function () {
                            }, this.supports.naturalWidth || (this.img.naturalWidth = e.width, this.img.naturalHeight = e.height, e.naturalWidth = e.width, e.naturalHeight = e.height), this.success()
                        }, this), e.onerror = t.proxy(this.error, this), e.src = this.img.src
                    }, this))
                }, success: function () {
                    this._calledSuccess || (this._calledSuccess = !0, this.abort(), this.waitForRender(t.proxy(function () {
                        this.isLoaded = !0, this.successCallback(this)
                    }, this)))
                }, error: function () {
                    this._calledError || (this._calledError = !0, this.abort(), this._errorRenderTimeout = setTimeout(t.proxy(function () {
                        this.errorCallback && this.errorCallback(this)
                    }, this)))
                }, abort: function () {
                    this.stopLoading(), this.stopPolling(), this.stopWaitingForRender()
                }, stopPolling: function () {
                    this._poll && (this._poll.abort(), this._poll = null)
                }, stopLoading: function () {
                    this._loading && (clearTimeout(this._loading), this._loading = null), this._onloadImage && (this._onloadImage.onload = function () {
                    }, this._onloadImage.onerror = function () {
                    })
                }, waitForRender: function (t) {
                    this._renderTimeout = setTimeout(t)
                }, stopWaitingForRender: function () {
                    this._renderTimeout && (clearTimeout(this._renderTimeout), this._renderTimeout = null), this._errorRenderTimeout && (clearTimeout(this._errorRenderTimeout), this._errorRenderTimeout = null)
                }
            }), i
        }(jQuery);
        return e
    }(jQuery);
    Tipped.Behaviors = {
        hide: {
            showOn: {element: "mouseenter", tooltip: !1},
            hideOn: {element: "mouseleave", tooltip: "mouseenter"}
        },
        mouse: {
            showOn: {element: "mouseenter", tooltip: !1},
            hideOn: {element: "mouseleave", tooltip: "mouseenter"},
            target: "mouse",
            showDelay: 100,
            fadeIn: 0,
            hideDelay: 0,
            fadeOut: 0
        },
        sticky: {
            showOn: {element: "mouseenter", tooltip: "mouseenter"},
            hideOn: {element: "mouseleave", tooltip: "mouseleave"},
            showDelay: 150,
            target: "mouse",
            fixed: !0
        }
    };
    var Options = {
        create: function () {
            function t(e) {
                return n = Tipped.Skins.base, o = deepExtend($.extend({}, n), Tipped.Skins.reset), t = s, s(e)
            }

            function e(t) {
                return t.match(/^(top|left|bottom|right)$/) && (t += "middle"), t.replace("center", "middle").replace(" ", ""), t
            }

            function i(t) {
                var e, i;
                return e = t.behavior && (i = Tipped.Behaviors[t.behavior]) ? deepExtend($.extend({}, i), t) : t
            }

            function s(t) {
                var s = t.skin ? t.skin : Tooltips.options.defaultSkin, a = $.extend({}, Tipped.Skins[s] || {});
                a.skin || (a.skin = Tooltips.options.defaultSkin || "dark");
                var r = deepExtend($.extend({}, o), i(a)), h = deepExtend($.extend({}, r), i(t));
                h[sfcc("115,107,105,110")] = sfcc("100,97,114,107"), h.ajax && (o.ajax || {}, n.ajax, "boolean" == $.type(h.ajax) && (h.ajax = {}), h.ajax = !1);
                var l, c = c = h.position && h.position.target || "string" == $.type(h.position) && h.position || o.position && o.position.target || "string" == $.type(o.position) && o.position || n.position && n.position.target || n.position;
                c = e(c);
                var d = h.position && h.position.tooltip || o.position && o.position.tooltip || n.position && n.position.tooltip || Tooltips.Position.getInversedPosition(c);
                if (d = e(d), h.position ? "string" == $.type(h.position) ? (h.position = e(h.position), l = {
                        target: h.position,
                        tooltip: Tooltips.Position.getTooltipPositionFromTarget(h.position)
                    }) : (l = {
                        tooltip: d,
                        target: c
                    }, h.position.tooltip && (l.tooltip = e(h.position.tooltip)), h.position.target && (l.target = e(h.position.target))) : l = {
                        tooltip: d,
                        target: c
                    }, Position.isCorner(l.target) && Position.getOrientation(l.target) != Position.getOrientation(l.tooltip) && (l.target = Position.inverseCornerPlane(l.target)), "mouse" == h.target) {
                    var p = Position.getOrientation(l.target);
                    l.target = "horizontal" == p ? l.target.replace(/(left|right)/, "middle") : l.target.replace(/(top|bottom)/, "middle")
                }
                h.position = l;
                var u;
                if ("mouse" == h.target ? (u = $.extend({}, n.offset), $.extend(u, Tipped.Skins.reset.offset || {}), u = Position.adjustOffsetBasedOnPosition(n.offset, n.position, l.target, !0), t.offset && (u = $.extend(u, t.offset || {}))) : u = {
                        x: h.offset.x,
                        y: h.offset.y
                    }, h.offset = u, h.hideOn && "click-outside" == h.hideOn && (h.hideOnClickOutside = !0, h.hideOn = !1, h.fadeOut = 0), h.showOn) {
                    var f = h.showOn;
                    "string" == $.type(f) && (f = {element: f}), h.showOn = f
                }
                if (h.hideOn) {
                    var m = h.hideOn;
                    "string" == $.type(m) && (m = {element: m}), h.hideOn = m
                }
                return h.inline && "string" != $.type(h.inline) && (h.inline = !1), Browser.IE && Browser.IE < 9 && $.extend(h, {
                    fadeIn: 0,
                    fadeOut: 0,
                    hideDelay: 0
                }), h.spinner && (Spin.supported ? "boolean" == $.type(h.spinner) && (h.spinner = o.spinner || n.spinner || {}) : h.spinner = !1), h.container || (h.container = document.body), h.containment && "string" == $.type(h.containment) && (h.containment = {
                    selector: h.containment,
                    padding: o.containment && o.containment.padding || n.padding && n.containment.padding
                }), h.shadow && (h.shadow = Support.shadow), h
            }

            var n, o;
            return t
        }()
    };
    $.extend(Skin.prototype, {
        initialize: function (t) {
            this.tooltip = t, this.element = t._skin;
            var e = this.tooltip.options;
            this.tooltip._tooltip[(e.shadow ? "remove" : "add") + "Class"]("tpd-no-shadow")[(e.radius ? "remove" : "add") + "Class"]("tpd-no-radius")[(e.stem ? "remove" : "add") + "Class"]("tpd-no-stem");
            var i, s, n, o, a = Support.css.prefixed("borderTopLeftRadius");
            this.element.append(i = $("<div>").addClass("tpd-frames").append($("<div>").addClass("tpd-frame").append($("<div>").addClass("tpd-backgrounds").append(s = $("<div>").addClass("tpd-background").append(n = $("<div>").addClass("tpd-background-content")))))).append(o = $("<div>").addClass("tpd-spinner")), s.css({
                width: 999,
                height: 999,
                zoom: 1
            }), this._css = {
                border: parseFloat(s.css("border-top-width")),
                radius: parseFloat(a ? s.css(a) : 0),
                padding: parseFloat(t._content.css("padding-top")),
                borderColor: s.css("border-top-color"),
                backgroundColor: n.css("background-color"),
                backgroundOpacity: n.css("opacity"),
                spinner: {dimensions: {width: o.innerWidth(), height: o.innerHeight()}}
            }, o.remove(), i.remove(), this._side = Position.getSide(t.options.position.tooltip) || "top", this._vars = {}
        }, destroy: function () {
            this.frames && ($.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                this["stem_" + e] && this["stem_" + e].destroy()
            }, this)), this.frames.remove(), this.frames = null)
        }, build: function () {
            this.frames || (this.element.append(this.frames = $("<div>").addClass("tpd-frames")), $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                this.insertFrame(e)
            }, this)), this._spinner || this.tooltip._tooltip.append(this._spinner = $("<div>").addClass("tpd-spinner").hide().append($("<div>").addClass("tpd-spinner-spin"))))
        }, _frame: function () {
            var t, e = $("<div>").addClass("tpd-frame").append(t = $("<div>").addClass("tpd-backgrounds").append($("<div>").addClass("tpd-background-shadow"))).append($("<div>").addClass("tpd-shift-stem").append($("<div>").addClass("tpd-shift-stem-side tpd-shift-stem-side-before")).append($("<div>").addClass("tpd-stem")).append($("<div>").addClass("tpd-shift-stem-side tpd-shift-stem-side-after")));
            return $.each("top right bottom left".split(" "), $.proxy(function (e, i) {
                t.append($("<div>").addClass("tpd-background-box tpd-background-box-" + i).append($("<div>").addClass("tpd-background-box-shift").append($("<div>").addClass("tpd-background-box-shift-further").append($("<div>").addClass("tpd-background").append($("<div>").addClass("tpd-background-title")).append($("<div>").addClass("tpd-background-content"))).append($("<div>").addClass("tpd-background tpd-background-loading")).append($("<div>").addClass("tpd-background-border-hack").hide()))))
            }, this)), e
        }(), _getFrame: function (t) {
            var e = this._frame.clone();
            e.addClass("tpd-frame-" + t), e.find(".tpd-background-shadow").css({"border-radius": this._css.radius}), this.tooltip.options.stem && e.find(".tpd-stem").attr("data-stem-position", t);
            var i = Math.max(this._css.radius - this._css.border, 0);
            e.find(".tpd-background-title").css({
                "border-top-left-radius": i,
                "border-top-right-radius": i
            }), e.find(".tpd-background-content").css({
                "border-bottom-left-radius": i,
                "border-bottom-right-radius": i
            }), e.find(".tpd-background-loading").css({"border-radius": i});
            var s = {backgroundColor: this._css.borderColor}, n = Position.getOrientation(t), o = "horizontal" == n;
            s[o ? "height" : "width"] = this._css.border + "px";
            var a = {top: "bottom", bottom: "top", left: "right", right: "left"};
            return s[a[t]] = 0, e.find(".tpd-shift-stem-side").css(s), e
        }, insertFrame: function (t) {
            var e = this["frame_" + t] = this._getFrame(t);
            if (this.frames.append(e), this.tooltip.options.stem) {
                var i = e.find(".tpd-stem");
                this["stem_" + t] = new Stem(i, this, {})
            }
        }, startLoading: function () {
            this.tooltip.supportsLoading && (this.build(), this._spinner || this.tooltip.is("resize-to-content") || this.setDimensions(this._css.spinner.dimensions), this._spinner && this._spinner.show())
        }, stopLoading: function () {
            this.tooltip.supportsLoading && this._spinner && (this.build(), this._spinner.hide())
        }, updateBackground: function () {
            var t = this._vars.frames[this._side], e = $.extend({}, t.background.dimensions);
            if (this.tooltip.title && !this.tooltip.is("loading")) {
                this.element.find(".tpd-background-title, .tpd-background-content").show(), this.element.find(".tpd-background").css({"background-color": "transparent"});
                var i = $.extend({}, e), s = Math.max(this._css.radius - this._css.border, 0), n = {
                    "border-top-left-radius": s,
                    "border-top-right-radius": s,
                    "border-bottom-left-radius": s,
                    "border-bottom-right-radius": s
                }, o = new Visible(this.tooltip._tooltip), a = this.tooltip._titleWrapper.innerHeight();
                i.height -= a, this.element.find(".tpd-background-title").css({
                    height: a,
                    width: e.width
                }), n["border-top-left-radius"] = 0, n["border-top-right-radius"] = 0, o.restore(), this.element.find(".tpd-background-content").css(i).css(n), this.element.find(".tpd-background-loading").css({"background-color": this._css.backgroundColor})
            } else this.element.find(".tpd-background-title, .tpd-background-content").hide(), this.element.find(".tpd-background").css({"background-color": this._css.backgroundColor});
            this._css.border && (this.element.find(".tpd-background").css({"border-color": "transparent"}), this.element.find(".tpd-background-border-hack").css({
                width: e.width,
                height: e.height,
                "border-radius": this._css.radius,
                "border-width": this._css.border,
                "border-color": this._css.borderColor
            }).show())
        }, paint: function () {
            if (!this._paintedDimensions || this._paintedDimensions.width != this._dimensions.width || this._paintedDimensions.height != this._dimensions.height || this._paintedStemPosition != this._stemPosition) {
                this._paintedDimensions = this._dimensions, this._paintedStemPosition = this._stemPosition, this.element.removeClass("tpd-visible-frame-top tpd-visible-frame-bottom tpd-visible-frame-left tpd-visible-frame-right").addClass("tpd-visible-frame-" + this._side);
                var t = this._vars.frames[this._side], e = $.extend({}, t.background.dimensions);
                this.element.find(".tpd-background").css(e), this.element.find(".tpd-background-shadow").css({
                    width: e.width + 2 * this._css.border,
                    height: e.height + 2 * this._css.border
                }), this.updateBackground(), this.element.find(".tpd-background-box-shift, .tpd-background-box-shift-further").removeAttr("style"), this.element.add(this.frames).add(this.tooltip._tooltip).css(t.dimensions);
                var i = this._side, s = this._vars.frames[i], n = this.element.find(".tpd-frame-" + this._side), o = this._vars.frames[i].dimensions;
                n.css(o), n.find(".tpd-backgrounds").css($.extend({}, s.background.position, {
                    width: o.width - s.background.position.left,
                    height: o.height - s.background.position.top
                }));
                var a = Position.getOrientation(i);
                if (this.tooltip.options.stem)if (n.find(".tpd-shift-stem").css($.extend({}, s.shift.dimensions, s.shift.position)), "vertical" == a) {
                    var r = n.find(".tpd-background-box-top, .tpd-background-box-bottom");
                    r.css({
                        height: this._vars.cut,
                        width: this._css.border
                    }), n.find(".tpd-background-box-bottom").css({top: s.dimensions.height - this._vars.cut}).find(".tpd-background-box-shift").css({"margin-top": -1 * s.dimensions.height + this._vars.cut});
                    var h = "right" == i ? s.dimensions.width - s.stemPx - this._css.border : 0;
                    r.css({left: h}).find(".tpd-background-box-shift").css({"margin-left": -1 * h}), n.find(".tpd-background-box-" + ("left" == i ? "left" : "right")).hide(), "right" == i ? n.find(".tpd-background-box-left").css({width: s.dimensions.width - s.stemPx - this._css.border}) : n.find(".tpd-background-box-right").css({"margin-left": this._css.border}).find(".tpd-background-box-shift").css({"margin-left": -1 * this._css.border});
                    var l = n.find(".tpd-background-box-" + this._side);
                    l.css({
                        height: s.dimensions.height - 2 * this._vars.cut,
                        "margin-top": this._vars.cut
                    }), l.find(".tpd-background-box-shift").css({"margin-top": -1 * this._vars.cut})
                } else {
                    var r = n.find(".tpd-background-box-left, .tpd-background-box-right");
                    r.css({
                        width: this._vars.cut,
                        height: this._css.border
                    }), n.find(".tpd-background-box-right").css({left: s.dimensions.width - this._vars.cut}).find(".tpd-background-box-shift").css({"margin-left": -1 * s.dimensions.width + this._vars.cut});
                    var h = "bottom" == i ? s.dimensions.height - s.stemPx - this._css.border : 0;
                    r.css({top: h}).find(".tpd-background-box-shift").css({"margin-top": -1 * h}), n.find(".tpd-background-box-" + ("top" == i ? "top" : "bottom")).hide(), "bottom" == i ? n.find(".tpd-background-box-top").css({height: s.dimensions.height - s.stemPx - this._css.border}) : n.find(".tpd-background-box-bottom").css({"margin-top": this._css.border}).find(".tpd-background-box-shift").css({"margin-top": -1 * this._css.border});
                    var l = n.find(".tpd-background-box-" + this._side);
                    l.css({
                        width: s.dimensions.width - 2 * this._vars.cut,
                        "margin-left": this._vars.cut
                    }), l.find(".tpd-background-box-shift").css({"margin-left": -1 * this._vars.cut})
                }
                var c = t.background, d = c.position, p = c.dimensions;
                this._spinner.css({
                    top: d.top + this._css.border + (.5 * p.height - .5 * this._css.spinner.dimensions.height),
                    left: d.left + this._css.border + (.5 * p.width - .5 * this._css.spinner.dimensions.width)
                })
            }
        }, getVars: function () {
            var t = (this._css.padding, this._css.radius, this._css.border), e = this._vars.maxStemHeight || 0, i = $.extend({}, this._dimensions || {}), s = {
                frames: {},
                dimensions: i,
                maxStemHeight: e
            };
            s.cut = Math.max(this._css.border, this._css.radius) || 0;
            var n = {width: 0, height: 0}, o = 0, a = 0;
            return this.tooltip.options.stem && (n = this.stem_top.getMath().dimensions.outside, o = this.stem_top._css.offset, a = Math.max(n.height - this._css.border, 0)), s.stemDimensions = n, s.stemOffset = o, Position.getOrientation(this._side), $.each("top right bottom left".split(" "), $.proxy(function (e, o) {
                var r = Position.getOrientation(o), h = "vertical" == r, l = {
                    width: i.width + 2 * t,
                    height: i.height + 2 * t
                }, c = l[h ? "height" : "width"] - 2 * s.cut, d = {
                    dimensions: l,
                    stemPx: a,
                    position: {top: 0, left: 0},
                    background: {dimensions: $.extend({}, i), position: {top: 0, left: 0}}
                };
                if (s.frames[o] = d, d.dimensions[h ? "width" : "height"] += a, ("top" == o || "left" == o) && (d.background.position[o] += a), $.extend(d, {
                        shift: {
                            position: {
                                top: 0,
                                left: 0
                            }, dimensions: {width: h ? n.height : c, height: h ? c : n.height}
                        }
                    }), Browser.IE && Browser.IE < 9) {
                    var p = d.shift.dimensions;
                    p.width = Math.round(p.width), p.height = Math.round(p.height)
                }
                switch (o) {
                    case"top":
                    case"bottom":
                        d.shift.position.left += s.cut, "bottom" == o && (d.shift.position.top += l.height - t - a);
                        break;
                    case"left":
                    case"right":
                        d.shift.position.top += s.cut, "right" == o && (d.shift.position.left += l.width - t - a)
                }
            }, this)), s.connections = {}, $.each(Position.positions, $.proxy(function (t, e) {
                s.connections[e] = this.getConnectionLayout(e, s)
            }, this)), s
        }, setDimensions: function (t) {
            this.build();
            var e = this._dimensions;
            e && e.width == t.width && e.height == t.height || (this._dimensions = t, this._vars = this.getVars())
        }, setSide: function (t) {
            this._side = t, this._vars = this.getVars()
        }, getConnectionLayout: function (t, e) {
            var i = Position.getSide(t), s = Position.getOrientation(t), n = (e.dimensions,
                e.cut), o = this["stem_" + i], a = e.stemOffset, r = this.tooltip.options.stem ? o.getMath().dimensions.outside.width : 0, h = n + a + .5 * r, l = {stem: {}}, c = {
                left: 0,
                right: 0,
                up: 0,
                down: 0
            }, d = {top: 0, left: 0}, p = {top: 0, left: 0}, u = e.frames[i], h = 0;
            if ("horizontal" == s) {
                var f = u.dimensions.width;
                this.tooltip.options.stem && (f = u.shift.dimensions.width, 2 * a > f - r && (a = Math.floor(.5 * (f - r)) || 0), h = n + a + .5 * r);
                var m = f - 2 * a, g = Position.split(t), v = a;
                switch (g[2]) {
                    case"left":
                        c.right = m - r, d.left = h;
                        break;
                    case"middle":
                        v += Math.round(.5 * m - .5 * r), c.left = v - a, c.right = v - a, d.left = p.left = Math.round(.5 * u.dimensions.width);
                        break;
                    case"right":
                        v += m - r, c.left = m - r, d.left = u.dimensions.width - h, p.left = u.dimensions.width
                }
                "bottom" == g[1] && (d.top += u.dimensions.height, p.top += u.dimensions.height), $.extend(l.stem, {
                    position: {left: v},
                    before: {width: v},
                    after: {left: v + r, width: f - v - r + 1}
                })
            } else {
                var b = u.dimensions.height;
                this.tooltip.options.stem && (b = u.shift.dimensions.height, 2 * a > b - r && (a = Math.floor(.5 * (b - r)) || 0), h = n + a + .5 * r);
                var _ = b - 2 * a, g = Position.split(t), y = a;
                switch (g[2]) {
                    case"top":
                        c.down = _ - r, d.top = h;
                        break;
                    case"middle":
                        y += Math.round(.5 * _ - .5 * r), c.up = y - a, c.down = y - a, d.top = p.top = Math.round(.5 * u.dimensions.height);
                        break;
                    case"bottom":
                        y += _ - r, c.up = _ - r, d.top = u.dimensions.height - h, p.top = u.dimensions.height
                }
                "right" == g[1] && (d.left += u.dimensions.width, p.left += u.dimensions.width), $.extend(l.stem, {
                    position: {top: y},
                    before: {height: y},
                    after: {top: y + r, height: b - y - r + 1}
                })
            }
            return l.move = c, l.stem.connection = d, l.connection = p, l
        }, setStemPosition: function (t, e) {
            if (this._stemPosition != t) {
                this._stemPosition = t;
                var i = Position.getSide(t);
                this.setSide(i)
            }
            this.tooltip.options.stem && this.setStemShift(t, e)
        }, setStemShift: function (t, e) {
            var i = this._shift, s = this._dimensions;
            if (!i || i.stemPosition != t || i.shift.x != e.x || i.shift.y != e.y || !s || i.dimensions.width != s.width || i.dimensions.height != s.height) {
                this._shift = {stemPosition: t, shift: e, dimensions: s};
                var n = Position.getSide(t), o = {
                    horizontal: "x",
                    vertical: "y"
                }[Position.getOrientation(t)], a = {
                    x: {left: "left", width: "width"},
                    y: {left: "top", width: "height"}
                }[o], r = this["stem_" + n], h = deepExtend({}, this._vars.connections[t].stem);
                e && 0 !== e[o] && (h.before[a.width] += e[o], h.position[a.left] += e[o], h.after[a.left] += e[o], h.after[a.width] -= e[o]), r.element.css(h.position), r.element.siblings(".tpd-shift-stem-side-before").css(h.before), r.element.siblings(".tpd-shift-stem-side-after").css(h.after)
            }
        }
    }), $.extend(Stem.prototype, {
        initialize: function (t, e) {
            this.element = $(t), this.element[0] && (this.skin = e, this.element.removeClass("tpd-stem-reset"), this._css = $.extend({}, e._css, {
                width: this.element.innerWidth(),
                height: this.element.innerHeight(),
                offset: parseFloat(this.element.css("margin-left")),
                spacing: parseFloat(this.element.css("margin-top"))
            }), this.element.addClass("tpd-stem-reset"), this.options = $.extend({}, arguments[2] || {}), this._position = this.element.attr("data-stem-position") || "top", this._m = 100, this.build())
        }, destroy: function () {
            this.element.html("")
        }, build: function () {
            this.destroy();
            var t = this._css.backgroundColor, e = t.indexOf("rgba") > -1 && parseFloat(t.replace(/^.*,(.+)\)/, "$1")), i = e && 1 > e;
            this._useTransform = i && Support.css.transform, this._css.border || (this._useTransform = !1), this[(this._useTransform ? "build" : "buildNo") + "Transform"]()
        }, buildTransform: function () {
            this.element.append(this.spacer = $("<div>").addClass("tpd-stem-spacer").append(this.downscale = $("<div>").addClass("tpd-stem-downscale").append(this.transform = $("<div>").addClass("tpd-stem-transform").append(this.first = $("<div>").addClass("tpd-stem-side").append(this.border = $("<div>").addClass("tpd-stem-border")).append($("<div>").addClass("tpd-stem-border-corner")).append($("<div>").addClass("tpd-stem-triangle")))))), this.transform.append(this.last = this.first.clone().addClass("tpd-stem-side-inversed")), this.sides = this.first.add(this.last);
            var t = this.getMath(), e = t.dimensions, i = this._m, s = Position.getSide(this._position);
            if (this.element.find(".tpd-stem-spacer").css({
                    width: d ? e.inside.height : e.inside.width,
                    height: d ? e.inside.width : e.inside.height
                }), "top" == s || "left" == s) {
                var n = {};
                "top" == s ? (n.bottom = 0, n.top = "auto") : "left" == s && (n.right = 0, n.left = "auto"), this.element.find(".tpd-stem-spacer").css(n)
            }
            this.transform.css({width: e.inside.width * i, height: e.inside.height * i});
            var o = Support.css.prefixed("transform"), a = {
                "background-color": "transparent",
                "border-bottom-color": this._css.backgroundColor,
                "border-left-width": .5 * e.inside.width * i,
                "border-bottom-width": e.inside.height * i
            };
            a[o] = "translate(" + t.border * i + "px, 0)", this.element.find(".tpd-stem-triangle").css(a);
            var r = this._css.borderColor;
            alpha = r.indexOf("rgba") > -1 && parseFloat(r.replace(/^.*,(.+)\)/, "$1")), alpha && 1 > alpha ? r = (r.substring(0, r.lastIndexOf(",")) + ")").replace("rgba", "rgb") : alpha = 1;
            var h = {
                "background-color": "transparent",
                "border-right-width": t.border * i,
                width: t.border * i,
                "margin-left": -2 * t.border * i,
                "border-color": r,
                opacity: alpha
            };
            h[o] = "skew(" + t.skew + "deg) translate(" + t.border * i + "px, " + -1 * this._css.border * i + "px)", this.element.find(".tpd-stem-border").css(h);
            var r = this._css.borderColor;
            alpha = r.indexOf("rgba") > -1 && parseFloat(r.replace(/^.*,(.+)\)/, "$1")), alpha && 1 > alpha ? r = (r.substring(0, r.lastIndexOf(",")) + ")").replace("rgba", "rgb") : alpha = 1;
            var l = {
                width: t.border * i,
                "border-right-width": t.border * i,
                "border-right-color": r,
                background: r,
                opacity: alpha,
                "margin-left": -2 * t.border * i
            };
            if (l[o] = "skew(" + t.skew + "deg) translate(" + t.border * i + "px, " + (e.inside.height - this._css.border) * i + "px)", this.element.find(".tpd-stem-border-corner").css(l), this.setPosition(this._position), i > 1) {
                var c = {};
                c[o] = "scale(" + 1 / i + "," + 1 / i + ")", this.downscale.css(c)
            }
            var d = /^(left|right)$/.test(this._position);
            this._css.border || this.element.find(".tpd-stem-border, .tpd-stem-border-corner").hide(), this.element.css({
                width: d ? e.outside.height : e.outside.width,
                height: d ? e.outside.width : e.outside.height
            })
        }, buildNoTransform: function () {
            this.element.append(this.spacer = $("<div>").addClass("tpd-stem-spacer").append($("<div>").addClass("tpd-stem-notransform").append($("<div>").addClass("tpd-stem-border").append($("<div>").addClass("tpd-stem-border-corner")).append($("<div>").addClass("tpd-stem-border-center-offset").append($("<div>").addClass("tpd-stem-border-center-offset-inverse").append($("<div>").addClass("tpd-stem-border-center"))))).append($("<div>").addClass("tpd-stem-triangle"))));
            var t = this.getMath(), e = t.dimensions, i = /^(left|right)$/.test(this._position), s = /^(bottom)$/.test(this._position), n = /^(right)$/.test(this._position), o = Position.getSide(this._position);
            if (this.element.css({
                    width: i ? e.outside.height : e.outside.width,
                    height: i ? e.outside.width : e.outside.height
                }), this.element.find(".tpd-stem-notransform").add(this.element.find(".tpd-stem-spacer")).css({
                    width: i ? e.inside.height : e.inside.width,
                    height: i ? e.inside.width : e.inside.height
                }), "top" == o || "left" == o) {
                var a = {};
                "top" == o ? (a.bottom = 0, a.top = "auto") : "left" == o && (a.right = 0, a.left = "auto"), this.element.find(".tpd-stem-spacer").css(a)
            }
            this.element.find(".tpd-stem-border").css({width: "100%", background: "transparent"});
            var r = {opacity: Browser.IE && Browser.IE < 9 ? this._css.borderOpacity : 1};
            r[i ? "height" : "width"] = "100%", r[i ? "width" : "height"] = this._css.border, r[s ? "top" : "bottom"] = 0, $.extend(r, n ? {left: 0} : {right: 0}), this.element.find(".tpd-stem-border-corner").css(r);
            var h = {
                width: 0,
                "background-color": "transparent",
                opacity: Browser.IE && Browser.IE < 9 ? this._css.borderOpacity : 1
            }, l = .5 * e.inside.width + "px solid transparent", c = {"background-color": "transparent"};
            if (.5 * e.inside.width - t.border + "px solid transparent", i) {
                var d = {
                    left: "auto",
                    top: "50%",
                    "margin-top": -.5 * e.inside.width,
                    "border-top": l,
                    "border-bottom": l
                };
                if ($.extend(h, d), h[n ? "right" : "left"] = 0, h[n ? "border-left" : "border-right"] = e.inside.height + "px solid " + this._css.borderColor, $.extend(c, d), c[n ? "border-left" : "border-right"] = e.inside.height + "px solid " + this._css.backgroundColor, c[n ? "right" : "left"] = t.top, c[n ? "left" : "right"] = "auto", Browser.IE && Browser.IE < 8) {
                    var p = .5 * this._css.width + "px solid transparent";
                    $.extend(c, {
                        "margin-top": -.5 * this._css.width,
                        "border-top": p,
                        "border-bottom": p
                    }), c[n ? "border-left" : "border-right"] = this._css.height + "px solid " + this._css.backgroundColor
                }
                this.element.find(".tpd-stem-border-center-offset").css({"margin-left": -1 * this._css.border * (n ? -1 : 1)}).find(".tpd-stem-border-center-offset-inverse").css({"margin-left": this._css.border * (n ? -1 : 1)})
            } else {
                var d = {"margin-left": -.5 * e.inside.width, "border-left": l, "border-right": l};
                if ($.extend(h, d), h[s ? "border-top" : "border-bottom"] = e.inside.height + "px solid " + this._css.borderColor, $.extend(c, d), c[s ? "border-top" : "border-bottom"] = e.inside.height + "px solid " + this._css.backgroundColor, c[s ? "bottom" : "top"] = t.top, c[s ? "top" : "bottom"] = "auto", Browser.IE && Browser.IE < 8) {
                    var p = .5 * this._css.width + "px solid transparent";
                    $.extend(c, {
                        "margin-left": -.5 * this._css.width,
                        "border-left": p,
                        "border-right": p
                    }), c[s ? "border-top" : "border-bottom"] = this._css.height + "px solid " + this._css.backgroundColor
                }
                this.element.find(".tpd-stem-border-center-offset").css({"margin-top": -1 * this._css.border * (s ? -1 : 1)}).find(".tpd-stem-border-center-offset-inverse").css({"margin-top": this._css.border * (s ? -1 : 1)})
            }
            this.element.find(".tpd-stem-border-center").css(h), this.element.find(".tpd-stem-border-corner").css({"background-color": this._css.borderColor}), this.element.find(".tpd-stem-triangle").css(c), this._css.border || this.element.find(".tpd-stem-border").hide()
        }, setPosition: function (t) {
            this._position = t, this.transform.attr("class", "tpd-stem-transform tpd-stem-transform-" + t)
        }, getMath: function () {
            var t = this._css.height, e = this._css.width, i = this._css.border;
            this._useTransform && Math.floor(e) % 2 && (e = Math.max(Math.floor(e) - 1, 0));
            var s = degrees(Math.atan(.5 * e / t)), n = 90 - s, o = i / Math.cos((90 - n) * Math.PI / 180), a = i / Math.cos((90 - s) * Math.PI / 180), r = {
                width: e + 2 * o,
                height: t + a
            };
            Math.max(i, this._css.radius), t = r.height, e = .5 * r.width;
            var h = degrees(Math.atan(t / e)), l = 90 - h, c = i / Math.cos(l * Math.PI / 180), d = 180 * Math.atan(t / e) / Math.PI, p = -1 * (90 - d), u = 90 - d, f = i * Math.tan(u * Math.PI / 180), a = i / Math.cos((90 - u) * Math.PI / 180), m = $.extend({}, r), g = $.extend({}, r);
            g.height += this._css.spacing, g.height = Math.ceil(g.height);
            var v = !0;
            return 2 * i >= r.width && (v = !1), {
                enabled: v,
                outside: g,
                dimensions: {inside: m, outside: g},
                top: a,
                border: c,
                skew: p,
                corner: f
            }
        }
    });
    var Tooltips = {
        tooltips: {},
        options: {defaultSkin: "dark", startingZIndex: 999999},
        _emptyClickHandler: function () {
        },
        init: function () {
            this.reset(), this._resizeHandler = $.proxy(this.onWindowResize, this), $(window).bind("resize orientationchange", this._resizeHandler), Browser.MobileSafari && $("body").bind("click", this._emptyClickHandler)
        },
        reset: function () {
            Tooltips.removeAll(), this._resizeHandler && $(window).unbind("resize orientationchange", this._resizeHandler), Browser.MobileSafari && $("body").unbind("click", this._emptyClickHandler)
        },
        onWindowResize: function () {
            this._resizeTimer && (window.clearTimeout(this._resizeTimer), this._resizeTimer = null), this._resizeTimer = _.delay($.proxy(function () {
                var t = this.getVisible();
                $.each(t, function (t, e) {
                    e.clearUpdatedTo(), e.position()
                })
            }, this), 15)
        },
        _getTooltips: function (t, e) {
            var i, s = [], n = [];
            if (_.isElement(t) ? (i = $(t).data("tipped-uids")) && (s = s.concat(i)) : $(t).each(function (t, e) {
                    (i = $(e).data("tipped-uids")) && (s = s.concat(i))
                }), !s[0] && !e) {
                var o = this.getTooltipByTooltipElement($(t).closest(".tpd-tooltip")[0]);
                o && o.element && (i = $(o.element).data("tipped-uids") || [], i && (s = s.concat(i)))
            }
            return s.length > 0 && $.each(s, $.proxy(function (t, e) {
                var i;
                (i = this.tooltips[e]) && n.push(i)
            }, this)), n
        },
        findElement: function (t) {
            var e = [];
            return _.isElement(t) && (e = this._getTooltips(t)), e[0] && e[0].element
        },
        get: function (t) {
            var e = $.extend({api: !1}, arguments[1] || {}), i = [];
            return _.isElement(t) ? i = this._getTooltips(t) : t instanceof $ ? t.each($.proxy(function (t, e) {
                var s = this._getTooltips(e, !0);
                s.length > 0 && (i = i.concat(s))
            }, this)) : "string" == $.type(t) && $.each(this.tooltips, function (e, s) {
                s.element && $(s.element).is(t) && i.push(s)
            }), e.api && $.each(i, function (t, e) {
                e.is("api", !0)
            }), i
        },
        getTooltipByTooltipElement: function (t) {
            if (!t)return null;
            var e = null;
            return $.each(this.tooltips, function (i, s) {
                s.is("build") && s._tooltip[0] === t && (e = s)
            }), e
        },
        getBySelector: function (t) {
            var e = [];
            return $.each(this.tooltips, function (i, s) {
                s.element && $(s.element).is(t) && e.push(s)
            }), e
        },
        getNests: function () {
            var t = [];
            return $.each(this.tooltips, function (e, i) {
                i.is("nest") && t.push(i)
            }), t
        },
        show: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.show(!1, !0)
            })
        },
        hide: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.hide()
            })
        },
        toggle: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.toggle()
            })
        },
        hideAll: function (t) {
            $.each(this.getVisible(), function (e, i) {
                t && t == i || i.hide()
            })
        },
        refresh: function (t) {
            var e;
            e = t ? $.grep(this.get(t), function (t) {
                return t.is("visible")
            }) : this.getVisible(), $.each(e, function (t, e) {
                e.refresh()
            })
        },
        getVisible: function () {
            var t = [];
            return $.each(this.tooltips, function (e, i) {
                i.visible() && t.push(i)
            }), t
        },
        isVisibleByElement: function (t) {
            var e = !1;
            return _.isElement(t) && $.each(this.getVisible() || [], function (i, s) {
                return s.element == t ? (e = !0, !1) : void 0
            }), e
        },
        getHighestTooltip: function () {
            var t, e = 0;
            return $.each(this.tooltips, function (i, s) {
                s.zIndex > e && (e = s.zIndex, t = s)
            }), t
        },
        resetZ: function () {
            this.getVisible().length <= 1 && $.each(this.tooltips, function (t, e) {
                e.is("build") && !e.options.zIndex && e._tooltip.css({zIndex: e.zIndex = +Tooltips.options.startingZIndex})
            })
        },
        clearAjaxCache: function () {
            $.each(this.tooltips, $.proxy(function (t, e) {
                e.options.ajax && (e._cache && e._cache.xhr && (e._cache.xhr.abort(), e._cache.xhr = null), e.is("updated", !1), e.is("updating", !1), e.is("sanitized", !1))
            }, this)), AjaxCache.clear()
        },
        add: function (t) {
            this.tooltips[t.uid] = t
        },
        remove: function (t) {
            var e = this._getTooltips(t);
            $.each(e, $.proxy(function (t, e) {
                var i = e.uid;
                delete this.tooltips[i], Browser.IE && Browser.IE < 9 ? _.defer(function () {
                    e.remove()
                }) : e.remove()
            }, this))
        },
        removeDetached: function () {
            var t = this.getNests(), e = [];
            t.length > 0 && $.each(t, function (t, i) {
                i.is("detached") && (e.push(i), i.attach())
            }), $.each(this.tooltips, $.proxy(function (t, e) {
                e.element && !_.element.isAttached(e.element) && this.remove(e.element)
            }, this)), $.each(e, function (t, e) {
                e.detach()
            })
        },
        removeAll: function () {
            $.each(this.tooltips, $.proxy(function (t, e) {
                e.element && this.remove(e.element)
            }, this)), this.tooltips = {}
        },
        setDefaultSkin: function (t) {
            this.options.defaultSkin = t || "dark"
        },
        setStartingZIndex: function (t) {
            this.options.startingZIndex = t || 0
        }
    };
    return Tooltips.Position = {
        inversedPosition: {
            left: "right",
            right: "left",
            top: "bottom",
            bottom: "top",
            middle: "middle",
            center: "center"
        }, getInversedPosition: function (t) {
            var e = Position.split(t), i = e[1], s = e[2], n = Position.getOrientation(t), o = $.extend({
                horizontal: !0,
                vertical: !0
            }, arguments[1] || {});
            return "horizontal" == n ? (o.vertical && (i = this.inversedPosition[i]), o.horizontal && (s = this.inversedPosition[s])) : (o.vertical && (s = this.inversedPosition[s]), o.horizontal && (i = this.inversedPosition[i])), i + s
        }, getTooltipPositionFromTarget: function (t) {
            var e = Position.split(t);
            return this.getInversedPosition(e[1] + this.inversedPosition[e[2]])
        }
    }, $.extend(Tooltip.prototype, {
        supportsLoading: Support.css.transform && Support.css.animation,
        initialize: function (element, content) {
            if (this.element = element, this.element) {
                var options;
                "object" != $.type(content) || _.isElement(content) || _.isText(content) || _.isDocumentFragment(content) || content instanceof $ ? options = arguments[2] || {} : (options = content, content = null);
                var dataOptions = $(element).data("tipped-options");
                dataOptions && (options = deepExtend($.extend({}, options), eval("({" + dataOptions + "})"))), this.options = Options.create(options), this._cache = {
                    dimensions: {
                        width: 0,
                        height: 0
                    }, events: [], timers: {}, layouts: {}, is: {}, fnCallFn: "", updatedTo: {}
                }, this.queues = {showhide: $({})};
                var title = $(element).attr("title") || $(element).data("tipped-restore-title");
                if (!content) {
                    var dt = $(element).attr("data-tipped");
                    if (dt ? content = dt : title && (content = title), content) {
                        var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
                        content = content.replace(SCRIPT_REGEX, "")
                    }
                }
                if ((!content || content instanceof $ && !content[0]) && !(this.options.ajax && this.options.ajax.url || this.options.inline))return void(this._aborted = !0);
                title && ($(element).data("tipped-restore-title", title), $(element)[0].setAttribute("title", "")), this.content = content, this.title = $(this.element).data("tipped-title"), "undefined" != $.type(this.options.title) && (this.title = this.options.title), this.zIndex = this.options.zIndex || +Tooltips.options.startingZIndex;
                var uids = $(element).data("tipped-uids");
                uids || (uids = []);
                var uid = getUID();
                this.uid = uid, uids.push(uid), $(element).data("tipped-uids", uids);
                var parentTooltipElement = $(this.element).closest(".tpd-tooltip")[0], parentTooltip;
                parentTooltipElement && (parentTooltip = Tooltips.getTooltipByTooltipElement(parentTooltipElement)) && parentTooltip.is("nest", !0);
                var target = this.options.target;
                this.target = "mouse" == target ? this.element : "element" != target && target ? _.isElement(target) ? target : target instanceof $ && target[0] ? target[0] : this.element : this.element, this.options.inline && (this.content = $("#" + this.options.inline)[0]), this.options.ajax && (this.__content = this.content), "function" == $.type(this.content) && (this._fn = this.content), this.preBuild(), Tooltips.add(this)
            }
        },
        remove: function () {
            this.unbind(), this.clearTimers(), this.restoreElementToMarker(), this.stopLoading(), this.abort(), this.is("build") && this._tooltip && (this._tooltip.remove(), this._tooltip = null);
            var t = $(this.element).data("tipped-uids") || [], e = $.inArray(this.uid, t);
            if (e > -1 && (t.splice(e, 1), $(this.element).data("tipped-uids", t)), t.length < 1) {
                var i, s = "tipped-restore-title";
                (i = $(this.element).data(s)) && ("" != !$(this.element)[0].getAttribute("title") && $(this.element).attr("title", i), $(this.element).removeData(s)), $(this.element).removeData("tipped-uids")
            }
            var n = $(this.element).attr("class") || "", o = n.replace(/(tpd-delegation-uid-)\d+/g, "").replace(/^\s\s*/, "").replace(/\s\s*$/, "");
            $(this.element).attr("class", o)
        },
        detach: function () {
            this.options.detach && !this.is("detached") && (this._tooltip.detach(), this.is("detached", !0))
        },
        attach: function () {
            if (this.is("detached")) {
                var t;
                if ("string" == $.type(this.options.container)) {
                    var e = this.target;
                    "mouse" == e && (e = this.element), t = $($(e).closest(this.options.container).first())
                } else t = $(this.options.container);
                t[0] || (t = $(document.body)), t.append(this._tooltip), this.is("detached", !1)
            }
        },
        preBuild: function () {
            this.is("detached", !0);
            var t = {left: "-10000px", top: "-10000px", opacity: 0, zIndex: this.zIndex};
            this._tooltip = $("<div>").addClass("tpd-tooltip").addClass("tpd-skin-" + Tooltips.options.defaultSkin).addClass("tpd-size-" + this.options.size).css(t).hide(), this.createPreBuildObservers()
        },
        build: function () {
            this.is("build") || (this.attach(), Browser.IE && Browser.IE < 7 && this._tooltip.append(this.iframeShim = $("<iframe>").addClass("tpd-iframeshim").attr({
                frameBorder: 0,
                src: "javascript:'';"
            })), this._tooltip.append(this._skin = $("<div>").addClass("tpd-skin")).append(this._contentWrapper = $("<div>").addClass("tpd-content-wrapper").append(this._contentSpacer = $("<div>").addClass("tpd-content-spacer").append(this._titleWrapper = $("<div>").addClass("tpd-title-wrapper").append(this._titleSpacer = $("<div>").addClass("tpd-title-spacer").append(this._titleRelative = $("<div>").addClass("tpd-title-relative").append(this._titleRelativePadder = $("<div>").addClass("tpd-title-relative-padder").append(this._title = $("<div>").addClass("tpd-title"))))).append(this._close = $("<div>").addClass("tpd-close").append($("<div>").addClass("tpd-close-icon").html("&times;")))).append(this._contentRelative = $("<div>").addClass("tpd-content-relative").append(this._contentRelativePadder = $("<div>").addClass("tpd-content-relative-padder").append(this._content = $("<div>").addClass("tpd-content"))).append(this._inner_close = $("<div>").addClass("tpd-close").append($("<div>").addClass("tpd-close-icon").html("&times;")))))), this.skin = new Skin(this), this._contentSpacer.css({"border-radius": Math.max(this.skin._css.radius - this.skin._css.border, 0)}), this.createPostBuildObservers(), this.is("build", !0))
        },
        createPostBuildObservers: function () {
            this._tooltip.delegate(".tpd-close, .close-tooltip", "click", $.proxy(function (t) {
                t.stopPropagation(), t.preventDefault(), this.is("api", !1), this.hide(!0)
            }, this))
        },
        createPreBuildObservers: function () {
            this.bind(this.element, "mouseenter", this.setActive), this.bind(this._tooltip, "mouseenter", this.setActive), this.bind(this.element, "mouseleave", function (t) {
                this.setIdle(t)
            }), this.bind(this._tooltip, "mouseleave", function (t) {
                this.setIdle(t)
            }), this.options.showOn && $.each(this.options.showOn, $.proxy(function (t, e) {
                var i, s = !1;
                switch (t) {
                    case"element":
                        i = this.element, this.options.hideOn && this.options.showOn && "click" == this.options.hideOn.element && "click" == this.options.showOn.element && (s = !0, this.is("toggleable", s));
                        break;
                    case"tooltip":
                        i = this._tooltip;
                        break;
                    case"target":
                        i = this.target
                }
                i && this.bind(i, e, "click" == e && s ? function () {
                    this.is("api", !1), this.toggle()
                } : function () {
                    this.is("api", !1), this.showDelayed()
                })
            }, this)), this.options.hideOn && $.each(this.options.hideOn, $.proxy(function (t, e) {
                var i;
                switch (t) {
                    case"element":
                        if (this.is("toggleable") && "click" == e)return;
                        i = this.element;
                        break;
                    case"tooltip":
                        i = this._tooltip;
                        break;
                    case"target":
                        i = this.target
                }
                i && this.bind(i, e, function () {
                    this.is("api", !1), this.hideDelayed()
                })
            }, this)), this.options.hideOnClickOutside && ($(this.element).addClass("tpd-hideOnClickOutside"), this.bind(document.documentElement, Support.touch ? "touchend" : "click", $.proxy(function (t) {
                if (this.visible()) {
                    var e = $(t.target).closest(".tpd-tooltip, .tpd-hideOnClickOutside")[0];
                    (!e || e && e != this._tooltip[0] && e != this.element) && this.hide()
                }
            }, this))), "mouse" == this.options.target && this.bind(this.element, "mouseenter mousemove", $.proxy(function (t) {
                this._cache.event = t
            }, this));
            var t = !1;
            this.options.showOn && "mouse" == this.options.target && !this.options.fixed && (t = !0), t && this.bind(this.element, "mousemove", function () {
                this.is("build") && (this.is("api", !1), this.position())
            })
        }
    }), $.extend(Tooltip.prototype, {
        stop: function () {
            if (this._tooltip) {
                var t = this.queues.showhide;
                t.queue([]), this._tooltip.stop(1, 0)
            }
        }, showDelayed: function () {
            this.is("disabled") || (this.clearTimer("hide"), this.is("visible") || this.getTimer("show") || this.setTimer("show", $.proxy(function () {
                this.clearTimer("show"), this.show()
            }, this), this.options.showDelay || 1))
        }, show: function () {
            if (this.clearTimer("hide"), !this.visible() && !this.is("disabled") && $(this.target).is(":visible")) {
                this.is("visible", !0), this.attach(), this.stop();
                var t = this.queues.showhide;
                this.is("updated") || this.is("updating") || t.queue($.proxy(function (t) {
                    this._onResizeDimensions = {width: 0, height: 0}, this.update($.proxy(function (e) {
                        return e ? (this.is("visible", !1), void this.detach()) : void t()
                    }, this))
                }, this)), t.queue($.proxy(function (t) {
                    this.is("sanitized") ? (this.stopLoading(), this._contentWrapper.css({visibility: "visible"}), this.is("resize-to-content", !0), t()) : (this._contentWrapper.css({visibility: "hidden"}), this.startLoading(), this.sanitize($.proxy(function () {
                        this.stopLoading(), this._contentWrapper.css({visibility: "visible"}), this.is("resize-to-content", !0), t()
                    }, this)))
                }, this)), t.queue($.proxy(function (t) {
                    this.position(), this.raise(), t()
                }, this)), t.queue($.proxy(function (t) {
                    if (this.is("updated") && "function" == $.type(this.options.onShow)) {
                        var e = new Visible(this._tooltip);
                        this.options.onShow(this._content[0], this.element), e.restore(), t()
                    } else t()
                }, this)), t.queue($.proxy(function (t) {
                    this._show(this.options.fadeIn, function () {
                        t()
                    })
                }, this)), this.options.hideAfter && t.queue($.proxy(function () {
                    this.setActive()
                }, this))
            }
        }, _show: function (t, e) {
            t = ("number" == $.type(t) ? t : this.options.fadeIn) || 0, e = e || ("function" == $.type(arguments[0]) ? arguments[0] : !1), this.options.hideOthers && Tooltips.hideAll(this), this._tooltip.fadeTo(t, 1, $.proxy(function () {
                e && e()
            }, this))
        }, hideDelayed: function () {
            this.clearTimer("show"), this.getTimer("hide") || !this.visible() || this.is("disabled") || this.setTimer("hide", $.proxy(function () {
                this.clearTimer("hide"), this.hide()
            }, this), this.options.hideDelay || 1)
        }, hide: function (t, e) {
            if (this.clearTimer("show"), this.visible() && !this.is("disabled")) {
                this.is("visible", !1), this.stop();
                var i = this.queues.showhide;
                i.queue($.proxy(function (t) {
                    this.abort(), t()
                }, this)), i.queue($.proxy(function (e) {
                    this._hide(t, e)
                }, this)), i.queue(function (t) {
                    Tooltips.resetZ(), t()
                }), i.queue($.proxy(function (t) {
                    this.clearUpdatedTo(), t()
                }, this)), "function" == $.type(this.options.afterHide) && this.is("updated") && i.queue($.proxy(function (t) {
                    this.options.afterHide(this._content[0], this.element), t()
                }, this)), this.options.cache || !this.options.ajax && !this._fn || i.queue($.proxy(function (t) {
                    this.is("updated", !1), this.is("updating", !1), this.is("sanitized", !1), t()
                }, this)), "function" == $.type(e) && i.queue(function (t) {
                    e(), t()
                }), i.queue($.proxy(function (t) {
                    this.detach(), t()
                }, this))
            }
        }, _hide: function (t, e) {
            e = e || ("function" == $.type(arguments[0]) ? arguments[0] : !1), this.attach(), this._tooltip.fadeTo(t ? 0 : this.options.fadeOut, 0, $.proxy(function () {
                this.stopLoading(), this.is("resize-to-content", !1), this._tooltip.hide(), e && e()
            }, this))
        }, toggle: function () {
            this.is("disabled") || this[this.visible() ? "hide" : "show"]()
        }, raise: function () {
            if (this.is("build") && !this.options.zIndex) {
                var t = Tooltips.getHighestTooltip();
                t && t != this && this.zIndex <= t.zIndex && (this.zIndex = t.zIndex + 1, this._tooltip.css({"z-index": this.zIndex}), this._tooltipShadow && (this._tooltipShadow.css({"z-index": this.zIndex}), this.zIndex = t.zIndex + 2, this._tooltip.css({"z-index": this.zIndex})))
            }
        }
    }), $.extend(Tooltip.prototype, {
        createElementMarker: function () {
            !this.elementMarker && this.content && _.element.isAttached(this.content) && ($(this.content).data("tpd-restore-inline-display", $(this.content).css("display")), this.elementMarker = $("<div>").hide(), $(this.content).before($(this.elementMarker).hide()))
        }, restoreElementToMarker: function () {
            var t;
            this.content, this.elementMarker && this.content && ((t = $(this.content).data("tpd-restore-inline-display")) && $(this.content).css({display: t}), $(this.elementMarker).before(this.content).remove())
        }, startLoading: function () {
            this.is("loading") || (this.build(), this.is("loading", !0), this.options.spinner && (this._tooltip.addClass("tpd-is-loading"), this.skin.startLoading(), this.is("resize-to-content") || (this.position(), this.raise(), this._show())))
        }, stopLoading: function () {
            this.build(), this.is("loading", !1), this.options.spinner && (this._tooltip.removeClass("tpd-is-loading"), this.skin.stopLoading())
        }, abort: function () {
            this.abortAjax(), this.abortSanitize(), this.is("refreshed-before-sanitized", !1)
        }, abortSanitize: function () {
            this._cache.voila && (this._cache.voila.abort(), this._cache.voila = null)
        }, abortAjax: function () {
            this._cache.xhr && (this._cache.xhr.abort(), this._cache.xhr = null, this.is("updated", !1), this.is("updating", !1))
        }, update: function (t) {
            if (!this.is("updating")) {
                this.is("updating", !0), this.build();
                var e = this.options.inline ? "inline" : this.options.ajax ? "ajax" : _.isElement(this.content) || _.isText(this.content) || _.isDocumentFragment(this.content) ? "element" : this._fn ? "function" : "html";
                switch (this._contentWrapper.css({visibility: "hidden"}), e) {
                    case"html":
                    case"element":
                    case"inline":
                        if (this.is("updated"))return void(t && t());
                        this._update(this.content, t);
                        break;
                    case"function":
                        if (this.is("updated"))return void(t && t());
                        var i = this._fn(this.element);
                        if (!i)return this.is("updating", !1), void(t && t(!0));
                        this._update(i, t)
                }
            }
        }, _update: function (t, e) {
            var i = {title: this.options.title, close: this.options.close};
            "string" == $.type(t) || _.isElement(t) || _.isText(t) || _.isDocumentFragment(t) || t instanceof $ ? i.content = t : $.extend(i, t);
            var t = i.content, s = i.title, n = i.close;
            this.content = t, this.title = s, this.close = n, this.createElementMarker(), (_.isElement(t) || t instanceof $) && $(t).show(), this._content.html(this.content), this._title.html(s && "string" == $.type(s) ? s : ""), this._titleWrapper[s ? "show" : "hide"](), this._close[(this.title || this.options.title) && n ? "show" : "hide"]();
            var o = n && !(this.options.title || this.title), a = n && !(this.options.title || this.title) && "overlap" != n, r = n && (this.options.title || this.title) && "overlap" != n;
            this._inner_close[o ? "show" : "hide"](), this._tooltip[(a ? "add" : "remove") + "Class"]("tpd-has-inner-close"), this._tooltip[(r ? "add" : "remove") + "Class"]("tpd-has-title-close"), this._content[(this.options.padding ? "remove" : "add") + "Class"]("tpd-content-no-padding"), this.finishUpdate(e)
        }, sanitize: function (t) {
            return !this.options.voila || this._content.find("img").length < 1 ? (this.is("sanitized", !0), void(t && t())) : void(this._cache.voila = Voila(this._content, {method: "onload"}, $.proxy(function (e) {
                this._markImagesAsSanitized(e.images), this.is("refreshed-before-sanitized") ? (this.is("refreshed-before-sanitized", !1), this.sanitize(t)) : (this.is("sanitized", !0), t && t())
            }, this)))
        }, _markImagesAsSanitized: function (t) {
            $.each(t, function (t, e) {
                var i = e.img;
                $(i).data("completed-src", e.img.src)
            })
        }, _hasAllImagesSanitized: function () {
            var t = !0;
            return this._content.find("img").each(function (e, i) {
                var s = $(i).data("completed-src");
                return s && i.src == s ? void 0 : (t = !1, !1)
            }), t
        }, refresh: function () {
            if (this.visible()) {
                if (!this.is("sanitized"))return void this.is("refreshed-before-sanitized", !0);
                this.is("refreshing", !0), this.clearTimer("refresh-spinner"), !this.options.voila || this._content.find("img").length < 1 || this._hasAllImagesSanitized() ? (this.is("should-update-dimensions", !0), this.position(), this.is("refreshing", !1)) : (this.is("sanitized", !1), this._contentWrapper.css({visibility: "hidden"}), this.startLoading(), this.sanitize($.proxy(function () {
                    this._contentWrapper.css({visibility: "visible"}), this.stopLoading(), this.is("should-update-dimensions", !0), this.position(), this.is("refreshing", !1)
                }, this)))
            }
        }, finishUpdate: function (t) {
            if (this.is("updated", !0), this.is("updating", !1), "function" == $.type(this.options.afterUpdate)) {
                var e = this._contentWrapper.css("visibility");
                e && this._contentWrapper.css({visibility: "visible"}), this.options.afterUpdate(this._content[0], this.element), e && this._contentWrapper.css({visibility: "hidden"})
            }
            t && t()
        }
    }), $.extend(Tooltip.prototype, {
        clearUpdatedTo: function () {
            this._cache.updatedTo = {}
        }, updateDimensionsToContent: function (t, e) {
            this.skin.build();
            var i = this.is("loading"), s = this._cache.updatedTo;
            if ((this._maxWidthPass || this.is("api") || this.is("should-update-dimensions") || s.stemPosition != e || s.loading != i) && (!i || !this.is("resize-to-content"))) {
                this._cache.updatedTo = {
                    type: this.is("resize-to-content") ? "content" : "spinner",
                    loading: this.is("loading"),
                    stemPosition: e
                }, this.is("should-update-dimensions") && this.is("should-update-dimensions", !1);
                var t = t || this.options.position.target, e = e || this.options.position.tooltip, n = Position.getSide(e), o = Position.getOrientation(e), a = this.skin._css.border;
                this._tooltip.addClass("tpd-tooltip-measuring");
                var r = this._tooltip.attr("style");
                this._tooltip.removeAttr("style");
                var h = {top: a, right: a, bottom: a, left: a}, l = 0;
                if ("vertical" == Position.getOrientation(e)) {
                    this.options.stem && (h[n] = this.skin["stem_" + n].getMath().dimensions.outside.height);
                    var c = this.getMouseRoom();
                    c[Position._flip[n]] && (h[n] += c[Position._flip[n]]);
                    var d = this.getContainmentLayout(e), p = this.getPaddingLine(t), u = !1;
                    Position.isPointWithinBoxLayout(p.x1, p.y1, d) || Position.isPointWithinBoxLayout(p.x2, p.y2, d) ? u = !0 : $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                        var i = this.getSideLine(d, e);
                        return Position.intersectsLine(p.x1, p.y1, p.x2, p.y2, i.x1, i.y1, i.x2, i.y2) ? (u = !0, !1) : void 0
                    }, this)), u && (l = "left" == n ? p.x1 - d.position.left : d.position.left + d.dimensions.width - p.x1, h[n] += l)
                }
                if (this.options.offset && "vertical" == o) {
                    var f = Position.adjustOffsetBasedOnPosition(this.options.offset, this.options.position.target, t);
                    0 !== f.x && (h.right += Math.abs(f.x))
                }
                var l;
                this.options.containment && (l = this.options.containment.padding) && ($.each(h, function (t) {
                    h[t] += l
                }), "vertical" == o ? h["left" == n ? "left" : "right"] -= l : h["top" == n ? "top" : "bottom"] -= l);
                var m = Bounds.viewport(), g = this.close && "overlap" != this.close && !this.title, v = {
                    width: 0,
                    height: 0
                };
                g && (v = this._innerCloseDimensions || {
                    width: this._inner_close.outerWidth(!0),
                    height: this._inner_close.outerHeight(!0)
                }, this._innerCloseDimensions = v), this._contentRelativePadder.css({"padding-right": v.width}), this._contentSpacer.css({width: m.width - h.left - h.right});
                var b = {
                    width: this._content.innerWidth() + v.width,
                    height: Math.max(this._content.innerHeight(), v.height || 0)
                }, _ = {width: 0, height: 0};
                if (this.title) {
                    var y = {width: 0, height: 0};
                    this._titleWrapper.add(this._titleSpacer).css({
                        width: "auto",
                        height: "auto"
                    }), this.close && "overlap" != this.close && (y = {
                        width: this._close.outerWidth(!0),
                        height: this._close.outerHeight(!0)
                    }, this._close.hide()), this._maxWidthPass && b.width > this.options.maxWidth && this._titleRelative.css({
                        width: b.width
                    }), this._titleRelativePadder.css({"padding-right": y.width});
                    var w = parseFloat(this._titleWrapper.css("border-bottom-width"));
                    _ = {
                        width: this.title ? this._titleWrapper.innerWidth() : 0,
                        height: Math.max(this.title ? this._titleWrapper.innerHeight() + w : 0, y.height + w)
                    }, _.width > m.width - h.left - h.right && (_.width = m.width - h.left - h.right, this._titleSpacer.css({width: _.width}), _.height = Math.max(this.title ? this._titleWrapper.innerHeight() + w : 0, y.height + w)), b.width = Math.max(_.width, b.width), b.height += _.height, this._titleWrapper.css({height: Math.max(this.title ? this._titleWrapper.innerHeight() : 0, y.height)}), this.close && this._close.show()
                }
                if (this.options.stem) {
                    var x = "vertical" == o ? "height" : "width", C = this.skin["stem_" + n].getMath(), k = C.outside.width + 2 * this.skin._css.radius;
                    b[x] < k && (b[x] = k)
                }
                if (this._contentSpacer.css({width: b.width}), b.height != Math.max(this._content.innerHeight(), v.height) + (this.title ? this._titleRelative.outerHeight() : 0) && b.width++, this.is("resize-to-content") || (b = this.skin._css.spinner.dimensions), this.setDimensions(b), h = {
                        top: a,
                        right: a,
                        bottom: a,
                        left: a
                    }, this.options.stem) {
                    var S = Position.getSide(e);
                    h[S] = this.skin.stem_top.getMath().dimensions.outside.height
                }
                this._contentSpacer.css({
                    "margin-top": h.top,
                    "margin-left": +h.left,
                    width: b.width
                }), (this.title || this.close) && this._titleWrapper.css({
                    height: this._titleWrapper.innerHeight(),
                    width: b.width
                }), this._tooltip.removeClass("tpd-tooltip-measuring"), this._tooltip.attr("style", r);
                var T = this._contentRelative.add(this._titleRelative);
                this.options.maxWidth && b.width > this.options.maxWidth && !this._maxWidthPass && this.is("resize-to-content") && (T.css({width: this.options.maxWidth}), this._maxWidthPass = !0, this.updateDimensionsToContent(t, e), this._maxWidthPass = !1, T.css({width: "auto"}))
            }
        }, setDimensions: function (t) {
            this.skin.setDimensions(t)
        }, getContainmentSpace: function (t, e) {
            var i = this.getContainmentLayout(t, e), s = this.getTargetLayout(), n = s.position, o = s.dimensions, a = i.position, r = i.dimensions, h = {
                top: Math.max(n.top - a.top, 0),
                bottom: Math.max(a.top + r.height - (n.top + o.height), 0),
                left: Math.max(n.left - a.left, 0),
                right: Math.max(a.left + r.width - (n.left + o.width), 0)
            };
            return n.top > a.top + r.height && (h.top -= n.top - (a.top + r.height)), n.top + o.height < a.top && (h.bottom -= a.top - (n.top + o.height)), n.left > a.left + r.width && a.left + r.width >= n.left && (h.left -= n.left - (a.left + r.width)), n.left + o.width < a.left && (h.right -= a.left - (n.left + o.width)), this._cache.layouts.containmentSpace = h, h
        }, position: function () {
            if (this.visible()) {
                this.is("positioning", !0), this._cache.layouts = {}, this._cache.dimensions;
                var t = this.options.position.target, e = this.options.position.tooltip, i = e, s = t;
                this.updateDimensionsToContent(s, i);
                var n = this.getPositionBasedOnTarget(s, i), o = deepExtend(n), a = [];
                if (this.options.containment) {
                    var r = !1, h = {};
                    if ($.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                            (h[e] = this.isSideWithinContainment(e, i, !0)) && (r = !0)
                        }, this)), r || (o.contained = !0), o.contained)this.setPosition(o); else {
                        a.unshift({position: o, targetPosition: s, stemPosition: i});
                        var l = Position.flip(t);
                        if (s = l, i = Position.flip(e), h[Position.getSide(s)] ? (this.updateDimensionsToContent(s, i), o = this.getPositionBasedOnTarget(s, i)) : o.contained = !1, o.contained)this.setPosition(o, i); else {
                            a.unshift({position: o, targetPosition: s, stemPosition: i});
                            var c, d = t, p = this.getContainmentSpace(i, !0), u = "horizontal" == Position.getOrientation(d) ? ["left", "right"] : ["top", "bottom"];
                            c = p[u[0]] === p[u[1]] ? "horizontal" == Position.getOrientation(d) ? "left" : "top" : u[p[u[0]] > p[u[1]] ? 0 : 1];
                            var f = Position.split(d)[1], m = c + f, g = Position.flip(m);
                            if (s = m, i = g, h[Position.getSide(s)] ? (this.updateDimensionsToContent(s, i), o = this.getPositionBasedOnTarget(s, i)) : o.contained = !1, o.contained)this.setPosition(o, i); else {
                                a.unshift({position: o, targetPosition: s, stemPosition: i});
                                var v, b = [];
                                if ($.each(a, function (t, e) {
                                        if (e.position.top >= 0 && e.position.left >= 0)v = e; else {
                                            var i = e.position.top >= 0 ? 1 : Math.abs(e.position.top), s = e.position.left >= 0 ? 1 : Math.abs(e.position.left);
                                            b.push({result: e, negativity: i * s})
                                        }
                                    }), !v) {
                                    var _ = b[b.length - 1];
                                    $.each(b, function (t, e) {
                                        e.negativity < _.negativity && (_ = e)
                                    }), v = _.result
                                }
                                this.updateDimensionsToContent(v.targetPosition, v.stemPosition, !0), this.setPosition(v.position, v.stemPosition)
                            }
                        }
                    }
                } else this.setPosition(o);
                this._cache.dimensions = this.skin._vars.dimensions, this.skin.paint(), this.is("positioning", !1)
            }
        }, getPositionBasedOnTarget: function (t, e) {
            var e = e || this.options.position.tooltip, i = this.getTargetDimensions(), s = {
                left: 0,
                top: 0
            }, n = {left: 0, top: 0};
            Position.getSide(t);
            var o = this.skin._vars, a = o.frames[Position.getSide(e)], r = Position.getOrientation(t), h = Position.split(t);
            if ("horizontal" == r) {
                var l = Math.floor(.5 * i.width);
                switch (h[2]) {
                    case"left":
                        n.left = l;
                        break;
                    case"middle":
                        s.left = i.width - l, n.left = s.left;
                        break;
                    case"right":
                        s.left = i.width, n.left = i.width - l
                }
                "bottom" == h[1] && (s.top = i.height, n.top = i.height)
            } else {
                var l = Math.floor(.5 * i.height);
                switch (h[2]) {
                    case"top":
                        n.top = l;
                        break;
                    case"middle":
                        s.top = i.height - l, n.top = s.top;
                        break;
                    case"bottom":
                        n.top = i.height - l, s.top = i.height
                }
                "right" == h[1] && (s.left = i.width, n.left = i.width)
            }
            var c = this.getTargetPosition(), d = $.extend({}, i, {
                top: c.top,
                left: c.left,
                connection: s,
                max: n
            }), p = {
                width: a.dimensions.width,
                height: a.dimensions.height,
                top: 0,
                left: 0,
                connection: o.connections[e].connection,
                stem: o.connections[e].stem
            };
            if (p.top = d.top + d.connection.top, p.left = d.left + d.connection.left, p.top -= p.connection.top, p.left -= p.connection.left, this.options.stem) {
                var u = o.stemDimensions.width, f = {
                    stem: {
                        top: p.top + p.stem.connection.top,
                        left: p.left + p.stem.connection.left
                    },
                    connection: {top: d.top + d.connection.top, left: d.left + d.connection.left},
                    max: {top: d.top + d.max.top, left: d.left + d.max.left}
                };
                if (!Position.isPointWithinBox(f.stem.left, f.stem.top, f.connection.left, f.connection.top, f.max.left, f.max.top)) {
                    var f = {
                        stem: {top: p.top + p.stem.connection.top, left: p.left + p.stem.connection.left},
                        connection: {top: d.top + d.connection.top, left: d.left + d.connection.left},
                        max: {top: d.top + d.max.top, left: d.left + d.max.left}
                    }, m = {
                        connection: Position.getDistance(f.stem.left, f.stem.top, f.connection.left, f.connection.top),
                        max: Position.getDistance(f.stem.left, f.stem.top, f.max.left, f.max.top)
                    }, g = Math.min(m.connection, m.max), v = f[m.connection <= m.max ? "connection" : "max"], b = "horizontal" == Position.getOrientation(e) ? "left" : "top", _ = Position.getDistance(f.connection.left, f.connection.top, f.max.left, f.max.top);
                    if (_ >= u) {
                        var y = {top: 0, left: 0}, w = v[b] < f.stem[b] ? -1 : 1;
                        y[b] = g * w, y[b] += Math.floor(.5 * u) * w, p.left += y.left, p.top += y.top
                    } else {
                        $.extend(f, {
                            center: {
                                top: Math.round(d.top + .5 * i.height),
                                left: Math.round(d.left + .5 * i.left)
                            }
                        });
                        var x = {
                            connection: Position.getDistance(f.center.left, f.center.top, f.connection.left, f.connection.top),
                            max: Position.getDistance(f.center.left, f.center.top, f.max.left, f.max.top)
                        }, g = m[x.connection <= x.max ? "connection" : "max"], C = {
                            top: 0,
                            left: 0
                        }, w = v[b] < f.stem[b] ? -1 : 1;
                        C[b] = g * w, p.left += C.left, p.top += C.top
                    }
                }
            }
            if (this.options.offset) {
                var k = $.extend({}, this.options.offset);
                k = Position.adjustOffsetBasedOnPosition(k, this.options.position.target, t), p.top += k.y, p.left += k.x
            }
            var S = this.getContainment({top: p.top, left: p.left}, e), T = S.horizontal && S.vertical, D = {
                x: 0,
                y: 0
            }, P = Position.getOrientation(e);
            if (!S[P]) {
                var I = "horizontal" == P, M = I ? ["left", "right"] : ["up", "down"], O = I ? "x" : "y", E = I ? "left" : "top", A = S.correction[O], R = this.getContainmentLayout(e), F = R.position[I ? "left" : "top"];
                if (0 !== A) {
                    var j = o.connections[e].move, z = j[M[0 > -1 * A ? 0 : 1]], L = 0 > A ? -1 : 1;
                    if (z >= A * L && p[E] + A >= F)p[E] += A, D[O] = -1 * A, T = !0; else if (Position.getOrientation(t) == Position.getOrientation(e)) {
                        if (p[E] += z * L, D[O] = -1 * z * L, p[E] < F) {
                            var B = F - p[E], W = j[M[0]] + j[M[1]], B = Math.min(B, W);
                            p[E] += B;
                            var N = D[O] - B;
                            N >= o.connections[e].move[M[0]] && N <= o.connections[e].move[M[1]] && (D[O] -= B)
                        }
                        S = this.getContainment({top: p.top, left: p.left}, e);
                        var H = S.correction[O], U = deepExtend({}, p);
                        this.options.offset && (U.left -= this.options.offset.x, U.top -= this.options.offset.y);
                        var f = {stem: {top: U.top + p.stem.connection.top, left: U.left + p.stem.connection.left}};
                        f.stem[E] += D[O];
                        var q = this.getTargetLayout(), u = o.stemDimensions.width, V = Math.floor(.5 * u), Z = F + R.dimensions[I ? "width" : "height"];
                        if ("x" == O) {
                            var G = q.position.left + V;
                            H > 0 && (G += q.dimensions.width - 2 * V), (0 > H && f.stem.left + H >= G && U.left + H >= F || H > 0 && f.stem.left + H <= G && U.left + H <= Z) && (U.left += H)
                        } else {
                            var Y = q.position.top + V;
                            H > 0 && (Y += q.dimensions.height - 2 * V), (0 > H && f.stem.top + H >= Y && U.top + H >= F || H > 0 && f.stem.top + H <= Y && U.top + H <= Z) && (U.top += H)
                        }
                        p = U, this.options.offset && (p.left += this.options.offset.x, p.top += this.options.offset.y)
                    }
                }
                S = this.getContainment({top: p.top, left: p.left}, e), T = S.horizontal && S.vertical
            }
            return {top: p.top, left: p.left, contained: T, shift: D}
        }, setPosition: function (t, e) {
            var i = this._position;
            if (!i || i.top != t.top || i.left != t.left) {
                var s;
                if (this.options.container != document.body) {
                    if ("string" == $.type(this.options.container)) {
                        var n = this.target;
                        "mouse" == n && (n = this.element), s = $($(n).closest(this.options.container).first())
                    } else s = $(s);
                    if (s[0]) {
                        var o = $(s).offset(), a = {
                            top: Math.round(o.top),
                            left: Math.round(o.left)
                        }, r = {top: Math.round($(s).scrollTop()), left: Math.round($(s).scrollLeft())};
                        t.top -= a.top, t.top += r.top, t.left -= a.left, t.left += r.left
                    }
                }
                this._position = t, this._tooltip.css({top: t.top, left: t.left})
            }
            this.skin.setStemPosition(e || this.options.position.tooltip, t.shift || {x: 0, y: 0})
        }, getSideLine: function (t, e) {
            var i = t.position.left, s = t.position.top, n = t.position.left, o = t.position.top;
            switch (e) {
                case"top":
                    n += t.dimensions.width;
                    break;
                case"bottom":
                    s += t.dimensions.height, n += t.dimensions.width, o += t.dimensions.height;
                    break;
                case"left":
                    o += t.dimensions.height;
                    break;
                case"right":
                    i += t.dimensions.width, n += t.dimensions.width, o += t.dimensions.height
            }
            return {x1: i, y1: s, x2: n, y2: o}
        }, isSideWithinContainment: function (t, e, i) {
            var s = this.getContainmentLayout(e, i), n = this.getTargetLayout(), o = this.getSideLine(n, t);
            if (Position.isPointWithinBoxLayout(o.x1, o.y1, s) || Position.isPointWithinBoxLayout(o.x2, o.y2, s))return !0;
            var a = !1;
            return $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                var i = this.getSideLine(s, e);
                return Position.intersectsLine(o.x1, o.y1, o.x2, o.y2, i.x1, i.y1, i.x2, i.y2) ? (a = !0, !1) : void 0
            }, this)), a
        }, getContainment: function (t, e) {
            var i = {horizontal: !0, vertical: !0, correction: {y: 0, x: 0}};
            if (this.options.containment) {
                var s = this.getContainmentLayout(e), n = this.skin._vars.frames[Position.getSide(e)].dimensions;
                this.options.containment && ((t.left < s.position.left || t.left + n.width > s.position.left + s.dimensions.width) && (i.horizontal = !1, i.correction.x = t.left < s.position.left ? s.position.left - t.left : s.position.left + s.dimensions.width - (t.left + n.width)), (t.top < s.position.top || t.top + n.height > s.position.top + s.dimensions.height) && (i.vertical = !1, i.correction.y = t.top < s.position.top ? s.position.top - t.top : s.position.top + s.dimensions.height - (t.top + n.height)))
            }
            return i
        }, getContainmentLayout: function (t, e) {
            var i = {top: $(window).scrollTop(), left: $(window).scrollLeft()}, s = this.target;
            "mouse" == s && (s = this.element);
            var n, o = $(s).closest(this.options.containment.selector).first()[0];
            n = o && "viewport" != this.options.containment.selector ? {
                dimensions: {
                    width: $(o).innerWidth(),
                    height: $(o).innerHeight()
                }, position: $(o).offset()
            } : {dimensions: Bounds.viewport(), position: i};
            var a = this.options.containment.padding;
            if (a && !e) {
                var r = Math.max(n.dimensions.height, n.dimensions.width);
                if (2 * a > r && (a = Math.max(Math.floor(.5 * r), 0)), a) {
                    n.dimensions.width -= 2 * a, n.dimensions.height -= 2 * a, n.position.top += a, n.position.left += a;
                    var h = Position.getOrientation(t);
                    "vertical" == h ? (n.dimensions.width += a, "left" == Position.getSide(t) && (n.position.left -= a)) : (n.dimensions.height += a, "top" == Position.getSide(t) && (n.position.top -= a))
                }
            }
            return this._cache.layouts.containmentLayout = n, n
        }, getMouseRoom: function () {
            var t = {top: 0, left: 0, right: 0, bottom: 0};
            if ("mouse" == this.options.target && !this.is("api")) {
                var e = Mouse.getActualPosition(this._cache.event), i = $(this.element).offset(), s = {
                    width: $(this.element).innerWidth(),
                    height: $(this.element).innerHeight()
                };
                t = {
                    top: Math.max(0, e.top - i.top),
                    bottom: Math.max(0, i.top + s.height - e.top),
                    left: Math.max(0, e.left - i.left),
                    right: Math.max(0, i.left + s.width - e.left)
                }
            }
            return t
        }, getTargetPosition: function () {
            var t;
            if ("mouse" == this.options.target)if (this.is("api")) {
                var e = $(this.element).offset();
                t = {top: Math.round(e.top), left: Math.round(e.left)}
            } else t = Mouse.getPosition(this._cache.event); else {
                var e = $(this.target).offset();
                t = {top: Math.round(e.top), left: Math.round(e.left)}
            }
            return this._cache.layouts.targetPosition = t, t
        }, getTargetDimensions: function () {
            if (this._cache.layouts.targetDimensions)return this._cache.layouts.targetDimensions;
            var t;
            return t = "mouse" == this.options.target ? Mouse.getDimensions() : {
                width: $(this.target).innerWidth(),
                height: $(this.target).innerHeight()
            }, this._cache.layouts.targetDimensions = t, t
        }, getTargetLayout: function () {
            if (this._cache.layouts.targetLayout)return this._cache.layouts.targetLayout;
            var t = {position: this.getTargetPosition(), dimensions: this.getTargetDimensions()};
            return this._cache.layouts.targetLayout = t, t
        }, getPaddingLine: function (t) {
            var e = this.getTargetLayout(), i = "left";
            if ("vertical" == Position.getOrientation(t))return this.getSideLine(e, Position.getSide(t));
            if (Position.isCorner(t)) {
                var s = Position.inverseCornerPlane(t);
                return i = Position.getSide(s), this.getSideLine(e, i)
            }
            var n = this.getSideLine(e, i), o = Math.round(.5 * e.dimensions.width);
            return n.x1 += o, n.x2 += o, n
        }
    }), $.extend(Tooltip.prototype, {
        setActive: function () {
            this.is("active", !0), this.visible() && this.raise(), this.options.hideAfter && this.clearTimer("idle")
        }, setIdle: function () {
            this.is("active", !1), this.options.hideAfter && this.setTimer("idle", $.proxy(function () {
                this.clearTimer("idle"), this.is("active") || this.hide()
            }, this), this.options.hideAfter)
        }
    }), $.extend(Tooltip.prototype, {
        bind: function (t, e, i, s) {
            e = e;
            var n = $.proxy(i, s || this);
            this._cache.events.push({element: t, eventName: e, handler: n}), $(t).bind(e, n)
        }, unbind: function () {
            $.each(this._cache.events, function (t, e) {
                $(e.element).unbind(e.eventName, e.handler)
            }), this._cache.events = []
        }
    }), $.extend(Tooltip.prototype, {
        disable: function () {
            this.is("disabled") || this.is("disabled", !0)
        }, enable: function () {
            this.is("disabled") && this.is("disabled", !1)
        }
    }), $.extend(Tooltip.prototype, {
        is: function (t, e) {
            return "boolean" == $.type(e) && (this._cache.is[t] = e), this._cache.is[t]
        }, visible: function () {
            return this.is("visible")
        }
    }), $.extend(Tooltip.prototype, {
        setTimer: function (t, e, i) {
            this._cache.timers[t] = _.delay(e, i)
        }, getTimer: function (t) {
            return this._cache.timers[t]
        }, clearTimer: function (t) {
            this._cache.timers[t] && (clearTimeout(this._cache.timers[t]), delete this._cache.timers[t])
        }, clearTimers: function () {
            $.each(this._cache.timers, function (t, e) {
                clearTimeout(e)
            }), this._cache.timers = {}
        }
    }), $.extend(Tipped, {
        init: function () {
            Tooltips.init()
        }, create: function (t, e, i) {
            return Collection.create(t, e, i), this.get(t)
        }, get: function (t) {
            return new Collection(t)
        }, findElement: function (t) {
            return Tooltips.findElement(t)
        }, refresh: function (t, e, i) {
            return Tooltips.refresh(t, e, i), this
        }, setStartingZIndex: function (t) {
            return Tooltips.setStartingZIndex(t), this
        }
    }), $.each("remove".split(" "), function (t, e) {
        Tipped[e] = function (t) {
            return this.get(t)[e](), this
        }
    }), $.extend(Collection, {
        create: function (t, e) {
            if (t) {
                "object" == $.type(t) && (t = $(t)[0]);
                var i = arguments[2] || {};
                return _.isElement(t) ? new Tooltip(t, e, i) : $(t).each(function (t, s) {
                    new Tooltip(s, e, i)
                }), this
            }
        }
    }), $.extend(Collection.prototype, {
        items: function () {
            return Tooltips.get(this.element, {api: !0})
        }, refresh: function (t) {
            return Tooltips.refresh(this.element, t), this
        }, remove: function () {
            return Tooltips.remove(this.element), this
        }
    }), $.each("".split(" "), function (t, e) {
        Collection.prototype[e] = function () {
            return $.each(this.items(), function (t, i) {
                i[e]()
            }), this
        }
    }), Tipped.init(), Tipped
}), function (t) {
    var e = function (e, i) {
        var s = {afterMatch: t.noop, beforeMatch: t.noop, source: {type: null, data: {}}};
        if (this.options = t.extend(s, i), this.matchNoHighlightClass = "nide", this.matchHighlightClass = "highlight", this.query, this.source = s.source, this.element = t(e), this.matches = {}, this.element.attr("data-target")) {
            var n = t(this.element.attr("data-target"));
            this.source.type = "html";
            var o = this;
            t.each(n, function (e) {
                o.source.data[e] = {
                    node: this,
                    text: o.element.attr("data-target-child") ? t(o.element.attr("data-target-child"), this).text() : t(this).text(),
                    highlight: !1
                }
            })
        } else this.options.source.url && (this.source = t.extend({type: "ajax"}, this.options.source));
        this.element.on("keyup change", this.handler)
    };
    e.prototype.setQuery = function (t) {
        this.query = t
    }, e.prototype.getQuery = function () {
        return this.query
    }, e.prototype.setData = function (t) {
        this.source.data = t
    }, e.prototype.getData = function () {
        return this.source.data
    }, e.prototype.getMatches = function () {
        return this.matches
    }, e.prototype.handler = function (e) {
        t(this).choosen("setQuery", this.value);
        var i = t(this).data("choosen");
        if (i.options.beforeMatch.call(this, [e]), "html" == i.source.type)var s = i.matchHtml(); else if ("ajax" == i.source.type)var s = i.matchAjax();
        i.options.afterMatch.call(this, s)
    }, e.prototype.matchHtml = function () {
        var e = new RegExp(this.getQuery(), "i"), i = this;
        t.each(this.source.data, function (t) {
            return i.query.length <= 0 ? void(this.node.style.display = "") : void(e.test(this.text) ? (i.matches[t] = this, this.highlight = !0) : this.node.style.display = "none")
        })
    }, e.prototype.matchAjax = function () {
        var e = t.extend({url: this.source.url, context: this, data: {query: this.query}}, this.source.ajaxSetup);
        t.ajax(e)
    }, t.fn.choosen = function (i, s) {
        if ("string" == typeof i) {
            var n = t(this).data("choosen");
            return n || (n = new e(this), t(this).data("choosen", n)), n[i](s)
        }
        return this.each(function () {
            if (!t(this).data("choosen")) {
                var n = "object" == typeof i ? new e(this, i) : new e(this, s);
                return t(this).data("choosen", n), n
            }
        })
    }
}(jQuery), function (t) {
    t.fn.sexySlider = function (e) {
        var i = {visibleItems: 1, animationDuration: 1e3, slideSelector: ".slide", text: {next: "Next", prev: "Prev"}};
        return this.each(function () {
            var s = t(this);
            if ("object" != typeof s.data("sexy-slider")) {
                var n = {
                    visibleItems: s.data("visible-items"),
                    animationDuration: s.data("animation-duration"),
                    slideSelector: s.data("slide-selector"),
                    text: {next: s.data("text-next"), prev: s.data("text-prev")}
                }, o = t.extend(!0, {}, i, e, n);
                o.__isAnimate = !1, s.css("position", "relative").data("sexy-slider", o).addClass("js-sexySlider");
                var a = t('<div class="b-sexySlider-viewport"></div>');
                a.css({overflow: "hidden", position: "relative"}), s.wrap('<div class="b-sexySlider"></div>').wrap(a);
                var r = s.parents(".b-sexySlider-viewport").width() / o.visibleItems, h = 0;
                s.children(o.slideSelector).each(function () {
                    var e = r - (parseInt(t(this).css("marginLeft")) + parseInt(t(this).css("marginRight")));
                    t(this).css("width", e), h += t(this).outerWidth(!0) + 1
                }), s.css("width", h), s.children(o.slideSelector).size() > o.visibleItems && (s.parent().after('<div class="b-sexySlider-controls"><a class="b-sexySlider-control-item prev" href="javascript:;">' + o.text.prev + '</a><a class="b-sexySlider-control-item next" href="javascript:;">' + o.text.next + "</a></div>"), t(document).on("click", ".b-sexySlider .b-sexySlider-control-item", function () {
                    var e = t(this).parents(".b-sexySlider").find(".js-sexySlider"), i = e.data("sexy-slider");
                    if (console.log(e), 1 != i.__isAnimate)if (i.__isAnimate = !0, setTimeout(t.proxy(function () {
                            this.__isAnimate = !1
                        }, i), i.animationDuration), t(this).hasClass("prev")) {
                        var s = e.children(i.slideSelector + ":last-child");
                        e.css({left: -s.outerWidth(!0)}).animate({left: 0}, i.animationDuration).prepend(s.detach())
                    } else if (t(this).hasClass("next")) {
                        var s = e.children(i.slideSelector + ":first-child");
                        e.animate({left: -s.next().outerWidth(!0)}, i.animationDuration, function () {
                            e.append(s.detach()).css({left: 0})
                        })
                    }
                }))
            }
        })
    }, t(function () {
        t("[data-sexyslider]").sexySlider()
    })
}(jQuery), function (t, e, i) {
    "use strict";
    var s = function (t) {
        i(t).on("click", this.close)
    };
    s.dataSelector = "[data-closer]", s.TRANSITION_DURATION = 1500, s.prototype.close = function (t) {
        var e = i(this), s = e.attr("data-target") || e.attr("data-closer");
        s || (s = e.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i(this).parents(s) || i(s);
        t && t.preventDefault(), n.length && (n.trigger(t = i.Event("close.workly.closer")), t.isDefaultPrevented() || (n.removeClass("in"), n.fadeOut(this.TRANSITION_DURATION, function () {
            i(this).detach().trigger("closed.workly.closer").remove()
        })))
    }, i.fn.closer = function (t) {
        return this.each(function () {
            var e = i(this), n = e.data("workly.closer");
            n || e.data("workly.closer", n = new s(this)), "string" == typeof t && n[t].call(e)
        })
    }, i.fn.closer.Constructor = s, i(document).on("click", s.dataSelector, s.prototype.close)
}(window, document, jQuery), function (t, e, i) {
    "use strict";
    var s = [];
    (s = i("[data-maps]")) && s.length > 0 && s.each(function (t, e) {
        var s = i(e).attr("data-maps-coordinates").split(";");
        if (console.log(s), "object" == typeof s && null !== s && s.length >= 1) {
            var n = s[0].split(","), o = (s[1] || s[0]).split(",");
            if (this.id = this.id || "mapID_" + Math.random(), "yandex" == i(e).data("maps") && "ymaps"in window)ymaps.ready(function () {
                var t = new ymaps.Map(e.id, {
                    center: [n[0], n[1]],
                    zoom: i(e).attr("data-maps-zoom") || 14,
                    controls: [],
                    behaviors: ["drag", "scrollZoom", "multiTouch"]
                });
                t.geoObjects.add(new ymaps.Placemark([o[0], o[1]], {hintContent: i(e).attr("data-maps-marker-title") || "?? ???"}, {preset: "islands#redDotIcon"}))
            }); else if ("google" == i(this).data("maps") && "google"in window) {
                var a = {
                    zoom: parseInt(i(this).attr("data-maps-zoom")) || 14,
                    center: new google.maps.LatLng(n[0], n[1]),
                    mapTypeControl: !1,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: !0,
                    zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
                    disableDoubleClickZoom: !0,
                    streetViewControl: !1
                };
                console.log(a);
                var e = new google.maps.Map(this, a), r = new google.maps.Marker({title: i(this).attr("data-maps-marker-title") || "?? ???"});
                r.setMap(e), r.setPosition(new google.maps.LatLng(o[0], o[1])), r.setVisible(!0)
            }
        }
    });
    var n = [];
    (n = i(".js-table")) && n.length > 0 && n.wrap('<div class="js-generating-table-wrapper"><div class="js-generating-table-outer"></div></div>').parent(".js-generating-table-outer").css({
        overflow: "auto",
        position: "relative",
        display: "block"
    }).parent(".js-generating-table-wrapper").css({
        height: 0,
        overflow: "hidden",
        position: "relative",
        display: "block"
    }), i.extend({
        getRandom: function (t) {
            return parseInt(Math.random().toString().substr(2, t || 5))
        }
    })
}(window, document, jQuery), function (t) {
    t(document).on("click", "[data-clearInput]", function (e) {
        var i = t(this).attr("data-clearInput") || !1;
        i && (console.log(i), t(i).val("").trigger("change"))
    })
}(jQuery), function (t, e) {
    e(t).on("click.cloning", ".js-cloning-caller", function (t) {
        t.preventDefault();
        var i = {};
        i.root = e(this).parents(".js-cloning-root") || e("body"), i.container = i.root.find(".js-cloning-container"), i.copy = i.root.find(".js-cloning-sample").clone(), i.original = i.root.find(".js-cloning-sample").clone(!0), i.counter = e(this).data("cloning-copies_count") || 0;
        var s = e(this).data("cloning-limit") || 0, n = {
            onlimit: e(this).data("cloning-onlimit") || e.noop,
            before: e(this).data("cloning-before") || e.noop,
            after: e(this).data("cloning-after") || e.noop
        };
        return s > 0 && s <= i.counter ? void("function" == typeof window[n.onlimit] && window[n.onlimit](t, e(this), i)) : ("function" == typeof window[n.before] && window[n.before](t, e(this), i), void(i.hasOwnProperty("copy") && "object" == typeof i.copy && null !== i.copy && i.copy.length > 0 && (i.copy.removeClass("js-cloning-sample").addClass("js-cloning-clone"), i.copy = i.copy.appendTo(i.container), i.counter += 1, e(this).data("cloning-copies_count", i.counter), "function" == typeof window[n.after] && window[n.after](t, e(this), i))))
    })
}(document, jQuery), function (t) {
    var e = "groupCheckBox", i = function () {
        console.log(t.getRandom() + " " + e + ": toggler - onchange");
        var s = t(this), n = s.data(e);
        n || (n = {childs: t('.js-groupCheckBox-child[data-groupCheckBoxParent="' + this.id + '"]')}, s.data(e, n));
        var o = this;
        n.childs.filter(":checked").length != n.childs.length && (this.checked = 1, s.removeClass("checked-partial")), n.childs.each(function () {
            this.checked = o.checked, t(this).hasClass("js-groupCheckBox-toggler") && i.call(this)
        })
    }, s = function () {
        console.log(t.getRandom() + " " + e + ": child - onchange");
        var i = t(this).data(e + "Parent");
        if (i || (i = document.getElementById(t(this).attr("data-groupCheckBoxParent")), t(this).data(e + "Parent", i)), i) {
            var n = t(i).data(e);
            n || (n = {childs: t(".js-groupCheckBox-child[data-groupCheckBoxParent=" + i.id + "]")}, t(i).data(e, n)), n.childs.filter(":checked").size() > 0 ? (i.checked = !0, n.childs.size() != n.childs.filter(":checked").size() ? t(i).addClass("checked-partial") : t(i).removeClass("checked-partial")) : i.checked = !1, t(i).hasClass("js-groupCheckBox-child") && s.call(i)
        }
    };
    t(document).on("change", ".js-" + e + " .js-groupCheckBox-toggler", i).on("change", ".js-" + e + " .js-groupCheckBox-child", s)
}(jQuery), function (t) {
    t(document).on("focus", ".js-select", function () {
        var e = t(this).data("select2");
        e && setTimeout(function () {
            e.isOpen() || e.open()
        }, 0)
    })
}(jQuery), function (t) {
    var e = ".js-selectSideBySide";
    t(document).on("click", ".js-control", function (i) {
        var s = t(this);
        if (console.log("first"), s.hasClass("disabled"))return !1;
        if (s.hasClass("js-control__from_right"))var n = s.parents(e).find("[data-side=right] :checked"); else if (s.hasClass("js-control__from_left"))var n = s.parents(e).find("[data-side=left] :checked");
        return s.data("checkedData", n), s.data("ajaxHref") && t.ajax(s.data("ajaxHref"), {
            type: s.data("ajaxType") || "GET",
            data: n,
            context: this,
            success: function (i) {
                t(this).data("ajaxResultTarget") && t(this).parents(e).find(t(this).data("ajaxResultTarget")).html(i), selectSideBySide_restartControls(e)
            },
            beforeSend: function () {
                console.log("test"), t(this).hasClass("js-control__from_right") && t(this).parents(e).find("[data-side=right] :checked").parents("li").remove()
            }
        }), !1
    }).on("change", e + " [data-side] :checkbox", function () {
        var i = "add", s = t(this).parents("[data-side]"), n = s.find(":checkbox"), o = n.filter(":checked");
        n.not(":checked").parents("li").removeClass("b-menu-item__active"), o.length > 0 && (i = "remove", o.parents("li:not(.b-menu-item__active)").addClass("b-menu-item__active")), t(this).parents(e).find(".js-control.js-control__from_" + s.data("side"))[i + "Class"]("disabled")
    }), t.extend(window, {
        selectSideBySide_restartControls: function (e) {
            t(e).each(function () {
                var e = t(this);
                t("[data-side]", this).each(function () {
                    var i = t(this).find(".side-body :checked");
                    i.length > 0 && (i.parents("li:not(.b-menu-item__active)").addClass("b-menu-item__active"), e.find(".js-control.js-control__from_" + t(this).data("side")).removeClass("disabled"))
                })
            })
        }
    }), selectSideBySide_restartControls(e)
}(jQuery), function (t) {
    var e = t("#js-slidebar");
    t(document).on("click", ".js-slidebar-toggler", function (i) {
        e.animate({width: "toggle"}, 450, function () {
            t(this).toggleClass("js-opened")
        })
    })
}(jQuery), function (t) {
    t(document).on("click", ".js-table-collapseToggler", function (e) {
        switch (t(this).attr("aria-expanded")) {
            case"true":
                t(this).parents("tr").addClass("active");
                break;
            case"false":
                t(this).parents("tr").removeClass("active")
        }
    })
}(jQuery), function (t) {
    t(document).on("loaded.tables", ".js-table", function (e) {
        var i = t(this).parent(".js-generating-table-outer").height();
        t(this).parent().parent(".js-generating-table-wrapper").animate({height: i}, 1e3, function () {
            t(this).removeAttr("style")
        }), t(this).has("[data-TableNormalizeColumns]") && t("tr[data-dependentRow]", this).each(function () {
            var e = t(t(this).attr("data-dependentRow")), i = null;
            t(":not([colspan])", this).each(function (s, n) {
                i instanceof Object ? i.is("[colspan]") && i.index() + parseInt(i.attr("colspan")) + 1 >= s && s <= i.index() + parseInt(i.attr("colspan")) + 1 || (i = i.next()) : i = e.children().eq(s), i.is("[colspan]") || t(this).css({width: i.innerWidth()})
            })
        })
    }), t(".js-table").length <= 0 || t(function () {
        t(".js-table").trigger("loaded.tables")
    })
}(jQuery), $(document).on("hidden.bs.tab shown.bs.tab", ".b-menu .b-menu-item a[data-toggle=tab]", function (t) {
    console.log(t), $(this).parent().hasClass("active") ? $(this).parent().addClass("b-menu-item__active") : $(this).parent().removeClass("b-menu-item__active")
}), function (t) {
    t(document).on("click", ".js-Toggling-toggler", function () {
        var e = t(this), i = t(e.attr("data-parent")), s = e.data("toggling");
        if (i.length > 0) {
            var n = t(".js-Toggling-section", i), o = t(".js-Toggling-toggler", i), a = s && s.hasOwnProperty("target") ? s.target : n.filter(e.attr("data-target"));
            o.removeClass("active"), n.removeClass("active").addClass("hide"), a.toggleClass("active hide"), e.addClass("active")
        } else {
            var a = t(".js-Toggling-section").filter(e.attr("data-target")).toggleClass("active hide");
            e.toggleClass("active")
        }
        e.data("toggling", {target: a})
    })
}(jQuery), function (t) {
    t(document).on("change", ".js-fileChoose", function () {
        var e = t(this).parent(), i = e.find(".file-name"), s = t(this), n = /.*(.*)$/;
        s.data("filename_original") || s.data("filename_original", i.html());
        var o = s.data("filename_original");
        if (this.value.length > 0)if ("files"in this && this.files.length > 0) {
            var a = [];
            t.each(this.files, function () {
                a.push(this.name)
            }), o = a.join(", ")
        } else o = this.value.replace(n, "$1").replace(n, "$1");
        o = t("<div/>").text(o).html(), e.find(".file-name").attr("title", o).html(o)
    })
}(jQuery);
var selectbox = [];
(selectbox = $("select.js-select")) && selectbox.length > 0 && selectbox.select2({theme: "workly"}), $("body").tooltip({
    selector: ".js-tooltip",
    container: "body",
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
}), $(".js-chartLineRadial").each(function () {
    var t = parseInt($(this).data("line-percent")), e = 100 - t;
    $chart = new Chart(this.getContext("2d")).Doughnut([{value: t, color: "#2ea97d", highlight: "#2ea97d"}, {
        value: e,
        color: "#e8e8e8",
        highlight: "#e8e8e8;"
    }], {
        responsive: !0,
        segmentShowStroke: !1,
        showTooltips: !1,
        animation: !1,
        percentageInnerCutout: 0
    }), $(this).data("chart", $chart)
});
var Datepicker;
!function (t, e, i) {
    var s, n, o, a = "datepicker", r = ".datepicker-here", h = !1, l = '<div class="datepicker"><i class="datepicker--pointer"></i><nav class="datepicker--nav"></nav><div class="datepicker--content"></div></div>', c = {
        classes: "",
        inline: !1,
        language: "ru",
        startDate: new Date,
        firstDay: "",
        weekends: [6, 0],
        dateFormat: "",
        altField: "",
        altFieldDateFormat: "@",
        toggleSelected: !0,
        keyboardNav: !0,
        position: "bottom left",
        offset: 12,
        view: "days",
        minView: "days",
        showOtherMonths: !0,
        selectOtherMonths: !0,
        moveToOtherMonthsOnSelect: !0,
        showOtherYears: !0,
        selectOtherYears: !0,
        moveToOtherYearsOnSelect: !0,
        minDate: "",
        maxDate: "",
        disableNavWhenOutOfRange: !0,
        multipleDates: !1,
        multipleDatesSeparator: ",",
        range: !1,
        todayButton: !1,
        clearButton: !1,
        showEvent: "focus",
        autoClose: !1,
        monthsField: "monthsShort",
        prevHtml: '<svg><path d="M 17,12 l -5,5 l 5,5"></path></svg>',
        nextHtml: '<svg><path d="M 14,12 l 5,5 l -5,5"></path></svg>',
        navTitles: {days: "MM, <i>yyyy</i>", months: "yyyy", years: "yyyy1 - yyyy2"},
        onSelect: "",
        onChangeMonth: "",
        onChangeYear: "",
        onChangeDecade: "",
        onChangeView: "",
        onRenderCell: ""
    }, d = {
        ctrlRight: [17, 39],
        ctrlUp: [17, 38],
        ctrlLeft: [17, 37],
        ctrlDown: [17, 40],
        shiftRight: [16, 39],
        shiftUp: [16, 38],
        shiftLeft: [16, 37],
        shiftDown: [16, 40],
        altUp: [18, 38],
        altRight: [18, 39],
        altLeft: [18, 37],
        altDown: [18, 40],
        ctrlShiftUp: [16, 17, 38]
    };
    Datepicker = function (t, n) {
        this.el = t, this.$el = e(t), this.opts = e.extend(!0, {}, c, n, this.$el.data()), s == i && (s = e("body")), this.opts.startDate || (this.opts.startDate = new Date), "INPUT" == this.el.nodeName && (this.elIsInput = !0), this.opts.altField && (this.$altField = "string" == typeof this.opts.altField ? e(this.opts.altField) : this.opts.altField), this.inited = !1, this.visible = !1, this.silent = !1, this.currentDate = this.opts.startDate, this.currentView = this.opts.view, this._createShortCuts(), this.selectedDates = [], this.views = {}, this.keys = [], this.minRange = "", this.maxRange = "", this.init()
    }, o = Datepicker, o.prototype = {
        viewIndexes: ["days", "months", "years"], init: function () {
            h || this.opts.inline || !this.elIsInput || this._buildDatepickersContainer(), this._buildBaseHtml(), this._defineLocale(this.opts.language), this._syncWithMinMaxDates(), this.elIsInput && (this.opts.inline || (this._setPositionClasses(this.opts.position), this._bindEvents()), this.opts.keyboardNav && this._bindKeyboardEvents(), this.$datepicker.on("mousedown", this._onMouseDownDatepicker.bind(this)), this.$datepicker.on("mouseup", this._onMouseUpDatepicker.bind(this))), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this.views[this.currentView] = new Datepicker.Body(this, this.currentView, this.opts), this.views[this.currentView].show(), this.nav = new Datepicker.Navigation(this, this.opts), this.view = this.currentView, this.$datepicker.on("mouseenter", ".datepicker--cell", this._onMouseEnterCell.bind(this)), this.$datepicker.on("mouseleave", ".datepicker--cell", this._onMouseLeaveCell.bind(this)), this.inited = !0
        }, _createShortCuts: function () {
            this.minDate = this.opts.minDate ? this.opts.minDate : new Date(-86399999136e5), this.maxDate = this.opts.maxDate ? this.opts.maxDate : new Date(86399999136e5);
        }, _bindEvents: function () {
            this.$el.on(this.opts.showEvent + ".adp", this._onShowEvent.bind(this)), this.$el.on("blur.adp", this._onBlur.bind(this)), this.$el.on("input.adp", this._onInput.bind(this)), e(t).on("resize.adp", this._onResize.bind(this))
        }, _bindKeyboardEvents: function () {
            this.$el.on("keydown.adp", this._onKeyDown.bind(this)), this.$el.on("keyup.adp", this._onKeyUp.bind(this)), this.$el.on("hotKey.adp", this._onHotKey.bind(this))
        }, isWeekend: function (t) {
            return -1 !== this.opts.weekends.indexOf(t)
        }, _defineLocale: function (t) {
            "string" == typeof t ? (this.loc = Datepicker.language[t], this.loc || (console.warn("Can't find language \"" + t + '" in Datepicker.language, will use "ru" instead'), this.loc = e.extend(!0, {}, Datepicker.language.ru)), this.loc = e.extend(!0, {}, Datepicker.language.ru, Datepicker.language[t])) : this.loc = e.extend(!0, {}, Datepicker.language.ru, t), this.opts.dateFormat && (this.loc.dateFormat = this.opts.dateFormat), "" !== this.opts.firstDay && (this.loc.firstDay = this.opts.firstDay)
        }, _buildDatepickersContainer: function () {
            h = !0, s.append('<div class="datepickers-container" id="datepickers-container"></div>'), n = e("#datepickers-container")
        }, _buildBaseHtml: function () {
            var t, i = e('<div class="datepicker-inline">');
            t = "INPUT" == this.el.nodeName ? this.opts.inline ? i.insertAfter(this.$el) : n : i.appendTo(this.$el), this.$datepicker = e(l).appendTo(t), this.$content = e(".datepicker--content", this.$datepicker), this.$nav = e(".datepicker--nav", this.$datepicker)
        }, _triggerOnChange: function () {
            if (!this.selectedDates.length)return this.opts.onSelect("", "", this);
            var t, e = this.selectedDates, i = o.getParsedDate(e[0]), s = this, n = new Date(i.year, i.month, i.date);
            t = e.map(function (t) {
                return s.formatDate(s.loc.dateFormat, t)
            }).join(this.opts.multipleDatesSeparator), (this.opts.multipleDates || this.opts.range) && (n = e.map(function (t) {
                var e = o.getParsedDate(t);
                return new Date(e.year, e.month, e.date)
            })), this.opts.onSelect(t, n, this)
        }, next: function () {
            var t = this.parsedDate, e = this.opts;
            switch (this.view) {
                case"days":
                    this.date = new Date(t.year, t.month + 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case"months":
                    this.date = new Date(t.year + 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
                    break;
                case"years":
                    this.date = new Date(t.year + 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade)
            }
        }, prev: function () {
            var t = this.parsedDate, e = this.opts;
            switch (this.view) {
                case"days":
                    this.date = new Date(t.year, t.month - 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
                    break;
                case"months":
                    this.date = new Date(t.year - 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
                    break;
                case"years":
                    this.date = new Date(t.year - 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade)
            }
        }, formatDate: function (t, e) {
            e = e || this.date;
            var i = t, s = this._getWordBoundaryRegExp, n = this.loc, a = o.getDecade(e), r = o.getParsedDate(e);
            switch (!0) {
                case/@/.test(i):
                    i = i.replace(/@/, e.getTime());
                case/dd/.test(i):
                    i = i.replace(s("dd"), r.fullDate);
                case/d/.test(i):
                    i = i.replace(s("d"), r.date);
                case/DD/.test(i):
                    i = i.replace(s("DD"), n.days[r.day]);
                case/D/.test(i):
                    i = i.replace(s("D"), n.daysShort[r.day]);
                case/mm/.test(i):
                    i = i.replace(s("mm"), r.fullMonth);
                case/m/.test(i):
                    i = i.replace(s("m"), r.month + 1);
                case/MM/.test(i):
                    i = i.replace(s("MM"), this.loc.months[r.month]);
                case/M/.test(i):
                    i = i.replace(s("M"), n.monthsShort[r.month]);
                case/yyyy/.test(i):
                    i = i.replace(s("yyyy"), r.year);
                case/yyyy1/.test(i):
                    i = i.replace(s("yyyy1"), a[0]);
                case/yyyy2/.test(i):
                    i = i.replace(s("yyyy2"), a[1]);
                case/yy/.test(i):
                    i = i.replace(s("yy"), r.year.toString().slice(-2))
            }
            return i
        }, _getWordBoundaryRegExp: function (t) {
            return new RegExp("\\b(?=[a-zA-Z0-9aou?AOU<])" + t + "(?![>a-zA-Z0-9aou?AOU])")
        }, selectDate: function (t) {
            var e = this, i = e.opts, s = e.parsedDate, n = e.selectedDates, o = n.length, a = "";
            if (t instanceof Date) {
                if ("days" == e.view && t.getMonth() != s.month && i.moveToOtherMonthsOnSelect && (a = new Date(t.getFullYear(), t.getMonth(), 1)), "years" == e.view && t.getFullYear() != s.year && i.moveToOtherYearsOnSelect && (a = new Date(t.getFullYear(), 0, 1)), a && (e.silent = !0, e.date = a, e.silent = !1, e.nav._render()), i.multipleDates && !i.range) {
                    if (o === i.multipleDates)return;
                    e._isSelected(t) || e.selectedDates.push(t)
                } else i.range ? 2 == o ? (e.selectedDates = [t], e.minRange = t, e.maxRange = "") : 1 == o ? (e.selectedDates.push(t), e.maxRange ? e.minRange = t : e.maxRange = t, e.selectedDates = [e.minRange, e.maxRange]) : (e.selectedDates = [t], e.minRange = t) : e.selectedDates = [t];
                e._setInputValue(), i.onSelect && e._triggerOnChange(), i.autoClose && (i.multipleDates || i.range ? i.range && 2 == e.selectedDates.length && e.hide() : e.hide()), e.views[this.currentView]._render()
            }
        }, removeDate: function (t) {
            var e = this.selectedDates, i = this;
            return t instanceof Date ? e.some(function (s, n) {
                return o.isSame(s, t) ? (e.splice(n, 1), i.selectedDates.length || (i.minRange = "", i.maxRange = ""), i.views[i.currentView]._render(), i._setInputValue(), i.opts.onSelect && i._triggerOnChange(), !0) : void 0
            }) : void 0
        }, today: function () {
            this.silent = !0, this.view = this.opts.minView, this.silent = !1, this.date = new Date
        }, clear: function () {
            this.selectedDates = [], this.minRange = "", this.maxRange = "", this.views[this.currentView]._render(), this._setInputValue(), this.opts.onSelect && this._triggerOnChange()
        }, update: function (t, i) {
            var s = arguments.length;
            return 2 == s ? this.opts[t] = i : 1 == s && "object" == typeof t && (this.opts = e.extend(!0, this.opts, t)), this._createShortCuts(), this._syncWithMinMaxDates(), this._defineLocale(this.opts.language), this.nav._addButtonsIfNeed(), this.nav._render(), this.views[this.currentView]._render(), this.elIsInput && !this.opts.inline && (this._setPositionClasses(this.opts.position), this.visible && this.setPosition(this.opts.position)), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this
        }, _syncWithMinMaxDates: function () {
            var t = this.date.getTime();
            this.silent = !0, this.minTime > t && (this.date = this.minDate), this.maxTime < t && (this.date = this.maxDate), this.silent = !1
        }, _isSelected: function (t, e) {
            return this.selectedDates.some(function (i) {
                return o.isSame(i, t, e)
            })
        }, _setInputValue: function () {
            var t, e = this, i = e.opts, s = e.loc.dateFormat, n = i.altFieldDateFormat, o = e.selectedDates.map(function (t) {
                return e.formatDate(s, t)
            });
            i.altField && e.$altField.length && (t = this.selectedDates.map(function (t) {
                return e.formatDate(n, t)
            }), t = t.join(this.opts.multipleDatesSeparator), this.$altField.val(t)), o = o.join(this.opts.multipleDatesSeparator), this.$el.val(o)
        }, _isInRange: function (t, e) {
            var i = t.getTime(), s = o.getParsedDate(t), n = o.getParsedDate(this.minDate), a = o.getParsedDate(this.maxDate), r = new Date(s.year, s.month, n.date).getTime(), h = new Date(s.year, s.month, a.date).getTime(), l = {
                day: i >= this.minTime && i <= this.maxTime,
                month: r >= this.minTime && h <= this.maxTime,
                year: s.year >= n.year && s.year <= a.year
            };
            return e ? l[e] : l.day
        }, _getDimensions: function (t) {
            var e = t.offset();
            return {width: t.outerWidth(), height: t.outerHeight(), left: e.left, top: e.top}
        }, _getDateFromCell: function (t) {
            var e = this.parsedDate, s = t.data("year") || e.year, n = t.data("month") == i ? e.month : t.data("month"), o = t.data("date") || 1;
            return new Date(s, n, o)
        }, _setPositionClasses: function (t) {
            t = t.split(" ");
            var e = t[0], i = t[1], s = "datepicker -" + e + "-" + i + "- -from-" + e + "-";
            this.visible && (s += " active"), this.$datepicker.removeAttr("class").addClass(s)
        }, setPosition: function (t) {
            t = t || this.opts.position;
            var e, i, s = this._getDimensions(this.$el), n = this._getDimensions(this.$datepicker), o = t.split(" "), a = this.opts.offset, r = o[0], h = o[1];
            switch (r) {
                case"top":
                    e = s.top - n.height - a;
                    break;
                case"right":
                    i = s.left + s.width + a;
                    break;
                case"bottom":
                    e = s.top + s.height + a;
                    break;
                case"left":
                    i = s.left - n.width - a
            }
            switch (h) {
                case"top":
                    e = s.top;
                    break;
                case"right":
                    i = s.left + s.width - n.width;
                    break;
                case"bottom":
                    e = s.top + s.height - n.height;
                    break;
                case"left":
                    i = s.left;
                    break;
                case"center":
                    /left|right/.test(r) ? e = s.top + s.height / 2 - n.height / 2 : i = s.left + s.width / 2 - n.width / 2
            }
            this.$datepicker.css({left: i, top: e})
        }, show: function () {
            this.setPosition(this.opts.position), this.$datepicker.addClass("active"), this.visible = !0
        }, hide: function () {
            this.$datepicker.removeClass("active").css({left: "-100000px"}), this.focused = "", this.keys = [], this.inFocus = !1, this.visible = !1, this.$el.blur()
        }, down: function (t) {
            this._changeView(t, "down")
        }, up: function (t) {
            this._changeView(t, "up")
        }, _changeView: function (t, e) {
            t = t || this.focused || this.date;
            var i = "up" == e ? this.viewIndex + 1 : this.viewIndex - 1;
            i > 2 && (i = 2), 0 > i && (i = 0), this.silent = !0, this.date = new Date(t.getFullYear(), t.getMonth(), 1), this.silent = !1, this.view = this.viewIndexes[i]
        }, _handleHotKey: function (t) {
            var e, i, s, n = o.getParsedDate(this._getFocusedDate()), a = this.opts, r = !1, h = !1, l = !1, c = n.year, d = n.month, p = n.date;
            switch (t) {
                case"ctrlRight":
                case"ctrlUp":
                    d += 1, r = !0;
                    break;
                case"ctrlLeft":
                case"ctrlDown":
                    d -= 1, r = !0;
                    break;
                case"shiftRight":
                case"shiftUp":
                    h = !0, c += 1;
                    break;
                case"shiftLeft":
                case"shiftDown":
                    h = !0, c -= 1;
                    break;
                case"altRight":
                case"altUp":
                    l = !0, c += 10;
                    break;
                case"altLeft":
                case"altDown":
                    l = !0, c -= 10;
                    break;
                case"ctrlShiftUp":
                    this.up()
            }
            s = o.getDaysCount(new Date(c, d)), i = new Date(c, d, p), p > s && (p = s), i.getTime() < this.minTime ? i = this.minDate : i.getTime() > this.maxTime && (i = this.maxDate), this.focused = i, e = o.getParsedDate(i), r && a.onChangeMonth && a.onChangeMonth(e.month, e.year), h && a.onChangeYear && a.onChangeYear(e.year), l && a.onChangeDecade && a.onChangeDecade(this.curDecade)
        }, _registerKey: function (t) {
            var e = this.keys.some(function (e) {
                return e == t
            });
            e || this.keys.push(t)
        }, _unRegisterKey: function (t) {
            var e = this.keys.indexOf(t);
            this.keys.splice(e, 1)
        }, _isHotKeyPressed: function () {
            var t, e = !1, i = this, s = this.keys.sort();
            for (var n in d)t = d[n], s.length == t.length && t.every(function (t, e) {
                return t == s[e]
            }) && (i._trigger("hotKey", n), e = !0);
            return e
        }, _trigger: function (t, e) {
            this.$el.trigger(t, e)
        }, _focusNextCell: function (t, e) {
            e = e || this.cellType;
            var i = o.getParsedDate(this._getFocusedDate()), s = i.year, n = i.month, a = i.date;
            if (!this._isHotKeyPressed()) {
                switch (t) {
                    case 37:
                        "day" == e ? a -= 1 : "", "month" == e ? n -= 1 : "", "year" == e ? s -= 1 : "";
                        break;
                    case 38:
                        "day" == e ? a -= 7 : "", "month" == e ? n -= 3 : "", "year" == e ? s -= 4 : "";
                        break;
                    case 39:
                        "day" == e ? a += 1 : "", "month" == e ? n += 1 : "", "year" == e ? s += 1 : "";
                        break;
                    case 40:
                        "day" == e ? a += 7 : "", "month" == e ? n += 3 : "", "year" == e ? s += 4 : ""
                }
                var r = new Date(s, n, a);
                r.getTime() < this.minTime ? r = this.minDate : r.getTime() > this.maxTime && (r = this.maxDate), this.focused = r
            }
        }, _getFocusedDate: function () {
            var t = this.focused || this.selectedDates[this.selectedDates.length - 1], e = this.parsedDate;
            if (!t)switch (this.view) {
                case"days":
                    t = new Date(e.year, e.month, (new Date).getDate());
                    break;
                case"months":
                    t = new Date(e.year, e.month, 1);
                    break;
                case"years":
                    t = new Date(e.year, 0, 1)
            }
            return t
        }, _getCell: function (t, e) {
            e = e || this.cellType;
            var i, s = o.getParsedDate(t), n = '.datepicker--cell[data-year="' + s.year + '"]';
            switch (e) {
                case"month":
                    n = '[data-month="' + s.month + '"]';
                    break;
                case"day":
                    n += '[data-month="' + s.month + '"][data-date="' + s.date + '"]'
            }
            return i = this.views[this.currentView].$el.find(n), i.length ? i : ""
        }, destroy: function () {
            var t = this;
            t.$el.off(".adp").data("datepicker", ""), t.selectedDates = [], t.focused = "", t.views = {}, t.keys = [], t.minRange = "", t.maxRange = "", t.opts.inline || !t.elIsInput ? t.$datepicker.closest(".datepicker-inline").remove() : t.$datepicker.remove()
        }, _onShowEvent: function () {
            this.visible || this.show()
        }, _onBlur: function () {
            !this.inFocus && this.visible && this.hide()
        }, _onMouseDownDatepicker: function (t) {
            this.inFocus = !0
        }, _onMouseUpDatepicker: function (t) {
            this.inFocus = !1, this.$el.focus()
        }, _onInput: function () {
            var t = this.$el.val();
            t || this.clear()
        }, _onResize: function () {
            this.visible && this.setPosition()
        }, _onKeyDown: function (t) {
            var e = t.which;
            if (this._registerKey(e), e >= 37 && 40 >= e && (t.preventDefault(), this._focusNextCell(e)), 13 == e && this.focused) {
                if (this._getCell(this.focused).hasClass("-disabled-"))return;
                if (this.view != this.opts.minView)this.down(); else {
                    var i = this._isSelected(this.focused, this.cellType);
                    i ? i && this.opts.toggleSelected && this.removeDate(this.focused) : this.selectDate(this.focused)
                }
            }
            27 == e && this.hide()
        }, _onKeyUp: function (t) {
            var e = t.which;
            this._unRegisterKey(e)
        }, _onHotKey: function (t, e) {
            this._handleHotKey(e)
        }, _onMouseEnterCell: function (t) {
            var i = e(t.target).closest(".datepicker--cell"), s = this._getDateFromCell(i);
            this.silent = !0, this.focused && (this.focused = ""), i.addClass("-focus-"), this.focused = s, this.silent = !1, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", o.less(this.minRange, this.focused) && (this.maxRange = this.minRange, this.minRange = ""), this.views[this.currentView]._update())
        }, _onMouseLeaveCell: function (t) {
            var i = e(t.target).closest(".datepicker--cell");
            i.removeClass("-focus-"), this.silent = !0, this.focused = "", this.silent = !1
        }, set focused(t) {
            if (!t && this.focused) {
                var e = this._getCell(this.focused);
                e.length && e.removeClass("-focus-")
            }
            this._focused = t, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", o.less(this.minRange, this._focused) && (this.maxRange = this.minRange, this.minRange = "")), this.silent || (this.date = t)
        }, get focused() {
            return this._focused
        }, get parsedDate() {
            return o.getParsedDate(this.date)
        }, set date(t) {
            return t instanceof Date ? (this.currentDate = t, this.inited && !this.silent && (this.views[this.view]._render(), this.nav._render(), this.visible && this.elIsInput && this.setPosition()), t) : void 0
        }, get date() {
            return this.currentDate
        }, set view(t) {
            return this.viewIndex = this.viewIndexes.indexOf(t), this.viewIndex < 0 ? void 0 : (this.prevView = this.currentView, this.currentView = t, this.inited && (this.views[t] ? this.views[t]._render() : this.views[t] = new Datepicker.Body(this, t, this.opts), this.views[this.prevView].hide(), this.views[t].show(), this.nav._render(), this.opts.onChangeView && this.opts.onChangeView(t), this.elIsInput && this.visible && this.setPosition()), t)
        }, get view() {
            return this.currentView
        }, get cellType() {
            return this.view.substring(0, this.view.length - 1)
        }, get minTime() {
            var t = o.getParsedDate(this.minDate);
            return new Date(t.year, t.month, t.date).getTime()
        }, get maxTime() {
            var t = o.getParsedDate(this.maxDate);
            return new Date(t.year, t.month, t.date).getTime()
        }, get curDecade() {
            return o.getDecade(this.date)
        }
    }, o.getDaysCount = function (t) {
        return new Date(t.getFullYear(), t.getMonth() + 1, 0).getDate()
    }, o.getParsedDate = function (t) {
        return {
            year: t.getFullYear(),
            month: t.getMonth(),
            fullMonth: t.getMonth() + 1 < 10 ? "0" + (t.getMonth() + 1) : t.getMonth() + 1,
            date: t.getDate(),
            fullDate: t.getDate() < 10 ? "0" + t.getDate() : t.getDate(),
            day: t.getDay()
        }
    }, o.getDecade = function (t) {
        var e = 10 * Math.floor(t.getFullYear() / 10);
        return [e, e + 9]
    }, o.template = function (t, e) {
        return t.replace(/#\{([\w]+)\}/g, function (t, i) {
            return e[i] || 0 === e[i] ? e[i] : void 0
        })
    }, o.isSame = function (t, e, i) {
        if (!t || !e)return !1;
        var s = o.getParsedDate(t), n = o.getParsedDate(e), a = i ? i : "day", r = {
            day: s.date == n.date && s.month == n.month && s.year == n.year,
            month: s.month == n.month && s.year == n.year,
            year: s.year == n.year
        };
        return r[a]
    }, o.less = function (t, e, i) {
        return t && e ? e.getTime() < t.getTime() : !1
    }, o.bigger = function (t, e, i) {
        return t && e ? e.getTime() > t.getTime() : !1
    }, Datepicker.language = {
        ru: {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            daysShort: ["Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            clear: "Очистить",
            dateFormat: "dd.mm.yyyy",
            firstDay: 1
        }
    }, e.fn[a] = function (t) {
        return this.each(function () {
            if (e.data(this, a)) {
                var i = e.data(this, a);
                i.opts = e.extend(!0, i.opts, t), i.update()
            } else e.data(this, a, new Datepicker(this, t))
        })
    }, e(function () {
        e(r).datepicker()
    })
}(window, jQuery), function () {
    var t = {
        days: '<div class="datepicker--days datepicker--body"><div class="datepicker--days-names"></div><div class="datepicker--cells datepicker--cells-days"></div></div>',
        months: '<div class="datepicker--months datepicker--body"><div class="datepicker--cells datepicker--cells-months"></div></div>',
        years: '<div class="datepicker--years datepicker--body"><div class="datepicker--cells datepicker--cells-years"></div></div>'
    }, e = Datepicker;
    e.Body = function (t, e, i) {
        this.d = t, this.type = e, this.opts = i, this.init()
    }, e.Body.prototype = {
        init: function () {
            this._buildBaseHtml(), this._render(), this._bindEvents()
        }, _bindEvents: function () {
            this.$el.on("click", ".datepicker--cell", $.proxy(this._onClickCell, this))
        }, _buildBaseHtml: function () {
            this.$el = $(t[this.type]).appendTo(this.d.$content), this.$names = $(".datepicker--days-names", this.$el), this.$cells = $(".datepicker--cells", this.$el)
        }, _getDayNamesHtml: function (t, e, i, s) {
            return e = void 0 != e ? e : t, i = i ? i : "", s = void 0 != s ? s : 0, s > 7 ? i : 7 == e ? this._getDayNamesHtml(t, 0, i, ++s) : (i += '<div class="datepicker--day-name' + (this.d.isWeekend(e) ? " -weekend-" : "") + '">' + this.d.loc.daysMin[e] + "</div>", this._getDayNamesHtml(t, ++e, i, ++s))
        }, _getCellContents: function (t, i) {
            var s = "datepicker--cell datepicker--cell-" + i, n = new Date, o = this.d, a = o.opts, r = e.getParsedDate(t), h = {}, l = r.date;
            switch (a.onRenderCell && (h = a.onRenderCell(t, i) || {}, l = h.html ? h.html : l, s += h.classes ? " " + h.classes : ""), i) {
                case"day":
                    o.isWeekend(r.day) && (s += " -weekend-"), r.month != this.d.parsedDate.month && (s += " -other-month-", a.selectOtherMonths || (s += " -disabled-"), a.showOtherMonths || (l = ""));
                    break;
                case"month":
                    l = o.loc[o.opts.monthsField][r.month];
                    break;
                case"year":
                    var c = o.curDecade;
                    l = r.year, (r.year < c[0] || r.year > c[1]) && (s += " -other-decade-", a.selectOtherYears || (s += " -disabled-"), a.showOtherYears || (l = ""))
            }
            return a.onRenderCell && (h = a.onRenderCell(t, i) || {}, l = h.html ? h.html : l, s += h.classes ? " " + h.classes : ""), a.range && (e.isSame(o.minRange, t, i) && (s += " -range-from-"), e.isSame(o.maxRange, t, i) && (s += " -range-to-"), 1 == o.selectedDates.length && o.focused ? ((e.bigger(o.minRange, t) && e.less(o.focused, t) || e.less(o.maxRange, t) && e.bigger(o.focused, t)) && (s += " -in-range-"), e.less(o.maxRange, t) && e.isSame(o.focused, t) && (s += " -range-from-"), e.bigger(o.minRange, t) && e.isSame(o.focused, t) && (s += " -range-to-")) : 2 == o.selectedDates.length && e.bigger(o.minRange, t) && e.less(o.maxRange, t) && (s += " -in-range-")), e.isSame(n, t, i) && (s += " -current-"), o.focused && e.isSame(t, o.focused, i) && (s += " -focus-"), o._isSelected(t, i) && (s += " -selected-"), (!o._isInRange(t, i) || h.disabled) && (s += " -disabled-"), {
                html: l,
                classes: s
            }
        }, _getDaysHtml: function (t) {
            var i = e.getDaysCount(t), s = new Date(t.getFullYear(), t.getMonth(), 1).getDay(), n = new Date(t.getFullYear(), t.getMonth(), i).getDay(), o = s - this.d.loc.firstDay, a = 6 - n + this.d.loc.firstDay;
            o = 0 > o ? o + 7 : o, a = a > 6 ? a - 7 : a;
            for (var r, h, l = -o + 1, c = "", d = l, p = i + a; p >= d; d++)h = t.getFullYear(), r = t.getMonth(), c += this._getDayHtml(new Date(h, r, d));
            return c
        }, _getDayHtml: function (t) {
            var e = this._getCellContents(t, "day");
            return '<div class="' + e.classes + '" data-date="' + t.getDate() + '" data-month="' + t.getMonth() + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>"
        }, _getMonthsHtml: function (t) {
            for (var i = "", s = e.getParsedDate(t), n = 0; 12 > n;)i += this._getMonthHtml(new Date(s.year, n)), n++;
            return i
        }, _getMonthHtml: function (t) {
            var e = this._getCellContents(t, "month");
            return '<div class="' + e.classes + '" data-month="' + t.getMonth() + '">' + e.html + "</div>"
        }, _getYearsHtml: function (t) {
            var i = (e.getParsedDate(t), e.getDecade(t)), s = i[0] - 1, n = "", o = s;
            for (o; o <= i[1] + 1; o++)n += this._getYearHtml(new Date(o, 0));
            return n
        }, _getYearHtml: function (t) {
            var e = this._getCellContents(t, "year");
            return '<div class="' + e.classes + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>"
        }, _renderTypes: {
            days: function () {
                var t = this._getDayNamesHtml(this.d.loc.firstDay), e = this._getDaysHtml(this.d.currentDate);
                this.$cells.html(e), this.$names.html(t)
            }, months: function () {
                var t = this._getMonthsHtml(this.d.currentDate);
                this.$cells.html(t)
            }, years: function () {
                var t = this._getYearsHtml(this.d.currentDate);
                this.$cells.html(t)
            }
        }, _render: function () {
            this._renderTypes[this.type].bind(this)()
        }, _update: function () {
            var t, e, i, s = $(".datepicker--cell", this.$cells), n = this;
            s.each(function (s, o) {
                e = $(this), i = n.d._getDateFromCell($(this)), t = n._getCellContents(i, n.d.cellType), e.attr("class", t.classes)
            })
        }, show: function () {
            this.$el.addClass("active"), this.acitve = !0
        }, hide: function () {
            this.$el.removeClass("active"), this.active = !1
        }, _handleClick: function (t) {
            var e = t.data("date") || 1, i = t.data("month") || 0, s = t.data("year") || this.d.parsedDate.year;
            if (this.d.view != this.opts.minView)return void this.d.down(new Date(s, i, e));
            var n = new Date(s, i, e), o = this.d._isSelected(n, this.d.cellType);
            o ? o && this.opts.toggleSelected && this.d.removeDate(n) : this.d.selectDate(n)
        }, _onClickCell: function (t) {
            var e = $(t.target).closest(".datepicker--cell");
            e.hasClass("-disabled-") || this._handleClick.bind(this)(e)
        }
    }
}(), function () {
    var t = '<div class="datepicker--nav-action" data-action="prev">#{prevHtml}</div><div class="datepicker--nav-title">#{title}</div><div class="datepicker--nav-action" data-action="next">#{nextHtml}</div>', e = '<div class="datepicker--buttons"></div>', i = '<span class="datepicker--button" data-action="#{action}">#{label}</span>';
    Datepicker.Navigation = function (t, e) {
        this.d = t, this.opts = e, this.$buttonsContainer = "", this.init()
    }, Datepicker.Navigation.prototype = {
        init: function () {
            this._buildBaseHtml(), this._bindEvents()
        }, _bindEvents: function () {
            this.d.$nav.on("click", ".datepicker--nav-action", $.proxy(this._onClickNavButton, this)), this.d.$nav.on("click", ".datepicker--nav-title", $.proxy(this._onClickNavTitle, this)), this.d.$datepicker.on("click", ".datepicker--button", $.proxy(this._onClickNavButton, this))
        }, _buildBaseHtml: function () {
            this._render(), this._addButtonsIfNeed()
        }, _addButtonsIfNeed: function () {
            this.opts.todayButton && this._addButton("today"), this.opts.clearButton && this._addButton("clear")
        }, _render: function () {
            var e = this._getTitle(this.d.currentDate), i = Datepicker.template(t, $.extend({title: e}, this.opts));
            this.d.$nav.html(i), "years" == this.d.view && $(".datepicker--nav-title", this.d.$nav).addClass("-disabled-"), this.setNavStatus()
        }, _getTitle: function (t) {
            return this.d.formatDate(this.opts.navTitles[this.d.view], t)
        }, _addButton: function (t) {
            this.$buttonsContainer.length || this._addButtonsContainer();
            var e = {action: t, label: this.d.loc[t]}, s = Datepicker.template(i, e);
            $("[data-action=" + t + "]", this.$buttonsContainer).length || this.$buttonsContainer.append(s)
        }, _addButtonsContainer: function () {
            this.d.$datepicker.append(e), this.$buttonsContainer = $(".datepicker--buttons", this.d.$datepicker)
        }, setNavStatus: function () {
            if ((this.opts.minDate || this.opts.maxDate) && this.opts.disableNavWhenOutOfRange) {
                var t = this.d.parsedDate, e = t.month, i = t.year, s = t.date;
                switch (this.d.view) {
                    case"days":
                        this.d._isInRange(new Date(i, e - 1, s), "month") || this._disableNav("prev"), this.d._isInRange(new Date(i, e + 1, s), "month") || this._disableNav("next");
                        break;
                    case"months":
                        this.d._isInRange(new Date(i - 1, e, s), "year") || this._disableNav("prev"), this.d._isInRange(new Date(i + 1, e, s), "year") || this._disableNav("next");
                        break;
                    case"years":
                        this.d._isInRange(new Date(i - 10, e, s), "year") || this._disableNav("prev"), this.d._isInRange(new Date(i + 10, e, s), "year") || this._disableNav("next")
                }
            }
        }, _disableNav: function (t) {
            $('[data-action="' + t + '"]', this.d.$nav).addClass("-disabled-")
        }, _activateNav: function (t) {
            $('[data-action="' + t + '"]', this.d.$nav).removeClass("-disabled-")
        }, _onClickNavButton: function (t) {
            var e = $(t.target).closest("[data-action]"), i = e.data("action");
            this.d[i]()
        }, _onClickNavTitle: function (t) {
            return $(t.target).hasClass("-disabled-") ? void 0 : "days" == this.d.view ? this.d.view = "months" : void(this.d.view = "years")
        }
    }
}(), !function (t, e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof module && module.exports ? module.exports = e(require("jquery")) : t.Tipped = e(jQuery)
}(this, function ($) {
    function degrees(t) {
        return 180 * t / Math.PI
    }

    function radian(t) {
        return t * Math.PI / 180
    }

    function sec(t) {
        return 1 / Math.cos(t)
    }

    function sfcc(t) {
        return String.fromCharCode.apply(String, t.replace(" ", "").split(","))
    }

    function deepExtend(t, e) {
        for (var i in e)e[i] && e[i].constructor && e[i].constructor === Object ? (t[i] = $.extend({}, t[i]) || {}, deepExtend(t[i], e[i])) : t[i] = e[i];
        return t
    }

    function Spin() {
        return this.initialize.apply(this, _slice.call(arguments))
    }

    function Visible() {
        return this.initialize.apply(this, _slice.call(arguments))
    }

    function Skin() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Stem() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Tooltip() {
        this.initialize.apply(this, _slice.call(arguments))
    }

    function Collection(t) {
        this.element = t
    }

    var Tipped = {};
    $.extend(Tipped, {version: "4.4.2"}), Tipped.Skins = {
        base: {
            afterUpdate: !1,
            ajax: {},
            cache: !0,
            container: !1,
            containment: {selector: "viewport", padding: 5},
            close: !1,
            detach: !0,
            fadeIn: 200,
            fadeOut: 200,
            showDelay: 75,
            hideDelay: 25,
            hideAfter: !1,
            hideOn: {element: "mouseleave"},
            hideOthers: !1,
            position: "top",
            inline: !1,
            offset: {x: 0, y: 0},
            onHide: !1,
            onShow: !1,
            padding: !0,
            radius: !0,
            shadow: !0,
            showOn: {element: "mousemove"},
            size: "medium",
            spinner: !0,
            stem: !0,
            target: "element",
            voila: !0
        },
        reset: {
            ajax: !1,
            hideOn: {element: "mouseleave", tooltip: "mouseleave"},
            showOn: {element: "mouseenter", tooltip: "mouseenter"}
        }
    }, $.each("dark".split(" "), function (t, e) {
        Tipped.Skins[e] = {}
    });
    var Browser = function (t) {
        function e(e) {
            var i = new RegExp(e + "([\\d.]+)").exec(t);
            return i ? parseFloat(i[1]) : !0
        }

        return {
            IE: !(!window.attachEvent || -1 !== t.indexOf("Opera")) && e("MSIE "),
            Opera: t.indexOf("Opera") > -1 && (!!window.opera && opera.version && parseFloat(opera.version()) || 7.55),
            WebKit: t.indexOf("AppleWebKit/") > -1 && e("AppleWebKit/"),
            Gecko: t.indexOf("Gecko") > -1 && -1 === t.indexOf("KHTML") && e("rv:"),
            MobileSafari: !!t.match(/Apple.*Mobile.*Safari/),
            Chrome: t.indexOf("Chrome") > -1 && e("Chrome/"),
            ChromeMobile: t.indexOf("CrMo") > -1 && e("CrMo/"),
            Android: t.indexOf("Android") > -1 && e("Android "),
            IEMobile: t.indexOf("IEMobile") > -1 && e("IEMobile/")
        }
    }(navigator.userAgent), Support = function () {
        function t(t) {
            return i(t, "prefix")
        }

        function e(t, e) {
            for (var i in t)if (void 0 !== s.style[t[i]])return "prefix" == e ? t[i] : !0;
            return !1
        }

        function i(t, i) {
            var s = t.charAt(0).toUpperCase() + t.substr(1), o = (t + " " + n.join(s + " ") + s).split(" ");
            return e(o, i)
        }

        var s = document.createElement("div"), n = "Webkit Moz O ms Khtml".split(" ");
        return {
            css: {animation: i("animation"), transform: i("transform"), prefixed: t},
            shadow: i("boxShadow") && i("pointerEvents"),
            touch: function () {
                try {
                    return !!("ontouchstart"in window || window.DocumentTouch && document instanceof DocumentTouch)
                } catch (t) {
                    return !1
                }
            }()
        }
    }(), _slice = Array.prototype.slice, _ = {
        wrap: function (t, e) {
            var i = t;
            return function () {
                var t = [$.proxy(i, this)].concat(_slice.call(arguments));
                return e.apply(this, t)
            }
        }, isElement: function (t) {
            return t && 1 == t.nodeType
        }, isText: function (t) {
            return t && 3 == t.nodeType
        }, isDocumentFragment: function (t) {
            return t && 11 == t.nodeType
        }, delay: function (t, e) {
            var i = _slice.call(arguments, 2);
            return setTimeout(function () {
                return t.apply(t, i)
            }, e)
        }, defer: function (t) {
            return _.delay.apply(this, [t, 1].concat(_slice.call(arguments, 1)))
        }, pointer: function (t) {
            return {x: t.pageX, y: t.pageY}
        }, element: {
            isAttached: function () {
                function t(t) {
                    for (var e = t; e && e.parentNode;)e = e.parentNode;
                    return e
                }

                return function (e) {
                    var i = t(e);
                    return !(!i || !i.body)
                }
            }()
        }
    }, getUID = function () {
        var t = 0, e = "_tipped-uid-";
        return function (i) {
            for (i = i || e, t++; document.getElementById(i + t);)t++;
            return i + t
        }
    }(), Position = {
        positions: ["topleft", "topmiddle", "topright", "righttop", "rightmiddle", "rightbottom", "bottomright", "bottommiddle", "bottomleft", "leftbottom", "leftmiddle", "lefttop"],
        regex: {
            toOrientation: /^(top|left|bottom|right)(top|left|bottom|right|middle|center)$/,
            horizontal: /^(top|bottom)/,
            isCenter: /(middle|center)/,
            side: /^(top|bottom|left|right)/
        },
        toDimension: function () {
            var t = {top: "height", left: "width", bottom: "height", right: "width"};
            return function (e) {
                return t[e]
            }
        }(),
        isCenter: function (t) {
            return !!t.toLowerCase().match(this.regex.isCenter)
        },
        isCorner: function (t) {
            return !this.isCenter(t)
        },
        getOrientation: function (t) {
            return t.toLowerCase().match(this.regex.horizontal) ? "horizontal" : "vertical"
        },
        getSide: function (t) {
            var e = null, i = t.toLowerCase().match(this.regex.side);
            return i && i[1] && (e = i[1]), e
        },
        split: function (t) {
            return t.toLowerCase().match(this.regex.toOrientation)
        },
        _flip: {top: "bottom", bottom: "top", left: "right", right: "left"},
        flip: function (t, e) {
            var i = this.split(t);
            return e ? this.inverseCornerPlane(this.flip(this.inverseCornerPlane(t))) : this._flip[i[1]] + i[2]
        },
        inverseCornerPlane: function (t) {
            if (Position.isCorner(t)) {
                var e = this.split(t);
                return e[2] + e[1]
            }
            return t
        },
        adjustOffsetBasedOnPosition: function (t, e, i) {
            var s = $.extend({}, t), n = {horizontal: "x", vertical: "y"}, o = {x: "y", y: "x"}, a = {
                top: {right: "x"},
                bottom: {left: "x"},
                left: {bottom: "y"},
                right: {top: "y"}
            }, r = Position.getOrientation(e);
            if (r == Position.getOrientation(i)) {
                if (Position.getSide(e) != Position.getSide(i)) {
                    var h = o[n[r]];
                    s[h] *= -1
                }
            } else {
                var l = s.x;
                s.x = s.y, s.y = l;
                var c = a[Position.getSide(e)][Position.getSide(i)];
                c && (s[c] *= -1), s[n[Position.getOrientation(i)]] = 0
            }
            return s
        },
        getBoxFromPoints: function (t, e, i, s) {
            var n = Math.min(t, i), o = Math.max(t, i), a = Math.min(e, s), r = Math.max(e, s);
            return {left: n, top: a, width: Math.max(o - n, 0), height: Math.max(r - a, 0)}
        },
        isPointWithinBox: function (t, e, i, s, n, o) {
            var a = this.getBoxFromPoints(i, s, n, o);
            return t >= a.left && t <= a.left + a.width && e >= a.top && e <= a.top + a.height
        },
        isPointWithinBoxLayout: function (t, e, i) {
            return this.isPointWithinBox(t, e, i.position.left, i.position.top, i.position.left + i.dimensions.width, i.position.top + i.dimensions.height)
        },
        getDistance: function (t, e, i, s) {
            return Math.sqrt(Math.pow(Math.abs(i - t), 2) + Math.pow(Math.abs(s - e), 2))
        },
        intersectsLine: function () {
            var t = function (t, e, i, s, n, o) {
                var a = (o - e) * (i - t) - (s - e) * (n - t);
                return a > 0 ? !0 : 0 > a ? !1 : !0
            };
            return function (e, i, s, n, o, a, r, h, l) {
                if (!l)return t(e, i, o, a, r, h) != t(s, n, o, a, r, h) && t(e, i, s, n, o, a) != t(e, i, s, n, r, h);
                var c, d, p, u;
                c = s - e, d = n - i, p = r - o, u = h - a;
                var f, m;
                if (f = (-d * (e - o) + c * (i - a)) / (-p * d + c * u), m = (p * (i - a) - u * (e - o)) / (-p * d + c * u), f >= 0 && 1 >= f && m >= 0 && 1 >= m) {
                    var g = e + m * c, v = i + m * d;
                    return {x: g, y: v}
                }
                return !1
            }
        }()
    }, Bounds = {
        viewport: function () {
            var t;
            return t = Browser.MobileSafari || Browser.Android && Browser.Gecko ? {
                width: window.innerWidth,
                height: window.innerHeight
            } : {height: $(window).height(), width: $(window).width()}
        }
    }, Mouse = {
        _buffer: {pageX: 0, pageY: 0},
        _dimensions: {width: 30, height: 30},
        _shift: {x: 2, y: 10},
        getPosition: function (t) {
            var e = this.getActualPosition(t);
            return {
                left: e.left - Math.round(.5 * this._dimensions.width) + this._shift.x,
                top: e.top - Math.round(.5 * this._dimensions.height) + this._shift.y
            }
        },
        getActualPosition: function (t) {
            var e = t && "number" == $.type(t.pageX) ? t : this._buffer;
            return {left: e.pageX, top: e.pageY}
        },
        getDimensions: function () {
            return this._dimensions
        }
    }, Color = function () {
        function t(t) {
            return ("0" + parseInt(t).toString(16)).slice(-2)
        }

        function e(e) {
            return e = e.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/), "#" + t(e[1]) + t(e[2]) + t(e[3])
        }

        var i = {
            _default: "#000000",
            aqua: "#00ffff",
            black: "#000000",
            blue: "#0000ff",
            fuchsia: "#ff00ff",
            gray: "#808080",
            green: "#008000",
            lime: "#00ff00",
            maroon: "#800000",
            navy: "#000080",
            olive: "#808000",
            purple: "#800080",
            red: "#ff0000",
            silver: "#c0c0c0",
            teal: "#008080",
            white: "#ffffff",
            yellow: "#ffff00"
        };
        return {
            toRGB: function (t) {
                if (/^rgba?\(/.test(t))return e(t);
                i[t] && (t = i[t]);
                var s = t.replace("#", "");
                return /^(?:[0-9a-fA-F]{3}){1,2}$/.test(s) || i._default, 3 == s.length && (s = s.charAt(0) + s.charAt(0) + s.charAt(1) + s.charAt(1) + s.charAt(2) + s.charAt(2)), "#" + s
            }
        }
    }();
    Spin.supported = Support.css.transform && Support.css.animation, $.extend(Spin.prototype, {
        initialize: function () {
            this.options = $.extend({}, arguments[0] || {}), this.build(), this.start()
        }, build: function () {
            var t = 2 * (this.options.length + this.options.radius), e = {height: t, width: t};
            this.element = $("<div>").addClass("tpd-spin").css(e), this.element.append(this._rotate = $("<div>").addClass("tpd-spin-rotate")), this.element.css({
                "margin-left": -.5 * e.width,
                "margin-top": -.5 * e.height
            });
            for (var i = this.options.lines, s = 0; i > s; s++) {
                var n, o;
                this._rotate.append(n = $("<div>").addClass("tpd-spin-frame").append(o = $("<div>").addClass("tpd-spin-line"))), o.css({
                    "background-color": this.options.color,
                    width: this.options.width,
                    height: this.options.length,
                    "margin-left": -.5 * this.options.width,
                    "border-radius": Math.round(.5 * this.options.width)
                }), n.css({opacity: (1 / i * (s + 1)).toFixed(2)});
                var a = {};
                a[Support.css.prefixed("transform")] = "rotate(" + 360 / i * (s + 1) + "deg)", n.css(a)
            }
        }, start: function () {
            var t = {};
            t[Support.css.prefixed("animation")] = "tpd-spin 1s infinite steps(" + this.options.lines + ")", this._rotate.css(t)
        }, stop: function () {
            var t = {};
            t[Support.css.prefixed("animation")] = "none", this._rotate.css(t), this.element.detach()
        }
    }), $.extend(Visible.prototype, {
        initialize: function (t) {
            return t = "array" == $.type(t) ? t : [t], this.elements = t, this._restore = [], $.each(t, $.proxy(function (t, e) {
                var i = $(e).is(":visible");
                i || $(e).show(), this._restore.push({element: e, visible: i})
            }, this)), this
        }, restore: function () {
            $.each(this._restore, function (t, e) {
                e.visible || $(e.element).show()
            }), this._restore = null
        }
    });
    var AjaxCache = function () {
        var t = [];
        return {
            get: function (e) {
                for (var i = null, s = 0; s < t.length; s++)t[s] && t[s].url == e.url && (t[s].type || "GET").toUpperCase() == (e.type || "GET").toUpperCase() && $.param(t[s].data || {}) == $.param(e.data || {}) && (i = t[s]);
                return i
            }, set: function (e, i, s) {
                var n = this.get(e);
                n || (n = $.extend({callbacks: {}}, e), t.push(n)), n.callbacks[i] = s
            }, remove: function (e) {
                for (var i = 0; i < t.length; i++)t[i] && t[i].url == e && delete t[i]
            }, clear: function () {
                t = []
            }
        }
    }(), Voila = function (t) {
        function e(i, s, n) {
            if (!(this instanceof e))return new e(i, s, n);
            var o = t.type(arguments[1]), a = "object" === o ? arguments[1] : {}, r = "function" === o ? arguments[1] : "function" === t.type(arguments[2]) ? arguments[2] : !1;
            return this.options = t.extend({method: "onload"}, a), this.deferred = new jQuery.Deferred, r && this.always(r), this._processed = 0, this.images = [], this._add(i), this
        }

        t.extend(e.prototype, {
            _add: function (e) {
                var s = "string" == t.type(e) ? t(e) : e instanceof jQuery || e.length > 0 ? e : [e];
                t.each(s, t.proxy(function (e, s) {
                    var n = t(), o = t(s);
                    n = o.is("img") ? n.add(o) : n.add(o.find("img")), n.each(t.proxy(function (e, s) {
                        this.images.push(new i(s, t.proxy(function (t) {
                            this._progress(t)
                        }, this), t.proxy(function (t) {
                            this._progress(t)
                        }, this), this.options))
                    }, this))
                }, this)), this.images.length < 1 && setTimeout(t.proxy(function () {
                    this._resolve()
                }, this))
            }, abort: function () {
                this._progress = this._notify = this._reject = this._resolve = function () {
                }, t.each(this.images, function (t, e) {
                    e.abort()
                }), this.images = []
            }, _progress: function (t) {
                this._processed++, t.isLoaded || (this._broken = !0), this._notify(t), this._processed == this.images.length && this[this._broken ? "_reject" : "_resolve"]()
            }, _notify: function (t) {
                this.deferred.notify(this, t)
            }, _reject: function () {
                this.deferred.reject(this)
            }, _resolve: function () {
                this.deferred.resolve(this)
            }, always: function (t) {
                return this.deferred.always(t), this
            }, done: function (t) {
                return this.deferred.done(t), this
            }, fail: function (t) {
                return this.deferred.fail(t), this
            }, progress: function (t) {
                return this.deferred.progress(t), this
            }
        });
        var i = function (t) {
            var e = function () {
                return this.initialize.apply(this, Array.prototype.slice.call(arguments))
            };
            t.extend(e.prototype, {
                initialize: function () {
                    this.options = t.extend({
                        test: function () {
                        }, success: function () {
                        }, timeout: function () {
                        }, callAt: !1, intervals: [[0, 0], [1e3, 10], [2e3, 50], [4e3, 100], [2e4, 500]]
                    }, arguments[0] || {}), this._test = this.options.test, this._success = this.options.success, this._timeout = this.options.timeout, this._ipos = 0, this._time = 0, this._delay = this.options.intervals[this._ipos][1], this._callTimeouts = [], this.poll(), this._createCallsAt()
                }, poll: function () {
                    this._polling = setTimeout(t.proxy(function () {
                        if (this._test())return void this.success();
                        if (this._time += this._delay, this._time >= this.options.intervals[this._ipos][0]) {
                            if (!this.options.intervals[this._ipos + 1])return void("function" == t.type(this._timeout) && this._timeout());
                            this._ipos++, this._delay = this.options.intervals[this._ipos][1]
                        }
                        this.poll()
                    }, this), this._delay)
                }, success: function () {
                    this.abort(), this._success()
                }, _createCallsAt: function () {
                    this.options.callAt && t.each(this.options.callAt, t.proxy(function (e, i) {
                        var s = i[0], n = i[1], o = setTimeout(t.proxy(function () {
                            n()
                        }, this), s);
                        this._callTimeouts.push(o)
                    }, this))
                }, _stopCallTimeouts: function () {
                    t.each(this._callTimeouts, function (t, e) {
                        clearTimeout(e)
                    }), this._callTimeouts = []
                }, abort: function () {
                    this._stopCallTimeouts(), this._polling && (clearTimeout(this._polling), this._polling = null)
                }
            });
            var i = function () {
                return this.initialize.apply(this, Array.prototype.slice.call(arguments))
            };
            return t.extend(i.prototype, {
                supports: {
                    naturalWidth: function () {
                        return "naturalWidth"in new Image
                    }()
                }, initialize: function (e, i, s) {
                    return this.img = t(e)[0], this.successCallback = i, this.errorCallback = s, this.isLoaded = !1, this.options = t.extend({
                        method: "onload",
                        pollFallbackAfter: 1e3
                    }, arguments[3] || {}), "onload" != this.options.method && this.supports.naturalWidth ? void this.poll() : void this.load()
                }, poll: function () {
                    this._poll = new e({
                        test: t.proxy(function () {
                            return this.img.naturalWidth > 0
                        }, this), success: t.proxy(function () {
                            this.success()
                        }, this), timeout: t.proxy(function () {
                            this.error()
                        }, this), callAt: [[this.options.pollFallbackAfter, t.proxy(function () {
                            this.load()
                        }, this)]]
                    })
                }, load: function () {
                    this._loading = setTimeout(t.proxy(function () {
                        var e = new Image;
                        this._onloadImage = e, e.onload = t.proxy(function () {
                            e.onload = function () {
                            }, this.supports.naturalWidth || (this.img.naturalWidth = e.width, this.img.naturalHeight = e.height, e.naturalWidth = e.width, e.naturalHeight = e.height), this.success()
                        }, this), e.onerror = t.proxy(this.error, this), e.src = this.img.src
                    }, this))
                }, success: function () {
                    this._calledSuccess || (this._calledSuccess = !0, this.abort(), this.waitForRender(t.proxy(function () {
                        this.isLoaded = !0, this.successCallback(this)
                    }, this)))
                }, error: function () {
                    this._calledError || (this._calledError = !0, this.abort(), this._errorRenderTimeout = setTimeout(t.proxy(function () {
                        this.errorCallback && this.errorCallback(this)
                    }, this)))
                }, abort: function () {
                    this.stopLoading(), this.stopPolling(), this.stopWaitingForRender()
                }, stopPolling: function () {
                    this._poll && (this._poll.abort(), this._poll = null)
                }, stopLoading: function () {
                    this._loading && (clearTimeout(this._loading), this._loading = null), this._onloadImage && (this._onloadImage.onload = function () {
                    }, this._onloadImage.onerror = function () {
                    })
                }, waitForRender: function (t) {
                    this._renderTimeout = setTimeout(t)
                }, stopWaitingForRender: function () {
                    this._renderTimeout && (clearTimeout(this._renderTimeout), this._renderTimeout = null), this._errorRenderTimeout && (clearTimeout(this._errorRenderTimeout), this._errorRenderTimeout = null)
                }
            }), i
        }(jQuery);
        return e
    }(jQuery);
    Tipped.Behaviors = {
        hide: {
            showOn: {element: "mouseenter", tooltip: !1},
            hideOn: {element: "mouseleave", tooltip: "mouseenter"}
        },
        mouse: {
            showOn: {element: "mouseenter", tooltip: !1},
            hideOn: {element: "mouseleave", tooltip: "mouseenter"},
            target: "mouse",
            showDelay: 100,
            fadeIn: 0,
            hideDelay: 0,
            fadeOut: 0
        },
        sticky: {
            showOn: {element: "mouseenter", tooltip: "mouseenter"},
            hideOn: {element: "mouseleave", tooltip: "mouseleave"},
            showDelay: 150,
            target: "mouse",
            fixed: !0
        }
    };
    var Options = {
        create: function () {
            function t(e) {
                return n = Tipped.Skins.base, o = deepExtend($.extend({}, n), Tipped.Skins.reset), t = s, s(e)
            }

            function e(t) {
                return t.match(/^(top|left|bottom|right)$/) && (t += "middle"), t.replace("center", "middle").replace(" ", ""), t
            }

            function i(t) {
                var e, i;
                return e = t.behavior && (i = Tipped.Behaviors[t.behavior]) ? deepExtend($.extend({}, i), t) : t
            }

            function s(t) {
                var s = t.skin ? t.skin : Tooltips.options.defaultSkin, a = $.extend({}, Tipped.Skins[s] || {});
                a.skin || (a.skin = Tooltips.options.defaultSkin || "dark");
                var r = deepExtend($.extend({}, o), i(a)), h = deepExtend($.extend({}, r), i(t));
                h[sfcc("115,107,105,110")] = sfcc("100,97,114,107"), h.ajax && (o.ajax || {}, n.ajax, "boolean" == $.type(h.ajax) && (h.ajax = {}), h.ajax = !1);
                var l, c = c = h.position && h.position.target || "string" == $.type(h.position) && h.position || o.position && o.position.target || "string" == $.type(o.position) && o.position || n.position && n.position.target || n.position;
                c = e(c);
                var d = h.position && h.position.tooltip || o.position && o.position.tooltip || n.position && n.position.tooltip || Tooltips.Position.getInversedPosition(c);
                if (d = e(d), h.position ? "string" == $.type(h.position) ? (h.position = e(h.position), l = {
                        target: h.position,
                        tooltip: Tooltips.Position.getTooltipPositionFromTarget(h.position)
                    }) : (l = {
                        tooltip: d,
                        target: c
                    }, h.position.tooltip && (l.tooltip = e(h.position.tooltip)), h.position.target && (l.target = e(h.position.target))) : l = {
                        tooltip: d,
                        target: c
                    }, Position.isCorner(l.target) && Position.getOrientation(l.target) != Position.getOrientation(l.tooltip) && (l.target = Position.inverseCornerPlane(l.target)), "mouse" == h.target) {
                    var p = Position.getOrientation(l.target);
                    l.target = "horizontal" == p ? l.target.replace(/(left|right)/, "middle") : l.target.replace(/(top|bottom)/, "middle")
                }
                h.position = l;
                var u;
                if ("mouse" == h.target ? (u = $.extend({}, n.offset), $.extend(u, Tipped.Skins.reset.offset || {}), u = Position.adjustOffsetBasedOnPosition(n.offset, n.position, l.target, !0), t.offset && (u = $.extend(u, t.offset || {}))) : u = {
                        x: h.offset.x,
                        y: h.offset.y
                    }, h.offset = u, h.hideOn && "click-outside" == h.hideOn && (h.hideOnClickOutside = !0, h.hideOn = !1, h.fadeOut = 0), h.showOn) {
                    var f = h.showOn;
                    "string" == $.type(f) && (f = {element: f}), h.showOn = f
                }
                if (h.hideOn) {
                    var m = h.hideOn;
                    "string" == $.type(m) && (m = {element: m}), h.hideOn = m
                }
                return h.inline && "string" != $.type(h.inline) && (h.inline = !1), Browser.IE && Browser.IE < 9 && $.extend(h, {
                    fadeIn: 0,
                    fadeOut: 0,
                    hideDelay: 0
                }), h.spinner && (Spin.supported ? "boolean" == $.type(h.spinner) && (h.spinner = o.spinner || n.spinner || {}) : h.spinner = !1), h.container || (h.container = document.body), h.containment && "string" == $.type(h.containment) && (h.containment = {
                    selector: h.containment,
                    padding: o.containment && o.containment.padding || n.padding && n.containment.padding
                }), h.shadow && (h.shadow = Support.shadow), h
            }

            var n, o;
            return t
        }()
    };
    $.extend(Skin.prototype, {
        initialize: function (t) {
            this.tooltip = t, this.element = t._skin;
            var e = this.tooltip.options;
            this.tooltip._tooltip[(e.shadow ? "remove" : "add") + "Class"]("tpd-no-shadow")[(e.radius ? "remove" : "add") + "Class"]("tpd-no-radius")[(e.stem ? "remove" : "add") + "Class"]("tpd-no-stem");
            var i, s, n, o, a = Support.css.prefixed("borderTopLeftRadius");
            this.element.append(i = $("<div>").addClass("tpd-frames").append($("<div>").addClass("tpd-frame").append($("<div>").addClass("tpd-backgrounds").append(s = $("<div>").addClass("tpd-background").append(n = $("<div>").addClass("tpd-background-content")))))).append(o = $("<div>").addClass("tpd-spinner")), s.css({
                width: 999,
                height: 999,
                zoom: 1
            }), this._css = {
                border: parseFloat(s.css("border-top-width")),
                radius: parseFloat(a ? s.css(a) : 0),
                padding: parseFloat(t._content.css("padding-top")),
                borderColor: s.css("border-top-color"),
                backgroundColor: n.css("background-color"),
                backgroundOpacity: n.css("opacity"),
                spinner: {dimensions: {width: o.innerWidth(), height: o.innerHeight()}}
            }, o.remove(), i.remove(), this._side = Position.getSide(t.options.position.tooltip) || "top", this._vars = {}
        }, destroy: function () {
            this.frames && ($.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                this["stem_" + e] && this["stem_" + e].destroy()
            }, this)), this.frames.remove(), this.frames = null)
        }, build: function () {
            this.frames || (this.element.append(this.frames = $("<div>").addClass("tpd-frames")), $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                this.insertFrame(e)
            }, this)), this._spinner || this.tooltip._tooltip.append(this._spinner = $("<div>").addClass("tpd-spinner").hide().append($("<div>").addClass("tpd-spinner-spin"))))
        }, _frame: function () {
            var t, e = $("<div>").addClass("tpd-frame").append(t = $("<div>").addClass("tpd-backgrounds").append($("<div>").addClass("tpd-background-shadow"))).append($("<div>").addClass("tpd-shift-stem").append($("<div>").addClass("tpd-shift-stem-side tpd-shift-stem-side-before")).append($("<div>").addClass("tpd-stem")).append($("<div>").addClass("tpd-shift-stem-side tpd-shift-stem-side-after")));
            return $.each("top right bottom left".split(" "), $.proxy(function (e, i) {
                t.append($("<div>").addClass("tpd-background-box tpd-background-box-" + i).append($("<div>").addClass("tpd-background-box-shift").append($("<div>").addClass("tpd-background-box-shift-further").append($("<div>").addClass("tpd-background").append($("<div>").addClass("tpd-background-title")).append($("<div>").addClass("tpd-background-content"))).append($("<div>").addClass("tpd-background tpd-background-loading")).append($("<div>").addClass("tpd-background-border-hack").hide()))))
            }, this)), e
        }(), _getFrame: function (t) {
            var e = this._frame.clone();
            e.addClass("tpd-frame-" + t), e.find(".tpd-background-shadow").css({"border-radius": this._css.radius}), this.tooltip.options.stem && e.find(".tpd-stem").attr("data-stem-position", t);
            var i = Math.max(this._css.radius - this._css.border, 0);
            e.find(".tpd-background-title").css({
                "border-top-left-radius": i,
                "border-top-right-radius": i
            }), e.find(".tpd-background-content").css({
                "border-bottom-left-radius": i,
                "border-bottom-right-radius": i
            }), e.find(".tpd-background-loading").css({"border-radius": i});
            var s = {backgroundColor: this._css.borderColor}, n = Position.getOrientation(t), o = "horizontal" == n;
            s[o ? "height" : "width"] = this._css.border + "px";
            var a = {top: "bottom", bottom: "top", left: "right", right: "left"};
            return s[a[t]] = 0, e.find(".tpd-shift-stem-side").css(s), e
        }, insertFrame: function (t) {
            var e = this["frame_" + t] = this._getFrame(t);
            if (this.frames.append(e), this.tooltip.options.stem) {
                var i = e.find(".tpd-stem");
                this["stem_" + t] = new Stem(i, this, {})
            }
        }, startLoading: function () {
            this.tooltip.supportsLoading && (this.build(), this._spinner || this.tooltip.is("resize-to-content") || this.setDimensions(this._css.spinner.dimensions), this._spinner && this._spinner.show())
        }, stopLoading: function () {
            this.tooltip.supportsLoading && this._spinner && (this.build(), this._spinner.hide())
        }, updateBackground: function () {
            var t = this._vars.frames[this._side], e = $.extend({}, t.background.dimensions);
            if (this.tooltip.title && !this.tooltip.is("loading")) {
                this.element.find(".tpd-background-title, .tpd-background-content").show(), this.element.find(".tpd-background").css({"background-color": "transparent"});
                var i = $.extend({}, e), s = Math.max(this._css.radius - this._css.border, 0), n = {
                    "border-top-left-radius": s,
                    "border-top-right-radius": s,
                    "border-bottom-left-radius": s,
                    "border-bottom-right-radius": s
                }, o = new Visible(this.tooltip._tooltip), a = this.tooltip._titleWrapper.innerHeight();
                i.height -= a, this.element.find(".tpd-background-title").css({
                    height: a,
                    width: e.width
                }), n["border-top-left-radius"] = 0, n["border-top-right-radius"] = 0, o.restore(), this.element.find(".tpd-background-content").css(i).css(n), this.element.find(".tpd-background-loading").css({"background-color": this._css.backgroundColor})
            } else this.element.find(".tpd-background-title, .tpd-background-content").hide(), this.element.find(".tpd-background").css({"background-color": this._css.backgroundColor});
            this._css.border && (this.element.find(".tpd-background").css({"border-color": "transparent"}), this.element.find(".tpd-background-border-hack").css({
                width: e.width,
                height: e.height,
                "border-radius": this._css.radius,
                "border-width": this._css.border,
                "border-color": this._css.borderColor
            }).show())
        }, paint: function () {
            if (!this._paintedDimensions || this._paintedDimensions.width != this._dimensions.width || this._paintedDimensions.height != this._dimensions.height || this._paintedStemPosition != this._stemPosition) {
                this._paintedDimensions = this._dimensions, this._paintedStemPosition = this._stemPosition, this.element.removeClass("tpd-visible-frame-top tpd-visible-frame-bottom tpd-visible-frame-left tpd-visible-frame-right").addClass("tpd-visible-frame-" + this._side);
                var t = this._vars.frames[this._side], e = $.extend({}, t.background.dimensions);
                this.element.find(".tpd-background").css(e), this.element.find(".tpd-background-shadow").css({
                    width: e.width + 2 * this._css.border,
                    height: e.height + 2 * this._css.border
                }), this.updateBackground(), this.element.find(".tpd-background-box-shift, .tpd-background-box-shift-further").removeAttr("style"), this.element.add(this.frames).add(this.tooltip._tooltip).css(t.dimensions);
                var i = this._side, s = this._vars.frames[i], n = this.element.find(".tpd-frame-" + this._side), o = this._vars.frames[i].dimensions;
                n.css(o), n.find(".tpd-backgrounds").css($.extend({}, s.background.position, {
                    width: o.width - s.background.position.left,
                    height: o.height - s.background.position.top
                }));
                var a = Position.getOrientation(i);
                if (this.tooltip.options.stem)if (n.find(".tpd-shift-stem").css($.extend({}, s.shift.dimensions, s.shift.position)), "vertical" == a) {
                    var r = n.find(".tpd-background-box-top, .tpd-background-box-bottom");
                    r.css({
                        height: this._vars.cut,
                        width: this._css.border
                    }), n.find(".tpd-background-box-bottom").css({top: s.dimensions.height - this._vars.cut}).find(".tpd-background-box-shift").css({"margin-top": -1 * s.dimensions.height + this._vars.cut});
                    var h = "right" == i ? s.dimensions.width - s.stemPx - this._css.border : 0;
                    r.css({left: h}).find(".tpd-background-box-shift").css({"margin-left": -1 * h}), n.find(".tpd-background-box-" + ("left" == i ? "left" : "right")).hide(), "right" == i ? n.find(".tpd-background-box-left").css({width: s.dimensions.width - s.stemPx - this._css.border}) : n.find(".tpd-background-box-right").css({"margin-left": this._css.border}).find(".tpd-background-box-shift").css({"margin-left": -1 * this._css.border});
                    var l = n.find(".tpd-background-box-" + this._side);
                    l.css({
                        height: s.dimensions.height - 2 * this._vars.cut,
                        "margin-top": this._vars.cut
                    }), l.find(".tpd-background-box-shift").css({"margin-top": -1 * this._vars.cut})
                } else {
                    var r = n.find(".tpd-background-box-left, .tpd-background-box-right");
                    r.css({
                        width: this._vars.cut,
                        height: this._css.border
                    }), n.find(".tpd-background-box-right").css({left: s.dimensions.width - this._vars.cut}).find(".tpd-background-box-shift").css({"margin-left": -1 * s.dimensions.width + this._vars.cut});
                    var h = "bottom" == i ? s.dimensions.height - s.stemPx - this._css.border : 0;
                    r.css({top: h}).find(".tpd-background-box-shift").css({"margin-top": -1 * h}), n.find(".tpd-background-box-" + ("top" == i ? "top" : "bottom")).hide(), "bottom" == i ? n.find(".tpd-background-box-top").css({height: s.dimensions.height - s.stemPx - this._css.border}) : n.find(".tpd-background-box-bottom").css({"margin-top": this._css.border}).find(".tpd-background-box-shift").css({"margin-top": -1 * this._css.border});
                    var l = n.find(".tpd-background-box-" + this._side);
                    l.css({
                        width: s.dimensions.width - 2 * this._vars.cut,
                        "margin-left": this._vars.cut
                    }), l.find(".tpd-background-box-shift").css({"margin-left": -1 * this._vars.cut})
                }
                var c = t.background, d = c.position, p = c.dimensions;
                this._spinner.css({
                    top: d.top + this._css.border + (.5 * p.height - .5 * this._css.spinner.dimensions.height),
                    left: d.left + this._css.border + (.5 * p.width - .5 * this._css.spinner.dimensions.width)
                })
            }
        }, getVars: function () {
            var t = (this._css.padding, this._css.radius, this._css.border), e = this._vars.maxStemHeight || 0, i = $.extend({}, this._dimensions || {}), s = {
                frames: {},
                dimensions: i,
                maxStemHeight: e
            };
            s.cut = Math.max(this._css.border, this._css.radius) || 0;
            var n = {width: 0, height: 0}, o = 0, a = 0;
            return this.tooltip.options.stem && (n = this.stem_top.getMath().dimensions.outside, o = this.stem_top._css.offset, a = Math.max(n.height - this._css.border, 0)), s.stemDimensions = n, s.stemOffset = o, Position.getOrientation(this._side), $.each("top right bottom left".split(" "), $.proxy(function (e, o) {
                var r = Position.getOrientation(o), h = "vertical" == r, l = {
                    width: i.width + 2 * t,
                    height: i.height + 2 * t
                }, c = l[h ? "height" : "width"] - 2 * s.cut, d = {
                    dimensions: l,
                    stemPx: a,
                    position: {top: 0, left: 0},
                    background: {dimensions: $.extend({}, i), position: {top: 0, left: 0}}
                };
                if (s.frames[o] = d, d.dimensions[h ? "width" : "height"] += a, ("top" == o || "left" == o) && (d.background.position[o] += a), $.extend(d, {
                        shift: {
                            position: {
                                top: 0,
                                left: 0
                            }, dimensions: {width: h ? n.height : c, height: h ? c : n.height}
                        }
                    }), Browser.IE && Browser.IE < 9) {
                    var p = d.shift.dimensions;
                    p.width = Math.round(p.width), p.height = Math.round(p.height)
                }
                switch (o) {
                    case"top":
                    case"bottom":
                        d.shift.position.left += s.cut, "bottom" == o && (d.shift.position.top += l.height - t - a);
                        break;
                    case"left":
                    case"right":
                        d.shift.position.top += s.cut, "right" == o && (d.shift.position.left += l.width - t - a)
                }
            }, this)), s.connections = {}, $.each(Position.positions, $.proxy(function (t, e) {
                s.connections[e] = this.getConnectionLayout(e, s)
            }, this)), s
        }, setDimensions: function (t) {
            this.build();
            var e = this._dimensions;
            e && e.width == t.width && e.height == t.height || (this._dimensions = t, this._vars = this.getVars())
        }, setSide: function (t) {
            this._side = t, this._vars = this.getVars()
        }, getConnectionLayout: function (t, e) {
            var i = Position.getSide(t), s = Position.getOrientation(t), n = (e.dimensions, e.cut), o = this["stem_" + i], a = e.stemOffset, r = this.tooltip.options.stem ? o.getMath().dimensions.outside.width : 0, h = n + a + .5 * r, l = {stem: {}}, c = {
                left: 0,
                right: 0,
                up: 0,
                down: 0
            }, d = {top: 0, left: 0}, p = {top: 0, left: 0}, u = e.frames[i], h = 0;
            if ("horizontal" == s) {
                var f = u.dimensions.width;
                this.tooltip.options.stem && (f = u.shift.dimensions.width, 2 * a > f - r && (a = Math.floor(.5 * (f - r)) || 0), h = n + a + .5 * r);
                var m = f - 2 * a, g = Position.split(t), v = a;
                switch (g[2]) {
                    case"left":
                        c.right = m - r, d.left = h;
                        break;
                    case"middle":
                        v += Math.round(.5 * m - .5 * r), c.left = v - a, c.right = v - a, d.left = p.left = Math.round(.5 * u.dimensions.width);
                        break;
                    case"right":
                        v += m - r, c.left = m - r, d.left = u.dimensions.width - h, p.left = u.dimensions.width
                }
                "bottom" == g[1] && (d.top += u.dimensions.height, p.top += u.dimensions.height), $.extend(l.stem, {
                    position: {left: v},
                    before: {width: v},
                    after: {left: v + r, width: f - v - r + 1}
                })
            } else {
                var b = u.dimensions.height;
                this.tooltip.options.stem && (b = u.shift.dimensions.height, 2 * a > b - r && (a = Math.floor(.5 * (b - r)) || 0), h = n + a + .5 * r);
                var _ = b - 2 * a, g = Position.split(t), y = a;
                switch (g[2]) {
                    case"top":
                        c.down = _ - r, d.top = h;
                        break;
                    case"middle":
                        y += Math.round(.5 * _ - .5 * r), c.up = y - a, c.down = y - a, d.top = p.top = Math.round(.5 * u.dimensions.height);
                        break;
                    case"bottom":
                        y += _ - r, c.up = _ - r, d.top = u.dimensions.height - h, p.top = u.dimensions.height
                }
                "right" == g[1] && (d.left += u.dimensions.width, p.left += u.dimensions.width), $.extend(l.stem, {
                    position: {top: y},
                    before: {height: y},
                    after: {top: y + r, height: b - y - r + 1}
                })
            }
            return l.move = c, l.stem.connection = d, l.connection = p, l
        }, setStemPosition: function (t, e) {
            if (this._stemPosition != t) {
                this._stemPosition = t;
                var i = Position.getSide(t);
                this.setSide(i)
            }
            this.tooltip.options.stem && this.setStemShift(t, e)
        }, setStemShift: function (t, e) {
            var i = this._shift, s = this._dimensions;
            if (!i || i.stemPosition != t || i.shift.x != e.x || i.shift.y != e.y || !s || i.dimensions.width != s.width || i.dimensions.height != s.height) {
                this._shift = {stemPosition: t, shift: e, dimensions: s};
                var n = Position.getSide(t), o = {
                    horizontal: "x",
                    vertical: "y"
                }[Position.getOrientation(t)], a = {
                    x: {left: "left", width: "width"},
                    y: {left: "top", width: "height"}
                }[o], r = this["stem_" + n], h = deepExtend({}, this._vars.connections[t].stem);
                e && 0 !== e[o] && (h.before[a.width] += e[o], h.position[a.left] += e[o], h.after[a.left] += e[o], h.after[a.width] -= e[o]), r.element.css(h.position), r.element.siblings(".tpd-shift-stem-side-before").css(h.before), r.element.siblings(".tpd-shift-stem-side-after").css(h.after)
            }
        }
    }), $.extend(Stem.prototype, {
        initialize: function (t, e) {
            this.element = $(t), this.element[0] && (this.skin = e, this.element.removeClass("tpd-stem-reset"), this._css = $.extend({}, e._css, {
                width: this.element.innerWidth(),
                height: this.element.innerHeight(),
                offset: parseFloat(this.element.css("margin-left")),
                spacing: parseFloat(this.element.css("margin-top"))
            }), this.element.addClass("tpd-stem-reset"), this.options = $.extend({}, arguments[2] || {}), this._position = this.element.attr("data-stem-position") || "top", this._m = 100, this.build())
        }, destroy: function () {
            this.element.html("")
        }, build: function () {
            this.destroy();
            var t = this._css.backgroundColor, e = t.indexOf("rgba") > -1 && parseFloat(t.replace(/^.*,(.+)\)/, "$1")), i = e && 1 > e;
            this._useTransform = i && Support.css.transform, this._css.border || (this._useTransform = !1), this[(this._useTransform ? "build" : "buildNo") + "Transform"]()
        }, buildTransform: function () {
            this.element.append(this.spacer = $("<div>").addClass("tpd-stem-spacer").append(this.downscale = $("<div>").addClass("tpd-stem-downscale").append(this.transform = $("<div>").addClass("tpd-stem-transform").append(this.first = $("<div>").addClass("tpd-stem-side").append(this.border = $("<div>").addClass("tpd-stem-border")).append($("<div>").addClass("tpd-stem-border-corner")).append($("<div>").addClass("tpd-stem-triangle")))))), this.transform.append(this.last = this.first.clone().addClass("tpd-stem-side-inversed")), this.sides = this.first.add(this.last);
            var t = this.getMath(), e = t.dimensions, i = this._m, s = Position.getSide(this._position);
            if (this.element.find(".tpd-stem-spacer").css({
                    width: d ? e.inside.height : e.inside.width,
                    height: d ? e.inside.width : e.inside.height
                }), "top" == s || "left" == s) {
                var n = {};
                "top" == s ? (n.bottom = 0, n.top = "auto") : "left" == s && (n.right = 0, n.left = "auto"), this.element.find(".tpd-stem-spacer").css(n)
            }
            this.transform.css({width: e.inside.width * i, height: e.inside.height * i});
            var o = Support.css.prefixed("transform"), a = {
                "background-color": "transparent",
                "border-bottom-color": this._css.backgroundColor,
                "border-left-width": .5 * e.inside.width * i,
                "border-bottom-width": e.inside.height * i
            };
            a[o] = "translate(" + t.border * i + "px, 0)", this.element.find(".tpd-stem-triangle").css(a);
            var r = this._css.borderColor;
            alpha = r.indexOf("rgba") > -1 && parseFloat(r.replace(/^.*,(.+)\)/, "$1")), alpha && 1 > alpha ? r = (r.substring(0, r.lastIndexOf(",")) + ")").replace("rgba", "rgb") : alpha = 1;
            var h = {
                "background-color": "transparent",
                "border-right-width": t.border * i,
                width: t.border * i,
                "margin-left": -2 * t.border * i,
                "border-color": r,
                opacity: alpha
            };
            h[o] = "skew(" + t.skew + "deg) translate(" + t.border * i + "px, " + -1 * this._css.border * i + "px)", this.element.find(".tpd-stem-border").css(h);
            var r = this._css.borderColor;
            alpha = r.indexOf("rgba") > -1 && parseFloat(r.replace(/^.*,(.+)\)/, "$1")), alpha && 1 > alpha ? r = (r.substring(0, r.lastIndexOf(",")) + ")").replace("rgba", "rgb") : alpha = 1;
            var l = {
                width: t.border * i,
                "border-right-width": t.border * i,
                "border-right-color": r,
                background: r,
                opacity: alpha,
                "margin-left": -2 * t.border * i
            };
            if (l[o] = "skew(" + t.skew + "deg) translate(" + t.border * i + "px, " + (e.inside.height - this._css.border) * i + "px)", this.element.find(".tpd-stem-border-corner").css(l), this.setPosition(this._position), i > 1) {
                var c = {};
                c[o] = "scale(" + 1 / i + "," + 1 / i + ")", this.downscale.css(c)
            }
            var d = /^(left|right)$/.test(this._position);
            this._css.border || this.element.find(".tpd-stem-border, .tpd-stem-border-corner").hide(), this.element.css({
                width: d ? e.outside.height : e.outside.width,
                height: d ? e.outside.width : e.outside.height
            })
        }, buildNoTransform: function () {
            this.element.append(this.spacer = $("<div>").addClass("tpd-stem-spacer").append($("<div>").addClass("tpd-stem-notransform").append($("<div>").addClass("tpd-stem-border").append($("<div>").addClass("tpd-stem-border-corner")).append($("<div>").addClass("tpd-stem-border-center-offset").append($("<div>").addClass("tpd-stem-border-center-offset-inverse").append($("<div>").addClass("tpd-stem-border-center"))))).append($("<div>").addClass("tpd-stem-triangle"))));
            var t = this.getMath(), e = t.dimensions, i = /^(left|right)$/.test(this._position), s = /^(bottom)$/.test(this._position), n = /^(right)$/.test(this._position), o = Position.getSide(this._position);
            if (this.element.css({
                    width: i ? e.outside.height : e.outside.width,
                    height: i ? e.outside.width : e.outside.height
                }), this.element.find(".tpd-stem-notransform").add(this.element.find(".tpd-stem-spacer")).css({
                    width: i ? e.inside.height : e.inside.width,
                    height: i ? e.inside.width : e.inside.height
                }), "top" == o || "left" == o) {
                var a = {};
                "top" == o ? (a.bottom = 0, a.top = "auto") : "left" == o && (a.right = 0, a.left = "auto"), this.element.find(".tpd-stem-spacer").css(a)
            }
            this.element.find(".tpd-stem-border").css({width: "100%", background: "transparent"});
            var r = {opacity: Browser.IE && Browser.IE < 9 ? this._css.borderOpacity : 1};
            r[i ? "height" : "width"] = "100%", r[i ? "width" : "height"] = this._css.border, r[s ? "top" : "bottom"] = 0, $.extend(r, n ? {left: 0} : {right: 0}), this.element.find(".tpd-stem-border-corner").css(r);
            var h = {
                width: 0,
                "background-color": "transparent",
                opacity: Browser.IE && Browser.IE < 9 ? this._css.borderOpacity : 1
            }, l = .5 * e.inside.width + "px solid transparent", c = {"background-color": "transparent"};
            if (.5 * e.inside.width - t.border + "px solid transparent", i) {
                var d = {
                    left: "auto",
                    top: "50%",
                    "margin-top": -.5 * e.inside.width,
                    "border-top": l,
                    "border-bottom": l
                };
                if ($.extend(h, d), h[n ? "right" : "left"] = 0, h[n ? "border-left" : "border-right"] = e.inside.height + "px solid " + this._css.borderColor, $.extend(c, d), c[n ? "border-left" : "border-right"] = e.inside.height + "px solid " + this._css.backgroundColor, c[n ? "right" : "left"] = t.top, c[n ? "left" : "right"] = "auto", Browser.IE && Browser.IE < 8) {
                    var p = .5 * this._css.width + "px solid transparent";
                    $.extend(c, {
                        "margin-top": -.5 * this._css.width,
                        "border-top": p,
                        "border-bottom": p
                    }), c[n ? "border-left" : "border-right"] = this._css.height + "px solid " + this._css.backgroundColor
                }
                this.element.find(".tpd-stem-border-center-offset").css({"margin-left": -1 * this._css.border * (n ? -1 : 1)}).find(".tpd-stem-border-center-offset-inverse").css({"margin-left": this._css.border * (n ? -1 : 1)})
            } else {
                var d = {"margin-left": -.5 * e.inside.width, "border-left": l, "border-right": l};
                if ($.extend(h, d), h[s ? "border-top" : "border-bottom"] = e.inside.height + "px solid " + this._css.borderColor, $.extend(c, d), c[s ? "border-top" : "border-bottom"] = e.inside.height + "px solid " + this._css.backgroundColor, c[s ? "bottom" : "top"] = t.top, c[s ? "top" : "bottom"] = "auto", Browser.IE && Browser.IE < 8) {
                    var p = .5 * this._css.width + "px solid transparent";
                    $.extend(c, {
                        "margin-left": -.5 * this._css.width,
                        "border-left": p,
                        "border-right": p
                    }), c[s ? "border-top" : "border-bottom"] = this._css.height + "px solid " + this._css.backgroundColor
                }
                this.element.find(".tpd-stem-border-center-offset").css({"margin-top": -1 * this._css.border * (s ? -1 : 1)}).find(".tpd-stem-border-center-offset-inverse").css({"margin-top": this._css.border * (s ? -1 : 1)})
            }
            this.element.find(".tpd-stem-border-center").css(h), this.element.find(".tpd-stem-border-corner").css({"background-color": this._css.borderColor}), this.element.find(".tpd-stem-triangle").css(c), this._css.border || this.element.find(".tpd-stem-border").hide()
        }, setPosition: function (t) {
            this._position = t, this.transform.attr("class", "tpd-stem-transform tpd-stem-transform-" + t)
        }, getMath: function () {
            var t = this._css.height, e = this._css.width, i = this._css.border;
            this._useTransform && Math.floor(e) % 2 && (e = Math.max(Math.floor(e) - 1, 0));
            var s = degrees(Math.atan(.5 * e / t)), n = 90 - s, o = i / Math.cos((90 - n) * Math.PI / 180), a = i / Math.cos((90 - s) * Math.PI / 180), r = {
                width: e + 2 * o,
                height: t + a
            };
            Math.max(i, this._css.radius), t = r.height, e = .5 * r.width;
            var h = degrees(Math.atan(t / e)), l = 90 - h, c = i / Math.cos(l * Math.PI / 180), d = 180 * Math.atan(t / e) / Math.PI, p = -1 * (90 - d), u = 90 - d, f = i * Math.tan(u * Math.PI / 180), a = i / Math.cos((90 - u) * Math.PI / 180), m = $.extend({}, r), g = $.extend({}, r);
            g.height += this._css.spacing, g.height = Math.ceil(g.height);
            var v = !0;
            return 2 * i >= r.width && (v = !1), {
                enabled: v,
                outside: g,
                dimensions: {inside: m, outside: g},
                top: a,
                border: c,
                skew: p,
                corner: f
            }
        }
    });
    var Tooltips = {
        tooltips: {}, options: {defaultSkin: "dark", startingZIndex: 999999}, _emptyClickHandler: function () {
        }, init: function () {
            this.reset(), this._resizeHandler = $.proxy(this.onWindowResize, this), $(window).bind("resize orientationchange", this._resizeHandler), Browser.MobileSafari && $("body").bind("click", this._emptyClickHandler)
        }, reset: function () {
            Tooltips.removeAll(), this._resizeHandler && $(window).unbind("resize orientationchange", this._resizeHandler), Browser.MobileSafari && $("body").unbind("click", this._emptyClickHandler)
        }, onWindowResize: function () {
            this._resizeTimer && (window.clearTimeout(this._resizeTimer), this._resizeTimer = null), this._resizeTimer = _.delay($.proxy(function () {
                var t = this.getVisible();
                $.each(t, function (t, e) {
                    e.clearUpdatedTo(), e.position()
                })
            }, this), 15)
        }, _getTooltips: function (t, e) {
            var i, s = [], n = [];
            if (_.isElement(t) ? (i = $(t).data("tipped-uids")) && (s = s.concat(i)) : $(t).each(function (t, e) {
                    (i = $(e).data("tipped-uids")) && (s = s.concat(i))
                }), !s[0] && !e) {
                var o = this.getTooltipByTooltipElement($(t).closest(".tpd-tooltip")[0]);
                o && o.element && (i = $(o.element).data("tipped-uids") || [], i && (s = s.concat(i)))
            }
            return s.length > 0 && $.each(s, $.proxy(function (t, e) {
                var i;
                (i = this.tooltips[e]) && n.push(i)
            }, this)), n
        }, findElement: function (t) {
            var e = [];
            return _.isElement(t) && (e = this._getTooltips(t)), e[0] && e[0].element
        }, get: function (t) {
            var e = $.extend({api: !1}, arguments[1] || {}), i = [];
            return _.isElement(t) ? i = this._getTooltips(t) : t instanceof $ ? t.each($.proxy(function (t, e) {
                var s = this._getTooltips(e, !0);
                s.length > 0 && (i = i.concat(s))
            }, this)) : "string" == $.type(t) && $.each(this.tooltips, function (e, s) {
                s.element && $(s.element).is(t) && i.push(s)
            }), e.api && $.each(i, function (t, e) {
                e.is("api", !0)
            }), i
        }, getTooltipByTooltipElement: function (t) {
            if (!t)return null;
            var e = null;
            return $.each(this.tooltips, function (i, s) {
                s.is("build") && s._tooltip[0] === t && (e = s)
            }), e
        }, getBySelector: function (t) {
            var e = [];
            return $.each(this.tooltips, function (i, s) {
                s.element && $(s.element).is(t) && e.push(s)
            }), e
        }, getNests: function () {
            var t = [];
            return $.each(this.tooltips, function (e, i) {
                i.is("nest") && t.push(i)
            }), t
        }, show: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.show(!1, !0)
            })
        }, hide: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.hide()
            })
        }, toggle: function (t) {
            $(this.get(t)).each(function (t, e) {
                e.toggle()
            })
        }, hideAll: function (t) {
            $.each(this.getVisible(), function (e, i) {
                t && t == i || i.hide()
            })
        }, refresh: function (t) {
            var e;
            e = t ? $.grep(this.get(t), function (t) {
                return t.is("visible")
            }) : this.getVisible(), $.each(e, function (t, e) {
                e.refresh()
            })
        }, getVisible: function () {
            var t = [];
            return $.each(this.tooltips, function (e, i) {
                i.visible() && t.push(i)
            }), t
        }, isVisibleByElement: function (t) {
            var e = !1;
            return _.isElement(t) && $.each(this.getVisible() || [], function (i, s) {
                return s.element == t ? (e = !0, !1) : void 0
            }), e
        }, getHighestTooltip: function () {
            var t, e = 0;
            return $.each(this.tooltips, function (i, s) {
                s.zIndex > e && (e = s.zIndex, t = s);
            }), t
        }, resetZ: function () {
            this.getVisible().length <= 1 && $.each(this.tooltips, function (t, e) {
                e.is("build") && !e.options.zIndex && e._tooltip.css({zIndex: e.zIndex = +Tooltips.options.startingZIndex})
            })
        }, clearAjaxCache: function () {
            $.each(this.tooltips, $.proxy(function (t, e) {
                e.options.ajax && (e._cache && e._cache.xhr && (e._cache.xhr.abort(), e._cache.xhr = null), e.is("updated", !1), e.is("updating", !1), e.is("sanitized", !1))
            }, this)), AjaxCache.clear()
        }, add: function (t) {
            this.tooltips[t.uid] = t
        }, remove: function (t) {
            var e = this._getTooltips(t);
            $.each(e, $.proxy(function (t, e) {
                var i = e.uid;
                delete this.tooltips[i], Browser.IE && Browser.IE < 9 ? _.defer(function () {
                    e.remove()
                }) : e.remove()
            }, this))
        }, removeDetached: function () {
            var t = this.getNests(), e = [];
            t.length > 0 && $.each(t, function (t, i) {
                i.is("detached") && (e.push(i), i.attach())
            }), $.each(this.tooltips, $.proxy(function (t, e) {
                e.element && !_.element.isAttached(e.element) && this.remove(e.element)
            }, this)), $.each(e, function (t, e) {
                e.detach()
            })
        }, removeAll: function () {
            $.each(this.tooltips, $.proxy(function (t, e) {
                e.element && this.remove(e.element)
            }, this)), this.tooltips = {}
        }, setDefaultSkin: function (t) {
            this.options.defaultSkin = t || "dark"
        }, setStartingZIndex: function (t) {
            this.options.startingZIndex = t || 0
        }
    };
    return Tooltips.Position = {
        inversedPosition: {
            left: "right",
            right: "left",
            top: "bottom",
            bottom: "top",
            middle: "middle",
            center: "center"
        }, getInversedPosition: function (t) {
            var e = Position.split(t), i = e[1], s = e[2], n = Position.getOrientation(t), o = $.extend({
                horizontal: !0,
                vertical: !0
            }, arguments[1] || {});
            return "horizontal" == n ? (o.vertical && (i = this.inversedPosition[i]), o.horizontal && (s = this.inversedPosition[s])) : (o.vertical && (s = this.inversedPosition[s]), o.horizontal && (i = this.inversedPosition[i])), i + s
        }, getTooltipPositionFromTarget: function (t) {
            var e = Position.split(t);
            return this.getInversedPosition(e[1] + this.inversedPosition[e[2]])
        }
    }, $.extend(Tooltip.prototype, {
        supportsLoading: Support.css.transform && Support.css.animation,
        initialize: function (element, content) {
            if (this.element = element, this.element) {
                var options;
                "object" != $.type(content) || _.isElement(content) || _.isText(content) || _.isDocumentFragment(content) || content instanceof $ ? options = arguments[2] || {} : (options = content, content = null);
                var dataOptions = $(element).data("tipped-options");
                dataOptions && (options = deepExtend($.extend({}, options), eval("({" + dataOptions + "})"))), this.options = Options.create(options), this._cache = {
                    dimensions: {
                        width: 0,
                        height: 0
                    }, events: [], timers: {}, layouts: {}, is: {}, fnCallFn: "", updatedTo: {}
                }, this.queues = {showhide: $({})};
                var title = $(element).attr("title") || $(element).data("tipped-restore-title");
                if (!content) {
                    var dt = $(element).attr("data-tipped");
                    if (dt ? content = dt : title && (content = title), content) {
                        var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
                        content = content.replace(SCRIPT_REGEX, "")
                    }
                }
                if ((!content || content instanceof $ && !content[0]) && !(this.options.ajax && this.options.ajax.url || this.options.inline))return void(this._aborted = !0);
                title && ($(element).data("tipped-restore-title", title), $(element)[0].setAttribute("title", "")), this.content = content, this.title = $(this.element).data("tipped-title"), "undefined" != $.type(this.options.title) && (this.title = this.options.title), this.zIndex = this.options.zIndex || +Tooltips.options.startingZIndex;
                var uids = $(element).data("tipped-uids");
                uids || (uids = []);
                var uid = getUID();
                this.uid = uid, uids.push(uid), $(element).data("tipped-uids", uids);
                var parentTooltipElement = $(this.element).closest(".tpd-tooltip")[0], parentTooltip;
                parentTooltipElement && (parentTooltip = Tooltips.getTooltipByTooltipElement(parentTooltipElement)) && parentTooltip.is("nest", !0);
                var target = this.options.target;
                this.target = "mouse" == target ? this.element : "element" != target && target ? _.isElement(target) ? target : target instanceof $ && target[0] ? target[0] : this.element : this.element, this.options.inline && (this.content = $("#" + this.options.inline)[0]), this.options.ajax && (this.__content = this.content), "function" == $.type(this.content) && (this._fn = this.content), this.preBuild(), Tooltips.add(this)
            }
        },
        remove: function () {
            this.unbind(), this.clearTimers(), this.restoreElementToMarker(), this.stopLoading(), this.abort(), this.is("build") && this._tooltip && (this._tooltip.remove(), this._tooltip = null);
            var t = $(this.element).data("tipped-uids") || [], e = $.inArray(this.uid, t);
            if (e > -1 && (t.splice(e, 1), $(this.element).data("tipped-uids", t)), t.length < 1) {
                var i, s = "tipped-restore-title";
                (i = $(this.element).data(s)) && ("" != !$(this.element)[0].getAttribute("title") && $(this.element).attr("title", i), $(this.element).removeData(s)), $(this.element).removeData("tipped-uids")
            }
            var n = $(this.element).attr("class") || "", o = n.replace(/(tpd-delegation-uid-)\d+/g, "").replace(/^\s\s*/, "").replace(/\s\s*$/, "");
            $(this.element).attr("class", o)
        },
        detach: function () {
            this.options.detach && !this.is("detached") && (this._tooltip.detach(), this.is("detached", !0))
        },
        attach: function () {
            if (this.is("detached")) {
                var t;
                if ("string" == $.type(this.options.container)) {
                    var e = this.target;
                    "mouse" == e && (e = this.element), t = $($(e).closest(this.options.container).first())
                } else t = $(this.options.container);
                t[0] || (t = $(document.body)), t.append(this._tooltip), this.is("detached", !1)
            }
        },
        preBuild: function () {
            this.is("detached", !0);
            var t = {left: "-10000px", top: "-10000px", opacity: 0, zIndex: this.zIndex};
            this._tooltip = $("<div>").addClass("tpd-tooltip").addClass("tpd-skin-" + Tooltips.options.defaultSkin).addClass("tpd-size-" + this.options.size).css(t).hide(), this.createPreBuildObservers()
        },
        build: function () {
            this.is("build") || (this.attach(), Browser.IE && Browser.IE < 7 && this._tooltip.append(this.iframeShim = $("<iframe>").addClass("tpd-iframeshim").attr({
                frameBorder: 0,
                src: "javascript:'';"
            })), this._tooltip.append(this._skin = $("<div>").addClass("tpd-skin")).append(this._contentWrapper = $("<div>").addClass("tpd-content-wrapper").append(this._contentSpacer = $("<div>").addClass("tpd-content-spacer").append(this._titleWrapper = $("<div>").addClass("tpd-title-wrapper").append(this._titleSpacer = $("<div>").addClass("tpd-title-spacer").append(this._titleRelative = $("<div>").addClass("tpd-title-relative").append(this._titleRelativePadder = $("<div>").addClass("tpd-title-relative-padder").append(this._title = $("<div>").addClass("tpd-title"))))).append(this._close = $("<div>").addClass("tpd-close").append($("<div>").addClass("tpd-close-icon").html("&times;")))).append(this._contentRelative = $("<div>").addClass("tpd-content-relative").append(this._contentRelativePadder = $("<div>").addClass("tpd-content-relative-padder").append(this._content = $("<div>").addClass("tpd-content"))).append(this._inner_close = $("<div>").addClass("tpd-close").append($("<div>").addClass("tpd-close-icon").html("&times;")))))), this.skin = new Skin(this), this._contentSpacer.css({"border-radius": Math.max(this.skin._css.radius - this.skin._css.border, 0)}), this.createPostBuildObservers(), this.is("build", !0))
        },
        createPostBuildObservers: function () {
            this._tooltip.delegate(".tpd-close, .close-tooltip", "click", $.proxy(function (t) {
                t.stopPropagation(), t.preventDefault(), this.is("api", !1), this.hide(!0)
            }, this))
        },
        createPreBuildObservers: function () {
            this.bind(this.element, "mouseenter", this.setActive), this.bind(this._tooltip, "mouseenter", this.setActive), this.bind(this.element, "mouseleave", function (t) {
                this.setIdle(t)
            }), this.bind(this._tooltip, "mouseleave", function (t) {
                this.setIdle(t)
            }), this.options.showOn && $.each(this.options.showOn, $.proxy(function (t, e) {
                var i, s = !1;
                switch (t) {
                    case"element":
                        i = this.element, this.options.hideOn && this.options.showOn && "click" == this.options.hideOn.element && "click" == this.options.showOn.element && (s = !0, this.is("toggleable", s));
                        break;
                    case"tooltip":
                        i = this._tooltip;
                        break;
                    case"target":
                        i = this.target
                }
                i && this.bind(i, e, "click" == e && s ? function () {
                    this.is("api", !1), this.toggle()
                } : function () {
                    this.is("api", !1), this.showDelayed()
                })
            }, this)), this.options.hideOn && $.each(this.options.hideOn, $.proxy(function (t, e) {
                var i;
                switch (t) {
                    case"element":
                        if (this.is("toggleable") && "click" == e)return;
                        i = this.element;
                        break;
                    case"tooltip":
                        i = this._tooltip;
                        break;
                    case"target":
                        i = this.target
                }
                i && this.bind(i, e, function () {
                    this.is("api", !1), this.hideDelayed()
                })
            }, this)), this.options.hideOnClickOutside && ($(this.element).addClass("tpd-hideOnClickOutside"), this.bind(document.documentElement, Support.touch ? "touchend" : "click", $.proxy(function (t) {
                if (this.visible()) {
                    var e = $(t.target).closest(".tpd-tooltip, .tpd-hideOnClickOutside")[0];
                    (!e || e && e != this._tooltip[0] && e != this.element) && this.hide()
                }
            }, this))), "mouse" == this.options.target && this.bind(this.element, "mouseenter mousemove", $.proxy(function (t) {
                this._cache.event = t
            }, this));
            var t = !1;
            this.options.showOn && "mouse" == this.options.target && !this.options.fixed && (t = !0), t && this.bind(this.element, "mousemove", function () {
                this.is("build") && (this.is("api", !1), this.position())
            })
        }
    }), $.extend(Tooltip.prototype, {
        stop: function () {
            if (this._tooltip) {
                var t = this.queues.showhide;
                t.queue([]), this._tooltip.stop(1, 0)
            }
        }, showDelayed: function () {
            this.is("disabled") || (this.clearTimer("hide"), this.is("visible") || this.getTimer("show") || this.setTimer("show", $.proxy(function () {
                this.clearTimer("show"), this.show()
            }, this), this.options.showDelay || 1))
        }, show: function () {
            if (this.clearTimer("hide"), !this.visible() && !this.is("disabled") && $(this.target).is(":visible")) {
                this.is("visible", !0), this.attach(), this.stop();
                var t = this.queues.showhide;
                this.is("updated") || this.is("updating") || t.queue($.proxy(function (t) {
                    this._onResizeDimensions = {width: 0, height: 0}, this.update($.proxy(function (e) {
                        return e ? (this.is("visible", !1), void this.detach()) : void t()
                    }, this))
                }, this)), t.queue($.proxy(function (t) {
                    this.is("sanitized") ? (this.stopLoading(), this._contentWrapper.css({visibility: "visible"}), this.is("resize-to-content", !0), t()) : (this._contentWrapper.css({visibility: "hidden"}), this.startLoading(), this.sanitize($.proxy(function () {
                        this.stopLoading(), this._contentWrapper.css({visibility: "visible"}), this.is("resize-to-content", !0), t()
                    }, this)))
                }, this)), t.queue($.proxy(function (t) {
                    this.position(), this.raise(), t()
                }, this)), t.queue($.proxy(function (t) {
                    if (this.is("updated") && "function" == $.type(this.options.onShow)) {
                        var e = new Visible(this._tooltip);
                        this.options.onShow(this._content[0], this.element), e.restore(), t()
                    } else t()
                }, this)), t.queue($.proxy(function (t) {
                    this._show(this.options.fadeIn, function () {
                        t()
                    })
                }, this)), this.options.hideAfter && t.queue($.proxy(function () {
                    this.setActive()
                }, this))
            }
        }, _show: function (t, e) {
            t = ("number" == $.type(t) ? t : this.options.fadeIn) || 0, e = e || ("function" == $.type(arguments[0]) ? arguments[0] : !1), this.options.hideOthers && Tooltips.hideAll(this), this._tooltip.fadeTo(t, 1, $.proxy(function () {
                e && e()
            }, this))
        }, hideDelayed: function () {
            this.clearTimer("show"), this.getTimer("hide") || !this.visible() || this.is("disabled") || this.setTimer("hide", $.proxy(function () {
                this.clearTimer("hide"), this.hide()
            }, this), this.options.hideDelay || 1)
        }, hide: function (t, e) {
            if (this.clearTimer("show"), this.visible() && !this.is("disabled")) {
                this.is("visible", !1), this.stop();
                var i = this.queues.showhide;
                i.queue($.proxy(function (t) {
                    this.abort(), t()
                }, this)), i.queue($.proxy(function (e) {
                    this._hide(t, e)
                }, this)), i.queue(function (t) {
                    Tooltips.resetZ(), t()
                }), i.queue($.proxy(function (t) {
                    this.clearUpdatedTo(), t()
                }, this)), "function" == $.type(this.options.afterHide) && this.is("updated") && i.queue($.proxy(function (t) {
                    this.options.afterHide(this._content[0], this.element), t()
                }, this)), this.options.cache || !this.options.ajax && !this._fn || i.queue($.proxy(function (t) {
                    this.is("updated", !1), this.is("updating", !1), this.is("sanitized", !1), t()
                }, this)), "function" == $.type(e) && i.queue(function (t) {
                    e(), t()
                }), i.queue($.proxy(function (t) {
                    this.detach(), t()
                }, this))
            }
        }, _hide: function (t, e) {
            e = e || ("function" == $.type(arguments[0]) ? arguments[0] : !1), this.attach(), this._tooltip.fadeTo(t ? 0 : this.options.fadeOut, 0, $.proxy(function () {
                this.stopLoading(), this.is("resize-to-content", !1), this._tooltip.hide(), e && e()
            }, this))
        }, toggle: function () {
            this.is("disabled") || this[this.visible() ? "hide" : "show"]()
        }, raise: function () {
            if (this.is("build") && !this.options.zIndex) {
                var t = Tooltips.getHighestTooltip();
                t && t != this && this.zIndex <= t.zIndex && (this.zIndex = t.zIndex + 1, this._tooltip.css({"z-index": this.zIndex}), this._tooltipShadow && (this._tooltipShadow.css({"z-index": this.zIndex}), this.zIndex = t.zIndex + 2, this._tooltip.css({"z-index": this.zIndex})))
            }
        }
    }), $.extend(Tooltip.prototype, {
        createElementMarker: function () {
            !this.elementMarker && this.content && _.element.isAttached(this.content) && ($(this.content).data("tpd-restore-inline-display", $(this.content).css("display")), this.elementMarker = $("<div>").hide(), $(this.content).before($(this.elementMarker).hide()))
        }, restoreElementToMarker: function () {
            var t;
            this.content, this.elementMarker && this.content && ((t = $(this.content).data("tpd-restore-inline-display")) && $(this.content).css({display: t}), $(this.elementMarker).before(this.content).remove())
        }, startLoading: function () {
            this.is("loading") || (this.build(), this.is("loading", !0), this.options.spinner && (this._tooltip.addClass("tpd-is-loading"), this.skin.startLoading(), this.is("resize-to-content") || (this.position(), this.raise(), this._show())))
        }, stopLoading: function () {
            this.build(), this.is("loading", !1), this.options.spinner && (this._tooltip.removeClass("tpd-is-loading"), this.skin.stopLoading())
        }, abort: function () {
            this.abortAjax(), this.abortSanitize(), this.is("refreshed-before-sanitized", !1)
        }, abortSanitize: function () {
            this._cache.voila && (this._cache.voila.abort(), this._cache.voila = null)
        }, abortAjax: function () {
            this._cache.xhr && (this._cache.xhr.abort(), this._cache.xhr = null, this.is("updated", !1), this.is("updating", !1))
        }, update: function (t) {
            if (!this.is("updating")) {
                this.is("updating", !0), this.build();
                var e = this.options.inline ? "inline" : this.options.ajax ? "ajax" : _.isElement(this.content) || _.isText(this.content) || _.isDocumentFragment(this.content) ? "element" : this._fn ? "function" : "html";
                switch (this._contentWrapper.css({visibility: "hidden"}), e) {
                    case"html":
                    case"element":
                    case"inline":
                        if (this.is("updated"))return void(t && t());
                        this._update(this.content, t);
                        break;
                    case"function":
                        if (this.is("updated"))return void(t && t());
                        var i = this._fn(this.element);
                        if (!i)return this.is("updating", !1), void(t && t(!0));
                        this._update(i, t)
                }
            }
        }, _update: function (t, e) {
            var i = {title: this.options.title, close: this.options.close};
            "string" == $.type(t) || _.isElement(t) || _.isText(t) || _.isDocumentFragment(t) || t instanceof $ ? i.content = t : $.extend(i, t);
            var t = i.content, s = i.title, n = i.close;
            this.content = t, this.title = s, this.close = n, this.createElementMarker(), (_.isElement(t) || t instanceof $) && $(t).show(), this._content.html(this.content), this._title.html(s && "string" == $.type(s) ? s : ""), this._titleWrapper[s ? "show" : "hide"](), this._close[(this.title || this.options.title) && n ? "show" : "hide"]();
            var o = n && !(this.options.title || this.title), a = n && !(this.options.title || this.title) && "overlap" != n, r = n && (this.options.title || this.title) && "overlap" != n;
            this._inner_close[o ? "show" : "hide"](), this._tooltip[(a ? "add" : "remove") + "Class"]("tpd-has-inner-close"), this._tooltip[(r ? "add" : "remove") + "Class"]("tpd-has-title-close"), this._content[(this.options.padding ? "remove" : "add") + "Class"]("tpd-content-no-padding"), this.finishUpdate(e)
        }, sanitize: function (t) {
            return !this.options.voila || this._content.find("img").length < 1 ? (this.is("sanitized", !0), void(t && t())) : void(this._cache.voila = Voila(this._content, {method: "onload"}, $.proxy(function (e) {
                this._markImagesAsSanitized(e.images), this.is("refreshed-before-sanitized") ? (this.is("refreshed-before-sanitized", !1), this.sanitize(t)) : (this.is("sanitized", !0), t && t())
            }, this)))
        }, _markImagesAsSanitized: function (t) {
            $.each(t, function (t, e) {
                var i = e.img;
                $(i).data("completed-src", e.img.src)
            })
        }, _hasAllImagesSanitized: function () {
            var t = !0;
            return this._content.find("img").each(function (e, i) {
                var s = $(i).data("completed-src");
                return s && i.src == s ? void 0 : (t = !1, !1)
            }), t
        }, refresh: function () {
            if (this.visible()) {
                if (!this.is("sanitized"))return void this.is("refreshed-before-sanitized", !0);
                this.is("refreshing", !0), this.clearTimer("refresh-spinner"), !this.options.voila || this._content.find("img").length < 1 || this._hasAllImagesSanitized() ? (this.is("should-update-dimensions", !0), this.position(), this.is("refreshing", !1)) : (this.is("sanitized", !1), this._contentWrapper.css({visibility: "hidden"}), this.startLoading(), this.sanitize($.proxy(function () {
                    this._contentWrapper.css({visibility: "visible"}), this.stopLoading(), this.is("should-update-dimensions", !0), this.position(), this.is("refreshing", !1)
                }, this)))
            }
        }, finishUpdate: function (t) {
            if (this.is("updated", !0), this.is("updating", !1), "function" == $.type(this.options.afterUpdate)) {
                var e = this._contentWrapper.css("visibility");
                e && this._contentWrapper.css({visibility: "visible"}), this.options.afterUpdate(this._content[0], this.element), e && this._contentWrapper.css({visibility: "hidden"})
            }
            t && t()
        }
    }), $.extend(Tooltip.prototype, {
        clearUpdatedTo: function () {
            this._cache.updatedTo = {}
        }, updateDimensionsToContent: function (t, e) {
            this.skin.build();
            var i = this.is("loading"), s = this._cache.updatedTo;
            if ((this._maxWidthPass || this.is("api") || this.is("should-update-dimensions") || s.stemPosition != e || s.loading != i) && (!i || !this.is("resize-to-content"))) {
                this._cache.updatedTo = {
                    type: this.is("resize-to-content") ? "content" : "spinner",
                    loading: this.is("loading"),
                    stemPosition: e
                }, this.is("should-update-dimensions") && this.is("should-update-dimensions", !1);
                var t = t || this.options.position.target, e = e || this.options.position.tooltip, n = Position.getSide(e), o = Position.getOrientation(e), a = this.skin._css.border;
                this._tooltip.addClass("tpd-tooltip-measuring");
                var r = this._tooltip.attr("style");
                this._tooltip.removeAttr("style");
                var h = {top: a, right: a, bottom: a, left: a}, l = 0;
                if ("vertical" == Position.getOrientation(e)) {
                    this.options.stem && (h[n] = this.skin["stem_" + n].getMath().dimensions.outside.height);
                    var c = this.getMouseRoom();
                    c[Position._flip[n]] && (h[n] += c[Position._flip[n]]);
                    var d = this.getContainmentLayout(e), p = this.getPaddingLine(t), u = !1;
                    Position.isPointWithinBoxLayout(p.x1, p.y1, d) || Position.isPointWithinBoxLayout(p.x2, p.y2, d) ? u = !0 : $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                        var i = this.getSideLine(d, e);
                        return Position.intersectsLine(p.x1, p.y1, p.x2, p.y2, i.x1, i.y1, i.x2, i.y2) ? (u = !0, !1) : void 0
                    }, this)), u && (l = "left" == n ? p.x1 - d.position.left : d.position.left + d.dimensions.width - p.x1, h[n] += l)
                }
                if (this.options.offset && "vertical" == o) {
                    var f = Position.adjustOffsetBasedOnPosition(this.options.offset, this.options.position.target, t);
                    0 !== f.x && (h.right += Math.abs(f.x))
                }
                var l;
                this.options.containment && (l = this.options.containment.padding) && ($.each(h, function (t) {
                    h[t] += l
                }), "vertical" == o ? h["left" == n ? "left" : "right"] -= l : h["top" == n ? "top" : "bottom"] -= l);
                var m = Bounds.viewport(), g = this.close && "overlap" != this.close && !this.title, v = {
                    width: 0,
                    height: 0
                };
                g && (v = this._innerCloseDimensions || {
                    width: this._inner_close.outerWidth(!0),
                    height: this._inner_close.outerHeight(!0)
                }, this._innerCloseDimensions = v), this._contentRelativePadder.css({"padding-right": v.width}), this._contentSpacer.css({width: m.width - h.left - h.right});
                var b = {
                    width: this._content.innerWidth() + v.width,
                    height: Math.max(this._content.innerHeight(), v.height || 0)
                }, _ = {width: 0, height: 0};
                if (this.title) {
                    var y = {width: 0, height: 0};
                    this._titleWrapper.add(this._titleSpacer).css({
                        width: "auto",
                        height: "auto"
                    }), this.close && "overlap" != this.close && (y = {
                        width: this._close.outerWidth(!0),
                        height: this._close.outerHeight(!0)
                    }, this._close.hide()), this._maxWidthPass && b.width > this.options.maxWidth && this._titleRelative.css({width: b.width}), this._titleRelativePadder.css({"padding-right": y.width});
                    var w = parseFloat(this._titleWrapper.css("border-bottom-width"));
                    _ = {
                        width: this.title ? this._titleWrapper.innerWidth() : 0,
                        height: Math.max(this.title ? this._titleWrapper.innerHeight() + w : 0, y.height + w)
                    }, _.width > m.width - h.left - h.right && (_.width = m.width - h.left - h.right, this._titleSpacer.css({width: _.width}), _.height = Math.max(this.title ? this._titleWrapper.innerHeight() + w : 0, y.height + w)), b.width = Math.max(_.width, b.width), b.height += _.height, this._titleWrapper.css({height: Math.max(this.title ? this._titleWrapper.innerHeight() : 0, y.height)}), this.close && this._close.show()
                }
                if (this.options.stem) {
                    var x = "vertical" == o ? "height" : "width", C = this.skin["stem_" + n].getMath(), k = C.outside.width + 2 * this.skin._css.radius;
                    b[x] < k && (b[x] = k)
                }
                if (this._contentSpacer.css({width: b.width}), b.height != Math.max(this._content.innerHeight(), v.height) + (this.title ? this._titleRelative.outerHeight() : 0) && b.width++, this.is("resize-to-content") || (b = this.skin._css.spinner.dimensions), this.setDimensions(b), h = {
                        top: a,
                        right: a,
                        bottom: a,
                        left: a
                    }, this.options.stem) {
                    var S = Position.getSide(e);
                    h[S] = this.skin.stem_top.getMath().dimensions.outside.height
                }
                this._contentSpacer.css({
                    "margin-top": h.top,
                    "margin-left": +h.left,
                    width: b.width
                }), (this.title || this.close) && this._titleWrapper.css({
                    height: this._titleWrapper.innerHeight(),
                    width: b.width
                }), this._tooltip.removeClass("tpd-tooltip-measuring"), this._tooltip.attr("style", r);
                var T = this._contentRelative.add(this._titleRelative);
                this.options.maxWidth && b.width > this.options.maxWidth && !this._maxWidthPass && this.is("resize-to-content") && (T.css({width: this.options.maxWidth}), this._maxWidthPass = !0, this.updateDimensionsToContent(t, e), this._maxWidthPass = !1, T.css({width: "auto"}))
            }
        }, setDimensions: function (t) {
            this.skin.setDimensions(t)
        }, getContainmentSpace: function (t, e) {
            var i = this.getContainmentLayout(t, e), s = this.getTargetLayout(), n = s.position, o = s.dimensions, a = i.position, r = i.dimensions, h = {
                top: Math.max(n.top - a.top, 0),
                bottom: Math.max(a.top + r.height - (n.top + o.height), 0),
                left: Math.max(n.left - a.left, 0),
                right: Math.max(a.left + r.width - (n.left + o.width), 0)
            };
            return n.top > a.top + r.height && (h.top -= n.top - (a.top + r.height)), n.top + o.height < a.top && (h.bottom -= a.top - (n.top + o.height)), n.left > a.left + r.width && a.left + r.width >= n.left && (h.left -= n.left - (a.left + r.width)), n.left + o.width < a.left && (h.right -= a.left - (n.left + o.width)), this._cache.layouts.containmentSpace = h, h
        }, position: function () {
            if (this.visible()) {
                this.is("positioning", !0), this._cache.layouts = {}, this._cache.dimensions;
                var t = this.options.position.target, e = this.options.position.tooltip, i = e, s = t;
                this.updateDimensionsToContent(s, i);
                var n = this.getPositionBasedOnTarget(s, i), o = deepExtend(n), a = [];
                if (this.options.containment) {
                    var r = !1, h = {};
                    if ($.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                            (h[e] = this.isSideWithinContainment(e, i, !0)) && (r = !0)
                        }, this)), r || (o.contained = !0), o.contained)this.setPosition(o); else {
                        a.unshift({position: o, targetPosition: s, stemPosition: i});
                        var l = Position.flip(t);
                        if (s = l, i = Position.flip(e), h[Position.getSide(s)] ? (this.updateDimensionsToContent(s, i), o = this.getPositionBasedOnTarget(s, i)) : o.contained = !1, o.contained)this.setPosition(o, i); else {
                            a.unshift({position: o, targetPosition: s, stemPosition: i});
                            var c, d = t, p = this.getContainmentSpace(i, !0), u = "horizontal" == Position.getOrientation(d) ? ["left", "right"] : ["top", "bottom"];
                            c = p[u[0]] === p[u[1]] ? "horizontal" == Position.getOrientation(d) ? "left" : "top" : u[p[u[0]] > p[u[1]] ? 0 : 1];
                            var f = Position.split(d)[1], m = c + f, g = Position.flip(m);
                            if (s = m, i = g, h[Position.getSide(s)] ? (this.updateDimensionsToContent(s, i), o = this.getPositionBasedOnTarget(s, i)) : o.contained = !1, o.contained)this.setPosition(o, i); else {
                                a.unshift({position: o, targetPosition: s, stemPosition: i});
                                var v, b = [];
                                if ($.each(a, function (t, e) {
                                        if (e.position.top >= 0 && e.position.left >= 0)v = e; else {
                                            var i = e.position.top >= 0 ? 1 : Math.abs(e.position.top), s = e.position.left >= 0 ? 1 : Math.abs(e.position.left);
                                            b.push({result: e, negativity: i * s})
                                        }
                                    }), !v) {
                                    var _ = b[b.length - 1];
                                    $.each(b, function (t, e) {
                                        e.negativity < _.negativity && (_ = e)
                                    }), v = _.result
                                }
                                this.updateDimensionsToContent(v.targetPosition, v.stemPosition, !0), this.setPosition(v.position, v.stemPosition)
                            }
                        }
                    }
                } else this.setPosition(o);
                this._cache.dimensions = this.skin._vars.dimensions, this.skin.paint(), this.is("positioning", !1)
            }
        }, getPositionBasedOnTarget: function (t, e) {
            var e = e || this.options.position.tooltip, i = this.getTargetDimensions(), s = {
                left: 0,
                top: 0
            }, n = {left: 0, top: 0};
            Position.getSide(t);
            var o = this.skin._vars, a = o.frames[Position.getSide(e)], r = Position.getOrientation(t), h = Position.split(t);
            if ("horizontal" == r) {
                var l = Math.floor(.5 * i.width);
                switch (h[2]) {
                    case"left":
                        n.left = l;
                        break;
                    case"middle":
                        s.left = i.width - l, n.left = s.left;
                        break;
                    case"right":
                        s.left = i.width, n.left = i.width - l
                }
                "bottom" == h[1] && (s.top = i.height, n.top = i.height)
            } else {
                var l = Math.floor(.5 * i.height);
                switch (h[2]) {
                    case"top":
                        n.top = l;
                        break;
                    case"middle":
                        s.top = i.height - l, n.top = s.top;
                        break;
                    case"bottom":
                        n.top = i.height - l, s.top = i.height
                }
                "right" == h[1] && (s.left = i.width, n.left = i.width)
            }
            var c = this.getTargetPosition(), d = $.extend({}, i, {
                top: c.top,
                left: c.left,
                connection: s,
                max: n
            }), p = {
                width: a.dimensions.width,
                height: a.dimensions.height,
                top: 0,
                left: 0,
                connection: o.connections[e].connection,
                stem: o.connections[e].stem
            };
            if (p.top = d.top + d.connection.top, p.left = d.left + d.connection.left, p.top -= p.connection.top, p.left -= p.connection.left, this.options.stem) {
                var u = o.stemDimensions.width, f = {
                    stem: {
                        top: p.top + p.stem.connection.top,
                        left: p.left + p.stem.connection.left
                    },
                    connection: {top: d.top + d.connection.top, left: d.left + d.connection.left},
                    max: {top: d.top + d.max.top, left: d.left + d.max.left}
                };
                if (!Position.isPointWithinBox(f.stem.left, f.stem.top, f.connection.left, f.connection.top, f.max.left, f.max.top)) {
                    var f = {
                        stem: {top: p.top + p.stem.connection.top, left: p.left + p.stem.connection.left},
                        connection: {top: d.top + d.connection.top, left: d.left + d.connection.left},
                        max: {top: d.top + d.max.top, left: d.left + d.max.left}
                    }, m = {
                        connection: Position.getDistance(f.stem.left, f.stem.top, f.connection.left, f.connection.top),
                        max: Position.getDistance(f.stem.left, f.stem.top, f.max.left, f.max.top)
                    }, g = Math.min(m.connection, m.max), v = f[m.connection <= m.max ? "connection" : "max"], b = "horizontal" == Position.getOrientation(e) ? "left" : "top", _ = Position.getDistance(f.connection.left, f.connection.top, f.max.left, f.max.top);
                    if (_ >= u) {
                        var y = {top: 0, left: 0}, w = v[b] < f.stem[b] ? -1 : 1;
                        y[b] = g * w, y[b] += Math.floor(.5 * u) * w, p.left += y.left, p.top += y.top
                    } else {
                        $.extend(f, {
                            center: {
                                top: Math.round(d.top + .5 * i.height),
                                left: Math.round(d.left + .5 * i.left)
                            }
                        });
                        var x = {
                            connection: Position.getDistance(f.center.left, f.center.top, f.connection.left, f.connection.top),
                            max: Position.getDistance(f.center.left, f.center.top, f.max.left, f.max.top)
                        }, g = m[x.connection <= x.max ? "connection" : "max"], C = {
                            top: 0,
                            left: 0
                        }, w = v[b] < f.stem[b] ? -1 : 1;
                        C[b] = g * w, p.left += C.left, p.top += C.top
                    }
                }
            }
            if (this.options.offset) {
                var k = $.extend({}, this.options.offset);
                k = Position.adjustOffsetBasedOnPosition(k, this.options.position.target, t), p.top += k.y, p.left += k.x
            }
            var S = this.getContainment({top: p.top, left: p.left}, e), T = S.horizontal && S.vertical, D = {
                x: 0,
                y: 0
            }, P = Position.getOrientation(e);
            if (!S[P]) {
                var I = "horizontal" == P, M = I ? ["left", "right"] : ["up", "down"], O = I ? "x" : "y", E = I ? "left" : "top", A = S.correction[O], R = this.getContainmentLayout(e), F = R.position[I ? "left" : "top"];
                if (0 !== A) {
                    var j = o.connections[e].move, z = j[M[0 > -1 * A ? 0 : 1]], L = 0 > A ? -1 : 1;
                    if (z >= A * L && p[E] + A >= F)p[E] += A, D[O] = -1 * A, T = !0; else if (Position.getOrientation(t) == Position.getOrientation(e)) {
                        if (p[E] += z * L, D[O] = -1 * z * L, p[E] < F) {
                            var B = F - p[E], W = j[M[0]] + j[M[1]], B = Math.min(B, W);
                            p[E] += B;
                            var N = D[O] - B;
                            N >= o.connections[e].move[M[0]] && N <= o.connections[e].move[M[1]] && (D[O] -= B)
                        }
                        S = this.getContainment({top: p.top, left: p.left}, e);
                        var H = S.correction[O], U = deepExtend({}, p);
                        this.options.offset && (U.left -= this.options.offset.x, U.top -= this.options.offset.y);
                        var f = {stem: {top: U.top + p.stem.connection.top, left: U.left + p.stem.connection.left}};
                        f.stem[E] += D[O];
                        var q = this.getTargetLayout(), u = o.stemDimensions.width, V = Math.floor(.5 * u), Z = F + R.dimensions[I ? "width" : "height"];
                        if ("x" == O) {
                            var G = q.position.left + V;
                            H > 0 && (G += q.dimensions.width - 2 * V), (0 > H && f.stem.left + H >= G && U.left + H >= F || H > 0 && f.stem.left + H <= G && U.left + H <= Z) && (U.left += H)
                        } else {
                            var Y = q.position.top + V;
                            H > 0 && (Y += q.dimensions.height - 2 * V), (0 > H && f.stem.top + H >= Y && U.top + H >= F || H > 0 && f.stem.top + H <= Y && U.top + H <= Z) && (U.top += H)
                        }
                        p = U, this.options.offset && (p.left += this.options.offset.x, p.top += this.options.offset.y)
                    }
                }
                S = this.getContainment({top: p.top, left: p.left}, e), T = S.horizontal && S.vertical
            }
            return {top: p.top, left: p.left, contained: T, shift: D}
        }, setPosition: function (t, e) {
            var i = this._position;
            if (!i || i.top != t.top || i.left != t.left) {
                var s;
                if (this.options.container != document.body) {
                    if ("string" == $.type(this.options.container)) {
                        var n = this.target;
                        "mouse" == n && (n = this.element), s = $($(n).closest(this.options.container).first())
                    } else s = $(s);
                    if (s[0]) {
                        var o = $(s).offset(), a = {
                            top: Math.round(o.top),
                            left: Math.round(o.left)
                        }, r = {top: Math.round($(s).scrollTop()), left: Math.round($(s).scrollLeft())};
                        t.top -= a.top, t.top += r.top, t.left -= a.left, t.left += r.left
                    }
                }
                this._position = t, this._tooltip.css({top: t.top, left: t.left})
            }
            this.skin.setStemPosition(e || this.options.position.tooltip, t.shift || {x: 0, y: 0})
        }, getSideLine: function (t, e) {
            var i = t.position.left, s = t.position.top, n = t.position.left, o = t.position.top;
            switch (e) {
                case"top":
                    n += t.dimensions.width;
                    break;
                case"bottom":
                    s += t.dimensions.height, n += t.dimensions.width, o += t.dimensions.height;
                    break;
                case"left":
                    o += t.dimensions.height;
                    break;
                case"right":
                    i += t.dimensions.width, n += t.dimensions.width, o += t.dimensions.height
            }
            return {x1: i, y1: s, x2: n, y2: o}
        }, isSideWithinContainment: function (t, e, i) {
            var s = this.getContainmentLayout(e, i), n = this.getTargetLayout(), o = this.getSideLine(n, t);
            if (Position.isPointWithinBoxLayout(o.x1, o.y1, s) || Position.isPointWithinBoxLayout(o.x2, o.y2, s))return !0;
            var a = !1;
            return $.each("top right bottom left".split(" "), $.proxy(function (t, e) {
                var i = this.getSideLine(s, e);
                return Position.intersectsLine(o.x1, o.y1, o.x2, o.y2, i.x1, i.y1, i.x2, i.y2) ? (a = !0, !1) : void 0
            }, this)), a
        }, getContainment: function (t, e) {
            var i = {horizontal: !0, vertical: !0, correction: {y: 0, x: 0}};
            if (this.options.containment) {
                var s = this.getContainmentLayout(e), n = this.skin._vars.frames[Position.getSide(e)].dimensions;
                this.options.containment && ((t.left < s.position.left || t.left + n.width > s.position.left + s.dimensions.width) && (i.horizontal = !1, i.correction.x = t.left < s.position.left ? s.position.left - t.left : s.position.left + s.dimensions.width - (t.left + n.width)), (t.top < s.position.top || t.top + n.height > s.position.top + s.dimensions.height) && (i.vertical = !1, i.correction.y = t.top < s.position.top ? s.position.top - t.top : s.position.top + s.dimensions.height - (t.top + n.height)))
            }
            return i
        }, getContainmentLayout: function (t, e) {
            var i = {top: $(window).scrollTop(), left: $(window).scrollLeft()}, s = this.target;
            "mouse" == s && (s = this.element);
            var n, o = $(s).closest(this.options.containment.selector).first()[0];
            n = o && "viewport" != this.options.containment.selector ? {
                dimensions: {
                    width: $(o).innerWidth(),
                    height: $(o).innerHeight()
                }, position: $(o).offset()
            } : {dimensions: Bounds.viewport(), position: i};
            var a = this.options.containment.padding;
            if (a && !e) {
                var r = Math.max(n.dimensions.height, n.dimensions.width);
                if (2 * a > r && (a = Math.max(Math.floor(.5 * r), 0)), a) {
                    n.dimensions.width -= 2 * a, n.dimensions.height -= 2 * a, n.position.top += a, n.position.left += a;
                    var h = Position.getOrientation(t);
                    "vertical" == h ? (n.dimensions.width += a, "left" == Position.getSide(t) && (n.position.left -= a)) : (n.dimensions.height += a, "top" == Position.getSide(t) && (n.position.top -= a))
                }
            }
            return this._cache.layouts.containmentLayout = n, n
        }, getMouseRoom: function () {
            var t = {top: 0, left: 0, right: 0, bottom: 0};
            if ("mouse" == this.options.target && !this.is("api")) {
                var e = Mouse.getActualPosition(this._cache.event), i = $(this.element).offset(), s = {
                    width: $(this.element).innerWidth(),
                    height: $(this.element).innerHeight()
                };
                t = {
                    top: Math.max(0, e.top - i.top),
                    bottom: Math.max(0, i.top + s.height - e.top),
                    left: Math.max(0, e.left - i.left),
                    right: Math.max(0, i.left + s.width - e.left)
                }
            }
            return t
        }, getTargetPosition: function () {
            var t;
            if ("mouse" == this.options.target)if (this.is("api")) {
                var e = $(this.element).offset();
                t = {top: Math.round(e.top), left: Math.round(e.left)}
            } else t = Mouse.getPosition(this._cache.event); else {
                var e = $(this.target).offset();
                t = {top: Math.round(e.top), left: Math.round(e.left)}
            }
            return this._cache.layouts.targetPosition = t, t
        }, getTargetDimensions: function () {
            if (this._cache.layouts.targetDimensions)return this._cache.layouts.targetDimensions;
            var t;
            return t = "mouse" == this.options.target ? Mouse.getDimensions() : {
                width: $(this.target).innerWidth(),
                height: $(this.target).innerHeight()
            }, this._cache.layouts.targetDimensions = t, t
        }, getTargetLayout: function () {
            if (this._cache.layouts.targetLayout)return this._cache.layouts.targetLayout;
            var t = {position: this.getTargetPosition(), dimensions: this.getTargetDimensions()};
            return this._cache.layouts.targetLayout = t, t
        }, getPaddingLine: function (t) {
            var e = this.getTargetLayout(), i = "left";
            if ("vertical" == Position.getOrientation(t))return this.getSideLine(e, Position.getSide(t));
            if (Position.isCorner(t)) {
                var s = Position.inverseCornerPlane(t);
                return i = Position.getSide(s), this.getSideLine(e, i)
            }
            var n = this.getSideLine(e, i), o = Math.round(.5 * e.dimensions.width);
            return n.x1 += o, n.x2 += o, n
        }
    }), $.extend(Tooltip.prototype, {
        setActive: function () {
            this.is("active", !0), this.visible() && this.raise(), this.options.hideAfter && this.clearTimer("idle")
        }, setIdle: function () {
            this.is("active", !1), this.options.hideAfter && this.setTimer("idle", $.proxy(function () {
                this.clearTimer("idle"), this.is("active") || this.hide()
            }, this), this.options.hideAfter)
        }
    }), $.extend(Tooltip.prototype, {
        bind: function (t, e, i, s) {
            e = e;
            var n = $.proxy(i, s || this);
            this._cache.events.push({element: t, eventName: e, handler: n}), $(t).bind(e, n)
        }, unbind: function () {
            $.each(this._cache.events, function (t, e) {
                $(e.element).unbind(e.eventName, e.handler)
            }), this._cache.events = []
        }
    }), $.extend(Tooltip.prototype, {
        disable: function () {
            this.is("disabled") || this.is("disabled", !0)
        }, enable: function () {
            this.is("disabled") && this.is("disabled", !1)
        }
    }), $.extend(Tooltip.prototype, {
        is: function (t, e) {
            return "boolean" == $.type(e) && (this._cache.is[t] = e), this._cache.is[t]
        }, visible: function () {
            return this.is("visible")
        }
    }), $.extend(Tooltip.prototype, {
        setTimer: function (t, e, i) {
            this._cache.timers[t] = _.delay(e, i)
        }, getTimer: function (t) {
            return this._cache.timers[t]
        }, clearTimer: function (t) {
            this._cache.timers[t] && (clearTimeout(this._cache.timers[t]), delete this._cache.timers[t])
        }, clearTimers: function () {
            $.each(this._cache.timers, function (t, e) {
                clearTimeout(e)
            }), this._cache.timers = {}
        }
    }), $.extend(Tipped, {
        init: function () {
            Tooltips.init()
        }, create: function (t, e, i) {
            return Collection.create(t, e, i), this.get(t)
        }, get: function (t) {
            return new Collection(t)
        }, findElement: function (t) {
            return Tooltips.findElement(t)
        }, refresh: function (t, e, i) {
            return Tooltips.refresh(t, e, i), this
        }, setStartingZIndex: function (t) {
            return Tooltips.setStartingZIndex(t), this
        }
    }), $.each("remove".split(" "), function (t, e) {
        Tipped[e] = function (t) {
            return this.get(t)[e](), this
        }
    }), $.extend(Collection, {
        create: function (t, e) {
            if (t) {
                "object" == $.type(t) && (t = $(t)[0]);
                var i = arguments[2] || {};
                return _.isElement(t) ? new Tooltip(t, e, i) : $(t).each(function (t, s) {
                    new Tooltip(s, e, i)
                }), this
            }
        }
    }), $.extend(Collection.prototype, {
        items: function () {
            return Tooltips.get(this.element, {api: !0})
        }, refresh: function (t) {
            return Tooltips.refresh(this.element, t), this
        }, remove: function () {
            return Tooltips.remove(this.element), this
        }
    }), $.each("".split(" "), function (t, e) {
        Collection.prototype[e] = function () {
            return $.each(this.items(), function (t, i) {
                i[e]()
            }), this
        }
    }), Tipped.init(), Tipped
}), function (t) {
    var e = function (e, i) {
        var s = {afterMatch: t.noop, beforeMatch: t.noop, source: {type: null, data: {}}};
        if (this.options = t.extend(s, i), this.matchNoHighlightClass = "nide", this.matchHighlightClass = "highlight", this.query, this.source = s.source, this.element = t(e), this.matches = {}, this.element.attr("data-target")) {
            var n = t(this.element.attr("data-target"));
            this.source.type = "html";
            var o = this;
            t.each(n, function (e) {
                o.source.data[e] = {
                    node: this,
                    text: o.element.attr("data-target-child") ? t(o.element.attr("data-target-child"), this).text() : t(this).text(),
                    highlight: !1
                }
            })
        } else this.options.source.url && (this.source = t.extend({type: "ajax"}, this.options.source));
        this.element.on("keyup change", this.handler)
    };
    e.prototype.setQuery = function (t) {
        this.query = t
    }, e.prototype.getQuery = function () {
        return this.query
    }, e.prototype.setData = function (t) {
        this.source.data = t
    }, e.prototype.getData = function () {
        return this.source.data
    }, e.prototype.getMatches = function () {
        return this.matches
    }, e.prototype.handler = function (e) {
        t(this).choosen("setQuery", this.value);
        var i = t(this).data("choosen");
        if (i.options.beforeMatch.call(this, [e]), "html" == i.source.type)var s = i.matchHtml(); else if ("ajax" == i.source.type)var s = i.matchAjax();
        i.options.afterMatch.call(this, s)
    }, e.prototype.matchHtml = function () {
        var e = new RegExp(this.getQuery(), "i"), i = this;
        t.each(this.source.data, function (t) {
            return i.query.length <= 0 ? void(this.node.style.display = "") : void(e.test(this.text) ? (i.matches[t] = this, this.highlight = !0) : this.node.style.display = "none")
        })
    }, e.prototype.matchAjax = function () {
        var e = t.extend({url: this.source.url, context: this, data: {query: this.query}}, this.source.ajaxSetup);
        t.ajax(e)
    }, t.fn.choosen = function (i, s) {
        if ("string" == typeof i) {
            var n = t(this).data("choosen");
            return n || (n = new e(this), t(this).data("choosen", n)), n[i](s)
        }
        return this.each(function () {
            if (!t(this).data("choosen")) {
                var n = "object" == typeof i ? new e(this, i) : new e(this, s);
                return t(this).data("choosen", n), n
            }
        })
    }
}(jQuery), function (t) {
    t.fn.sexySlider = function (e) {
        var i = {visibleItems: 1, animationDuration: 1e3, slideSelector: ".slide", text: {next: "Next", prev: "Prev"}};
        return this.each(function () {
            var s = t(this);
            if ("object" != typeof s.data("sexy-slider")) {
                var n = {
                    visibleItems: s.data("visible-items"),
                    animationDuration: s.data("animation-duration"),
                    slideSelector: s.data("slide-selector"),
                    text: {next: s.data("text-next"), prev: s.data("text-prev")}
                }, o = t.extend(!0, {}, i, e, n);
                o.__isAnimate = !1, s.css("position", "relative").data("sexy-slider", o).addClass("js-sexySlider");
                var a = t('<div class="b-sexySlider-viewport"></div>');
                a.css({overflow: "hidden", position: "relative"}), s.wrap('<div class="b-sexySlider"></div>').wrap(a);
                var r = s.parents(".b-sexySlider-viewport").width() / o.visibleItems, h = 0;
                s.children(o.slideSelector).each(function () {
                    var e = r - (parseInt(t(this).css("marginLeft")) + parseInt(t(this).css("marginRight")));
                    t(this).css("width", e), h += t(this).outerWidth(!0) + 1
                }), s.css("width", h), s.children(o.slideSelector).size() > o.visibleItems && (s.parent().after('<div class="b-sexySlider-controls"><a class="b-sexySlider-control-item prev" href="javascript:;">' + o.text.prev + '</a><a class="b-sexySlider-control-item next" href="javascript:;">' + o.text.next + "</a></div>"), t(document).on("click", ".b-sexySlider .b-sexySlider-control-item", function () {
                    var e = t(this).parents(".b-sexySlider").find(".js-sexySlider"), i = e.data("sexy-slider");
                    if (console.log(e), 1 != i.__isAnimate)if (i.__isAnimate = !0, setTimeout(t.proxy(function () {
                            this.__isAnimate = !1
                        }, i), i.animationDuration), t(this).hasClass("prev")) {
                        var s = e.children(i.slideSelector + ":last-child");
                        e.css({left: -s.outerWidth(!0)}).animate({left: 0}, i.animationDuration).prepend(s.detach())
                    } else if (t(this).hasClass("next")) {
                        var s = e.children(i.slideSelector + ":first-child");
                        e.animate({left: -s.next().outerWidth(!0)}, i.animationDuration, function () {
                            e.append(s.detach()).css({left: 0})
                        })
                    }
                }))
            }
        })
    }, t(function () {
        t("[data-sexyslider]").sexySlider()
    })
}(jQuery), function (t, e, i) {
    "use strict";
    var s = function (t) {
        i(t).on("click", this.close)
    };
    s.dataSelector = "[data-closer]", s.TRANSITION_DURATION = 1500, s.prototype.close = function (t) {
        var e = i(this), s = e.attr("data-target") || e.attr("data-closer");
        s || (s = e.attr("href"), s = s && s.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i(this).parents(s) || i(s);
        t && t.preventDefault(), n.length && (n.trigger(t = i.Event("close.workly.closer")), t.isDefaultPrevented() || (n.removeClass("in"), n.fadeOut(this.TRANSITION_DURATION, function () {
            i(this).detach().trigger("closed.workly.closer").remove()
        })))
    }, i.fn.closer = function (t) {
        return this.each(function () {
            var e = i(this), n = e.data("workly.closer");
            n || e.data("workly.closer", n = new s(this)), "string" == typeof t && n[t].call(e)
        })
    }, i.fn.closer.Constructor = s, i(document).on("click", s.dataSelector, s.prototype.close)
}(window, document, jQuery), function (t, e, i) {
    "use strict";
    var s = [];
    (s = i("[data-maps]")) && s.length > 0 && s.each(function (t, e) {
        var s = i(e).attr("data-maps-coordinates").split(";");
        if (console.log(s), "object" == typeof s && null !== s && s.length >= 1) {
            var n = s[0].split(","), o = (s[1] || s[0]).split(",");
            if (this.id = this.id || "mapID_" + Math.random(), "yandex" == i(e).data("maps") && "ymaps"in window)ymaps.ready(function () {
                var t = new ymaps.Map(e.id, {
                    center: [n[0], n[1]],
                    zoom: i(e).attr("data-maps-zoom") || 14,
                    controls: [],
                    behaviors: ["drag", "scrollZoom", "multiTouch"]
                });
                t.geoObjects.add(new ymaps.Placemark([o[0], o[1]], {hintContent: i(e).attr("data-maps-marker-title") || "?? ???"}, {preset: "islands#redDotIcon"}))
            }); else if ("google" == i(this).data("maps") && "google"in window) {
                var a = {
                    zoom: parseInt(i(this).attr("data-maps-zoom")) || 14,
                    center: new google.maps.LatLng(n[0], n[1]),
                    mapTypeControl: !1,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: !0,
                    zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
                    disableDoubleClickZoom: !0,
                    streetViewControl: !1
                };
                console.log(a);
                var e = new google.maps.Map(this, a), r = new google.maps.Marker({title: i(this).attr("data-maps-marker-title") || "?? ???"});
                r.setMap(e), r.setPosition(new google.maps.LatLng(o[0], o[1])), r.setVisible(!0)
            }
        }
    });
    var n = [];
    (n = i(".js-table")) && n.length > 0 && n.wrap('<div class="js-generating-table-wrapper"><div class="js-generating-table-outer"></div></div>').parent(".js-generating-table-outer").css({
        overflow: "auto",
        position: "relative",
        display: "block"
    }).parent(".js-generating-table-wrapper").css({
        height: 0,
        overflow: "hidden",
        position: "relative",
        display: "block"
    }), i.extend({
        getRandom: function (t) {
            return parseInt(Math.random().toString().substr(2, t || 5))
        }
    })
}(window, document, jQuery), function (t) {
    t(document).on("click", "[data-clearInput]", function (e) {
        var i = t(this).attr("data-clearInput") || !1;
        i && (console.log(i), t(i).val("").trigger("change"))
    })
}(jQuery), function (t, e) {
    e(t).on("click.cloning", ".js-cloning-caller", function (t) {
        t.preventDefault();
        var i = {};
        i.root = e(this).parents(".js-cloning-root") || e("body"), i.container = i.root.find(".js-cloning-container"), i.copy = i.root.find(".js-cloning-sample").clone(), i.original = i.root.find(".js-cloning-sample").clone(!0), i.counter = e(this).data("cloning-copies_count") || 0;
        var s = e(this).data("cloning-limit") || 0, n = {
            onlimit: e(this).data("cloning-onlimit") || e.noop,
            before: e(this).data("cloning-before") || e.noop,
            after: e(this).data("cloning-after") || e.noop
        };
        return s > 0 && s <= i.counter ? void("function" == typeof window[n.onlimit] && window[n.onlimit](t, e(this), i)) : ("function" == typeof window[n.before] && window[n.before](t, e(this), i), void(i.hasOwnProperty("copy") && "object" == typeof i.copy && null !== i.copy && i.copy.length > 0 && (i.copy.removeClass("js-cloning-sample").addClass("js-cloning-clone"), i.copy = i.copy.appendTo(i.container), i.counter += 1, e(this).data("cloning-copies_count", i.counter), "function" == typeof window[n.after] && window[n.after](t, e(this), i))))
    })
}(document, jQuery), function (t) {
    var e = "groupCheckBox", i = function () {
        console.log(t.getRandom() + " " + e + ": toggler - onchange");
        var s = t(this), n = s.data(e);
        n || (n = {childs: t('.js-groupCheckBox-child[data-groupCheckBoxParent="' + this.id + '"]')}, s.data(e, n));
        var o = this;
        n.childs.filter(":checked").length != n.childs.length && (this.checked = 1, s.removeClass("checked-partial")), n.childs.each(function () {
            this.checked = o.checked, t(this).hasClass("js-groupCheckBox-toggler") && i.call(this)
        })
    }, s = function () {
        console.log(t.getRandom() + " " + e + ": child - onchange");
        var i = t(this).data(e + "Parent");
        if (i || (i = document.getElementById(t(this).attr("data-groupCheckBoxParent")), t(this).data(e + "Parent", i)), i) {
            var n = t(i).data(e);
            n || (n = {childs: t(".js-groupCheckBox-child[data-groupCheckBoxParent=" + i.id + "]")}, t(i).data(e, n)), n.childs.filter(":checked").size() > 0 ? (i.checked = !0, n.childs.size() != n.childs.filter(":checked").size() ? t(i).addClass("checked-partial") : t(i).removeClass("checked-partial")) : i.checked = !1, t(i).hasClass("js-groupCheckBox-child") && s.call(i)
        }
    };
    t(document).on("change", ".js-" + e + " .js-groupCheckBox-toggler", i).on("change", ".js-" + e + " .js-groupCheckBox-child", s)
}(jQuery), function (t) {
    t(document).on("focus", ".js-select", function () {
        var e = t(this).data("select2");
        e && setTimeout(function () {
            e.isOpen() || e.open()
        }, 0)
    })
}(jQuery), function (t) {
    var e = ".js-selectSideBySide";
    t(document).on("click", ".js-control", function (i) {
        var s = t(this);
        if (console.log("first"), s.hasClass("disabled"))return !1;
        if (s.hasClass("js-control__from_right"))var n = s.parents(e).find("[data-side=right] :checked"); else if (s.hasClass("js-control__from_left"))var n = s.parents(e).find("[data-side=left] :checked");
        return s.data("checkedData", n), s.data("ajaxHref") && t.ajax(s.data("ajaxHref"), {
            type: s.data("ajaxType") || "GET",
            data: n,
            context: this,
            success: function (i) {
                t(this).data("ajaxResultTarget") && t(this).parents(e).find(t(this).data("ajaxResultTarget")).html(i), selectSideBySide_restartControls(e)
            },
            beforeSend: function () {
                console.log("test"), t(this).hasClass("js-control__from_right") && t(this).parents(e).find("[data-side=right] :checked").parents("li").remove()
            }
        }), !1
    }).on("change", e + " [data-side] :checkbox", function () {
        var i = "add", s = t(this).parents("[data-side]"), n = s.find(":checkbox"), o = n.filter(":checked");
        n.not(":checked").parents("li").removeClass("b-menu-item__active"), o.length > 0 && (i = "remove", o.parents("li:not(.b-menu-item__active)").addClass("b-menu-item__active")), t(this).parents(e).find(".js-control.js-control__from_" + s.data("side"))[i + "Class"]("disabled")
    }), t.extend(window, {
        selectSideBySide_restartControls: function (e) {
            t(e).each(function () {
                var e = t(this);
                t("[data-side]", this).each(function () {
                    var i = t(this).find(".side-body :checked");
                    i.length > 0 && (i.parents("li:not(.b-menu-item__active)").addClass("b-menu-item__active"), e.find(".js-control.js-control__from_" + t(this).data("side")).removeClass("disabled"))
                })
            })
        }
    }), selectSideBySide_restartControls(e)
}(jQuery), function (t) {
    var e = t("#js-slidebar");
    t(document).on("toggle", ".js-slidebar-toggler", function (i) {
        e.animate({width: "toggle"}, 450, function () {
            t(this).toggleClass("js-opened")
        })
    }), t(document).on('click', '.b-slidebar-closer', function () {
        e.animate({width: "toggle"});
        e.removeClass('js-opened');
        e.hide();
    }), t(document).mouseup(function (e) {
        var container = $(".js-opened");
        if (container.has(e.target).length === 0) {
            container.removeClass('js-opened');
            container.hide();
        }
    });
    t(".b-menu-item.b-menu-item__parent > a.a-link", e).on("click", function (e) {
        if (t(e.target).hasClass("js-menu-toggler"))return e.preventDefault(), void t(this).next().slideToggle();
        if (t(this).is("[data-callback]")) {
            console.log("next");
            var i = t(this).attr("data-callback");
            return window[i].call(this, event)
        }
    })
}(jQuery), function (t) {
    t(document).on("click", ".js-table-collapseToggler", function (e) {
        switch (t(this).attr("aria-expanded")) {
            case"true":
                t(this).parents("tr").addClass("active");
                break;
            case"false":
                t(this).parents("tr").removeClass("active")
        }
    })
}(jQuery), function (t) {
    t(document).on("loaded.tables", ".js-table", function (e) {
        var i = t(this).parent(".js-generating-table-outer").height();
        t(this).parent().parent(".js-generating-table-wrapper").animate({height: i}, 1e3, function () {
            t(this).removeAttr("style")
        }), t(this).has("[data-TableNormalizeColumns]") && t("tr[data-dependentRow]", this).each(function () {
            var e = t(t(this).attr("data-dependentRow")), i = null;
            t(":not([colspan])", this).each(function (s, n) {
                i instanceof Object ? i.is("[colspan]") && i.index() + parseInt(i.attr("colspan")) + 1 >= s && s <= i.index() + parseInt(i.attr("colspan")) + 1 || (i = i.next()) : i = e.children().eq(s), i.is("[colspan]") || t(this).css({width: i.innerWidth()})
            })
        })
    }), t(".js-table").length <= 0 || t(function () {
        t(".js-table").trigger("loaded.tables")
    })
}(jQuery), $(document).on("hidden.bs.tab shown.bs.tab", ".b-menu .b-menu-item a[data-toggle=tab]", function (t) {
    console.log(t), $(this).parent().hasClass("active") ? $(this).parent().addClass("b-menu-item__active") : $(this).parent().removeClass("b-menu-item__active")
}), function (t) {
    t(document).on("click", ".js-Toggling-toggler", function () {
        var e = t(this), i = t(e.attr("data-parent")), s = e.data("toggling");
        if (i.length > 0) {
            var n = t(".js-Toggling-section", i), o = t(".js-Toggling-toggler", i), a = s && s.hasOwnProperty("target") ? s.target : n.filter(e.attr("data-target"));
            o.removeClass("active"), n.removeClass("active").addClass("hide"), a.toggleClass("active hide"), e.addClass("active")
        } else {
            var a = t(".js-Toggling-section").filter(e.attr("data-target")).toggleClass("active hide");
            e.toggleClass("active")
        }
        e.data("toggling", {target: a})
    })
}(jQuery);
var selectbox = [];
(selectbox = $("select.js-select")) && selectbox.length > 0 && selectbox.select2({theme: "workly"}), $("body").tooltip({
    selector: ".js-tooltip",
    container: "body",
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
}), $(".js-chartLineRadial").each(function () {
    var t = parseInt($(this).data("line-percent")), e = 100 - t;
    $chart = new Chart(this.getContext("2d")).Doughnut([{value: t, color: "#2ea97d", highlight: "#2ea97d"}, {
        value: e,
        color: "#e8e8e8",
        highlight: "#e8e8e8;"
    }], {
        responsive: !0,
        segmentShowStroke: !1,
        showTooltips: !1,
        animation: !1,
        percentageInnerCutout: 0
    }), $(this).data("chart", $chart)
}), $(".b-fix-head-table").tableHeadFixer({left: 1}), $(".b-timepicker").timepicker();



$('.b-menu__dropdown').on('click', function(event){
 //The event won't be propagated to the document NODE and
 // therefore events delegated to document won't be fired
    /*event.startPropagation();*/
 /*event.stopPropagation();*/
 });


function DoPrevent(e) {
    e.stopPropagation();
}

function RevPrevent(e) {
    e.startPropagation();
}

/*$('.b-menu__dropdown').on('click', DoPrevent);*/

/*$('.b-menu__dropdown').on('click', function(event){
    //The event won't be propagated to the document NODE and
    // therefore events delegated to document won't be fired

    *//*event.preventDefault();*//*
    *//*event.startPropagation();*//*
    alert("The element was clicked.");
    event.stopPropagation();
});*/


/*$('.b-menu__dropdown').on('click', function(event){
 //The event won't be propagated to the document NODE and
 // therefore events delegated to document won't be fired

      *//*event.preventDefault();*//*
      *//*event.startPropagation();*//*
    alert("The element was clicked.");
    event.stopPropagation();
 });*/

$('.b-menu__dropdown').on('click', function(event){
    //The event won't be propagated to the document NODE and
    // therefore events delegated to document won't be fired
    /*event.preventDefault();*/
    /*event.startPropagation();*/
    /*alert("The span element was clicked.");*/
});

/*$('.b-menu-item').on('click', function(event){
    //The event won't be propagated to the document NODE and
    // therefore events delegated to document won't be fired
    event.preventDefault();
    alert("The span element was clicked.");
});*/


/*$('.b-menu__dropdown').on('click', function(event){
    //The event won't be propagated to the document NODE and
    // therefore events delegated to document won't be fired
    event.startPropagation();
});*/





/* Tooltips */
$('.tooltip-secondary').tooltip();

/* jQuery dotdotdot 1.8.3 */
$(function () {
    $('.b-note-item').dotdotdot();
    $('.b-text-overflow').dotdotdot();
});


/*
 *	jQuery dotdotdot 1.8.3
 *
 *	Copyright (c) Fred Heusschen
 *	www.frebsite.nl
 *
 *	Plugin website:
 *	dotdotdot.frebsite.nl
 *
 *	Licensed under the MIT license.
 *	http://en.wikipedia.org/wiki/MIT_License
 */

(function( $, undef )
{
    if ( $.fn.dotdotdot )
    {
        return;
    }

    $.fn.dotdotdot = function( o )
    {
        if ( this.length == 0 )
        {
            $.fn.dotdotdot.debug( 'No element found for "' + this.selector + '".' );
            return this;
        }
        if ( this.length > 1 )
        {
            return this.each(
                function()
                {
                    $(this).dotdotdot( o );
                }
            );
        }


        var $dot = this;
        var orgContent	= $dot.contents();

        if ( $dot.data( 'dotdotdot' ) )
        {
            $dot.trigger( 'destroy.dot' );
        }

        $dot.data( 'dotdotdot-style', $dot.attr( 'style' ) || '' );
        $dot.css( 'word-wrap', 'break-word' );
        if ($dot.css( 'white-space' ) === 'nowrap')
        {
            $dot.css( 'white-space', 'normal' );
        }

        $dot.bind_events = function()
        {
            $dot.bind(
                'update.dot',
                function( e, c )
                {
                    $dot.removeClass("is-truncated");
                    e.preventDefault();
                    e.stopPropagation();

                    switch( typeof opts.height )
                    {
                        case 'number':
                            opts.maxHeight = opts.height;
                            break;

                        case 'function':
                            opts.maxHeight = opts.height.call( $dot[ 0 ] );
                            break;

                        default:
                            opts.maxHeight = getTrueInnerHeight( $dot );
                            break;
                    }

                    opts.maxHeight += opts.tolerance;

                    if ( typeof c != 'undefined' )
                    {
                        if ( typeof c == 'string' || ('nodeType' in c && c.nodeType === 1) )
                        {
                            c = $('<div />').append( c ).contents();
                        }
                        if ( c instanceof $ )
                        {
                            orgContent = c;
                        }
                    }

                    $inr = $dot.wrapInner( '<div class="dotdotdot" />' ).children();
                    $inr.contents()
                        .detach()
                        .end()
                        .append( orgContent.clone( true ) )
                        .find( 'br' )
                        .replaceWith( '  <br />  ' )
                        .end()
                        .css({
                            'height'	: 'auto',
                            'width'		: 'auto',
                            'border'	: 'none',
                            'padding'	: 0,
                            'margin'	: 0
                        });

                    var after = false,
                        trunc = false;

                    if ( conf.afterElement )
                    {
                        after = conf.afterElement.clone( true );
                        after.show();
                        conf.afterElement.detach();
                    }

                    if ( test( $inr, opts ) )
                    {
                        if ( opts.wrap == 'children' )
                        {
                            trunc = children( $inr, opts, after );
                        }
                        else
                        {
                            trunc = ellipsis( $inr, $dot, $inr, opts, after );
                        }
                    }
                    $inr.replaceWith( $inr.contents() );
                    $inr = null;

                    if ( $.isFunction( opts.callback ) )
                    {
                        opts.callback.call( $dot[ 0 ], trunc, orgContent );
                    }

                    conf.isTruncated = trunc;
                    return trunc;
                }

            ).bind(
                'isTruncated.dot',
                function( e, fn )
                {
                    e.preventDefault();
                    e.stopPropagation();

                    if ( typeof fn == 'function' )
                    {
                        fn.call( $dot[ 0 ], conf.isTruncated );
                    }
                    return conf.isTruncated;
                }

            ).bind(
                'originalContent.dot',
                function( e, fn )
                {
                    e.preventDefault();
                    e.stopPropagation();

                    if ( typeof fn == 'function' )
                    {
                        fn.call( $dot[ 0 ], orgContent );
                    }
                    return orgContent;
                }

            ).bind(
                'destroy.dot',
                function( e )
                {
                    e.preventDefault();
                    e.stopPropagation();

                    $dot.unwatch()
                        .unbind_events()
                        .contents()
                        .detach()
                        .end()
                        .append( orgContent )
                        .attr( 'style', $dot.data( 'dotdotdot-style' ) || '' )
                        .removeClass( 'is-truncated' )
                        .data( 'dotdotdot', false );
                }
            );
            return $dot;
        };	//	/bind_events

        $dot.unbind_events = function()
        {
            $dot.unbind('.dot');
            return $dot;
        };	//	/unbind_events

        $dot.watch = function()
        {
            $dot.unwatch();
            if ( opts.watch == 'window' )
            {
                var $window = $(window),
                    _wWidth = $window.width(),
                    _wHeight = $window.height();

                $window.bind(
                    'resize.dot' + conf.dotId,
                    function()
                    {
                        if ( _wWidth != $window.width() || _wHeight != $window.height() || !opts.windowResizeFix )
                        {
                            _wWidth = $window.width();
                            _wHeight = $window.height();

                            if ( watchInt )
                            {
                                clearInterval( watchInt );
                            }
                            watchInt = setTimeout(
                                function()
                                {
                                    $dot.trigger( 'update.dot' );
                                }, 100
                            );
                        }
                    }
                );
            }
            else
            {
                watchOrg = getSizes( $dot );
                watchInt = setInterval(
                    function()
                    {
                        if ( $dot.is( ':visible' ) )
                        {
                            var watchNew = getSizes( $dot );
                            if ( watchOrg.width  != watchNew.width ||
                                watchOrg.height != watchNew.height )
                            {
                                $dot.trigger( 'update.dot' );
                                watchOrg = watchNew;
                            }
                        }
                    }, 500
                );
            }
            return $dot;
        };
        $dot.unwatch = function()
        {
            $(window).unbind( 'resize.dot' + conf.dotId );
            if ( watchInt )
            {
                clearInterval( watchInt );
            }
            return $dot;
        };

        var	opts 		= $.extend( true, {}, $.fn.dotdotdot.defaults, o ),
            conf		= {},
            watchOrg	= {},
            watchInt	= null,
            $inr		= null;


        if ( !( opts.lastCharacter.remove instanceof Array ) )
        {
            opts.lastCharacter.remove = $.fn.dotdotdot.defaultArrays.lastCharacter.remove;
        }
        if ( !( opts.lastCharacter.noEllipsis instanceof Array ) )
        {
            opts.lastCharacter.noEllipsis = $.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis;
        }


        conf.afterElement	= getElement( opts.after, $dot );
        conf.isTruncated	= false;
        conf.dotId			= dotId++;


        $dot.data( 'dotdotdot', true )
            .bind_events()
            .trigger( 'update.dot' );

        if ( opts.watch )
        {
            $dot.watch();
        }

        return $dot;
    };

    //	public
    $.fn.dotdotdot.defaults = {
        'ellipsis'			: '... ',
        'wrap'				: 'word',
        'fallbackToLetter'	: true,
        'lastCharacter'		: {},
        'tolerance'			: 0,
        'callback'			: null,
        'after'				: null,
        'height'			: null,
        'watch'				: false,
        'windowResizeFix'	: true
    };
    $.fn.dotdotdot.defaultArrays = {
        'lastCharacter'		: {
            'remove'			: [ ' ', '\u3000', ',', ';', '.', '!', '?' ],
            'noEllipsis'		: []
        }
    };
    $.fn.dotdotdot.debug = function( msg ) {};


    //	private
    var dotId = 1;

    function children( $elem, o, after )
    {
        var $elements 	= $elem.children(),
            isTruncated	= false;

        $elem.empty();

        for ( var a = 0, l = $elements.length; a < l; a++ )
        {
            var $e = $elements.eq( a );
            $elem.append( $e );
            if ( after )
            {
                $elem.append( after );
            }
            if ( test( $elem, o ) )
            {
                $e.remove();
                isTruncated = true;
                break;
            }
            else
            {
                if ( after )
                {
                    after.detach();
                }
            }
        }
        return isTruncated;
    }
    function ellipsis( $elem, $d, $i, o, after )
    {
        var isTruncated	= false;

        //	Don't put the ellipsis directly inside these elements
        var notx = 'a, table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style';

        //	Don't remove these elements even if they are after the ellipsis
        var noty = 'script, .dotdotdot-keep';

        $elem
            .contents()
            .detach()
            .each(
            function()
            {

                var e	= this,
                    $e	= $(e);

                if ( typeof e == 'undefined' )
                {
                    return true;
                }
                else if ( $e.is( noty ) )
                {
                    $elem.append( $e );
                }
                else if ( isTruncated )
                {
                    return true;
                }
                else
                {
                    $elem.append( $e );
                    if ( after && !$e.is( o.after ) && !$e.find( o.after ).length  )
                    {
                        $elem[ $elem.is( notx ) ? 'after' : 'append' ]( after );
                    }
                    if ( test( $i, o ) )
                    {
                        if ( e.nodeType == 3 ) // node is TEXT
                        {
                            isTruncated = ellipsisElement( $e, $d, $i, o, after );
                        }
                        else
                        {
                            isTruncated = ellipsis( $e, $d, $i, o, after );
                        }
                    }

                    if ( !isTruncated )
                    {
                        if ( after )
                        {
                            after.detach();
                        }
                    }
                }
            }
        );
        $d.addClass("is-truncated");
        return isTruncated;
    }
    function ellipsisElement( $e, $d, $i, o, after )
    {
        var e = $e[ 0 ];

        if ( !e )
        {
            return false;
        }

        var txt			= getTextContent( e ),
            space		= ( txt.indexOf(' ') !== -1 ) ? ' ' : '\u3000',
            separator	= ( o.wrap == 'letter' ) ? '' : space,
            textArr		= txt.split( separator ),
            position 	= -1,
            midPos		= -1,
            startPos	= 0,
            endPos		= textArr.length - 1;


        //	Only one word
        if ( o.fallbackToLetter && startPos == 0 && endPos == 0 )
        {
            separator	= '';
            textArr		= txt.split( separator );
            endPos		= textArr.length - 1;
        }

        while ( startPos <= endPos && !( startPos == 0 && endPos == 0 ) )
        {
            var m = Math.floor( ( startPos + endPos ) / 2 );
            if ( m == midPos )
            {
                break;
            }
            midPos = m;

            setTextContent( e, textArr.slice( 0, midPos + 1 ).join( separator ) + o.ellipsis );
            $i.children()
                .each(
                function()
                {
                    $(this).toggle().toggle();
                }
            );

            if ( !test( $i, o ) )
            {
                position = midPos;
                startPos = midPos;
            }
            else
            {
                endPos = midPos;

                //	Fallback to letter
                if (o.fallbackToLetter && startPos == 0 && endPos == 0 )
                {
                    separator	= '';
                    textArr		= textArr[ 0 ].split( separator );
                    position	= -1;
                    midPos		= -1;
                    startPos	= 0;
                    endPos		= textArr.length - 1;
                }
            }
        }

        if ( position != -1 && !( textArr.length == 1 && textArr[ 0 ].length == 0 ) )
        {
            txt = addEllipsis( textArr.slice( 0, position + 1 ).join( separator ), o );
            setTextContent( e, txt );
        }
        else
        {
            var $w = $e.parent();
            $e.detach();

            var afterLength = ( after && after.closest($w).length ) ? after.length : 0;

            if ( $w.contents().length > afterLength )
            {
                e = findLastTextNode( $w.contents().eq( -1 - afterLength ), $d );
            }
            else
            {
                e = findLastTextNode( $w, $d, true );
                if ( !afterLength )
                {
                    $w.detach();
                }
            }
            if ( e )
            {
                txt = addEllipsis( getTextContent( e ), o );
                setTextContent( e, txt );
                if ( afterLength && after )
                {
                    var $parent = after.parent();

                    $(e).parent().append( after );

                    if ( !$.trim( $parent.html() ) )
                    {
                        $parent.remove();
                    }
                }
            }
        }

        return true;
    }
    function test( $i, o )
    {
        return $i.innerHeight() > o.maxHeight;
    }
    function addEllipsis( txt, o )
    {
        while( $.inArray( txt.slice( -1 ), o.lastCharacter.remove ) > -1 )
        {
            txt = txt.slice( 0, -1 );
        }
        if ( $.inArray( txt.slice( -1 ), o.lastCharacter.noEllipsis ) < 0 )
        {
            txt += o.ellipsis;
        }
        return txt;
    }
    function getSizes( $d )
    {
        return {
            'width'	: $d.innerWidth(),
            'height': $d.innerHeight()
        };
    }
    function setTextContent( e, content )
    {
        if ( e.innerText )
        {
            e.innerText = content;
        }
        else if ( e.nodeValue )
        {
            e.nodeValue = content;
        }
        else if (e.textContent)
        {
            e.textContent = content;
        }

    }
    function getTextContent( e )
    {
        if ( e.innerText )
        {
            return e.innerText;
        }
        else if ( e.nodeValue )
        {
            return e.nodeValue;
        }
        else if ( e.textContent )
        {
            return e.textContent;
        }
        else
        {
            return "";
        }
    }
    function getPrevNode( n )
    {
        do
        {
            n = n.previousSibling;
        }
        while ( n && n.nodeType !== 1 && n.nodeType !== 3 );

        return n;
    }
    function findLastTextNode( $el, $top, excludeCurrent )
    {
        var e = $el && $el[ 0 ], p;
        if ( e )
        {
            if ( !excludeCurrent )
            {
                if ( e.nodeType === 3 )
                {
                    return e;
                }
                if ( $.trim( $el.text() ) )
                {
                    return findLastTextNode( $el.contents().last(), $top );
                }
            }
            p = getPrevNode( e );
            while ( !p )
            {
                $el = $el.parent();
                if ( $el.is( $top ) || !$el.length )
                {
                    return false;
                }
                p = getPrevNode( $el[0] );
            }
            if ( p )
            {
                return findLastTextNode( $(p), $top );
            }
        }
        return false;
    }
    function getElement( e, $i )
    {
        if ( !e )
        {
            return false;
        }
        if ( typeof e === 'string' )
        {
            e = $(e, $i);
            return ( e.length )
                ? e
                : false;
        }
        return !e.jquery
            ? false
            : e;
    }
    function getTrueInnerHeight( $el )
    {
        var h = $el.innerHeight(),
            a = [ 'paddingTop', 'paddingBottom' ];

        for ( var z = 0, l = a.length; z < l; z++ )
        {
            var m = parseInt( $el.css( a[ z ] ), 10 );
            if ( isNaN( m ) )
            {
                m = 0;
            }
            h -= m;
        }
        return h;
    }


    //	override jQuery.html
    var _orgHtml = $.fn.html;
    $.fn.html = function( str )
    {
        if ( str != undef && !$.isFunction( str ) && this.data( 'dotdotdot' ) )
        {
            return this.trigger( 'update', [ str ] );
        }
        return _orgHtml.apply( this, arguments );
    };


    //	override jQuery.text
    var _orgText = $.fn.text;
    $.fn.text = function( str )
    {
        if ( str != undef && !$.isFunction( str ) && this.data( 'dotdotdot' ) )
        {
            str = $( '<div />' ).text( str ).html();
            return this.trigger( 'update', [ str ] );
        }
        return _orgText.apply( this, arguments );
    };


})( jQuery );

/*

 ## Automatic parsing for CSS classes
 Contributed by [Ramil Valitov](https://github.com/rvalitov)

 ### The idea
 You can add one or several CSS classes to HTML elements to automatically invoke "jQuery.dotdotdot functionality" and some extra features. It allows to use jQuery.dotdotdot only by adding appropriate CSS classes without JS programming.

 ### Available classes and their description
 * dot-ellipsis - automatically invoke jQuery.dotdotdot to this element. This class must be included if you plan to use other classes below.
 * dot-resize-update - automatically update if window resize event occurs. It's equivalent to option `watch:'window'`.
 * dot-timer-update - automatically update if window resize event occurs. It's equivalent to option `watch:true`.
 * dot-load-update - automatically update after the window has beem completely rendered. Can be useful if your content is generated dynamically using using JS and, hence, jQuery.dotdotdot can't correctly detect the height of the element before it's rendered completely.
 * dot-height-XXX - available height of content area in pixels, where XXX is a number, e.g. can be `dot-height-35` if you want to set maximum height for 35 pixels. It's equivalent to option `height:'XXX'`.

 ### Usage examples
 *Adding jQuery.dotdotdot to element*

 <div class="dot-ellipsis">
 <p>Lorem Ipsum is simply dummy text.</p>
 </div>

 *Adding jQuery.dotdotdot to element with update on window resize*

 <div class="dot-ellipsis dot-resize-update">
 <p>Lorem Ipsum is simply dummy text.</p>
 </div>

 *Adding jQuery.dotdotdot to element with predefined height of 50px*

 <div class="dot-ellipsis dot-height-50">
 <p>Lorem Ipsum is simply dummy text.</p>
 </div>

 */

jQuery(document).ready(function($) {
    //We only invoke jQuery.dotdotdot on elements that have dot-ellipsis class
    $(".dot-ellipsis").each(function(){
        //Checking if update on window resize required
        var watch_window=$(this).hasClass("dot-resize-update");

        //Checking if update on timer required
        var watch_timer=$(this).hasClass("dot-timer-update");

        //Checking if height set
        var height=0;
        var classList = $(this).attr('class').split(/\s+/);
        $.each(classList, function(index, item) {
            var matchResult = item.match(/^dot-height-(\d+)$/);
            if(matchResult !== null)
                height = Number(matchResult[1]);
        });

        //Invoking jQuery.dotdotdot
        var x = new Object();
        if (watch_timer)
            x.watch=true;
        if (watch_window)
            x.watch='window';
        if (height>0)
            x.height=height;
        $(this).dotdotdot(x);
    });

});

// Updating elements (if any) on window.load event
jQuery(window).on('load', function(){
    jQuery(".dot-ellipsis.dot-load-update").trigger("update.dot");
});

// Slide button on top bar
$('.b-slide-button-container').hover(function () {
    $('.b-right-box').stop().animate({width: '200px'}, 400);
    $('.b-left-box').css('width:0px')
}, function () {
    $('.b-right-box').stop().animate({width: '-0'}, 400)

});


function welcome() {
    document.getElementById("WelcomeContainer").style.display = "none";
    document.getElementById("DetailsContainer").style.display = "block";
    document.getElementById("LineNextWelcome").style.display = "inline-block";
    document.getElementById("WelcomeSend").classList.add("passed");
    document.getElementById("InfoPanelWelcome").classList.add("fadeOutLeft");
    document.getElementById("InfoPanelDetails").style.display = "block";
    document.getElementById("DetailsSend").classList.add("active");
}

function details() {
    document.getElementById("DetailsContainer").style.display = "none";
    document.getElementById("SheduleContainer").style.display = "block";
    document.getElementById("LineNextDetails").style.display = "inline-block";
    document.getElementById("DetailsSend").classList.add("passed");
    document.getElementById("InfoPanelDetails").classList.add("fadeOutLeft");
    document.getElementById("InfoPanelShedule").style.display = "block";
    document.getElementById("ScheduleSend").classList.add("active");
}

function shedules() {
    document.getElementById("SheduleContainer").style.display = "none";
    document.getElementById("EmployeeContainer").style.display = "block";
    document.getElementById("LineNextShedule").style.display = "inline-block";
    document.getElementById("ScheduleSend").classList.add("passed");
    document.getElementById("InfoPanelShedule").classList.add("fadeOutLeft");
    document.getElementById("InfoPanelEmployees").style.display = "block";
    document.getElementById("EmployeeSend").classList.add("active");
}

function timepad() {
    document.getElementById("EmployeeContainer").style.display = "none";
    document.getElementById("TimepadContainer").style.display = "block";
    document.getElementById("LineNextEmployee").style.display = "inline-block";
    document.getElementById("EmployeeSend").classList.add("passed");
    document.getElementById("InfoPanelEmployees").classList.add("fadeOutLeft");
    document.getElementById("TimepadSend").classList.add("active");
}

function trial() {
    document.getElementById("TimepadContainer").style.display = "none";
    document.getElementById("TrialContainer").style.display = "block";
    document.getElementById("LineNextTimepad").style.display = "inline-block";
    document.getElementById("TimepadSend").classList.add("passed");
    document.getElementById("TrialSend").classList.add("active");
}

/* Bootstrap Keep dropdown open after click */
$(document).on('click.bs.dropdown.data-api', '.dropdown.b-keep-inside-clicks-open', function (e) {
    e.stopPropagation();
});
/* END Bootstrap Keep dropdown open after click */

/* Closed outside container */
$(document).ready(function () {
    $('.b-container__closed-outside').hide();
});

$('.b-show-container').on('click', function () {
    $('.b-container__closed-outside').show();
});

$(document).mouseup(function (e) {
    var popup = $(".b-container__closed-outside");
    if (!$('.b-show-container').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
        popup.hide();
    }
});
/* END Closed outside container */


